<?php

//======================================================================
// GLOBAL SETTINGS
//======================================================================
  require_once("config-local.php");

  define("DS", DIRECTORY_SEPARATOR);

  define("CONFIG_PATH", __DIR__);
  define("CONFIG_FILE", CONFIG_PATH."/config.ini");

  define("PUBLIC_HTML_FOLDER_PATH", __DIR__."/../../public_html");
  define("IMAGE_UPLOAD_FOLDER_PATH", PUBLIC_HTML_FOLDER_PATH."/uploads");

  $config = parse_ini_file(CONFIG_FILE);
  date_default_timezone_set($config['timezone']);

  define("HOME_PAGE_URL", "home");
  define("LOGIN_PAGE_URL", "accounts/login");

  define("MODULES_PATH", CONFIG_PATH."/../modules");

  define("NAV_DATA", __DIR__."/../core/navData.php");
  define("CONTROLLER_DATA", __DIR__."/../core/controllerConfig.php");

  define("SERVICES_DATA", __DIR__."/../core/serviceConfig.php");

//======================================================================
// ERROR REPORTING
//======================================================================
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  //ini_set('log_errors', 1);

  /**
   * If set to true, all custom error messages will be written to the error log.
   */
  define("LOG_ERRORS", 1);

//======================================================================
// ROUTING
//======================================================================
  define("ROUTES_PATH", __DIR__."/../routes");

//======================================================================
// SHARED RESOURCES
//======================================================================
  define("RESOURCE_PATH", __DIR__.DS."..".DS."resources");
  define("LANG_PATH", RESOURCE_PATH.DS."lang".DS."en");
  define("VERIFICATION_I18_MESSAGES", LANG_PATH.DS."validation.php");

  define("SHARED_RESOURCES", RESOURCE_PATH.DS."shared");
  define("LAYOUTS_PATH", SHARED_RESOURCES.DS."layouts");
  define("CONTROLLER_PATH", SHARED_RESOURCES.DS."controllers");
  // Services parent class
  define("SERVICES_PARENT", SHARED_RESOURCES.DS."services".DS."Service.php");

  // Controller parent class
  require_once CONTROLLER_PATH."/Controller.php";

  define("UTIL_PATH", RESOURCE_PATH.DS."util");
  require_once UTIL_PATH."/Utils.php";

//======================================================================
// CORE MODULES
//======================================================================

  /**
   * Core Folder Path
   */
  define("CORE_FOLDER_PATH", __DIR__.DS."..".DS."core");

  //-----------------------------------------------------
  // Accounts Core module
  //-----------------------------------------------------
  require_once CORE_FOLDER_PATH.DS."accounts".DS."config.php";

  //-----------------------------------------------------
  // Admin Core module
  //-----------------------------------------------------
  require_once CORE_FOLDER_PATH.DS."admin".DS."config.php";

  //-----------------------------------------------------
  // Error Core module
  //-----------------------------------------------------
  require_once CORE_FOLDER_PATH.DS."error".DS."config.php";

  //-----------------------------------------------------
  // Home Core module
  //-----------------------------------------------------
  require_once CORE_FOLDER_PATH.DS."home".DS."config.php";


  //-----------------------------------------------------
  // Pages Core module
  //-----------------------------------------------------
  require_once CORE_FOLDER_PATH.DS."pages".DS."config.php";

  //-----------------------------------------------------
  // Posts Core module
  //-----------------------------------------------------
  require_once CORE_FOLDER_PATH.DS."posts".DS."config.php";

  //-----------------------------------------------------
  // Database module
  //-----------------------------------------------------
  require_once RESOURCE_PATH.DS."database".DS."config.php";

  //-----------------------------------------------------
  // Mail module
  //-----------------------------------------------------
  require_once RESOURCE_PATH.DS."mail".DS."config.php";

  //-----------------------------------------------------
  // Session module
  //-----------------------------------------------------
  require_once RESOURCE_PATH.DS."session".DS."config.php";

  //-----------------------------------------------------
  // Edit Lock Manager module
  //-----------------------------------------------------
  require_once RESOURCE_PATH.DS."editlockmanager".DS."config.php";

  //-----------------------------------------------------
  // File Handler module
  //-----------------------------------------------------
  require_once RESOURCE_PATH.DS."filehandler".DS."config.php";

?>

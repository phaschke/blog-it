<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">

  <div class="row margin-bottom-md">
    <div class="col-sm-12 justify-content-center text-center">
      <h2>Admin Panel</h2>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <i class="fas fa-chart-line"></i> Statistics
    </div>
    <div class="card-block">
      <p class="card-text">Statistics</p>
    </div>
  </div>

  <?php
  //TODO: ADMIN MODULE LOADER
  include_once PAGES_VIEW_PATH. "/admin/panel/managePages.php";

  include_once POSTS_VIEW_PATH. "/admin/panel/managePosts.php" ?>

  <?php include_once BREWERIES_PATH . "/views/admin/panel/manageBreweries.php" ?>

  <?php include_once ACCOUNTS_VIEW_PATH . "/admin/panel/manageUsers.php" ?>


</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

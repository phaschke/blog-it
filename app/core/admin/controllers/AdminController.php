<?php

class AdminController extends Controller {

  private $modules;

  public function __construct($modules) {

    $this->modules = $modules;

    parent::__construct();

  }

  /**
   *
   */
  public function panel() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Admin Panel";
    $pageContent['session'] = $this->session;

    //error_log( print_r($this->modules, TRUE) );

    require_once ADMIN_VIEW_PATH . "/panel.php";

  }

}



?>

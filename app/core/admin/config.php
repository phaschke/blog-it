<?php
//======================================================================
// ADMIN CORE MODULE CONFIG
//======================================================================

  /**
   * URL To Admin Panel
   */
   define("ADMIN_PANEL", "./admin/panel");

  /**
   * Path To Admin Module Folder
   */
   define("ADMIN_PATH", __DIR__);

   /**
    * Path To Admin Views
    */
    define("ADMIN_VIEW_PATH", ADMIN_PATH."/views");


?>

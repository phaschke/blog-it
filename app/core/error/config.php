<?php
//======================================================================
// ERROR CORE MODULE CONFIG
//======================================================================

  /**
   * Path To Accounts Module Folder
   */
   define("ERRORS_PATH", __DIR__);

   /**
    * Path To Error Views
    */
    define("ERRORS_VIEW_PATH", ERRORS_PATH."/views");


?>

<?php

class ErrorController {

  /**
   * The server has not found anything matching the Request-URI. No indication is given of whether the condition
   * is temporary or permanent.
   *
   * @return view error page
   */
  public function not_found() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Error";
    $pageContent['error'] = "404 Page Not Found";
    $pageContent['errorMessage'] = "The server did not find anything matching the request.";

    require_once(ERRORS_VIEW_PATH.DS."error.php");
  }

  /**
   * The server does not support the functionality required to fulfill the request.
   * This is the appropriate response when the server does not recognize the request method
   * and is not capable of supporting it for any resource.
   *
   * @return view error page
   */
  public function not_implemented() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Error";
    $pageContent['error'] = "501 Not Implemented";
    $pageContent['errorMessage'] = "This functionality is not yet implemented.";

    require_once(ERRORS_VIEW_PATH.DS."error.php");
  }

  /**
   * The server is stating the account you have currently logged in as does not have permission to perform the
   * action you are attempting. You may be trying to upload to the wrong directory or trying to delete a file.
   *
   * @return view error page
   */
  public function permission_denied() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Error";
    $pageContent['error'] = "550 Permission Denied";
    $pageContent['errorMessage'] = "You do not have permission to view this page.";

    require_once(ERRORS_VIEW_PATH.DS."error.php");

  }


}

//404 Not found

//501 Not implemented

//550 Permission denied

?>

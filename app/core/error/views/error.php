<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- This tag MUST be included on every page -->
    <base href="<?php echo BASE_URL ?>"/>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="resources/images/favicon.ico">

    <?php
      if($pageContent['pageTitle']) {
        echo "<title>".$pageContent['pageTitle']." | BlogIt!</title>";
      }
    ?>
    <title>BlogIt!</title>

    <!-- Bootstrap core CSS -->
    <link href="resources/bootstrap-4.0.0-beta-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this site -->
    <link href="css/defaultstyle.css" rel="stylesheet">

  </head>


  <div class="container">
    <div class="jumbotron">
      <h1 class="display-3"><?php echo $pageContent['error']; ?></h1>
      <p class="lead"><?php echo $pageContent['errorMessage']; ?></p>
      <hr class="my-4">
      <p class="lead">
        <a class="btn btn-primary btn-lg" href="<?php echo HOME_PAGE_URL; ?>" role="button">Return Home</a>
      </p>
    </div>

  </div>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
  <script src="resources/jquery-3.2.1.min.js"></script>
  <script src="resources/popper.js"></script>

  <!-- <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script> -->
  <script src="resources/bootstrap-4.0.0-beta-dist/js/bootstrap.min.js"></script>


  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="core/js/libraries/ie10-viewport-bug-workaround.js"></script>
</body>
</html>

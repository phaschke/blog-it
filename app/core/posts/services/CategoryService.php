<?php
// Include parent class
require_once SERVICES_PARENT;
/**
 * Category Service Class
 */
 Class CategoryService extends Service {

   private $categoryModel;

   public function __construct() {

     $DBConnection = new DBConnection();
     $categoryRepository = new CategoryRepository($DBConnection);
     $this->categoryModel = new CategoryModel($categoryRepository);

   }

   /**
    * Gets many category records
    */
   public function getCategories() {

     $result = $this->categoryModel->getCategories();
     if($this->checkError($result)) {

       if(count($result['categories']) == 0) {

         $result['message'] = getI18nMessage(CATEGORY_I18_MESSAGES, 'category_not_found');

       } else {

         $categoryData = array();

         foreach ($result['categories'] as $category) {

           $categoryData[] = $category->returnAsArray();
         }

         $result['categories'] = $categoryData;

       }

       echo json_encode(array('success_flag' => 1,
         'success_message' => $result ));
       return true;
     }
   }

   /**
    * Get a single category given a category id.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function getCategory($parameters) {

     $requiredParameters = array('categoryId');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

     $result = $this->categoryModel->getCategory($parameters['categoryId']);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result->returnAsArray() ));
         return true;
       }
     }
   }

   /**
    * Add a category.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function add($parameters) {

     $requiredParameters = array('categoryName', 'categoryDescription');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $categoryData = array("category_name" => $parameters['categoryName'],
                             "category_description" => $parameters['categoryDescription']
                        );

       $category = new Category($categoryData);

       $result = $this->categoryModel->addCategory($category);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Update a category.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function update($parameters) {

     $requiredParameters = array('categoryId', 'categoryName', 'categoryDescription');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $categoryData = array("category_name" => $parameters['categoryName'],
                             "category_description" => $parameters['categoryDescription']
                        );

       $category = new Category($categoryData);

       $category->setCategoryId($parameters['categoryId']);

       $result = $this->categoryModel->updateCategory($category);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Delete a category given a category id.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function delete($parameters) {

     $requiredParameters = array('categoryId');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

     $result = $this->categoryModel->deleteCategory($parameters['categoryId']);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

}

?>

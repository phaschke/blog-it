<?php
// Include parent class
require_once SERVICES_PARENT;
/**
 * Post Service Class
 */
 Class PostService extends Service {

   private $postModel;

   public function __construct() {

     $session = new Session();
     $DBConnection = new DBConnection();

     $editLockManagerRepository = new EditLockManagerRepository($DBConnection);
     $editLockManager = new EditLockManager($session, $editLockManagerRepository);

     $postRepository = new PostRepository($DBConnection);
     $this->postModel = new PostModel($postRepository, $editLockManager);

     $categoryRepository = new CategoryRepository($DBConnection);
     $this->categoryModel = new CategoryModel($categoryRepository);

   }

   /**
    * Add a post.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function add($parameters) {

     $post = $this->returnPostFromParameters($parameters);
     if($this->checkError($post)) {

       $result = $this->postModel->add($post);
       if($this->checkError($result)) {
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Delete a post using edit lock manager.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function delete() {

     $result = $this->postModel->delete();
     if($this->checkError($result)) {

       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Update a post using edit lock manager.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function update($parameters) {

     $post = $this->returnPostFromParameters($parameters);
     if($this->checkError($post)) {

       $result = $this->postModel->update($post);
       if($this->checkError($result)) {
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Toggle a posts visibility using edit lock manager.
    */
   public function togglePostVisibility() {

     $result = $this->postModel->toggleVisibility();
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Get many posts given various parameters
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function getPosts($parameters) {



     $searchParameters = array();
     if(isset($parameters['searchParameters'])) {
       $searchParameters = $parameters['searchParameters'];
     }

     if(!isset($parameters['postCategory']) || ($parameters['postCategory'] == "")) {
       $parameters['postCategory'] = null;
     }
     if(!isset($parameters['yearMonth']) || ($parameters['yearMonth'] == "")) {
       $parameters['yearMonth'] = null;
     }

     $requiredParameters = array('numRecords', 'offset', 'yearMonth', 'postStatus', 'postCategory');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->postModel->getPosts($parameters['numRecords'], $parameters['offset'], $parameters['yearMonth'], $parameters['postStatus'], $parameters['postCategory'], $searchParameters);
       if($this->checkError($result)) {

         if($result['totalCount'] <= 0) {
           $result['message'] = getI18nMessage(POST_I18_MESSAGES, 'posts_not_found');

         } else {
           $postData = array();
           foreach ($result['posts'] as $post) {
             $postData[] = $post->returnAsArray("F jS, Y");
           }

           $result['posts'] = $postData;
         }

         // Success
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Get a single post given a post id.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function getPost($parameters) {

     $requiredParameters = array('postId');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {
       $result = $this->postModel->getPost($parameters['postId']);
       if($this->checkError($result)) {
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Helper function to return a post object from input parameters
    *
    * @param array $parameters
    * @return Post|Array The post object or error array
    */
   public function returnPostFromParameters($parameters) {

     $requiredParameters = array('postTitle', 'postBody', 'postSummary', 'postStatus', 'postCategory');


     // Verify required parameters exist
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {
       try {
         $postData = array("post_title" => $parameters['postTitle'],
                           "post_body" => $parameters['postBody'],
                           "post_summary" => $parameters['postSummary'],
                           "post_status" => $parameters['postStatus'],
                           "category_id" => $parameters['postCategory']
                          );
         // If the post is being saved, do not update the status
         if($postData["post_status"] == 'save') {
           $postData["post_status"] = null;
         }

         // Parse tags string and return array
         if($parameters['postTags'] !== "") {
           $postData['tags'] = array_filter(preg_split ('/[\s*,\s*]*,+[\s*,\s*]*/', $parameters['postTags']));
         }

         return new Post($postData);

       } catch (Exception $e) {

         return (array('status' => 0,
            'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_save_error'),
            'consoleError'=>__CLASS__.' '.__FUNCTION__." Error with service: ".$e));
       }
     }
   }



 }

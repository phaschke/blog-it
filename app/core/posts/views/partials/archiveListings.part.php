<?php
if(isset($pageContent['getPubDateError'])) {
   echo $pageContent['getPubDateError'];
 } else { ?>
<ol class="list-unstyled">
  <?php foreach ($pageContent['publicationDates'] as $publicationDate) { ?>
    <li><a href="./posts/bymonth/<?php echo $publicationDate['postYear'] ?>/<?php echo $publicationDate['postMonth'] ?>"><?php echo $publicationDate['postFormattedDate'] ?> (<?php echo $publicationDate['postCount'] ?>)</a></li>
  <?php }?>
</ol>
<a class="btn btn-outline-primary" href="./posts/archive">All Archives</a>
<?php } ?>

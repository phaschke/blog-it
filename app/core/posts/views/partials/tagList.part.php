<!-- Partial to display a list of given tags linked to the url /posts/list?tag={tagText}-->
<?php
if(isset($pageContent['getTagsError'])) {
   echo $pageContent['getTagsError'];
 } else { ?>
   <div class="row list-unstyled">
  <?php foreach($pageContent['tags'] as $tag) {?>
    <div class="col-sm-4">
      <li><a href="./posts/list?tag=<?php echo $tag[0]?>"><?php echo $tag[0]?></a></li>
    </div>
  <?php }?>
</div>
<?php }?>

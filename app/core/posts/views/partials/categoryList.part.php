<!-- Partial to display a list of given categories objects linked to the url /posts/category/{catagoryId}-->
<?php
if(isset($pageContent['getCategoriesError'])) {
   echo $pageContent['getCategoriesError'];
 } else { ?>
   <div class="list-unstyled">
  <?php foreach($pageContent['categories'] as $category) {?>
    <div>
      <li><a href="./posts/category/<?php echo $category->getCategoryId()?>"><?php echo $category->getCategoryName()?></a></li>
    </div>
  <?php }?>
</div>
<?php }?>

<?php
    if(isset($pageContent['errorGetPubDate'])) {
       echo $pageContent['errorGetPubDate'];
     } else {
       echo '<div class="row">';

      $year = "";
      foreach ($pageContent['publicationDates'] as $publicationDate) {

        // If a different year, add a new card
        if($publicationDate["postYear"] != $year) {
          if($year != "") {
            echo '
              </div>
                </div>
                  </div>
            ';
          }
          echo '
            <div class="col-sm-4">
              <div class="card">
                <div class="card-block">
                  <h4 class="card-title">'.$publicationDate["postYear"].'</h4>
          ';
        $year = $publicationDate["postYear"];
        }
        $listLink = $pageContent['baseLinkForPosts'];
        echo '<p class="card-text"><a href="'.$listLink.$publicationDate["postYear"].'/'.$publicationDate["postMonth"].'">'.$publicationDate["postFormattedDate"].' ('.$publicationDate["postCount"].')</a></p>';
      }
      ?>
          </div>
        </div>
      </div>
    </div> <!-- Close row -->

  <?php } ?>

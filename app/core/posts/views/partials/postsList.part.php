<?php
  if(isset($pageContent['getPostsError'])){
    echo $pageContent['getPostsError'];

  } else {

    foreach ($pageContent['posts'] as $post) {
?>
      <div class="blog-post">
        <h2 class="blog-post-title"><a href="./posts/show/<?php echo $post->getPostId()?>"><?php echo $post->getPostTitle();?></a></h2>
        <p class="blog-post-meta"><?php echo $post->getCreatedAt('F jS, Y'); ?> by <a href="<?php echo './accounts/user/'.urlencode($post->getUserName()); ?>"><?php echo $post->getUserName(); ?></a> | <?php echo $post->getPostViews();?> Views</p>

        <p><i><?php echo $post->getSummary(); ?></i></p>

      </div><!-- /.blog-post -->

      <hr>

<?php
      } // end foreach
    }
?>

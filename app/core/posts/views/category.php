<?php include_once LAYOUTS_PATH . DS ."header.php" ?>



<div class="container">
  <div class="row margin-bottom-md">
    <div class="col-sm-12">
      <a class="btn btn-outline-primary" href="./posts/archive">&#x2190; Back to Archives</a>
    </div>
  </div>

<?php
  if(isset($pageContent['getCategoryError'])) { ?>
    <div class="row justify-content-center">
      <h3><?php echo $pageContent['getCategoryError'];?></h3>
    </div>
  <?php
  } else {
  ?>

  <div class="row justify-content-center">
      <h2><?php echo $result->getCategoryName(); ?> Posts</h2>
  </div>

  <div class="row justify-content-center">
      <p><i><?php echo $result->getCategoryDescription(); ?></i></p>
  </div>

  <hr>

  <div id="posts-holder">
    <p>Loading...</p>
  </div>

  <div id="pagination"></div>

  <script>
    BlogIt.Post.initializePostPagination("", "published", "<?php echo $result->getCategoryId(); ?>", false, "BlogIt.Post.listPosts", "posts-holder", "view");
  </script>
<?php } ?>

</div>

<?php include_once LAYOUTS_PATH . DS . "footer.php" ?>

<!-- Used by BlogIt.Post.initializePostPagination and BlogIt.Post.listPosts. -->
<!-- COPIED ON VIEW postsByMonth -->
<script id="post_list_template" type="text/x-handlebars-template">
  {{#each posts}}
  <div class="blog-post">
    <h2 class="blog-post-title"><a href="./posts/show/{{post_id}}">{{post_title}}</a></h2>
    <p class="blog-post-meta">{{post_created_at}} by <a href="#">{{user_name}}</a> | {{post_views}} Views</p>
    <p><i>{{post_summary}}</i></p>
  </div>
{{/each}}
</script>

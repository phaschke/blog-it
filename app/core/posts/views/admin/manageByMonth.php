<?php include_once LAYOUTS_PATH . DS ."header.php" ?>

<div class="container">

  <div class="row margin-bottom-md">
    <a class="btn btn-outline-primary" href=<?php echo $pageContent['returnLink']; ?>>&#x2190; Back to <?php echo ucfirst($pageContent['status']); ?> Posts</a>
  </div>

  <div class="row justify-content-center">
    <h2>Manage <?php echo $pageContent['header']; ?></h2>
  </div>

  <div id="posts-holder">
    <p>Loading...</p>
  </div>

  <div id="pagination"></div>

  <script>
    BlogIt.Post.initializePostPagination("<?php echo $pageContent['yearMonth']; ?>", "<?php echo ucfirst($pageContent['status']); ?>", null, false, "BlogIt.Post.managePosts", "posts-holder", "edit", "");
  </script>


</div>

<?php include_once LAYOUTS_PATH . DS ."footer.php" ?>

<!-- Used by BlogIt.Post.initializePostPagination and BlogIt.Post.managePosts. -->
<script id="post_manage_list_template" type="text/x-handlebars-template">
  {{#each posts}}
  <div class="card">
    <div class="card-block">
    <h4 class="card-title">{{post_title}}</h4>
      <h6 class="card-subtitle mb-2 text-muted">Created on {{post_created_at}} by {{user_name}}</h6>
      <p class="card-text">{{post_summary}}</p>
      <p class="card-text">{{post_status}}</p>
      <a class="btn btn-primary" href="./posts/edit/{{post_id}}">Edit Post</a>
    </div>
  </div>
{{/each}}
</script>

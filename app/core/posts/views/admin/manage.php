<?php include_once LAYOUTS_PATH . DS . "header.php" ?>

<div class="container">

  <div class="row margin-bottom-md">
    <div class="col-sm-12">
      <a class="btn btn-outline-primary" href="<?php echo ADMIN_PANEL ?>">&#x2190; Back to Panel</a>
    </div>
  </div>

  <?php include_once POSTS_VIEW_PATH.DS."admin".DS."partials".DS."manageNav.part.php"; ?>

  <div class ="margin-bottom-lg">
    <div class="card">
      <div class="card-header">
        By Month
      </div>
      <div class="card-block text-center">
        <?php include_once POSTS_VIEW_PATH.DS."partials".DS."postsByYear.part.php" ?>
      </div>
    </div>
</div>

</div>

<?php include_once LAYOUTS_PATH . DS . "footer.php" ?>

<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">

  <div class="row margin-bottom-md">
    <a class="btn btn-outline-primary" href="./admin/viewposts">&#x2190; Back to All Posts</a>
  </div>

  <div class="row justify-content-center">
    <h2>Edit Posts</h2>
  </div>

  <div class="card">
    <div class="card-header">
      <?php echo $pageContent['header']; ?>
    </div>
    <div class="card-block">
    </div>
  </div> <!-- Close card -->

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

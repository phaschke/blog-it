<?php
  include_once LAYOUTS_PATH . DS . "header.php";

  if(isset($pageContent['message'])){
    include_once LAYOUTS_PATH . DS . "errorMessage.part.php";
    return false;
  }
?>

<div class="container">
  <div class="blog-post-form">

    <div class="row margin-bottom-md">
      <a class="btn btn-outline-primary" href="./posts/manage/<?php echo $pageContent['post']->getPostStatus(); ?>/<?php echo $pageContent['post']->getCreatedAt('Y'); ?>/<?php echo $pageContent['post']->getCreatedAt('m'); ?>">&#x2190; Back to Edit Posts</a>
    </div>

    <div class="row justify-content-center">
      <h2>Edit Post</h2>
    </div>

    <div class="text-right py-2">
        <?php
          if($pageContent['post']->getPostStatus() == 'published' || $pageContent['post']->getPostStatus() == 'hidden') {
            echo '
              <button type="button" id="togglePostVisibilityButton" class="btn btn-primary" onclick="BlogIt.Post.togglePostVisibility()">';
                echo ($pageContent['post']->getPostStatus() == 'published') ? "Hide Post" : "Unhide Post";
              echo '</button>
            ';
          }
          ?>

      <button type="button" id="deletePostButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Post.deletePostModal())">
        Delete Post
      </button>
    </div>

      <div id="alerts-holder-top">
        <!-- Alert holder -->
      </div>
      <div class="alert alert-info" role="alert">
        Post Added by User <?php echo $pageContent['post']->getUserName(); ?>.
      </div>

      <div class="form-group" id="editPostForm">

        <div class="form-group">
          <label for="postTitle">Title<small>*</small></label>
          <input type="text" class="form-control" id="postTitle" name="postTitle" maxlength="255" placeholder="Title" value="<?php echo $pageContent['post']->getPostTitle();?>" onkeyup="BlogIt.Post.postPreview()" required>
          <small id="postTitleErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postSummary">Summary<small>*</small></label>
          <textarea class="form-control" id="postSummary" name="postSummary" placeholder="Summary" rows="3" onkeyup="BlogIt.Post.postPreview()"><?php echo $pageContent['post']->getSummary();?></textarea>
          <small id="postSummaryErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postBody">Content<small>*</small></label>
          <textarea class="form-control" id="postBody" name="postBody" placeholder="Content" rows="10" onkeyup="BlogIt.Post.postPreview()"><?php echo $pageContent['post']->getBody();?></textarea>
          <small id="postBodyErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postCategory">Category</label>
          <select class="form-control" id="postCategory">
            <?php
              foreach ($pageContent['categories'] as $category) {
                if($category->getCategoryName() == $pageContent['post']->getCategoryName()) {
                  echo '<option value="'.$category->getCategoryId().'" selected="selected">'.$category->getCategoryName().'</option>';
                } else {
                  echo '<option value="'.$category->getCategoryId().'">'.$category->getCategoryName().'</option>';
                }
              }

             ?>
          </select>
        </div>

        <div class="form-group">
          <label for="postTags">Tags</label>
          <div class="alert alert-info" role="alert">
            <p>Separate many tags by commas.</p>
          </div>
          <textarea class="form-control" id="postTags" name="postTags" placeholder="Tags" rows="2"><?php echo $pageContent['post']->getTags("string"); ?></textarea>
          <small id="postTagsErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <div class="pull-right">
            <button type="button" id="updatePostButton" class="btn btn-primary" onclick="BlogIt.Post.updatePost()">
              Save Changes
            </button>

            <?php
              if($pageContent['post']->getPostStatus() == 'saved') {
                echo '
                  <button type="button" id="updatePostButton" class="btn btn-primary" onclick="BlogIt.Post.updatePost(\'publish\')">
                    Save and Publish
                  </button>
                ';
              }
            ?>
          </div>

          <div id="alerts-holder-bottom">
            <!-- Alert holder -->
          </div>

      </div>
    </div> <!-- End From -->
    </div>

    <div class="d-flex justify-content-between">
      <div>
        <h3>Post Preview:</h3>
      </div>
      <div>
        <button type="button" class="btn btn-primary" onclick="BlogIt.Post.postPreview()">
          Update Preview
        </button>
      </div>
    </div>


    <hr>


    <div id="postPreview">
      <!-- Div to hold post preview -->
    </div>

    <script>
      BlogIt.Post.postPreview();

      //body.onkeyup = function(){BlogIt.Post.IS_CHANGED = 1};

      //if(BlogIt.Post.IS_CHANGED == 1){
        // Note this will not work in IE
        window.onbeforeunload = async function(e) {

          await BlogIt.Post.unlockEditObject();
      //}

      };
    </script>

</div>

<script type="text/javascript">
//<![CDATA[
bkLib.onDomLoaded(function() {
    nicEditors.editors.push(
        new nicEditor({fullPanel:false, buttonList:['bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 'ul', 'fontSize', 'fontFamily', 'fontFormat', 'subscript', 'superscript',
          'strikethrough', 'removeformat', 'indent', 'outdent', 'hr', 'image', 'forecolor', 'bgcolor', 'link', 'unlink']}).panelInstance(
            document.getElementById("postBody")
        )
    );
    var editor = nicEditors.findEditor('postBody');
    editor.getElm().onkeyup = function() { BlogIt.Post.postPreview(); }
});
//]]>

window.onbeforeunload = function() {
  BlogIt.Post.unlockEditObject();
  return "Are you sure you wish to leave the page?";
}
</script>

<?php include_once LAYOUTS_PATH . "/modal.php" ?>
<?php include_once LAYOUTS_PATH . "/footer.php" ?>

<!-- Used by BlogIt.Post.deletePostModal -->
<script id="post_delete_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Confirm Post Deletion</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      Do you really want to delete this post?
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" onclick="BlogIt.Post.deletePost()">Delete Post</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</script>

<div class="card">
  <div class="card-header">
    <i class="fas fa-pen-fancy"></i> Manage Posts
  </div>
  <div class="card-block">
    <a class="btn btn-primary margin-bottom-sm" href="./posts/add">Add</a>
    <a class="btn btn-primary margin-bottom-sm" href="./posts/manage">Manage Posts</a>
    <a class="btn btn-primary margin-bottom-sm" href="./posts/managecategories">Manage Categories</a>
  </div>
</div>

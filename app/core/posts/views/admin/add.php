<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">

  <script>
    window.onbeforeunload = function() {
      return "Are you sure you wish to leave the page?";
    }
  </script>

  <div class="blog-post-form">

    <div class="row margin-bottom-md">
      <div class="col-sm-12">
        <a class="btn btn-outline-primary" href="./admin/panel">&#x2190; Back to Panel</a>
      </div>
    </div>

    <div id="alerts-holder-top">
      <!-- Alert holder -->
    </div>

    <div class="row justify-content-center">
      <h2>New Post</h2>
    </div>

      <form id="addPostForm">
      <div class="form-group">￼


        <div class="form-group">
          <label for="postTitle">Title<small>*</small></label>
          <input type="text" class="form-control" id="postTitle" name="postTitle" maxlength="255" placeholder="Title" onkeyup="BlogIt.Post.postPreview()" required>
          <small id="postTitleErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postSummary">Summary<small>*</small></label>
          <textarea class="form-control" id="postSummary" name="postSummary" placeholder="Summary" rows="3" onkeyup="BlogIt.Post.postPreview()"></textarea>
          <small id="postSummaryErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postBody">Content<small>*</small></label>
          <textarea class="form-control" id="postBody" name="postBody" placeholder="Content" rows="10" onkeypress="BlogIt.Post.postPreview()"></textarea>
          <small id="postBodyErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postCategory">Category</label>
          <select class="form-control" id="postCategory">
            <?php
              foreach ($pageContent['categories'] as $category) {
                  echo '<option value="'.$category->getCategoryId().'">'.$category->getCategoryName().'</option>';
              }

             ?>
          </select>
        </div>

        <div class="form-group">
          <label for="postTags">Tags</label>
          <div class="alert alert-info" role="alert">
            <p>Separate tags by comma.</p>
          </div>
          <textarea class="form-control" id="postTags" name="postTags" placeholder="Tags" rows="2"></textarea>
          <small id="postTagsErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <div class="pull-right">
            <button type="button" id="addPostButton" class="btn btn-primary" onclick="BlogIt.Post.publishPost()">
              Publish Post
            </button>
            <button type="button" id="savePostButton" class="btn btn-primary" onclick="BlogIt.Post.publishPost('save')">
              Save Post
            </button>
          </div>

          <div id="alerts-holder-bottom">
            <!-- Alert holder -->
          </div>

      </div>

    </div> <!-- End From -->
    </form>
    </div>


    <div>
      <h3>Post Preview:</h3>
    </div>

    <hr>


    <div id="postPreview">
      <!-- Div to hold post preview -->
    </div>

</div>

<script type="text/javascript">
//<![CDATA[
bkLib.onDomLoaded(function() {
    nicEditors.editors.push(
        new nicEditor({fullPanel:false, buttonList:['bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 'ul', 'fontSize', 'fontFamily', 'fontFormat', 'subscript', 'superscript',
          'strikethrough', 'removeformat', 'indent', 'outdent', 'hr', 'image', 'forecolor', 'bgcolor', 'link', 'unlink']}).panelInstance(
            document.getElementById("postBody")
        )
    );
    var editor = nicEditors.findEditor('postBody');
    editor.getElm().onkeyup = function() { BlogIt.Post.postPreview(); }
});
//]]>
</script>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

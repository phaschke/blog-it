<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">

  <div class="row margin-bottom-md">
    <a class="btn btn-outline-primary" href="./admin/panel">&#x2190; Back to Panel</a>
  </div>

  <div class="row justify-content-center">
    <h2>Edit Posts</h2>
  </div>

  <?php include_once LAYOUTS_PATH . "/postsByMonth.php" ?>


</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

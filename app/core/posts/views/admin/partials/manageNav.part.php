<div class="row justify-content-center margin-bottom-md">
  <h2>Manage <?php echo $pageContent['status'] ?> Posts</h2>
</div>
<hr>
<div class="row justify-content-center margin-bottom-md">

  <a class="btn margin-right-sm margin-bottom-sm <?php echo ($pageContent['status'] == 'All') ? 'btn-primary' : 'btn-outline-primary' ?>"
    href="<?php echo MANAGE_ALL ?>"> All Posts</a>

  <a class="btn margin-right-sm margin-bottom-sm <?php echo ($pageContent['status'] == 'Published') ? 'btn-primary' : 'btn-outline-primary' ?>"
    href="<?php echo MANAGE_PUBLISHED ?>"> Published Posts
  </a>

  <a class="btn margin-right-sm margin-bottom-sm <?php echo ($pageContent['status'] == 'Saved') ? 'btn-primary' : 'btn-outline-primary' ?>"
    href="<?php echo MANAGE_SAVED ?>"> Saved Posts
  </a>

  <a class="btn margin-bottom-sm <?php echo ($pageContent['status'] == 'Hidden') ? 'btn-primary' : 'btn-outline-primary' ?>"
    href="<?php echo MANAGE_HIDDEN ?>"> Hidden Posts
  </a>
</div>

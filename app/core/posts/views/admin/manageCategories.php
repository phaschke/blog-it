<?php include_once LAYOUTS_PATH . DS ."header.php" ?>

<script type="text/javascript" src="core/js/libraries/Category.js"></script>

<div class="container">

  <div class="row margin-bottom-md">
    <div class="col-sm-12">
      <a class="btn btn-outline-primary" href="<?php echo ADMIN_PANEL ?>">&#x2190; Back to Panel</a>
    </div>
  </div>

  <div class="row justify-content-center">
    <h2>Manage Post Categories</h2>
  </div>

  <hr>

  <div class="row justify-content-center">
    <div id="alerts-holder-top">
    </div>
  </div>

  <div class="row justify-content-center margin-bottom-sm">
    <div id="category-count-holder">
    </div>
  </div>

  <div class="row justify-content-md-center">
    <div id="category-list-holder">
    </div>
  </div>

  <script>
    BlogIt.Category.listCategories("category-count-holder", "category-list-holder");
  </script>

  <div class="row justify-content-center margin-bottom-lg margin-top-md">
    <button type="button" id="addCategoryButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Category.addCategoryModal())">
      Add Category
    </button>
  </div>


</div>

<?php include_once LAYOUTS_PATH . DS ."modal.php" ?>
<?php include_once LAYOUTS_PATH . DS ."footer.php" ?>


<!-- Used by BlogIt.Category.deleteCategoryModal -->
<script id="category_delete_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Confirm Category Deletion</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      Do you really want to delete this category?
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id="deleteCategoryButton" onclick="BlogIt.Category.deleteCategory({{categoryId}})">Delete Category</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Category.addCategoryModal -->
<script id="category_add_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Add A Post Category</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="addCategoryForm">
        <div class="form-group">
          <label for="categoryName">Category Name</label>
          <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Category Name" required>
          <small id="categoryNameErrorHolder" class="text-danger"></small>
        </div>
        <div class="form-group">
          <label for="postBody">Category Description</label>
          <textarea class="form-control" id="categoryDescription" name="categoryDescription" placeholder="Content" rows="10"></textarea>
          <small id="categoryDescriptionErrorHolder" class="text-danger"></small>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id="addCategoryButton" onclick="BlogIt.Category.addCategory()">Add Category</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Category.updateCategoryModal -->
<script id="category_update_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Add A Post Category</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="addCategoryForm">
        <div class="form-group">
          <label for="categoryName">Category Name</label>
          <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Category Name" value="{{category_name}}" required>
          <small id="categoryNameErrorHolder" class="text-danger"></small>
        </div>
        <div class="form-group">
          <label for="postBody">Category Description</label>
          <textarea class="form-control" id="categoryDescription" name="categoryDescription" placeholder="Content" rows="10">{{category_description}}</textarea>
          <small id="categoryDescriptionErrorHolder" class="text-danger"></small>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id="updateCategoryButton" onclick="BlogIt.Category.updateCategory({{category_id}})">Save Category</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Category.listCategories -->
<!-- For the first uneditable default category in the list -->
<script id="category_uneditable_list_template" type="text/x-handlebars-template">
  <ul class="list-group">
    <li class="list-group-item list-group-item-action flex-column align-items-start disabled">
      <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">{{category_name}}</h5>
      </div>
      <div>
        <p class="mb-1">{{category_description}}</p>
      </div>
    </li>
  </script>

<!-- Used by BlogIt.Category.listCategories-->
<script id="category_list_item_template" type="text/x-handlebars-template">
  {{#each categories}}
  <li class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1">{{category_name}}</h5>
    </div>
    <div>
      <p class="mb-1">{{category_description}}</p>
      <button type="button" id="editCategoryButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Category.updateCategoryModal('{{category_id}}'))">
        Edit
      </button>
      <button type="button" id="deleteCategoryButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Category.deleteCategoryModal('{{category_id}}'))">
        Delete
      </button>
    </div>
  </li>
  {{/each}}
</script>

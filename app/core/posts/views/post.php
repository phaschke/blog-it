<?php
  include_once LAYOUTS_PATH . DS . "header.php";

  if(isset($pageContent['message'])){
    include_once LAYOUTS_PATH . DS . "errorMessage.part.php";
    return false;
  }
  $post = $pageContent['post'];
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container">
  <div class="row">
        <div class="blog-post">
          <div class="col-sm-12">
            <h2 class="blog-post-title margin-bottom-md"><?php echo htmlspecialchars($post->getPostTitle());?></h2>
          </div>
          <div class="col-sm-12">
            <p class="blog-post-meta"><?php echo $post->getCreatedAt('F jS, Y'); ?> | <i class="fas fa-user"></i> <a href="./accounts/user/<?php echo $post->getUserName();?>"><?php echo $post->getUserName(); ?></a> | <i class="fas fa-eye"></i> <?php echo $post->getPostViews();?> Views<br>
              In <a href="posts/category/<?php echo $post->getCategoryId();?>"><?php echo $post->getCategoryName(); ?></a>
            </p>
          </div>
          <div class="col-sm-12">
            <div class="row margin-left-sm">
              <div class="margin-right-sm"><a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-size="large" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
              <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
            </div>
          </div>
          <div class="col-sm-12">
            <?php if($post->getTags("string") != "") { ?>
              <p class="blog-post-meta"><i class="fas fa-tags"></i> <?php echo $post->getTags("string");?></p>
            <?php }?>
          </div>
          <div class="col-sm-12">
            <p><i><?php echo $post->getSummary(); ?></i></p>
          </div>
          <hr>
          <div class="col-sm-12">
            <p><?php echo $post->getBody(); ?></p>
          </div>
        </div>

    </div>

</div>


<?php include_once LAYOUTS_PATH . "/footer.php" ?>

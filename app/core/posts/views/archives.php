<?php include_once LAYOUTS_PATH . DS ."header.php" ?>

<div class="container">

  <div class="row justify-content-center margin-bottom-md">
      <h1>Post Archive</h1>
  </div>
  <div class="row justify-content-center">
      <a class="btn btn-primary margin-right-sm" href="./posts/list?filter=all">View All</a>
      <a class="btn btn-primary" href="./posts/list?filter=popular">Most Popular</a>
  </div>

  <hr>
  <div class="card">
    <div class="card-header">
      By Month
    </div>
    <div class="card-block text-center">
      <?php include_once POSTS_VIEW_PATH.DS."partials".DS."postsByYear.part.php" ?>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      Categories
    </div>
    <div class="card-block text-center">
      <?php include_once POSTS_VIEW_PATH.DS."partials".DS."categoryList.part.php" ?>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      Tags
    </div>
    <div class="card-block text-center">
      <?php include_once POSTS_VIEW_PATH.DS."partials".DS."tagList.part.php" ?>
    </div>
  </div>

</div>

<?php include_once LAYOUTS_PATH . DS . "footer.php" ?>

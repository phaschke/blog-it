<?php include_once LAYOUTS_PATH . DS ."header.php" ?>

<div class="container">


  <div class="row justify-content-center margin-bottom-md">
      <h1><?php echo $pageContent['header']; ?></h1>
  </div>

  <div class="row">
    <div class="col-sm-12 clearfix">
      <a class="btn btn-outline-primary margin-bottom-md" href="./posts/archive">&#x2190; Back to Archives</a>
    </div>
  </div>

  <?php if($pageContent['showSearch']) { ?>
    <hr>
    <form id="postSearchForm">

      <div class="row margin-bottom-sm">
        <div class="col-sm-12 clearfix">
          <h4>Search Posts</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <select class="form-control" id="postSearchSelect">
                <option value="tag_text">By Tag</option>
                <option value="post_title">By Title</option>
                <option value="user_name">By Author</option>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <input type="text" class="form-control" id="searchTerm" name="searchTerm" maxlength="50" placeholder="Search Term" required>
            <small id="searchTermErrorHolder" class="text-danger"></small>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <button class="btn" type="button" id="searchPostsButton" class="btn btn-primary" onclick="BlogIt.Post.searchPosts()">Search</button>
          </div>
        </div>
      </div>
    </form>
  <?php } ?>

  <hr>

  <div id="posts-holder">
    <p>Loading...</p>
  </div>

  <div id="pagination"></div>

  <script>
    BlogIt.Post.initializePostPagination("", "published", null, false, "BlogIt.Post.listPosts", "posts-holder", "view", BlogIt.Post.getSearchParameters('<?php if(isset($pageContent['searchParameters'])) echo $pageContent['searchParameters']; ?>'));
  </script>


</div>

<?php include_once LAYOUTS_PATH . DS ."footer.php" ?>


<!-- Used by BlogIt.Post.initializePostPagination and BlogIt.Post.listPosts. -->
<!-- COPIED ON VIEW category-->
<script id="post_list_template" type="text/x-handlebars-template">
  {{#each posts}}
  <div class="blog-post">
    <h2 class="blog-post-title"><a href="./posts/show/{{post_id}}">{{post_title}}</a></h2>
    <p class="blog-post-meta">{{post_created_at}} by <a href="#">{{user_name}}</a> | {{post_views}} Views</p>
    <p><i>{{post_summary}}</i></p>
  </div>
{{/each}}
</script>

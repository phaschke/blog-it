<?php include_once LAYOUTS_PATH . DS ."header.php" ?>

<div class="container">

  <div class="row margin-bottom-sm">
    <div class="col-sm-12 justify-content-center">
      <a class="btn btn-outline-primary" href="./posts/archive">&#x2190; Back to Archives</a>
    </div>
  </div>

  <div class="row margin-bottom-sm">
    <div class="col-sm-12 justify-content-center text-center">
      <h2>Posts From <?php echo $pageContent['month']." ".$pageContent['year']?> </h2>
    </div>
  </div>

  <hr>

  <div id="posts-holder">
    <p>Loading...</p>
  </div>

  <div id="pagination"></div>

  <script>
    BlogIt.Post.initializePostPagination("<?php echo $pageContent['yearMonth']; ?>", "published", null, false, "BlogIt.Post.listPosts", "posts-holder", "view");
  </script>


</div>

<?php include_once LAYOUTS_PATH . DS ."footer.php" ?>


<!-- Used by BlogIt.Post.initializePostPagination and BlogIt.Post.listPosts. -->
<!-- COPIED ON VIEW category-->
<script id="post_list_template" type="text/x-handlebars-template">
  {{#each posts}}
  <div class="blog-post">
    <h2 class="blog-post-title"><a href="./posts/show/{{post_id}}">{{post_title}}</a></h2>
    <p class="blog-post-meta">{{post_created_at}} by <a href="./accounts/user/{{user_name}}">{{user_name}}</a> | {{post_views}} Views</p>
    <p><i>{{post_summary}}</i></p>
  </div>
{{/each}}
</script>

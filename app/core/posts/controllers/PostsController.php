<?php

class PostsController extends Controller {

  private $postModel;
  private $categoryModel;
  private $pageResources = POST_PAGE_RESOURCES;

  public function __construct() {

    parent::__construct();

    $session = new Session();
    $DBConnection = new DBConnection();

    $editLockManagerRepository = new EditLockManagerRepository($DBConnection);
    $editLockManager = new EditLockManager($session, $editLockManagerRepository);

    $postRepository = new PostRepository($DBConnection);
    $this->postModel = new PostModel($postRepository, $editLockManager);

    $categoryRepository = new CategoryRepository($DBConnection);
    $this->categoryModel = new CategoryModel($categoryRepository);

  }

  /**
   * Display the archive page
   * (Public)
   */
  public function archive() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Posts Archive";
    $pageContent['session'] = $this->session;

    // Get publication dates
    $result = $this->postModel->getPostsPublicationDates(10000, 'published', 'F');
    if(isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error";
      if($result['userError']) {
        $error = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['errorGetPubDate'] = $error;

    } else {
      $pageContent['publicationDates'] = $result;
    }
    // Base link needed for postsByMonth layout
    $pageContent['baseLinkForPosts'] = "./posts/bymonth/";

    // Get post categories
    $result = $this->categoryModel->getUsedCategories();
    if(isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error";
      if($result['userError']) {
        $error = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['getCategoriesError'] = $error;

    } else {

      $pageContent['categories'] = $result['categories'];
    }

    // Get list of post tags
    $result = $this->postModel->getTags();
    if(isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error";
      if($result['userError']) {
        $error = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['getTagsError'] = $error;

    } else {

      $pageContent['tags'] = $result;
    }

    require_once( POSTS_VIEW_PATH . "/archives.php" );
  }

  /**
   * Display posts bymonth given a year and a month
   * (Public)
   */
  public function bymonth() {

    if ( !isset($_GET["year"]) || !$_GET["year"] || !isset($_GET["month"]) || !$_GET["month"]) {
      redirect("./posts/bymonth");
      return;
    }
    $pageContent = array();

    $year = $_GET["year"];
    $month = $_GET["month"];

    $pageContent['yearMonth'] = $year.$month;

    $month = DateTime::createFromFormat('m', $_GET["month"]);
    $month = $month->format('F');

    $pageContent['year'] = $year;
    $pageContent['month'] = $month;

    $pageContent['pageTitle'] = "Posts From ".$month." ".$_GET["year"];
    $pageContent['session'] = $this->session;
    $pageContent['header'] = $pageContent['pageTitle'];

    require_once( POSTS_VIEW_PATH . DS . "postsByMonth.php" );

  }

  /**
   * Display posts by category
   * (Public)
   */
  public function category() {

    if ( !isset($_GET["id"]) || !$_GET["id"] ) {
      redirect("/posts/archive");
      return;
    }

    $categoryId = $_GET["id"];

    $pageContent['session'] = $this->session;
    $pageContent['pageTitle'] = "Posts by Category";

    // Get publication dates
    $result = $this->categoryModel->getCategory($categoryId);
    if(is_array($result) && isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error";
      if($result['userError']) {
        $error = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['getCategoryError'] = $error;

    } else {
      $pageContent['category'] = $result;
      $pageContent['pageTitle'] = "Posts in ".$result->getCategoryName();


    }

    require_once( POSTS_VIEW_PATH . DS . "category.php" );
  }

  /**
   * List posts
   */
  public function list() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Blog Posts";
    $pageContent['session'] = $this->session;

    // Start generation of search filtering json
    $pageContent['searchParameters'] = '';
    $pageContent['header'] = "Posts";
    $pageContent['showSearch'] = true;

    if ( isset($_GET["filter"]) && $_GET["filter"] ) {

      $filter = $_GET["filter"];
      if($filter == "popular") {

        $pageContent['searchParameters'] = '"filter":"popular"';
        $pageContent['header'] = "Most Popular Posts";
        $pageContent['showSearch'] = false;

      }
    }

    if ( isset($_GET["author"]) && $_GET["author"] ) {
      $author = $_GET["author"];
      if($pageContent['searchParameters'] != "") {
        $pageContent['searchParameters'] .= ", ";
        $pageContent['header'] .= " and ";
      }
      $pageContent['searchParameters'] .= '"user_name":"'.$author.'"';
      $pageContent['header'] .= " by Author ".$author;
      $pageContent['showSearch'] = false;

    }

    if ( isset($_GET["title"]) && $_GET["title"] ) {

      $title = $_GET["title"];
      if($pageContent['searchParameters'] != "") {
        $pageContent['searchParameters'] .= ", ";
        $pageContent['header'] .= " and ";
      }
      $pageContent['searchParameters'] .= '"post_title":"'.$title.'"';
      $pageContent['header'] .= " with a title ".$title;
      $pageContent['showSearch'] = false;
    }

    if ( isset($_GET["tag"]) && $_GET["tag"] ) {

      $tagText = $_GET["tag"];
      if($pageContent['searchParameters'] != "") {
        $pageContent['searchParameters'] .= ", ";
        $pageContent['header'] .= " and ";
      }
      $pageContent['searchParameters'] .= '"tag_text":"'.$tagText.'"';
      $pageContent['header'] .= " by Tag ".$tagText;
      $pageContent['showSearch'] = false;
    }

    if($pageContent['searchParameters'] != "") {
      $pageContent['searchParameters'] = "{".$pageContent['searchParameters'];
      $pageContent['searchParameters'] .= "}";
    } else {
      $pageContent['header'] = "All Posts";
    }
    // End creation of search filtering json

    require_once( POSTS_VIEW_PATH . "/list.php" );

  }

  /**
   * Display a post by ID
   * (Public)
   */
  public function show() {

    if ( !isset($_GET["id"]) || !$_GET["id"] ) {
      redirect(BASE_URL);
      return;
    }

    $pageContent = array();

    $postId = (int)$_GET["id"];
    $incrementPost = false;

    // Check for post view cookie
    if (!isset($_COOKIE[WEBSITE_NAME."article".$postId])) {
      setcookie(WEBSITE_NAME."article".$postId, $postId, time()+3600);
      $incrementPost = true;
    }

    // BreweryId, Increment post view
    $result = $this->postModel->getPost($postId, $incrementPost);

    if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

      if($result['userError']) {
        $pageContent['message'] = $result['userError'];
        $pageContent['pageTitle'] = "Post Not Found";
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
    } else {

      $pageContent['post'] = $result;
      $pageContent['pageTitle'] = $pageContent['post']->getPostTitle();
    }


    $pageContent['session'] = $this->session;

    require_once( POSTS_VIEW_PATH . DS . "post.php" );

  }

  /**
   * Manage posts
   * (Privileged)
   */
  public function manage() {

    $pageContent = array();
    $pageContent['session'] = $this->session;

    // If no status is set, display ALL
    if (!isset($_GET["status"])) {

      $status = "all";

    // Status is set, and status is published, saved, or hidden
    } elseif( isset($_GET["status"]) && (in_array($_GET["status"], array('published', 'saved', 'hidden'))) ) {

      $status = $_GET["status"];

    // ERROR
    } else {
      redirect(BASE_URL);
    }

    $pageContent['status'] = $status;

    // LIST POSTS BY A YEAR/MONTH
    if ( isset($_GET["year"]) && $_GET["year"] && isset($_GET["month"]) && $_GET["month"]) {

      $year = $_GET["year"];
      $month = $_GET["month"];

      $pageContent['yearMonth'] = $year.$month;

      $month = DateTime::createFromFormat('m', $_GET["month"]);
      $month = $month->format('F');

      $pageContent['pageTitle'] = "Manage Posts From ".$month." ".$_GET["year"];
      $pageContent['header'] = ucfirst($status)." Posts From ".$month." ".$_GET["year"];

      if(strtolower($status) == "all") {
        // Base link needed for postsByMonth layout
        $pageContent['returnLink'] = "./posts/manage/";
      } else {
        $pageContent['returnLink'] = "./posts/manage/".$status."/";
      }

      require_once POSTS_VIEW_PATH.DS."admin".DS."manageByMonth.php";


    // LIST POSTS BY YEARS/MONTHS
    } else {

      // Get publication dates
      $result = $this->postModel->getPostsPublicationDates(10000, strtolower($status), 'F');
      if(isset($result['status']) && $result['status'] == 0) {
        // Default error
        $error = "Error";
        if($result['userError']) {
          $error = $result['userError'];
        }
        if(LOG_ERRORS && isset($result['consoleError'])) {
          error_log($result['consoleError']);
        }
        $pageContent['errorGetPubDate'] = $error;

      } else {
        $pageContent['publicationDates'] = $result;
      }
      if(strtolower($status) == "all") {
        // Base link needed for postsByMonth layout
        $pageContent['baseLinkForPosts'] = "./posts/manage/";
      } else {
        $pageContent['baseLinkForPosts'] = "./posts/manage/".$status."/";
      }



      $status = ucfirst(strtolower($status));
      $pageContent['status'] = $status;

      $pageContent['pageTitle'] = "Manage ".$status." Posts";

      require_once POSTS_VIEW_PATH.DS."admin".DS."manage.php";
    }

  }

  /**
   * Add post page
   * (Privileged)
   */
  public function add() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Add a Post";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    // Get post categories list
    $result = $this->categoryModel->getCategories();
    if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

      if($result['userError']) {
        $pageContent['message'] = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
    }
    $pageContent['categories'] = $result['categories'];

    require_once POSTS_VIEW_PATH.DS."admin".DS."add.php";

  }

  /**
   * Display edit post page
   * (Privileged)
   */
  public function edit() {

    if ( !isset($_GET["id"]) || !$_GET["id"] ) {
      redirect(BASE_URL . "admin/panel");
    }

    $pageContent = array();

    $pageContent['pageTitle'] = "Edit a Post";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    // BreweryId, Increment post view
    $result = $this->postModel->edit((int)$_GET["id"]);
    if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

      if($result['userError']) {
        error_log("here!!!");
        $pageContent['message'] = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
    }
    $pageContent['post'] = $result;


    // Get post categories list
    $result = $this->categoryModel->getCategories();
    if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

      if($result['userError']) {
        $pageContent['message'] = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
    }
    $pageContent['categories'] = $result['categories'];

    require_once POSTS_VIEW_PATH.DS."admin".DS."edit.php";
  }

  /**
   * Display manage page
   * (Privileged)
   */
  public function managecategories() {
    $pageContent = array();

    $pageContent['pageTitle'] = "Manage Post Categories";
    $pageContent['session'] = $this->session;

    require_once( POSTS_VIEW_PATH.DS."admin".DS."manageCategories.php" );

  }

}

?>

<?php
/**
 * Category error messages
 *
 *
 */

return [

  'category_insert_error'     => 'There was an error adding the category, please try again.',
  'category_delete_error'     => 'Error deleting category, please try again',
  'category_delete_success'   => 'Deleted the category successfully',
  'category_load_error'       => 'Error loading categor(y|ies).',
  'category_save_error'       => 'Error saving category, please try again.',
  'category_edit_error'       => 'Error editing the category, please try again.',
  'category_not_found'        => 'Category not found.',
  'category_edit_success'     => 'Saved the category successfully.',
  'category_insert_success'   => 'Added the category successfully.',
  'category_save_success'     => 'Saved the category successfully.',

];

?>

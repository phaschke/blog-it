<?php
/**
 * Posts error messages
 *
 *
 */

return [

  'insert_post_error'           => 'There was an error adding the post, please try again.',
  'post_new_published_success'  => 'Successfully published post, edit the post <a href="./posts/edit/:postId">here</a> or view the post <a href="./posts/show/:postId">here</a>.',
  'post_new_saved_success'      => 'Successfully saved post, edit the post <a href="./posts/edit/:postId">here</a>.',
  'post_load_error'             => 'Error loading post(s).',
  'post_save_error'             => 'Error saving post, Please try again.',
  'post_edit_error'             => 'Error editing the post, Please try again.',
  'post_edit_published_success' => 'Saved post successfully, view the post <a href="./posts/show/:postId">here</a>.',
  'post_edit_saved_success'     => 'Saved post successfully.',
  'post_not_found'              => 'Requested post not found.',
  'posts_not_found'             => 'No posts found.',
  'error_adding_post'           => 'Error adding new post.',
  'error_deleting_post'         => 'Error deleting post.',
  'get_tags_error'              => 'Error retrieving tags.',
  'no_tags'                     => 'No tags to retrieve.',


];

?>

<?php
//======================================================================
// POSTS MODULE CONFIG
//======================================================================

/**
 * Path To Accounts Module Folder
 */
define("POSTS_PATH", __DIR__);

define("CATEGORY_I18_MESSAGES", POSTS_PATH.DS."resources".DS."lang".DS."en".DS."categories.php");

define("POST_I18_MESSAGES", POSTS_PATH.DS."resources".DS."lang".DS."en".DS."posts.php");

/**
 * Define module specific page css/js resources
 */
define("POST_PAGE_RESOURCES", array(
         [ 'type' => 'js',
           //'path' => 'http://js.nicedit.com/nicEdit-latest.js',
           'path' => 'resources/nicEdit_v0.9r25/nicEdit.js',
         ]
       ));

/**
 * TODO: Path To Post Interface
 */

/**
 * TODO: Path To Post Repository Interface
 */

/**
 * TODO: Path To PostService Interface
 */

/**
 * Require Post Model
 */
require_once POSTS_PATH.DS."models".DS."Post.php";

/**
 * Require Post Repository Model
 */
require_once POSTS_PATH.DS."models".DS."PostRepository.php";

/**
 * Require Post Model
 */
require_once POSTS_PATH.DS."models".DS."PostModel.php";

/**
 * Require Category Model
 */
require_once POSTS_PATH.DS."models".DS."Category.php";

/**
 * Require Category Repository Model
 */
require_once POSTS_PATH.DS."models".DS."CategoryRepository.php";

/**
  * Require Category Model
  */
require_once POSTS_PATH.DS."models".DS."CategoryModel.php";

/**
 * Path To Account Views
 */
define("POSTS_VIEW_PATH", POSTS_PATH.DS."views");

/**
 * URLs to admin manage posts pages
 */
define("MANAGE_POSTS_LINK", "./posts/manage");
define("MANAGE_ALL", "./posts/manage");
define("MANAGE_PUBLISHED", "./posts/manage/published");
define("MANAGE_SAVED", "./posts/manage/saved");
define("MANAGE_HIDDEN", "./posts/manage/hidden");

?>

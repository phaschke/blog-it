<?php

/**
 * Posts class
 */
 Class Post {
   //Properties
   /**
     * @var int The Post ID
     */
   public $postId = null;

   /**
     * @var int The User ID who authored the post
     */
   public $userId = null;

   /**
     * @var int The Category ID who authored the post
     */
   public $categoryId = null;

   /**
     * @var int The User ID who authored the post
     */
   public $postViews = null;

   /**
     * @var int The User ID who authored the post
     */
   public $postStatus = null;

   /**
     * @var string The User who authored the brewery
     */
   public $userName = null;

   /**
     * @var string The Category Name
     */
   public $categoryName = null;

   /**
     * @var string The title of the post
     */
   public $postTitle = null;

   /**
     * @var int The date the post was created
     */
   public $createdAt = null;

   /**
     * @var int The date the post was last updated
     */
   public $updatedAt = null;

   /**
     * @var string The summary of the post
     */
   public $summary = null;

   /**
    * @var array The array of tags associated with the post
    */
   protected $tags;

   /**
     * @var string The body of the post
     */
   public $body = null;

   /**
    * Set the post objects properties using supplied ArrayAccess
    *
    * @todo Add HTML purifier to post content
    *
    * @param assoc the posts properity values
    */

   public function __construct($data = array()) {

     if(isset($data['post_id'])) $this->setPostId($data['post_id']);
     if(isset($data['user_id'])) $this->setUserId($data['user_id']);
     if(isset($data['category_id'])) $this->setCategoryId($data['category_id']);
     if(isset($data['post_views'])) $this->setPostViews($data['post_views']);
     if(isset($data['post_status'])) $this->setPostStatus($data['post_status']);
     if(isset($data['user_name'])) $this->setUserName($data['user_name']);
     if(isset($data['category_name'])) $this->setCategoryName($data['category_name']);
     if(isset($data['post_title'])) $this->setPostTitle($data['post_title']);
     if(isset($data['post_created_at'])) $this->setCreatedAt($data['post_created_at']);
     if(isset($data['post_updated_at'])) $this->setUpdatedAt($data['post_updated_at']);
     if(isset($data['post_summary'])) $this->setSummary($data['post_summary']);
     if(isset($data['tags'])) $this->setTags($data['tags']);
     if(isset($data['post_body'])) $this->setBody($data['post_body']);
   }

   public function setPostId($postId) {
     $this->postId = (int) $postId;
   }

   public function getPostId() {
     return $this->postId;
   }

   public function setUserId($userId) {
     $this->userId = (int) $userId;
   }

   public function getUserId() {
     return $this->userId;
   }

   public function setCategoryId($categoryId) {
     $this->categoryId = (int) $categoryId;
   }

   public function getCategoryId() {
     return $this->categoryId;
   }

   public function setPostViews($postViews) {
     $this->postViews = (int) $postViews;
   }

   public function getPostViews() {
     return $this->postViews;
   }

   public function setPostStatus($postStatus) {
     $this->postStatus = $postStatus;
   }

   public function getPostStatus() {
     return $this->postStatus;
   }

   public function setUserName($userName) {
     $this->userName = $userName;
   }

   public function getUserName() {
     return $this->userName;
   }

   public function setCategoryName($categoryName) {
     $this->categoryName = $categoryName;
   }

   public function getCategoryName() {
     return $this->categoryName;
   }

   public function setPostTitle($postTitle) {
     $this->postTitle = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $postTitle);
   }

   public function getPostTitle() {
     return $this->postTitle;
   }

   public function setCreatedAt($createdAt) {
     $this->createdAt = $createdAt;
   }

   public function getCreatedAt($dateFormat=null) {
     $formattedDate = $this->createdAt;
     if($dateFormat && $formattedDate) {
       $formattedDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->getCreatedAt())->format($dateFormat);
     }
     return $formattedDate;
   }

   public function setUpdatedAt($updatedAt) {
     $this->updatedAt = $updatedAt;
   }

   public function getUpdatedAt($dateFormat=null) {
     $formattedDate = $this->updatedAt;
     if($dateFormat && $formattedDate) {
       $formattedDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->getUpdatedAt())->format($dateFormat);
     }
     return $formattedDate;
   }

   public function setSummary($summary) {
     $this->summary = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $summary);
   }

   public function getSummary() {
     return $this->summary;
   }

   public function setTags($tags) {
     $this->tags = $tags;
   }

   public function getTags($returnType="array") {
     if($returnType == "string") {

       $string = implode(', ', $this->tags);
       return $string;
     }
     return $this->tags;
   }

   public function setBody($body) {
     $this->body = $body;
   }

   public function getBody() {
     return $this->body;
   }

   public function returnAsArray($dateFormat=null) {

     $data = array();
     if(null !== $this->getPostId()) $data['post_id'] = $this->getPostId();
     if(null !== $this->getUserId()) $data['user_id'] = $this->getUserId();
     if(null !== $this->getCategoryId()) $data['category_id'] = $this->getCategoryId();
     if(null !== $this->getUserName()) $data['user_name'] = $this->getUserName();
     if(null !== $this->getCategoryName()) $data['category_name'] = $this->getCategoryName();
     if(null !== $this->getPostViews()) $data['post_views'] = $this->getPostViews();
     if(null !== $this->getPostStatus()) $data['post_status'] = $this->getPostStatus();
     if(null !== $this->getPostTitle()) $data['post_title'] = $this->getPostTitle();
     if(null !== $this->getCreatedAt($dateFormat)) $data['post_created_at'] = $this->getCreatedAt($dateFormat);
     if(null !== $this->getUpdatedAt($dateFormat)) $data['post_updated_at'] = $this->getUpdatedAt($dateFormat);
     if(null !== $this->getSummary()) $data['post_summary'] = $this->getSummary();
     if(null !== $this->getTags()) $data['tags'] = $this->getTags();
     if(null !== $this->getBody()) $data['post_body'] = $this->getBody();

     return $data;

     /*return array(
     'post_id' => $this->getPostId(),
     'user_id' => $this->getUserId(),
     'category_id' => $this->getCategoryId(),
     'user_name' => $this->getUserName(),
     'category_name' => $this->getCategoryName(),
     'post_views' => $this->getPostViews(),
     'post_status' => $this->getPostStatus(),
     'post_title' => $this->getPostTitle(),
     'post_created_at' => $this->getCreatedAt($dateFormat),
     'post_updated_at' => $this->getUpdatedAt($dateFormat),
     'post_summary' => $this->getSummary(),
     'post_body' => $this->getBody(),
   );*/
   }

 }


 ?>

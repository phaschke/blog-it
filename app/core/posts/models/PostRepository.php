<?php

/**
 * Post Repository related functions
 */
Class PostRepository {

  private $connection;

  public function __construct(DBConnection $DBConnection) {

    $this->connection = $DBConnection->getConnection();
  }

  /**
   * Retrieve a post
   *
   * @param int $postId
   * @return Post|False
   */
  public function getPost($postId, $incrementView, $status) {

    if($status == "published") {
      $sql = $this->connection->prepare('SELECT T1.*, T2.user_name, T3.category_name
          FROM posts AS T1
          INNER JOIN users AS T2 ON T1.user_id=T2.user_id
          INNER JOIN posts_categories AS T3 ON T1.category_id=T3.category_id
          WHERE post_id = :postId AND post_status LIKE \'published\'');

    } else {
      $sql = $this->connection->prepare('SELECT T1.*, T2.user_name, T3.category_name
          FROM posts AS T1
          INNER JOIN users AS T2 ON T1.user_id=T2.user_id
          INNER JOIN posts_categories AS T3 ON T1.category_id=T3.category_id
          WHERE post_id = :postId');
    }

    $sql->execute(array(':postId' => $postId));

    if($sql->rowCount() < 1) {
      return false;
    }

    $post = new Post($sql->fetch());

    $sql = $this->connection->prepare('SELECT tag_text
        FROM posts_tags
        WHERE post_id = :postId');
    $sql->execute(array(':postId' => $postId));

    $tags = array();
    while($row = $sql->fetch()) {
      $tags[] = $row['tag_text'];
    }
    $post->setTags($tags);

    if($incrementView) {
      $sql = $this->connection->prepare('UPDATE posts SET post_views = post_views + 1 WHERE post_id = :postId');
      $sql->execute(array(':postId' => $postId));
    }

    return $post;

  }

  /**
   * Returns all (or a range of) Posts by creation date DESC
   *
   * @param int $numPosts The number of posts to return (default: all)
   * @param int $yearMonth The date as format YYYYMM of the posts.
   * @return Array A two element array with posts => returned posts and totalRows => total rows selected.
   */
  public function getPosts($numPosts, $offset, $yearMonth, $status, $categoryId) {
    $sql = $this->connection->prepare('SELECT SQL_CALC_FOUND_ROWS T1.*, T2.user_name, T3.category_name
       FROM posts AS T1
       INNER JOIN users AS T2 ON T1.user_id=T2.user_id
       INNER JOIN posts_categories AS T3 ON T1.category_id=T3.category_id
       WHERE (:yearMonth IS NULL OR EXTRACT( YEAR_MONTH FROM post_created_at) = :yearMonth)
        AND (:status IS NULL OR post_status = :status)
        AND (:categoryId IS NULL OR T1.category_id = :categoryId)
       ORDER BY post_created_at DESC LIMIT :numPosts OFFSET :numOffset');

    $sql->bindValue(":numPosts", (int) $numPosts, PDO::PARAM_INT);
    $sql->bindValue(":numOffset", (int) $offset, PDO::PARAM_INT);
    $sql->bindValue(":yearMonth", $yearMonth, PDO::PARAM_STR);
    $sql->bindValue(":status", $status, PDO::PARAM_STR);
    $sql->bindValue(":categoryId", $categoryId, PDO::PARAM_STR);

    $sql->execute();
    $posts = array();

    while($row = $sql->fetch()) {
      $post = new Post($row);
      $posts[] = $post;
    }
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalPosts = $this->connection->query($sql)->fetch();

    return (array("posts" => $posts, "postCount" => $totalPosts[0]));
  }

  /**
   * Get a list of linked post IDs
   *
   * @param string linkedPostTag
   * @return array
   *
   * @throws PDOException
   */
  public function getPostsForList($numPosts, $offset, $yearMonth, $status, $categoryId, $searchParameters) {

    $whereClause = "";
    $innerJoinTags = " ";

    if(count($searchParameters) > 0) {
      if(isset($searchParameters['tag_text']) && $searchParameters['tag_text'] != "") {
        $innerJoinTags = " INNER JOIN posts_tags AS T3 ON T1.post_id = T3.post_id ";
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " T3.tag_text = :tag_text";
      }
      if(isset($searchParameters['user_name']) && $searchParameters['user_name'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " user_name LIKE :user_name";
      }
      if(isset($searchParameters['post_title']) && $searchParameters['post_title'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " post_title LIKE :post_title";
      }
    }

    if($whereClause != "") {
      $whereClause = " AND ".$whereClause;
    }

    $sql = $this->connection->prepare('SELECT DISTINCT T1.post_id, T1.post_title, T1.post_summary, T1.post_created_at, T1.post_views, T2.user_name, T4.category_name
      FROM posts as T1
      INNER JOIN users AS T2 ON T1.user_id = T2.user_id'.$innerJoinTags.'INNER JOIN posts_categories AS T4 ON T1.category_id=T4.category_id
      WHERE (:yearMonth IS NULL OR EXTRACT( YEAR_MONTH FROM post_created_at) = :yearMonth)
       AND (:status IS NULL OR post_status = :status)
       AND (:categoryId IS NULL OR T1.category_id = :categoryId)'.$whereClause.'
      ORDER BY post_created_at DESC LIMIT :numPosts OFFSET :numOffset');

    $sql->bindValue(":status", $status, PDO::PARAM_STR);
    $sql->bindValue(":numPosts", (int) $numPosts, PDO::PARAM_INT);
    $sql->bindValue(":numOffset", (int) $offset, PDO::PARAM_INT);
    $sql->bindValue(":yearMonth", $yearMonth, PDO::PARAM_STR);
    $sql->bindValue(":categoryId", $categoryId, PDO::PARAM_STR);
    if(isset($searchParameters['tag_text']) && $searchParameters['tag_text'] != "") $sql->bindValue(":tag_text", trim($searchParameters['tag_text']), PDO::PARAM_STR);
    if(isset($searchParameters['user_name']) && $searchParameters['user_name'] != "") $sql->bindValue(":user_name", trim($searchParameters['user_name']).'%', PDO::PARAM_STR);
    if(isset($searchParameters['post_title']) && $searchParameters['post_title'] != "") $sql->bindValue(":post_title", trim($searchParameters['post_title']).'%', PDO::PARAM_STR);

    $sql->execute();
    $posts = array();

    while($row = $sql->fetch()) {
      $post = new Post($row);
      $posts[] = $post;
    }
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalPosts = $this->connection->query($sql)->fetch();

    return (array("posts" => $posts, "postCount" => $totalPosts[0]));

  }

  /**
   * Gets posts by the most popular
   *
   * @param Post
   * @throws PDOException
   * @return array
   */
  public function getPopularPosts($numPosts, $offset) {

    $sql = $this->connection->prepare('SELECT DISTINCT T1.post_id, T1.post_title, T1.post_summary, T1.post_created_at, T1.post_views, T2.user_name
      FROM posts as T1
      INNER JOIN users AS T2 ON T1.user_id = T2.user_id
      WHERE T1.post_status = "published"
      ORDER BY post_views DESC LIMIT :numPosts OFFSET :numOffset');

    $sql->bindValue(":numPosts", (int) $numPosts, PDO::PARAM_INT);
    $sql->bindValue(":numOffset", (int) $offset, PDO::PARAM_INT);

    $sql->execute();
    $posts = array();

    while($row = $sql->fetch()) {
      $post = new Post($row);
      $posts[] = $post;
    }
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalPosts = $this->connection->query($sql)->fetch();

    return (array("posts" => $posts, "postCount" => $totalPosts[0]));

  }


  /**
   * Add a post to the database
   *
   * @param Post
   * @throws PDOException
   * @return Integer // The newly inserted post Id
   */
  public function insert(Post $post) {

    $sql = $this->connection->prepare('INSERT INTO posts (user_id, post_status, category_id, post_title, post_created_at, post_summary, post_body)
       VALUES (:userId, :status, :categoryId, :title, NOW(), :summary, :body)');
    $result = $sql->execute(array(':userId' => $post->getUserId(), ':status' => $post->getPostStatus(), ':categoryId' => $post->getCategoryId(),
    ':title' => $post->getPostTitle(), ':summary' => $post->getSummary(),
    ':body' => $post->getBody()));

    //Retrieve the id of the newly inserted post
    return $this->connection->lastInsertId();
  }

  /**
  * Update the current post object in the database by the id
  *
  * @param Post
  * @throws PDOException
  */
  public function update(Post $post) {

    $sql = $this->connection->prepare('UPDATE posts
          SET post_status = COALESCE(:status, post_status),
              category_id = :categoryId,
              post_title = :title,
              post_updated_at = NOW(),
              post_summary = :summary,
              post_body = :body
          WHERE post_id = :postId');
    $sql->execute(array(':status'=> $post->getPostStatus(), ':title' => $post->getPostTitle(),
                        ':summary' => $post->getSummary(), ':body' => $post->getBody(), ':postId' => $post->getPostId(),
                        ':categoryId' => $post->getCategoryId() ));

  }

  /**
   * Hide post
   *
   * @param int $postId
   * @throws PDOException
   */
  public function toggleVisibility($postId) {
    $sql = $this->connection->prepare('SELECT post_status FROM posts WHERE post_id = :postId LIMIT 1');
    $sql->execute(array(':postId' => $postId));
    $result = $sql->fetch(PDO::FETCH_ASSOC);

    if($result['post_status'] == 'hidden') {
      $postStatus = 'published';
    } else {
      $postStatus = 'hidden';
    }

    $sql = $this->connection->prepare('UPDATE posts SET post_status = :status WHERE post_id = :postId LIMIT 1');
    $sql->execute(array(':postId' => $postId, ':status' => $postStatus));
  }

  /**
   * Delete the current post object in the database by the id
   *
   * @param int $postId
   * @throws PDOException
   */
  public function delete($postId) {

  //Fetch time created and status for return link
    $sql = $this->connection->prepare('SELECT post_status, post_created_at FROM posts WHERE post_id = :postId LIMIT 1');
    $sql->execute(array(':postId' => $postId));
    $result = $sql->fetch(PDO::FETCH_ASSOC);
    $returnData = array("postStatus" => $result["post_status"], "createdAt" => $result["post_created_at"]);

    $sql = $this->connection->prepare('DELETE FROM posts WHERE post_id = :post_id LIMIT 1');
    $sql->execute(array(':post_id' => $postId));

    return $returnData;

  }

  /**
   * Insert new tags/ check and remove any tags not in input
   */
  public function updateTags($postId, $tags) {

    //Remove tags not in the tag array
    $qMarks = str_repeat('?,', count($tags) - 1) . '?';

    $sqlString = 'DELETE FROM posts_tags WHERE post_id = ? AND tag_text NOT IN ('.$qMarks.')';
    $sql = $this->connection->prepare($sqlString);
    $sql->execute(array_merge(array($postId), $tags));

    foreach($tags as $tag) {

      $sql = $this->connection->prepare('SELECT COUNT(*) AS count FROM posts_tags WHERE post_id = :postId AND tag_text = :tagText');
      $sql->execute(array(':postId' => $postId, ':tagText' => $tag));

      $result = $sql->fetch();
      if($result['count'] < 1) {
          // insert when tag is not already added
          $sql = $this->connection->prepare('INSERT INTO posts_tags (post_id, tag_text)
             VALUES (:postId, :tagText)');
          $result = $sql->execute(array(':postId' => $postId, ':tagText' => $tag));
          continue;
      }

    }
  }

  /**
   * Remove all tags from a post
   */
  public function deleteTags($postId) {

  }

  /**
   * Returns all months/years where posts have been written
   *
   * @param int numMonths  The number of months to return.
   * @param string format The format of the resulting strings.
   * @return Array
   */
  public function getPostsPublicationDates($numMonths, $status, $format) {
    $sql = $this->connection->prepare('SELECT EXTRACT( YEAR_MONTH FROM post_created_at) AS postCreateMonth, count(*) AS count
    FROM posts
    WHERE (:status IS NULL OR post_status = :status)
    GROUP BY postCreateMonth ORDER BY postCreateMonth DESC LIMIT :numMonths');
    $sql->bindValue(":numMonths", $numMonths, PDO::PARAM_INT);
    $sql->bindValue(":status", $status, PDO::PARAM_STR);
    $sql->execute();

    $publicationDates = array();

    while($row = $sql->fetch()) {
      $publicationDate = DateTime::createFromFormat('Ym', $row['postCreateMonth']);
      $publicationDates[] = ["postYear"=>$publicationDate->format('Y'), "postMonth"=>$publicationDate->format('m'), "postFormattedDate"=>$publicationDate->format($format), "postCount"=>$row['count']];
    }

    return $publicationDates;
  }

  /**
   * Get the total record count from the brewery record table
   *
   * @return int
   * @throws PDOException
   */
  public function getPostCount($yearMonth, $status, $categoryId) {

    $sql = $this->connection->prepare('SELECT COUNT(*)
      FROM posts
      WHERE (:yearMonth IS NULL OR EXTRACT( YEAR_MONTH FROM post_created_at) = :yearMonth)
        AND (:status IS NULL OR post_status = :status)
        AND (:categoryId IS NULL OR category_id = :categoryId)');

    $sql->execute(array(':yearMonth' => $yearMonth, ':status' => $status, ':categoryId' => $categoryId ));

    return $sql->fetchColumn();
  }

  /**
   * Return list of post tags from published posts
   *
   * @return array list of tags used in published posts ordered by the most used
   * @throws PDOException
   */
  public function getTags() {
    $sql = $this->connection->prepare('SELECT DISTINCT(T1.tag_text)
      FROM posts_tags AS T1 INNER JOIN posts AS T2 ON T1.post_id=T2.post_id
      WHERE T2.post_status = \'published\'
      GROUP BY T1.tag_text
      ORDER BY COUNT(*) DESC');

    $sql->execute();

    $tagList = array();
    while($tag = $sql->fetch()) {
      $tagList[] = $tag;
    }

    return $tagList;

  }


}




?>

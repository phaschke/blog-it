<?php

/**
 * Posts class
 */
 Class Category {
   //Properties
   /**
     * @var int The Category ID
     */
   public $categoryId = null;

   /**
     * @var string The name of the category
     */
   public $categoryName = null;

   /**
     * @var string The description of the category
     */
   public $categoryDescription = null;

   /**
    * Set the caregory objects properties using supplied ArrayAccess
    *
    * @todo Add HTML purifier to post content
    *
    * @param assoc the posts properity values
    */

   public function __construct($data = array()) {

     if(isset($data['category_id'])) $this->setCategoryId($data['category_id']);
     if(isset($data['category_name'])) $this->setCategoryName($data['category_name']);
     if(isset($data['category_description'])) $this->setCategoryDescription($data['category_description']);
   }

   public function setCategoryId($categoryId) {
     $this->categoryId = (int) $categoryId;
   }

   public function getCategoryId() {
     return $this->categoryId;
   }

   public function setCategoryName($categoryName) {
     $this->categoryName =  $categoryName;
   }

   public function getCategoryName() {
     return $this->categoryName;
   }

   public function setCategoryDescription($categoryDescription) {
     $this->categoryDescription = $categoryDescription;
   }

   public function getCategoryDescription() {
     return $this->categoryDescription;
   }

   public function returnAsArray() {
     return array(
     'category_id' => $this->getCategoryId(),
     'category_name' => $this->getCategoryName(),
     'category_description' => $this->getCategoryDescription()
    );
   }

 }


 ?>

<?php

/**
 * Category Repository related functions
 */
Class CategoryRepository {

  private $connection;

  public function __construct(DBConnection $DBConnection) {

    $this->connection = $DBConnection->getConnection();
  }

  /**
   *
   */
  public function getCategories($count) {

    $sql = $this->connection->prepare('SELECT SQL_CALC_FOUND_ROWS *
      FROM posts_categories
      LIMIT :count');
    $sql->bindValue(":count", $count, PDO::PARAM_INT);

    $sql->execute();
    $categories = array();

    while($row = $sql->fetch()) {
      $categories[] = new Category($row);
    }
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalCategories = $this->connection->query($sql)->fetch();

    return (array("categories" => $categories, "totalCategories" => $totalCategories[0]));
  }

  /**
   *
   */
  public function getUsedCategories($count) {

    $sql = $this->connection->prepare('SELECT SQL_CALC_FOUND_ROWS *
      FROM posts_categories
      WHERE category_id IN (SELECT category_id FROM posts WHERE post_status = "published")
      LIMIT :count');
    $sql->bindValue(":count", $count, PDO::PARAM_INT);

    $sql->execute();
    $categories = array();

    while($row = $sql->fetch()) {
      $categories[] = new Category($row);
    }
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalCategories = $this->connection->query($sql)->fetch();

    return (array("categories" => $categories, "totalCategories" => $totalCategories[0]));
  }

  /**
   * Function to return an category object by ID, or false if the category does not exist.
   *
   * @param int categoryId The ID of the requested category
   * @return Category|false Category on success, false if no category is found
   */
  public function getCategory($categoryId) {
    $sql = $this->connection->prepare('SELECT * FROM posts_categories WHERE category_id = :categoryId');
    $sql->execute(array(':categoryId' => $categoryId));

    $row = $sql->fetch();

    if(empty($row)) {
      return false;
    }
    return new Category($row);
    
  }

  /**
   *
   */
  public function insert(Category $category) {

    $sql = $this->connection->prepare('INSERT INTO posts_categories (category_name, category_description)
       VALUES (:categoryName, :categoryDescription)');

    $sql->execute(array(':categoryName' => $category->getCategoryName(), ':categoryDescription' => $category->getCategoryDescription()));

  }

  /**
   *
   */
  public function delete($categoryId) {

    try {
      // Begin a transaction, turning off autocommit
      $this->connection->beginTransaction();

      $sql = $this->connection->prepare('UPDATE posts SET category_id = 1 WHERE category_id = :categoryId');
      $sql->execute(array(':categoryId' => $categoryId));

      $sql = $this->connection->prepare('DELETE FROM posts_categories WHERE category_id = :categoryId');
      $sql->execute(array(':categoryId' => $categoryId));

      // No error, commit queries
      $this->connection->commit();

    } catch(Exception $e) {
      // Error with queries rollback to begin transation
      $this->connection->rollBack();

      return (array('status' => 0,
        'consoleError' => $e));
    }

  }

  /**
   *
   */
  public function update(Category $category) {

    $sql = $this->connection->prepare('UPDATE posts_categories
          SET category_name = COALESCE(:categoryName, category_name),
              category_description = COALESCE(:categoryDescription, category_description)
          WHERE category_id = :categoryId');
    $sql->execute(array(':categoryId' => $category->getCategoryId(),':categoryName' => $category->getCategoryName(),
                        ':categoryDescription' => $category->getCategoryDescription()));

  }


}

?>

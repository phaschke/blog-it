<?php
/**
 * Posts class
 */
 Class PostModel {
   //Properties

   private $postRepository;
   private $editLockManager;

   public function __construct(PostRepository $postRepository, $editLockManager) {

     $this->postRepository = $postRepository;
     $this->editLockManager = $editLockManager;

   }

   /**
    * Returns a Post object given a post id
    *
    * @param int The desired post id
    * @return Post|false The post object, or false if the record was not found or there was an error
    */
   public function getPost($postId, $incrementView=false) {

     try {
       $result = $this->postRepository->getPost($postId, $incrementView, "published");
       if(is_bool($result) && $result === false) {
         // No post was found
         return (array('status' => 0,
            'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_not_found')));
       }
       return $result;

     } catch (Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_load_error'),
          'consoleError'=>$e));
     }
   }

   /**
    * Returns all (or a range of) Posts
    *
    * @param int Optional The number of posts to return (default: all)
    * @param string Optional column and specification to order the posts by (default: creation date DESC)
    * @return Array|false A two element array with posts => returned posts and totalRows => total rows selected. False on error.
    */
   public function getPosts($numPosts=10000, $offset, $yearMonth, $status, $categoryId, $searchParameters=array()) {

     // Alias 'all' to NULL
     if(strcasecmp($status, 'all') == 0) {
       $status = NULL;
     }

     try {

       if(isset($searchParameters['filter']) && $searchParameters['filter']=='popular') {

         $posts = $this->postRepository->getPopularPosts($numPosts, $offset);
       } else {

         $posts = $this->postRepository->getPostsForList($numPosts, $offset, $yearMonth, $status, $categoryId, $searchParameters);
       }

       return array('totalCount'=>$posts['postCount'], 'posts'=>$posts['posts']);

     } catch (Exception $e) {
       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_load_error'),
          'consoleError'=>$e));
     }


   }

   /**
    * Get total post record count given specific
    *
    * @return array number of records or error message array
    */
   public function getPostCount($yearMonth, $status, $categoryId) {

     // Alias 'all' to NULL
     if(strcasecmp($status, 'all') == 0) {
       $status = NULL;
     }

     try {

       return array('count' => $this->postRepository->getPostCount($yearMonth, $status, $categoryId));

     } catch(Exception $e) {

       return (array('status' => 0,
         'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_load_error'),
         'consoleError' => $e));
     }
   }

   /**
    * Insert the current Post object into the database, and set it's newly found post id.
    *
    */
   public function add(Post $post) {

     $session = new Session();
     if($session->isSessionStarted()){
       $userId = $session->getSessionVariable('UUID');
     }

     $post->setUserId($userId);


     try {

       $postId = $this->postRepository->insert($post);

       if(count($post->getTags()) > 0) {
         $this->postRepository->updateTags($postId, $post->getTags());
       }

       if($post->getPostStatus() == 'published') {

         return getI18nMessage(POST_I18_MESSAGES, 'post_new_published_success', array(':postId' => $postId));

       } else {
         return getI18nMessage(POST_I18_MESSAGES, 'post_new_saved_success', array(':postId' => $postId));

       }

     } catch (Exception $e) {
       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'insert_post_error'),
          'consoleError'=>$e));
     }

   }

   /**
    * Edit a given post
    *
    * @return boolean true|false true on success, false on error
    */
   public function edit($postId) {

     if(!$this->editLockManager->lock('POST', $postId)) {
       return (array('status' => 0,
         'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_edit_error'),
         'consoleError' => __CLASS__.":".__METHOD__."Error setting edit object lock"));
     }

     $result = $this->postRepository->getPost($postId, false, "all");
     if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

       return (array('status' => 0,
         'userError' => $result['userError'],
         'consoleError' => $result['consoleError']));
     }

     return $result;

   }

   /**
    * Update the current post object in the database by the id
    *
    * @param Post post to be updated
    * @return boolean true|false true on success, false on error
    */
   public function update(Post $post) {

     $result = $this->editLockManager->verifyLockStatus('POST');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_save_error'),
          'consoleError'=>"User sessions edit type is mismatched."));
     }

     // set post object post ID and user ID
     $post->setPostId($result['EDIT_ID']);
     $post->setUserId($result['USER_ID']);

     // call repo function
     try {

       if(count($post->getTags()) > 0) {
         $this->postRepository->updateTags($post->getPostId(), $post->getTags());
       }

       $this->postRepository->update($post);

     } catch(Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_save_error'),
          'consoleError'=>$e));
     }

     error_log($post->getPostStatus());

     if($post->getPostStatus() == 'published') {
       return getI18nMessage(POST_I18_MESSAGES, 'post_edit_published_success', array(':postId' => $post->getPostId()));

     } else {
       return getI18nMessage(POST_I18_MESSAGES, 'post_edit_saved_success');

     }

   }

   /**
    * Delete the current post object in the database by the id
    *
    * @return boolean true|false true on success, false on error
    */
   public function delete() {

     $result = $this->editLockManager->verifyLockStatus('POST');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_save_error'),
          'consoleError'=>"User sessions edit type is mismatched."));
     }

     // set post object post ID and user ID
     $postId = $result['EDIT_ID'];
     $userId = $result['USER_ID'];

     try {

       $result = $this->postRepository->delete($postId);

     } catch(Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_save_error'),
          'consoleError'=>$e));
     }

     return MANAGE_POSTS_LINK.DS.$result['postStatus'].DS.DateTime::createFromFormat('Y-m-d', $result['createdAt'])->format('Y/m');

   }

   /**
    * Hide the current post object in the database by the id
    *
    * @todo implement hiding posts
    * @return boolean true|false true on success, false on error
    */
   public function toggleVisibility() {

     $result = $this->editLockManager->verifyLockStatus('POST');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_save_error'),
          'consoleError'=>"User sessions edit type is mismatched."));
     }

     // set post object post ID and user ID
     $postId = $result['EDIT_ID'];
     $userId = $result['USER_ID'];

     // call repo function
     try {
       $this->postRepository->toggleVisibility($postId);

     } catch(Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_save_error'),
          'consoleError'=>$e));
     }

     return getI18nMessage(POST_I18_MESSAGES, 'post_edit_success');

   }

    /**
     * Returns all months/years where posts have been written
     *
     * @param int numMonths Optional The number of months to return, default is all.
     * @param string status Optional The status of the posts, default is NULL (ALL).
     * @param string format Optional The format of the resulting strings, default is 'Month YYYY'.
     * @return Array
     */
    public function getPostsPublicationDates($numMonths=10000, $status=null, $format = "F Y") {

      // Alias 'all' to NULL
      if(strcasecmp($status, 'all') == 0) {
        $status = NULL;
      }

      try {
        $publicationDates = $this->postRepository->getPostsPublicationDates($numMonths, $status, $format);
        if(count($publicationDates) <= 0) {
          return array('status' => 0,
             'userError' => getI18nMessage(POST_I18_MESSAGES, 'posts_not_found'));
        }
        return $publicationDates;
      } catch (Exception $e) {
        return (array('status' => 0,
           'userError' => getI18nMessage(POST_I18_MESSAGES, 'post_load_error'),
           'consoleError'=>$e));
      }
    }

    /**
     * Returns array of tags used in published posts ordered by most used to least used
     *
     * @return array list of tags, or errors
     */
    public function getTags() {

      try {
        $tags = array();
        $tags = $this->postRepository->getTags();
        if(count($tags) <= 0) {
          return array('status' => 0,
             'userError' => getI18nMessage(POST_I18_MESSAGES, 'no_tags'));
        }

        return $tags;

      } catch (Exception $e) {
        return (array('status' => 0,
           'userError' => getI18nMessage(POST_I18_MESSAGES, 'get_tags_error'),
           'consoleError'=>$e));
      }
    }

}
 ?>

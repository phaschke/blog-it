<?php
/**
 * Posts class
 */
 Class CategoryModel {
   //Properties

   private $categoryRepository;


   public function __construct(CategoryRepository $categoryRepository) {

     $this->categoryRepository = $categoryRepository;

   }

   /**
    *
    */
   public function getCategory($categoryId) {

     try {
       $result = $this->categoryRepository->getCategory($categoryId);
       if(is_bool($result) && !$result) {
         return (array('status' => 0,
            'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_not_found')));

       } else {
          return $result;
       }

     } catch (Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_not_found'),
          'consoleError'=>$e));
     }
   }

   /**
    *
    */
   public function getCategories($count=9999) {

     try {
       return $this->categoryRepository->getCategories($count);

     } catch (Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_load_error'),
          'consoleError'=>$e));
     }
   }

   /**
    *
    */
   public function getUsedCategories($count=9999) {

     try {
       return $this->categoryRepository->getUsedCategories($count);

     } catch (Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_load_error'),
          'consoleError'=>$e));
     }
   }

   /**
    *
    */
   public function addCategory(Category $category) {

     try {
       $this->categoryRepository->insert($category);

       return getI18nMessage(CATEGORY_I18_MESSAGES, 'category_insert_success');

     } catch (Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_insert_error'),
          'consoleError'=>$e));
     }
   }

   /**
    *
    */
   public function updateCategory(Category $category) {

     try {
       $this->categoryRepository->update($category);

       return getI18nMessage(CATEGORY_I18_MESSAGES, 'category_save_success');

     } catch (Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_save_error'),
          'consoleError'=>$e));
     }
   }

   /**
    *
    */
   public function deleteCategory($categoryId) {

     if($categoryId == 1) {
       return (array('status' => 0,
          'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_delete_error'),
          'consoleError'=>'User attempted to remove category with id 1.'));
     }

     try {
       $this->categoryRepository->delete($categoryId);
       return getI18nMessage(CATEGORY_I18_MESSAGES, 'category_delete_success');

     } catch (Exception $e) {

       return (array('status' => 0,
          'userError' => getI18nMessage(CATEGORY_I18_MESSAGES, 'category_delete_error'),
          'consoleError'=>$e));
     }
   }



 }

 ?>

<?php

return array(
    'home' => [ 'type' => 'core',
              'folder' => 'home',
              'className' => 'HomeController',
              'public' => ['show'],
              'user' => [],
              'privileged' => []
            ],
    'pages' => [ 'type' => 'core',
                'folder' => 'pages',
                'className' => 'PagesController',
                'public' => ['about'],
                'user' => [],
                'privileged' => ['manage']
              ],
    'error' => [ 'type' => 'core',
                'folder' => 'error',
                'className' => 'ErrorController',
                'public' => ['not_found', 'not_implemented', 'permission_denied'],
                'user' => [],
                'privileged' => []
              ],
    'accounts' => [ 'type' => 'core',
                   'folder' => 'accounts',
                   'className' => 'AccountsController',
                   'public' => ['login', 'logout', 'register', 'verify', 'forgotpassword', 'resetpassword', 'user'],
                   'user' => ['settings'],
                   'privileged' => ['manage', 'manageuser']
                 ],
    'registration' => [ 'type' => 'core',
                   'folder' => 'accounts',
                   'className' => 'CompleteRegistrationController',
                   'public' => [],
                   'user' => ['complete'],
                   'privileged' => []
                 ],
    'posts' => [ 'type' => 'core',
                'folder' => 'posts',
                'className' => 'PostsController',
                'public' => ['archive', 'list', 'bymonth', 'category', 'show'],
                'user' => [],
                'privileged' => ['add', 'edit', 'manage', 'managecategories']
              ],
    'admin' => [ 'type' => 'core',
                'folder' => 'admin',
                'className' => 'AdminController',
                'public' => [],
                'user' => [],
                'privileged' => ['panel']
              ],
    'oauth' => [ 'type' => 'core',
                'folder' => 'accounts',
                'className' => 'OAuthController',
                'public' => [],
                'user' => [],
                'privileged' => []
                ]
  );


?>

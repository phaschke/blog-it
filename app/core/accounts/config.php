<?php
//======================================================================
// ACCOUNT CORE MODULE CONFIG
//======================================================================

/**
 * Path To Accounts Module Folder
 */
define("ACCOUNTS_PATH", __DIR__);

define("ACCOUNT_I18_MESSAGES", ACCOUNTS_PATH.DS."resources".DS."lang".DS."en".DS."accounts.php");

/**
 * URL To Complete Registation Page
 */
define("COMPLETE_REGISTRATION_URL", BASE_URL."registration/complete");

/**
 * TODO: Path To User Interface
 */

/**
 * TODO: Path To User Repository Interface
 */

/**
 * TODO: Path To UserAuthService Interface
 */

/**
 * Require Account Model
 */
require_once ACCOUNTS_PATH.DS."models".DS."Account.php";

/**
 * Require Account Repository Model
 */
require_once ACCOUNTS_PATH.DS."models".DS."AccountRepository.php";

/**
 * Require AccountAuth Service Model
 */
require_once ACCOUNTS_PATH.DS."models".DS."AccountAuthService.php";

/**
 * Require Account Model
 */
require_once ACCOUNTS_PATH.DS."models".DS."AccountModel.php";

/**
 * Path To Account Views
 */
define("ACCOUNTS_VIEW_PATH", ACCOUNTS_PATH.DS."views");

/**
 * Path To Admin Account Views
 */
define("ADMIN_ACCOUNTS_VIEW_PATH", ACCOUNTS_VIEW_PATH.DS."admin");

/**
 * URLs to admin manage account pages
 */
define("MANAGE_ACCOUNTS_LINK", "./accounts/manage");
define("MANAGE_ALL_ACCOUNTS", "./accounts/manage");
define("MANAGE_MUTED", "./accounts/manage/muted");


//-----------------------------------------------------
// OAuth Config
//-----------------------------------------------------

/**
 * Path To Oauth Module Folder
 */
define("OAUTH_PATH", __DIR__);

/**
 * Path To Oauth Module Folder
 */
define("OAUTH_RESOURCE_PATH", OAUTH_PATH.DS."resources");

/**
 * Use Google OAuth services
 */
define("USE_GOOGLE_OAUTH", 1);

/**
 * Use Facebook OAuth services
 */
define("USE_FACEBOOK_OAUTH", 0);

?>

<?php include_once LAYOUTS_PATH . "/header.php" ?>

<?php if(isset($pageContent['message'])) {
  echo $pageContent['message'];
  return false;
}
?>

<div class="container">

  <div class="row margin-bottom-lg">
    <div class="col-sm-12 justify-content-center text-center">
      <h2>Account Settings</h2>
    </div>
  </div>

  <div class="row margin-bottom-md">
    <div class="col-sm-12">
      <h4><?php echo $pageContent['userAccountData']->getAccountName();?></h4>
    </div>
  </div>

  <div class="row margin-bottom-sm">
    <div class="col-sm-12">
      <p><b>Joined:</b><br> <?php echo $pageContent['userAccountData']->getCreatedAt('F jS, Y \a\t g:i A'); ?></p>
    </div>
  </div>

  <div class="row margin-bottom-sm">
    <div class="col-sm-12">
      <p><b>Last seen:</b><br> <?php echo $pageContent['userAccountData']->getLastLogin('F jS, Y \a\t g:i A'); ?></p>
    </div>
  </div>

  <?php
    // If OAuth Account do not provide account settings modificaitons
    if(null !== $pageContent['userAccountData']->getOAuthProvider() && $pageContent['userAccountData']->getOAuthProvider() !== "") { ?>
        <div class="row margin-bottom-md">
          <div class="col-sm-12">
            <p><i>Logged in through <?php echo ucfirst($pageContent['userAccountData']->getOAuthProvider()) ?></i></p>
          </div>
        </div>

    <?php } else {
      require_once( ACCOUNTS_VIEW_PATH.DS."partials".DS."localAccountSettingsForm.part.php" );
    }

    if($pageContent['session']['permission'] === 'c') {

      require_once( ACCOUNTS_VIEW_PATH. DS."partials".DS."accountBioForm.part.php" );
    }

   ?>

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

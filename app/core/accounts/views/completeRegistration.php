<?php // include_once LAYOUTS_PATH . "/header.php" ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- This tag MUST be included on every page -->
    <base href="<?php echo BASE_URL ?>"/>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="resources/images/favicon.ico">

    <!-- Use the set page title, or default -->
    <?php
      if($pageContent['pageTitle']) {
        echo "<title>".$pageContent['pageTitle']." | BlogIt!</title>";
      } else {
        echo "<title>BlogIt!</title>";
      }
    ?>

    <!-- Bootstrap core CSS -->
    <link href="resources/bootstrap-4.0.0-beta-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Default Style -->
    <link href="css/defaultstyle.css" rel="stylesheet">


    <script src="resources/jquery-3.2.1.min.js"></script>
    <script src="resources/popper.js"></script>

    <!-- <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script> -->
    <script src="resources/bootstrap-4.0.0-beta-dist/js/bootstrap.min.js"></script>

    <!-- Add Handlebars Template Source Files-->
    <script type="text/javascript" src="core/js/libraries/handlebars-v4.0.5.js"></script>
		<script type="text/javascript" src="core/js/libraries/handlebars-select-helper.js"></script>
    <script src="core/js/templates/templates.js"></script>

    <!-- Add Main JS Library/Namespace -->
    <script type="text/javascript" src="core/js/libraries/Utils.js"></script>

    <!-- Add Default Pagination Libary -->
    <script type="text/javascript" src="core/js/libraries/Pagination.js"></script>

    <!-- Core javascript libraries -->
    <script type="text/javascript" src="core/js/libraries/Account.js"></script>
    <script type="text/javascript" src="core/js/libraries/Post.js"></script>
    <script type="text/javascript" src="core/js/libraries/Admin.js"></script>


    <!-- Include any required resources as defined in the controller -->
    <?php
      if(isset($pageContent['pageResources']) && $pageContent['pageResources']) {
        foreach($pageContent['pageResources'] as $pageResource) {

          if($pageResource['type'] == 'css') {
            echo '<link href="'.$pageResource['path'].'" rel="stylesheet">';
          }
          if($pageResource['type'] == 'js') {
            echo '<script type="text/javascript" src="'.$pageResource['path'].'"></script>';
          }
        }
      }
    ?>
  </head>

  <div class="blog-masthead">
    <div class="container">
      <?php
        $controller = $action = "";
        if(isset($_GET['controller'])) {
          $controller = $_GET['controller'];
        }
        if(isset($_GET['action'])) {
          $action = $_GET['action'];
        }
      ?>
      <nav class="nav blog-nav">

        <a class="nav-link ml-auto" href="./accounts/logout">Logout</a>

      </nav>
    </div>
  </div>

  <div class="blog-header">
    <div class="container">
      <h1 class="blog-title">Blog It!</h1>
      <p class="lead blog-description">The simple and easy blogging platform.</p>
    </div>
  </div>

  <body>

<div class="container">
  <div class="row">
    <h2>Complete Account Registration</h2>
  </div>

  <hr>

  <div class="row">

    <form id="completeRegistrationForm" method="POST">

    <div class="form-group" id="usernameFieldFormGroup">
      <label for="usernameField">
        Pick Your Username
      </label>
      <input type="text" class="form-control" id="usernameField" name="usernameField" placeholder="Username" maxlength="25" onkeypress="return BlogIt.Lib.disallowInputChars(event)" required>
      <small id="usernameErrorHolder" class="text-danger">
      </small>
    </div>

      <button type="button" id="completeRegistrationButton" class="btn btn-primary" onclick="BlogIt.Account.completeRegistration()">
        Finish Account Creation
      </button>
      <div id="alerts-holder">
        <!-- Alert holder -->
      </div>
    </form>
  </div>
</div>


</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

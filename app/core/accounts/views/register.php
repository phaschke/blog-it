<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">
  <div class="row margin-bottom-md">
    <h2>New User</h2>
  </div>
  <div class="row">
    <form id="registerForm" method="POST">

      <div class="form-group" id="emailFieldFormGroup">
        <label for="emailField">
          Email Address
        </label>
        <input type="email" class="form-control" id="emailField" name="emailField" placeholder="Email" maxLength="255" required>
        <small id="emailErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="form-group" id="usernameFieldFormGroup">
        <label for="usernameField">
          Username
        </label>
        <input type="text" class="form-control" id="usernameField" name="usernameField" placeholder="Username" maxLength="25" onkeypress="return BlogIt.Lib.disallowInputChars(event)" required>
        <small id="usernameErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="form-group" id="passwordFieldFormGroup">
        <label for="passwordField">
          Password
        </label>
        <input type="password" class="form-control" id="passwordField" name="passwordField" placeholder="Password" maxlength="512" required>
        <small id="passwordErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="form-group" id="retypePasswordFieldFormGroup">
        <label for="retypePasswordField">
          Retype Password
        </label>
        <input type="password" class="form-control" id="retypePasswordField" name="retypePasswordField" placeholder="Retype Password" maxlength="512" required>
        <small id="retypePasswordErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="pull-right margin-bottom-lg">
        <button type="button" id="registerButton" class="btn btn-primary" onclick="BlogIt.Account.register()">
          Create Account
        </button>

      </div>

      <div id="alerts-holder-bottom">
          <!-- Alert holder -->
      </div>
    </form>
  </div>

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

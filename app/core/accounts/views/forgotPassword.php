<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">
  <div class="row">
    <h2>Forgot Password</h2>
  </div>
  <div class="row">
    <p>Enter the email associated with your account, a reset email will be sent.</p>
  </div>
  <div class="row">
    <form id="forgotPasswordForm" method="POST">

      <div class="form-group" id="emailFieldFormGroup">

        <input type="email" class="form-control" id="emailField" name="emailField" placeholder="Email" maxLength="255" required>
        <small id="emailErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="pull-right">
        <button type="button" id="forgotPasswordButton" class="btn btn-primary" onclick="BlogIt.Account.sendPasswordReset()">
          Retrieve Your Password
        </button>
        <div id="alerts-holder">
          <!-- Alert holder -->
        </div>
      </div>
    </form>
  </div>

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

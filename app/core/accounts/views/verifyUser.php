<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">
  <div class="row">
    <h2>Verify Account</h2>
  </div>

  <?php if(isset($pageContent['message'])) { ?>
  <div class="row">
    <p><?php echo $pageContent['message'] ?></p>
  </div>
  <?php } ?>

  <?php  if(isset($pageContent['showReset']) && $pageContent['showReset'] == true) { ?>
  <div class="row">
    <form id="resendVerificationForm">
      <div class="form-group" id="emailFieldFormGroup">
        <label for="emailField">
          Email Address
        </label>
        <input type="email" class="form-control" id="emailField" name="emailField" placeholder="Email" required>
        <small id="emailErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="pull-right">
        <button type="button" id="resendVerificationButton" class="btn btn-primary" onclick="BlogIt.Account.resendVerificaton()">
          Resend Verification Email
        </button>
      </div>
    </form>
  </div>
  <div class="row">
    <div id="alerts-holder">
    </div>
  </div>
  <?php } ?>

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

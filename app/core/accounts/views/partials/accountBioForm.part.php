<hr>

<h4><i class="fas fa-edit"></i> Edit Account Bio</h4>
<br>

<form id="saveBioForm" method="POST">

  <div class="form-group">
    <textarea class="form-control" id="accountBio" name="accountBio" placeholder="Account Bio" maxlength="512" rows="3"><?php echo $pageContent['userAccountData']->getAccountBio(); ?></textarea>
    <small id="accountBioErrorHolder" class="text-danger"></small>
  </div>
  <div class="pull-right margin-bottom-lg">
    <button type="button" id="saveAccountBioButton" class="btn btn-primary" onclick="BlogIt.Account.saveAccountBio()">
      Save Account Bio
    </button>
    <div class="margin-top-md" id="account-bio-alerts-holder">
      <!-- Alert holder -->
    </div>
  </div>
</form>

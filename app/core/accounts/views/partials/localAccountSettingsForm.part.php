
  <hr>

  <h4>Change Your Password</h4>
  <br>

  <form id="changePasswordForm" method="POST">

    <div class="form-group" id="passwordFieldFormGroup">
      <label for="existingPasswordField">
        Exisiting Password
      </label>
      <input type="password" class="form-control" id="existingPasswordField" name="existingPasswordField" placeholder="Password" maxlength="512" required>
      <small id="existingPasswordErrorHolder" class="text-danger">
      </small>
    </div>

    <div class="form-group" id="passwordFieldFormGroup">
      <label for="passwordField">
        New Password
      </label>
      <input type="password" class="form-control" id="passwordField" name="passwordField" placeholder="Password" maxlength="512" required>
      <small id="passwordErrorHolder" class="text-danger">
      </small>
    </div>

    <div class="form-group" id="retypePasswordFieldFormGroup">
      <label for="retypePasswordField">
        Retype Password
      </label>
      <input type="password" class="form-control" id="retypePasswordField" name="retypePasswordField" placeholder="Retype Password" maxlength="512" required>
      <small id="retypePasswordErrorHolder" class="text-danger">
      </small>
    </div>

    <div class="pull-right">
      <button type="button" id="changePasswordButton" class="btn btn-primary" onclick="BlogIt.Account.changePassword()">
        Change Password
      </button>
      <div id="password-alerts-holder">
        <!-- Alert holder -->
      </div>
    </div>
  </form>

<?php
  include_once LAYOUTS_PATH . DS . "header.php";

  if(isset($pageContent['message'])){
    include_once LAYOUTS_PATH . DS . "errorMessage.part.php";
    return false;
  }
?>

<div class="container">

  <div class="row">
    <div class="col-sm-12 justify-content-center text-center">
      <h2>User <?php echo $pageContent['userAccountData']->getAccountName();?></h2>
    </div>
  </div>

  <div class="row margin-bottom-sm">
    <div class="col-sm-12">
      <p><b>Joined:</b><br> <?php echo $pageContent['userAccountData']->getCreatedAt('F jS, Y'); ?></p>
    </div>
  </div>

  <div class="row margin-bottom-sm">
    <div class="col-sm-12">
      <p><b>Last seen:</b><br> <?php echo $pageContent['userAccountData']->getLastLogin('F jS, Y'); ?></p>
    </div>
  </div>

  <?php
    if($pageContent['userAccountData']->getAccountBio() !== "") { ?>
      <hr><div class="row margin-bottom-lg">
        <div class="col-sm-12">
          <p><?php echo $pageContent['userAccountData']->getAccountBio();?></p>
        </div>
      </div>

    <?php } ?>

</div>


<?php include_once LAYOUTS_PATH . "/footer.php" ?>

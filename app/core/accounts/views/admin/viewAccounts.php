<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">

  <div class="row">
    <div class="col-sm-12">
      <a class="btn btn-outline-primary margin-bottom-md" href="./admin/panel">&#x2190; Back to Panel</a>
    </div>
  </div>

  <?php include_once ADMIN_ACCOUNTS_VIEW_PATH.DS."partials".DS."manageNav.part.php"; ?>

  <div class="margin-bottom-md" id="AccountEntries">
    <p>Loading...</p>
  </div>

  <div class="margin-bottom-lg" id="pagination"></div>

  <script>
    BlogIt.Account.initializeAccountsPagination(false, "BlogIt.Account.manageAccounts", "AccountEntries", <?php echo $pageContent['isMuted']; ?>);
  </script>


</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

<!-- Used by BlogIt.Account.initializeAccountsPagination and BlogIt.Account.manageAccounts. -->
<script id="account_manage_list_template" type="text/x-handlebars-template">
  {{#each accounts}}
  <div class="card">
    <div class="card-block">
      <h4 class="card-title">{{account_name}}
        {{#if muted_at}}
        <img src="resources/images/muteIcon-32.png" alt="[Muted]">
        {{/if}}
      </h4>
      <h6 class="card-subtitle mb-2 text-muted">Created on: {{account_created_at}}</h6>
      <h6 class="card-subtitle mb-2 text-muted">Last Login: {{account_last_login}}</h6>

      <a class="btn btn-primary" href="./accounts/manageuser/{{account_id}}">Manage User</a>
    </div>
  </div>
{{/each}}
</script>

<br>
<div class="row">

  <form id="completeRegistrationForm" method="POST">

    <div class="form-group">
      <label for="muteReason">Mute Reason</label>
      <textarea class="form-control" id="muteReason" name="muteReason" placeholder="Reason for Mute" maxLength="512" rows="3"></textarea>
      <small id="muteReasonErrorHolder" class="text-danger"></small>
    </div>

    <button type="button" id="muteAccountButton" class="btn btn-primary margin-bottom-lg" onclick="BlogIt.Account.muteAccount()">
      Mute Account
    </button>
    <div id="alerts-holder">
      <!-- Alert holder -->
    </div>

    </form>

  </div>

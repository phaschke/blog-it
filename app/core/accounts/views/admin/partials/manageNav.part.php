<div class="row justify-content-center margin-bottom-md">
  <h2>Manage <?php echo ucfirst($pageContent['status']) ?> Accounts</h2>
</div>

<div class="row margin-bottom-md justify-content-center">
    <a class="btn margin-right-sm <?php echo ($pageContent['status'] == 'all') ? 'btn-primary' : 'btn-outline-primary' ?>"
      href="<?php echo MANAGE_ALL_ACCOUNTS ?>"> All Accounts</a>

    <a class="btn <?php echo ($pageContent['status'] == 'muted') ? 'btn-primary' : 'btn-outline-primary' ?>"
      href="<?php echo MANAGE_MUTED ?>"> Muted Accounts
    </a>
</div>

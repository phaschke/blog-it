<br>
<div class="row margin-bottom-md">
  <p>
    <b>Muted On:</b> <?php echo $pageContent['userAccountData']->getMutedAt('F jS, Y \a\t H:i:s'); ?>
    <br>
    <b>Mute Reason:</b> <?php echo $pageContent['userAccountData']->getMutedReason(); ?>
  </p>
</div>

<div class="row">
  <button type="button" id="unmuteAccountButton" class="btn btn-primary margin-bottom-md" onclick="BlogIt.Account.unmuteAccount()">
    Unmute Account
  </button>
</div>

<div class="row">
  <div id="alerts-holder">
    <!-- Alert holder -->
  </div>
</div>

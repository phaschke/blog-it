<div class="card">
  <div class="card-header">
    <i class="fas fa-users"></i> Manage Users
  </div>
  <div class="card-block">
    <a class="btn btn-primary" href="./accounts/manage">Manage Users</a>
  </div>
</div>

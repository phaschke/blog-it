<?php
  include_once LAYOUTS_PATH . DS . "header.php";

  if(isset($pageContent['message'])){
    include_once LAYOUTS_PATH . DS . "errorMessage.part.php";
    return false;
  }
?>

<div class="container">

  <div class="row">
    <a class="btn btn-outline-primary" href="./accounts/manage">&#x2190; Back to Manage Accounts</a>
  </div>

  <div class="row justify-content-md-center margin-bottom-md">
    <h2>Manage <?php echo $pageContent['userAccountData']->getAccountName();?></h2>
  </div>

  <div class="row margin-bottom-sm">
    <p><b>Joined:</b> <br><?php echo $pageContent['userAccountData']->getCreatedAt('F jS, Y \a\t g:i A'); ?></p>
  </div>

  <div class="row margin-bottom-sm">
    <p><b>Last Seen:</b> <br><?php echo $pageContent['userAccountData']->getLastLogin('F jS, Y \a\t g:i A'); ?></p>
  </div>

  <div class="row margin-bottom-sm">
    <p><b>Account Email:</b> <br><?php echo $pageContent['userAccountData']->getAccountEmail(); ?></p>
  </div>

  <div class="row margin-bottom-sm">
    <p><b>Account Type:</b> <br><?php echo $pageContent['accountType']; ?></p>
  </div>

  <?php
      if(!empty($pageContent['userAccountData']->getAccountBio())) { ?>
        <hr>
        <div class="row">
          <p>
            <b>Bio Text:</b>
          <br>
            <?php echo $pageContent['userAccountData']->getAccountBio(); ?>
          </p>
        </div>
        <hr>
    <?php }

      if(!empty($pageContent['userAccountData']->getMutedAt())) {
        include_once ADMIN_ACCOUNTS_VIEW_PATH.DS.'partials'.DS.'unmuteAccountButton.part.php';
      } else {
        include_once ADMIN_ACCOUNTS_VIEW_PATH.DS.'partials'.DS.'muteAccountForm.part.php';
      }

  ?>

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

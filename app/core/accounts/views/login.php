<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">
  <div class="row margin-bottom-lg">
    <h2>Login</h2>
  </div>
  <?php if(isset($pageContent['message'])) {
    echo'<div class="row">
      <p>'.$pageContent["message"].'</p>
    </div>';
  } ?>
  <div class="row margin-bottom-sm">
    <h4>Google Login:</h4>
  </div>
  <div class="row">
    <?php include_once OAUTH_RESOURCE_PATH.DS."googleLoginButton.php" ?>
  </div>

  <hr>
  <div class="row margin-bottom-sm">
    <h4>Local Login:</h4>
  </div>
  <div class="row">

    <form id="loginForm" method="POST">

      <div class="form-group" id="emailFieldFormGroup">
        <label for="emailField">
          Email Address
        </label>
        <input type="email" class="form-control" id="emailField" name="emailField" placeholder="Email" maxLength="255" required>
        <small id="emailErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="form-group" id="passwordFieldFormGroup">
        <label for="passwordField">
          Password
        </label>
        <input type="password" class="form-control" id="passwordField" name="passwordField" placeholder="Password" maxlength="512" required>
        <small id="passwordErrorHolder" class="text-danger">
        </small>
      </div>

      <div class="margin-bottom-md">
        <p>or <a href="./accounts/register">Register</a></p>
      </div>

      <div class="pull-right">
          <button type="button" class="btn btn-default" onclick="BlogIt.Utils.redirect('./accounts/forgotpassword')">
          Forgot Password
          </button>

        <button type="button" id="loginButton" class="btn btn-primary" onclick="BlogIt.Account.login()">
          Login
        </button>
        <div class="margin-top-sm" id="alerts-holder">
          <!-- Alert holder -->
        </div>
      </div>
    </form>
  </div>


</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

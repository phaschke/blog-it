<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">
  <div class="row">
    <h2>Reset Password</h2>
  </div>

  <?php if(isset($pageContent['message'])) { ?>
    <div class="row">
      <p><?php echo $pageContent['message'] ?></p>
    </div>
  <?php } ?>

  <?php  if(isset($pageContent['showReset']) && $pageContent['showReset'] == true) { ?>
    <div class="row">
      <form id="resetPasswordForm" method="POST">

        <div class="form-group" id="passwordFieldFormGroup">
          <label for="passwordField">
            New Password
          </label>
          <input type="password" class="form-control" id="passwordField" name="passwordField" placeholder="Password" maxlength="512" required>
          <small id="passwordErrorHolder" class="text-danger">
          </small>
        </div>

        <div class="form-group" id="retypePasswordFieldFormGroup">
          <label for="retypePasswordField">
            Retype Password
          </label>
          <input type="password" class="form-control" id="retypePasswordField" name="retypePasswordField" placeholder="Retype Password" maxlength="512" required>
          <small id="retypePasswordErrorHolder" class="text-danger">
          </small>
        </div>

        <div class="pull-right">
          <button type="button" id="resetPasswordButton" class="btn btn-primary" onclick="BlogIt.Account.resetPassword(<?php echo "'".$pageContent['email']."', '".$pageContent['token']."'" ?>)">
            Reset Password
          </button>
          <div id="alerts-holder">
            <!-- Alert holder -->
          </div>
        </div>
      </form>
    </div>
  <?php } ?>

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

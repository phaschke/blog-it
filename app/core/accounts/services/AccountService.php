<?php
// Include parent class
require_once SERVICES_PARENT;
/**
 * Category Service Class
 */
 Class AccountService extends Service {

   private $accountModel;
   private $accountAuthService;

   public function __construct() {

     $session = new Session();

     $editLockManager = new EditLockManager($session);

     $DBConnection = new DBConnection();

     $accountRepository = new AccountRepository($DBConnection);
     $this->accountAuthService = new AccountAuthService($accountRepository, $session);
     $this->accountModel = new AccountModel($accountRepository, $editLockManager);

   }

   /**
    * Authenticate a local account given an email and a password
    *
    * PUBLIC
    */
   public function login($parameters) {

     $requiredParameters = array('email', 'password');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $email = $parameters['email'];
       $password = $parameters['password'];

       $result = $this->accountAuthService->authenticateLocalLogin($email, $password);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

   /**
    * Register a user account
    *
    * PUBLIC
    */
   public function register($parameters) {

     $requiredParameters = array('email', 'username', 'password', 'retypePassword');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $password = $parameters['password'];
       $retypePassword = $parameters['retypePassword'];

       $accountData = array("account_email" => $parameters['email'],
                            "account_name" => $parameters['username'],
                           );
       $account = new Account($accountData);

       $result = $this->accountAuthService->addUser($account, $password, $retypePassword);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

   /**
    * Complete account registration
    *
    * USER
    */
   public function completeRegistration($parameters) {

     $requiredParameters = array('username');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $accountData = array("account_name" => $parameters['username']);
       $account = new Account($accountData);

       $result = $this->accountAuthService->completeRegistration($account);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

   /**
    * Reset and send account verification
    *
    * PUBLIC
    */
   public function resendVerificaton($parameters) {

     $requiredParameters = array('email');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->accountAuthService->setVerificationToken($parameters['email']);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

   /**
    * Send password reset email
    *
    * PUBLIC
    */
   public function sendPasswordReset($parameters) {

     $requiredParameters = array('email');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->accountAuthService->sendPasswordReset($parameters['email']);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

   /**
    * Reset a users password
    *
    * PUBLIC
    */
   public function resetPassword($parameters) {

     $requiredParameters = array('email', 'token', 'password', 'retypePassword');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $email = $parameters['email'];
       $lostPasswordToken = $parameters['token'];
       $password = $parameters['password'];
       $retypePassword = $parameters['retypePassword'];

       $result = $this->accountAuthService->resetPassword($email, $lostPasswordToken, $password, $retypePassword);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

   /**
    * Get all accounts
    *
    * PRIVILLEGED
    */
   public function getMany($parameters) {

     $requiredParameters = array('onlyIsMuted', 'numRecords', 'offset');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $onlyIsMuted = $parameters['onlyIsMuted'];
       $numRecords = intval($parameters['numRecords']);
       $offset = intval($parameters['offset']);

       $result = $this->accountModel->getAccounts($numRecords, $offset, $onlyIsMuted);
       if($this->checkError($result)) {

         if($result['totalCount'] <= 0) {
           $result['message'] = getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_not_found');

         } else {
           $accountData = array();
           foreach ($result['accounts'] as $account) {
             $accountData[] = $account->returnAsArray("F jS, Y");
           }

           $result['accounts'] = $accountData;
         }

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result ));
         return true;
       }
     }
   }

   /**
    * Mute an account using edit lock manager.
    *
    * PRIVILLEGED
    */
   public function muteAccount($parameters) {

     $requiredParameters = array('reason');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->accountModel->muteAccount($parameters['reason']);
       if($this->checkError($result)) {
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Unmute an account using edit lock manager.
    *
    * PRIVILLEGED
    */
   public function unmuteAccount() {

     $result = $this->accountModel->unmuteAccount();
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Change loged in users account bio
    *
    * USER
    */
   public function saveAccountBio($parameters) {

     $requiredParameters = array('ACCOUNT_ID', 'accountBio');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->accountModel->updateAccountBio($parameters['ACCOUNT_ID'], $parameters['accountBio']);
       if($this->checkError($result)) {
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Update an account's password.
    *
    * USER
    */
   public function changePassword($parameters) {

     $requiredParameters = array('ACCOUNT_ID', 'existingPassword', 'newPassword', 'retypeNewPassword');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->accountAuthService->verifyAndChangePassword($parameters['ACCOUNT_ID'], $parameters['existingPassword'], $parameters['newPassword'],$parameters['retypeNewPassword']);
       if($this->checkError($result)) {
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Update an account's email address.
    *
    * USER
    */
   public function changeEmail() {

     echo json_encode(array('success_flag' => 0,
       'error_message' => "This action is not yet implemented."));
     return true;

   }

}

?>

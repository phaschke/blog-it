<?php

class CompleteRegistrationController {

  private $session;

  public function __construct() {

    $this->session = new Session();

    if($this->session->isSessionStarted()){

      if((!isset($_SESSION['USERNAME']) || (!empty($_SESSION['USERNAME'])))) {
        redirect(BASE_URL.HOME_PAGE_URL);
      }
    } else {
      redirect(BASE_URL.HOME_PAGE_URL);
    }

  }

  /*
   *
   */
  public function complete() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Complete Account";

    require_once( ACCOUNTS_VIEW_PATH . "/completeRegistration.php" );
  }

}

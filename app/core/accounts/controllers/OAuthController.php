account<?php

class OAuthController extends Controller {

  private $accountAuthService;

  public function __construct() {

    parent::__construct();

    $session = new Session();

    $DBConnection = new DBConnection();
    $accountRepository = new AccountRepository($DBConnection);
    $this->accountAuthService = new AccountAuthService($accountRepository, $session);

  }

  /**
   *
   */
  public function google() {
    require_once(OAUTH_RESOURCE_PATH.DS."googleConfig.php");

    if(isset($_GET['code'])) {
  		$gClient->authenticate($_GET['code']);
      $token = $gClient->getAccessToken();
  		$gClient->setAccessToken($token);


    	$payload = $gClient->verifyIdToken();
    	if ($payload) {
		    	$oAuthUid = $payload['sub'];
					//$oAuthProvider = $payload['iss'];
          $oAuthProvider = "google";
					$accountEmail = $payload['email'];
					$accountVerified = $payload['email_verified'];
					$accountName = $payload['name'];

          $accountData = array('account_email' => $accountEmail,
                            'account_real_name' => $accountName,
                            'account_oauth_provider' => $oAuthProvider,
                            'account_oauth_id' => $oAuthUid,
                            'account_verified' => $accountVerified,
                            );
          $account = new Account($accountData);

          $result = $this->accountAuthService->authenticateOAuthLogin($account, $token);
          if(isset($result['status']) && $result['status'] == 0) {
            // Error logging in
            // Log console errors
            if(LOG_ERRORS && isset($result['consoleError'])) {
              error_log($result['consoleError']);
            }
            // Redirect to login page with error
            ob_start();
      			header('Location: '.BASE_URL.LOGIN_PAGE_URL.'&message=Error logging in, please try again.');
      	   	ob_end_flush();
            die();
          }

          // Login sucessful, redirect to home page
          header('Location:'.BASE_URL.HOME_PAGE_URL);
          exit();

  		} else {
  			echo "Error: could not add account, Invalid ID token.";
  			return false;
    			// Invalid ID token
  		}
  	} else {
      // Error logging in
      // Log console errors
      $error = "Error in OAuthController, could not get google OAuth code.";
      if(isset($_GET['error'])) {
        $error = $_GET['error'];
      }
      if(LOG_ERRORS) {
        error_log($error);
      }
      // Redirect to login page with error
  		ob_start();
			//header('Location: '.BASE_URL.LOGIN_PAGE.'&message=Error logging in, please try again.');
      header('Location: '.BASE_URL.LOGIN_PAGE_URL.'&message=Error');
	   	ob_end_flush();
      die();
  	}
  }


  /**
   *
   */
  public function facebook() {
    echo "HERE";

  }



}




?>

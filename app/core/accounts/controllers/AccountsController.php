<?php

class AccountsController extends Controller {

  private $accountAuthService;
  private $accountModel;

  public function __construct() {

    parent::__construct();

    $session = new Session();
    $DBConnection = new DBConnection();

    $editLockManagerRepository = new EditLockManagerRepository($DBConnection);
    $editLockManager = new EditLockManager($session, $editLockManagerRepository);

    $accountRepository = new AccountRepository($DBConnection);

    $this->accountModel = new AccountModel($accountRepository, $editLockManager);
    $this->accountAuthService = new AccountAuthService($accountRepository, $session);

  }

  /**
   * Check if the page can be accessed based on the status of a user's session.
   *
   * @param string redirectURL
   */
  private function checkPageAccess($redirectURL) {

    if(isset($this->session['status'])) {
      redirect($redirectURL);
    }
    return true;
  }

  /**
   *
   */
  public function user() {

    $pageContent = array();

    if(isset($_GET["status"]) && $_GET["status"]) {

      $result = $this->accountModel->getAccountByUsername($_GET["status"]);
      if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

        if($result['userError']) {
          $pageContent['message'] = $result['userError'];
          $pageContent['pageTitle'] = "Account Not Found";
        }
        if(LOG_ERRORS && isset($result['consoleError'])) {
          error_log($result['consoleError']);
        }
      } else {
        $pageContent['userAccountData'] = $result;
        $pageContent['pageTitle'] = "View User";
      }

    } else if(isset($_GET["id"]) && $_GET["id"]) {
      $result = $this->accountModel->getAccount($_GET["id"]);
      if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

        if($result['userError']) {
          $pageContent['message'] = $result['userError'];
          $pageContent['pageTitle'] = "Account Not Found";
        }
        if(LOG_ERRORS && isset($result['consoleError'])) {
          error_log($result['consoleError']);
        }
      } else {
        $pageContent['userAccountData'] = $result;
        $pageContent['pageTitle'] = "View User";
      }

    } else {
      redirect(BASE_URL);
    }

    $pageContent['session'] = $this->session;


    require_once( ACCOUNTS_VIEW_PATH . "/user.php" );

  }

  /**
   *
   */
  public function settings() {

    $pageContent = array();

    $pageContent['pageTitle'] = "Manage Account";

    $pageContent['session'] = $this->session;


    // Get ID of logged in User
    $accountId = $this->session['accountId'];

    $pageContent['userAccountData'] = $this->accountModel->getAccount($accountId);


    require_once( ACCOUNTS_VIEW_PATH . "/settings.php" );

  }

  /**
   *
   */
  public function login() {

    // TODO: Implement this logic within router
    $this->checkPageAccess(BASE_URL."pages/home");

    $pageContent = array();

    $pageContent['pageTitle'] = "Login";

    $pageContent['session'] = $this->session;

    if(isset($_GET['message'])) {
      $pageContent['message'] = $_GET['message'];
    }

    require_once( ACCOUNTS_VIEW_PATH . "/login.php" );

  }

  /*
   *
   */
  public function logout() {

    $this->accountAuthService->logout();

    $redirectURL = BASE_URL.HOME_PAGE_URL;

    if(isset($_GET['redirect'])) {
      $redirectURL = $_GET['redirect'];
    }

    redirect($redirectURL);
  }

  /**
   *
   */
  public function register() {

    $this->checkPageAccess(BASE_URL."pages/home");

    $pageContent = array();

    $pageContent['pageTitle'] = "Register";
    $pageContent['session'] = $this->session;

    require_once( ACCOUNTS_VIEW_PATH . "/register.php" );

  }

  /**
   *
   */
  public function verify() {

    $this->checkPageAccess(BASE_URL."pages/home");

    $pageContent = array();

    $pageContent['pageTitle'] = "Verify Account";
    $pageContent['session'] = $this->session;

    $pageContent['message'] = null;
    $pageContent['showReset'] = null;

    if(isset($_GET["error"]) && $_GET["error"] == "notVerified") {
        $pageContent['message'] = "Error: Could not login, your account is not verified.<br>
          Check your email for the verification link or send a new email below.";
        $pageContent['showReset'] = true;

    } elseif( !isset($_GET["email"]) || !$_GET["email"] || !isset($_GET["token"]) || !$_GET["token"]) {
      $pageContent['message'] = "Error: Could not verify account. Please try again or resend verification.";
      $pageContent['showReset'] = true;

    } else {
      $email = $_GET["email"];
      $token = $_GET["token"];

      $result = $this->accountAuthService->verifyUser($email, $token);
      if(isset($result['status']) && $result['status'] == 0) {
        // Default error
        $error = "Could not verify account. Please try again or resend verification.";
        if($result['userError']) {
          $error = $result['userError'];
        }
        if(LOG_ERRORS && isset($result['consoleError'])) {
          error_log($result['consoleError']);
        }
        $pageContent['message'] = $error;
        $pageContent['showReset'] = true;

      } else {
        redirect(BASE_URL."accounts/login&message=Successfully verified your account.");
        return true;
      }
    }

    require_once( ACCOUNTS_VIEW_PATH . "/verifyUser.php" );

  }

  /**
   *
   */
  public function forgotpassword() {

    $this->checkPageAccess(BASE_URL."pages/home");

    $pageContent = array();

    $pageContent['pageTitle'] = "Forgot Password";
    $pageContent['session'] = $this->session;

    require_once( ACCOUNTS_VIEW_PATH . "/forgotPassword.php" );

  }

  /**
   *
   */
  public function resetpassword() {

    $this->checkPageAccess(BASE_URL."pages/home");

    $pageContent = array();

    $pageContent['pageTitle'] = "Reset Password";
    $pageContent['session'] = $this->session;

    $pageContent['showReset'] = true;

    // Check for POST values email/verification key and set values
    if( !isset($_GET["email"]) || !$_GET["email"] || !isset($_GET["token"]) || !$_GET["token"] ) {
      $pageContent['message'] = "Error: Could not send password reset. Please try again.";
      $pageContent['showReset'] = false;
    }

    $email = $_GET["email"];
    $token = $_GET["token"];

    $pageContent['email'] = $email;
    $pageContent['token'] = $token;

    // Verify verification key/email if valid display reset form
    $result = $this->accountAuthService->validatePasswordReset($email, $token);
    if(isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error sending password reset, please try again.";
      if($result['userError']) {
        $pageContent['message'] = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['userError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['showReset'] = false;
    }

    require_once( ACCOUNTS_VIEW_PATH . "/resetPassword.php" );

  }

  /**
   *
   * Privilleged Access
   */
  public function manage() {

    $pageContent = array();

    // If no status is set, display ALL
    if (!isset($_GET["status"])) {

      $status = "all";
      $isMuted = "false";

    // Status is set, and status is published, saved, or hidden
    } elseif( isset($_GET["status"]) && (in_array($_GET["status"], array('muted'))) ) {

      $status = $_GET["status"];
      $isMuted = "true";

    // ERROR
    } else {
      redirect(BASE_URL);
    }

    $pageContent['status'] = $status;
    $pageContent['isMuted'] = $isMuted;

    $pageContent['pageTitle'] = "Manage Accounts";
    $pageContent['session'] = $this->session;

    require_once( ADMIN_ACCOUNTS_VIEW_PATH . "/viewAccounts.php" );

  }

  /**
   *
   * Privilleged Access
   */
  public function manageuser() {

    $pageContent = array();

    if(isset($_GET["status"]) && $_GET["status"]) {

      $result = $this->accountModel->getAccountByUsername($_GET["status"]);
      if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

        if($result['userError']) {
          $pageContent['message'] = $result['userError'];
          $pageContent['pageTitle'] = "Account Not Found";
        }
        if(LOG_ERRORS && isset($result['consoleError'])) {
          error_log($result['consoleError']);
        }
      } else {
        $pageContent['userAccountData'] = $result;
        $accountId = $pageContent['userAccountData']->getAccountId();
      }


    } else if(isset($_GET["id"]) && $_GET["id"]) {

      $accountId = $_GET["id"];
      $result = $this->accountModel->getAccount($_GET["id"]);
      if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

        if($result['userError']) {
          $pageContent['message'] = $result['userError'];
          $pageContent['pageTitle'] = "Account Not Found";
        }
        if(LOG_ERRORS && isset($result['consoleError'])) {
          error_log($result['consoleError']);
        }
      } else {
        $pageContent['userAccountData'] = $result;
        $accountId = $pageContent['userAccountData']->getAccountId();
      }


    } else {
      redirect("./manage");
    }

    // If account was found
    if(isset($pageContent['userAccountData'])) {
      if(null !== $pageContent['userAccountData']->getOAuthProvider() && $pageContent['userAccountData']->getOAuthProvider() !== "") {
        $pageContent['accountType'] = ucfirst($pageContent['userAccountData']->getOAuthProvider());
      } else {
        $pageContent['accountType'] = "Local Account";
      }

      // Lock the account witht he lock manager
      $result = $this->accountModel->manageAccount($accountId);
      if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

        if($result['userError']) {
          $pageContent['message'] = $result['userError'];
        }
        if(LOG_ERRORS && isset($result['consoleError'])) {
          error_log($result['consoleError']);
        }
      }

      $pageContent['pageTitle'] = "Manage Accounts";

    }
    $pageContent['session'] = $this->session;

    require_once( ADMIN_ACCOUNTS_VIEW_PATH . "/manageUser.php" );

  }
}

?>

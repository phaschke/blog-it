<?php
/**
 * Account error messages
 *
 *
 */

return [

  'delete_user_error'         => 'Could not delete account, please try again.',
  'login_error'               => 'Could not login, please try again.',
  'verify_password_error'     => 'Could not verify password, please try again.',
  'verify_user_error'         => 'Could not verify user account, please try again or resend a verification email.',
  'resend_verification_error' => 'Could not resend verification, please try again.',

  'account_register_error'    => 'Could not create account, please try again.',
  'account_register_success'  => 'Account created successfully, check your email for the verification link.',

  'account_set_token_success' => 'Sent verification successfully to :email, check your email for the verification link.',
  'account_password_reset_success' => 'Sent password reset successfully to :email, check your email for the reset link.',
  'account_password_reset'    => 'Password successfully reset.',

  'incorrect_password'        => 'Incorrect login credentials.',
  'throttle_attempts'         => 'You have too many failed login attempts, please wait and try again.',
  'password_update_error'     => 'Could not update password, please try again.',
  'send_password_reset_error' => 'Could not send password reset email, please try again.',
  'password_token_expired'    => 'Could not reset your password, the reset token has expired, please send another reset email.',

  'email_not_registered'      => ':email is not registered.',
  'email_already_exists'      => 'Email already exists, try to login instead.',
  'account_username_exists'   => 'That username is already taken, please try another.',
  'account_complete_registration_error' => 'Could not complete account registration, please try again',
  'not_verified'              => 'Your account is not verified.',

  'account_find_error'        => 'Error finding account(s).',
  'account_load_error'        => 'Error loading account(s)',
  'account_not_found'         => 'No account(s) found.',
  'account_manage_error'      => 'Could not edit the account, please try again.',
  'account_mute_error'        => 'Error muting user, please try again.',
  'account_unmute_error'      => 'Error unmuting user, please try again.',
  'account_password_change_error' => 'Error changing your password, please try again.',
  'account_password_change_success' => 'Your password was successfully changed.',
  'account_save_bio_error'    => 'Error saving you account bio, please try again.',
  'account_save_bio_success'  => 'Successfully saved your bio.',


];

?>

<?php

//Include Google client library
require_once __DIR__.'/google-api-php-client-2.2.0/vendor/autoload.php';

/*
 * Configuration and setup Google API
 */
 //Call Google API
 $gClient = new Google_Client();
 $gClient->setAuthConfigFile(__DIR__.'/googleClientSecret.json');
 $gClient->setApplicationName("Brew Legit To Quit");
 $gClient->setRedirectUri('http://localhost/oauth/google');
 $gClient->addScope('https://www.googleapis.com/auth/userinfo.profile');
 $gClient->addScope('https://www.googleapis.com/auth/userinfo.email');

?>

<?php

/**
 * Class containing user Authentication functions
 */
Class AccountAuthService {

  private $accountRepository;
  private $session;

  public function __construct(AccountRepository $accountRepository, Session $session) {

    $this->accountRepository = $accountRepository;
    $this->session = $session;
  }

  /**
   * Check if a user has privilege
   *
   * @param int userId
   *
   * @return boolean true if user has permission, false if not
   */
   public function checkPrivilege($permission) {

     // Make sure there is a session started
     if(!$this->session->isSessionStarted()){
       return false;
     }
     // Retrieve the users session
     $userId = $this->session->getSessionVariable("UUID");
     $sessionPermission = $this->session->getSessionVariable("PERMISSION");

     $dbPermission = $this->accountRepository->getUserPermission($userId);

     if($dbPermission === $permission && $sessionPermission === $permission) {
       return true;
     }
     return false;
   }

   /**
    * Function handle adding a new local user
    *
    * @param User user the user object to be added
    * @param string password the users password to be hashed and added to the database
    * @param string retypePassword the users password to be verified
    */
   public function addUser(Account $account, $password, $retypePassword) {

     // Validate inputs
     $result = verifyInput($account->getAccountEmail(), array("required", "email"));
     if(isset($result["isError"])) {
       return (array('status' => 0,
        'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'Email', ':error'=>$result["errorMessage"]]),
        'consoleError'=>__CLASS__.":".__FUNCTION__.": Error creating account, error with email field: ".$result["errorMessage"]));
     }
     // Validate inputs
     $result = verifyInput($account->getAccountName(), array("required", array("minLength", 4)));
     if(isset($result["isError"])) {
       return (array('status' => 0,
       'userError'=> getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'Username', ':error'=>$result["errorMessage"]]),
       'consoleError' => __CLASS__.":".__FUNCTION__.": Error creating account, error with username field: ".$result["errorMessage"]));
     }
     $result = verifyInput($password, array("required", array("minLength", 6)));
     if(isset($result["isError"])) {
       return (array('status' => 0,
       'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'Password', ':error'=>$result["errorMessage"]]),
       'consoleError' => __CLASS__.":".__FUNCTION__.": Error creating account, error with password field: ".$result["errorMessage"]));
     }
     $result = verifyInput($retypePassword, array("required",array("minLength", 6)));
     if(isset($result["isError"])) {
       return (array('status' => 0,
       'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'retypePassword', ':error'=>$result["errorMessage"]]),
       'consoleError' => __CLASS__.":".__FUNCTION__.": Error creating account, error with retype password field: ".$result["errorMessage"]));
     }

     if(!($password === $retypePassword)) {
       return (array('status' => 0,
       'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'password_match'),
       'consoleError' => __CLASS__.":".__FUNCTION__.": Error creating account, passwords do not match."));
     }

     // Check to make sure email is unique
     try {
       if($this->accountRepository->emailExists($account->getAccountEmail())) {
         return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'email_already_exists'),
         'consoleError' => "[USER {$account->getAccountEmail()}]: ".__CLASS__.":".__FUNCTION__.":  Error creating account, email already exists."));
       }
     } catch (Exception $e) {
       return (array('status' => 0,
        'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_register_error'),
        'consoleError' => "[USER {$account->getAccountEmail()}]: ".$e));
     }

     // Verify username is unique
     try {
       if($this->accountRepository->usernameExists($account->getAccountName())) {
         return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_username_exists'),
         'consoleError' => "[USER {$account->getAccountName()}]: ".__CLASS__.":".__FUNCTION__.":  Error creating account, username already exists."));
       }
     } catch (Exception $e) {
       return (array('status' => 0,
        'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_register_error'),
        'consoleError' => "[USER {$account->getAccountName()}]: ".$e));
     }

     // Get current time
     $account->setCreatedAt((new DateTime())->format('U'));

     // Encrypt password
     $hashedPassword = $this->hashToken($password);
     if(!$hashedPassword) {
       // Failure to hash password
       return (array('status' => 0,
                     'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_register_error'),
                     'consoleError' => __CLASS__.":".__FUNCTION__.": Error hashing password."));
     }

     // Generate verification token
     $verificationTokenForEmail = $this->generateToken();
     $verificationTokenForDatabase = $this->hashToken($verificationTokenForEmail);

     if(!$verificationTokenForDatabase) {
       // Failure to hash verification token
       return (array('status' => 0,
                     'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_register_error'),
                     'consoleError' => __CLASS__.":".__FUNCTION__.": Error hashing verification token."));
     }

     // call user repo insert
     $userId = $this->accountRepository->insert($account, $hashedPassword, $verificationTokenForDatabase);
     if(isset($result['status']) && $result['status'] == 0) {
       return (array('status' => 0,
        'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_register_error'),
        'consoleError' => "[USER {$account->getAccountEmail()}]: ".$result['consoleError']));
     }

     // send verificaton email
     require_once(EMAIL_TEMPLATES);

     // From Email Templates.php
     $emailContents = generateNewUserEmail($account->getAccountEmail(), $verificationTokenForEmail);

     $emailer = new Emailer();

     if(!$emailer->sendEmail($emailContents['senderEmailAddress'], $emailContents['senderEmailName'], $account->getAccountEmail(), $emailContents['subject'], $emailContents['body'], $emailContents['altBody'])) {
       $error = "";
       // Remove user if email fails to send
       try {
         $this->accountRepository->delete($userId);
       } catch(Exception $e) {
         $error = ", and Error removing partially added user password into table: ".$e;
       }

       return (array('status' => 0,
                     'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_register_error'),
                     'consoleError' => __CLASS__.":".__FUNCTION__.": Error sending verification email".$error));
    }

    return getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_register_success');
   }

   /**
    * Function to completing an OAuth account registration
    *
    * @param string $username the username to be set
    *
    * @return string|array success redirect url ir error array
    */
   public function completeRegistration(Account $account) {

     // Make sure there is a session started
     if(!$this->session->isSessionStarted()){
       return (array('status' => 0,
       'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_complete_registration_error'),
       'consoleError' => __CLASS__.":".__FUNCTION__.":  Error creating account, no session is started."));
     }

     $userId = $this->session->getSessionVariable("UUID");
     if(!$userId) {
       // Could not get userId
       return (array('status' => 0,
       'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_complete_registration_error'),
       'consoleError' => __CLASS__.":".__FUNCTION__.":  Error creating account, could not get session userId"));
     }
     $account->setAccountId($userId);

     $result = verifyInput($account->getAccountName(), array("required", array("minLength", 4)));
     if(isset($result["isError"])) {
       return (array('status' => 0,
       'userError'=> getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'Username', ':error'=>$result["errorMessage"]]),
       'consoleError' => "[USER {$account->getAccountId()}]: ". __CLASS__.":".__FUNCTION__.": Error creating account, error with username: ".$result["errorMessage"]));
     }

     // Verify username is unique
     try {
       if($this->accountRepository->usernameExists($account->getAccountName())) {
         return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_username_exists'),
         'consoleError' => "[USER {$account->getAccountId()}]: ".__CLASS__.":".__FUNCTION__.":  Error creating account, username already exists."));
       }
     } catch (Exception $e) {
       return (array('status' => 0,
        'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_complete_registration_error'),
        'consoleError' => "[USER {$account->getAccountId()}]: ".$e));
     }

     try {
       $this->accountRepository->updateAccount($account);
       $this->logout();
       $this->login($userId);
       //$this->session->setSessionVariable("USERNAME", $userData['user_name']);

     } catch (Exception $e) {
       return (array('status' => 0,
        'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_complete_registration_error'),
        'consoleError' => "[USER {$account->getAccountName()}]: ".$e));
     }

     return HOME_PAGE_URL;

   }

  /**
   * Function to handle removing a user
   *
   * @param int userId user's Id to be removed
   *
   * @return true|array
   */
   public function delete($userId) {

     try {
       $this->accountRepository->delete($userId);

     } catch (Exception $e) {
       return (array('status' => 0,
          'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'delete_user_error'),
          'consoleError'=>$e));
     }
   }

  /**
   * Function to authenticatethe details of a local account and call the login function
   *
   * @param string email
   * @param string password
   *
   * @return true|array
   */
  public function authenticateLocalLogin($email, $password) {

    // Validate inputs
    $result = verifyInput($email, array("required", "email"));
    if(isset($result["isError"])) {
      return (array('status' => 0,
         'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'Email', ':error'=>$result["errorMessage"]]),
         'consoleError' => "[USER {$email}]: ".__CLASS__.":".__FUNCTION__.": Error logging in account, error with email field: ".$result["errorMessage"]));
    }

    $result = verifyInput($password, array("required"));
    if(isset($result["isError"])) {
      return (array('status' => 0,
         'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'Password', ':error'=>$result["errorMessage"]]),
         'consoleError' => "[USER {$email}]: ".__CLASS__.":".__FUNCTION__.": Error logging in account, error with password field: ".$result["errorMessage"]));
    }

    // Check to make sure account exists
    try {
      if(!$this->accountRepository->emailExists($email)) {
        return (array('status' => 0,
           'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'email_not_registered', [':email'=>$email]),
           'consoleError' => "[USER {$email}]: ".__CLASS__.':'.__FUNCTION__.': Error logging in account, account in not registered.'));
      }

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'login_error'),
         'consoleError'=>$e));
    }

    // Get user ID from email
    try {
      $userId = $this->accountRepository->getUserId($email);

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'login_error'),
         'consoleError'=>$e));
    }

    // Check if user is verified
    try {
      if(!$this->accountRepository->isVerified($userId)) {
        return (array('status' => 0,
          'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'not_verified')." <a href='./accounts/verify&error=notVerified' click here</a> to resend the verification email.",
          'consoleError' => "[USER {$userId}]: ".__CLASS__.":".__FUNCTION__.": Error logging in account, user is not verified."));
      }
    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'login_error'),
         'consoleError'=>$e));
    }

    $result = $this->checkLoginAttempts($userId);
    if(isset($result['status']) && $result['status'] == 0) {

      return (array('status' => 0,
        'userError' => $result['userError'],
        'consoleError' => $result['consoleError']));
    }

    $result = $this->verifyPassword($userId, $password);
    if(isset($result['status']) && $result['status'] == 0) {

      return (array('status' => 0,
        'userError' => $result['userError'],
        'consoleError' => $result['consoleError']));
    }

    $result = $this->login($userId);
    if(isset($result['status']) && $result['status'] == 0) {

      return (array('status' => 0,
        'userError' => $result['userError'],
        'consoleError' => $result['consoleError']));
    }

    return HOME_PAGE_URL;
  }

  /**
   * Check if a oauth user has logged in before, if not create user and call login
   *
   * @param string oAuthProvider
   * @param string oAuthUid
   * @param string userEmail
   * @param string userName
   * @param string token
   *
   * @return boolean true on success, false on error/failure
   */
  public function authenticateOAuthLogin(Account $user, $token) {

    // Check if the user has logged in before
    try {
      $verify = $this->accountRepository->verifyOAuthUser($user);
      if($verify) {
        $user->setAccountId($verify['user_id']);
        // Account has already been added, update the user.
        $this->accountRepository->updateOAuthUser($user);
        error_log("OAuth user already created");

      } else {
        // User has not been added, add the user, and get the newly inserted user's ID
        $result = $this->accountRepository->addOAuthUser($user);
        if(isset($result['status']) && $result['status'] == 0) {
          return (array('status' => 0,
            'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'login_error'),
            'consoleError' => $result['consoleError']));
        }
        error_log("Added OAuth user");

        //result is AccountID
        $userId = $result;
      }
    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'login_error'),
         'consoleError'=>$e));
    }

    // Call login function
    $result = $this->login($user->getAccountId(), $user->getOAuthProvider(), $token);
    if(isset($result['status']) && $result['status'] == 0) {

      return (array('status' => 0,
        'userError' => $result['userError'],
        'consoleError' => $result['consoleError']));
    }
    error_log("Logged user in");

    return true;

  }

  /**
   * Function to start a session of an authenticated user and set session variables
   *
   * @param int userId
   * @param string OAuthProvider (Optional) The oauth provider of the account, if applicable
   */
  private function login($userId, $OAuthProvider=null, $token=null) {

    try {
      $userData = $this->accountRepository->getUserLoginData($userId);
      $this->accountRepository->updateLoginTime($userId);

    } catch (Exception $e) {
      return (array('status' => 0,
        'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'login_error'),
        'consoleError' => "[USER {$userId}]: ".$e));
    }

    //require_once(SESSION);
    //$session = new Session();

    // Start session
    $this->session->startSession();
    $this->session->setSessionVariable("UUID", $userId);
    $this->session->setSessionVariable("EMAIL", $userData['user_email']);
    $this->session->setSessionVariable("USERNAME", $userData['user_name']);
    $this->session->setSessionVariable("PERMISSION", $userData['user_permission']);

    if($OAuthProvider) {
      $this->session->setSessionVariable("OAUTHPROVIDER", $OAuthProvider);
      $this->session->setSessionVariable("TOKEN", $token);
    }

    return true;
  }

  /**
   * TODO
   */
  public function logout() {
    try {
      //$session = new Session();
      if(!$this->session->isSessionStarted()) {
        $this->session->startSession();
      }

      //If user is logged in from a API login service.
  		if(isset($_SESSION['OAUTHPROVIDER'])) {
  			include_once(OAUTH_RESOURCE_PATH.'/googleConfig.php');

  			unset($_SESSION['OAUTHPROVIDER']);
  			unset($_SESSION['TOKEN']);

  			$gClient->revokeToken();
  		}

  		unset($_SESSION['UUID']);
  		unset($_SESSION['EMAIL']);
  		unset($_SESSION['USERNAME']);
  		unset($_SESSION['PERMISSION']);

      $this->session->destorySession();

      return true;

    } catch (Exception $e) {

      error_log("Error logging out.");
      return false;
    }

  }

  /**
   * Verify a given password to a given user id
   *
   * @param int userId the user id of the user
   * @param string password the given password
   * @return true|array true on success, array on error
   */
  public function verifyPassword($userId, $password) {

    // Get the user's password
    try {
      $hashedPassword = $this->accountRepository->getUserPassword($userId);

    } catch (Exception $e) {

      return (array('status' => 0,
                    'userError' =>  getI18nMessage(ACCOUNT_I18_MESSAGES, 'verify_password_error'),
                    'consoleError' => $e));
    }

    // Compare given password to password token from DB
    if(!$this->compareHash($password, $hashedPassword)) {

      // Add failed login attempt
      try {
        $this->accountRepository->addLoginAttempt($userId);

      } catch (Exception $e) {
        // TODO: Add error if logger class is created.
        // Do nothing if error adding login attempt
      }

      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'incorrect_password'),
                    'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.": Given password and password hash from the database do not match."));
    }

    return true;
  }

  /**
   * Verify a user given a user email and token
   *
   * @param string email
   * @param string token
   * @return true|array
   */
  public function verifyUser($email, $token) {

    // Get user's ID from email
    try {
      $userId = $this->accountRepository->getUserId($email);

    } catch (Exception $e) {
      return (array('status' => 0,
                    'userError '=> getI18nMessage(ACCOUNT_I18_MESSAGES, 'verify_user_error'),
                    'consoleError' => $e));
    }

    // Check if user is already verified
    if($this->accountRepository->isVerified($userId)) {
      return true;
    }

    // If user is verified get the verification token from the database
    try {
      $tokenHash = $this->accountRepository->getUserVerificationToken($userId);
    } catch (Exception $e) {

      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'verify_user_error'),
                    'consoleError' => $e));
    }

    // Compare to token in DB
    if(!$this->compareHash($token, $tokenHash)) {
      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'verify_user_error'),
                    'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.": Given token and token from the database do not match."));
    }

    // If the tokens match, verify the user
    try {
      $this->accountRepository->setUserVerified($userId);
    } catch (Exception $e) {

      return (array('status'=>0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'verify_user_error'),
                    'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.": Error verifying the account."));
    }
    // Return true on success
    return true;
  }

  /**
   * Check if an account has exceeded number of login attempts
   *
   * @param int userId The id of the user
   * @return true|array true on valid login attempts, array of error message on failure or error.
   */
  private function checkLoginAttempts($userId) {

    $time = time();
    // TODO: Add as customizable property
    $time = $time - (1 * 60 * 60);

    // Get the number of login attempts
     try {
       $numLoginAttempts = $this->accountRepository->getLoginAttempts($userId, $time);
       // If more than set number return error
       if($numLoginAttempts >= 5) {

         return (array('status' => 0,
                       'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'throttle_attempts'),
                       'consoleError' => "[USER {$userId}]: ".__CLASS__.":".__FUNCTION__.": Too many failed login attempts."));
       }

     } catch (Exception $e) {

       // TODO: If DB error on getting login attempts skip it?
     }


     return true;

  }

  /**
   * Add/Replace a hashed verification to a users account
   *
   * @param int userId the users Id (Optional)
   * @param array contents of the email to be sent
   * @return true|array true on success, array on error
   */
  public function setVerificationToken($email, $userId=null) {

    try {
      if(!$this->accountRepository->emailExists($email)) {
        return (array('status' => 0,
           'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'email_not_registered', [':email'=>$email]),
           'consoleError' => "[USER {$email}]: ".__CLASS__.':'.__FUNCTION__.': Error resending validation email, account in not registered.'));
      }
    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'resend_verification_error'),
         'consoleError' => "[USER {$email}]: ".$e));
    }


    // Get user's ID from email
    if($userId == null) {
      try {
        $userId = $this->accountRepository->getUserId($email);

      } catch (Exception $e) {
        return (array('status' => 0,
                      'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'resend_verification_error'),
                      'consoleError' => "[USER {$email}]: ".$e));
      }
    }

    // Generate verification token
    $verificationTokenForEmail = $this->generateToken();
    $verificationTokenForDatabase = $this->hashToken($verificationTokenForEmail);

    if(!$verificationTokenForDatabase) {
      // Failure to hash verification token
      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'resend_verification_error'),
                    'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.':  Error hashing verification token.'));
    }

    try {
      $this->accountRepository->setUserVerificationToken($userId, $verificationTokenForDatabase);
    } catch (Exception $e) {
      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'resend_verification_error'),
                    'consoleError' => "[USER {$userId}]: ".$e));
    }

    $emailer = new Emailer();

    require_once(EMAIL_TEMPLATES);
    $emailContents = generateVerificationEmail($email, $verificationTokenForEmail);

    if(!$emailer->sendEmail($emailContents['senderEmailAddress'], $emailContents['senderEmailName'], $email, $emailContents['subject'], $emailContents['body'], $emailContents['altBody'])) {

      return (array('status' => 0,
                   'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'resend_verification_error'),
                   'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.':  Error sending verification email.'));
    }

    return getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_set_token_success', [':email'=>$email]);
  }

  /**
   * Change a users password
   *
   * @param int userId the users Id
   * @param string newPassword the users new password
   * @return true|array true on success, array on error
   */
  public function changePassword($userId, $password, $retypePassword) {

    $result = verifyInput($password, array("required", array("minLength", 6)));
    if(isset($result["isError"])) {
      return (array('status' => 0,
      'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'Password', ':error'=>$result["errorMessage"]]),
      'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.': Error: Could not change password, problem with password field: '.$result["errorMessage"]));
    }
    $result = verifyInput($retypePassword, array("required", array("minLength", 6)));
    if(isset($result["isError"])) {
      return (array('status' => 0,
      'userError' => getI18nMessage(VERIFICATION_I18_MESSAGES, 'field_error', [':field'=>'reTypePassword', ':error'=>$result["errorMessage"]]),
      'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.': Error: Could not change password, problem with retype password field: '.$result["errorMessage"]));
    }

    if(!($password === $retypePassword)) {
      return (array('status' => 0,
      'userError'=> getI18nMessage(VERIFICATION_I18_MESSAGES, 'password_match'),
      'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.': Error: Could change password, passwords do not match.'));
    }

    try {
      $this->accountRepository->setUserPassword($userId, $this->hashToken($password));
    } catch (Exception $e) {
      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
                    'consoleError' => "[USER {$userId}]: ".$e));
    }

    return true;
  }

  /**
   * Generate, set, and send an email to a user for a password reset
   *
   * @param string email user's email
   * @return true|array true on success, array on error
   */
  public function sendPasswordReset($email) {

    try {
      if(!$this->accountRepository->emailExists($email)) {
        return (array('status' => 0,
           'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'email_not_registered', [':email'=>$email]),
           'consoleError' => "[USER {$email}]: ".__CLASS__.':'.__FUNCTION__.': Error sending password reset email, account in not registered.'));
      }
    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'send_password_reset_error'),
         'consoleError' => "[USER {$email}]: ".$e));

    }

    try {
      $userId = $this->accountRepository->getUserId($email);

    } catch (Exception $e) {
      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'send_password_reset_error'),
                    'consoleError' => "[USER {$email}]: ".$e));
    }

     // Generate verification token
     $passwordTokenForEmail = $this->generateToken();
     $passwordTokenForDatabase = $this->hashToken($passwordTokenForEmail);
     $tokenTime = getCurrentTime('Y-m-d H:i:s');

     if(!$passwordTokenForDatabase) {
       // Failure to hash verification token
       return (array('status' => 0,
                     'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'send_password_reset_error'),
                     'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.':  Error hashing password token.'));
     }

     try {
       $this->accountRepository->setUserResetPasswordToken($userId, $passwordTokenForDatabase, $tokenTime);
     } catch (Exception $e) {
       return (array('status' => 0,
                     'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'send_password_reset_error'),
                     'consoleError' => "[USER {$userId}]: ".$e));
     }

     $emailer = new Emailer();

     require_once(EMAIL_TEMPLATES);
     $emailContents = generatePasswordResetEmail($email, $passwordTokenForEmail);

     if(!$emailer->sendEmail($emailContents['senderEmailAddress'], $emailContents['senderEmailName'], $email, $emailContents['subject'], $emailContents['body'], $emailContents['altBody'])) {

       return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'send_password_reset_error'),
                    'consoleError' => "[USER {$userId}]: ".__CLASS__.':'.__FUNCTION__.':  Error sending password reset email.'));
     }

     return getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_password_reset_success', [':email'=>$email]);

  }

  /**
   * Validate a lost password tokens
   *
   * @param string email the user's email
   * @param string lostPasswordToken the user's new password
   * @return true|array true on success, array on error
   */
  public function validatePasswordReset($email, $lostPasswordToken) {
    // Verify the account exists
    try {
      if(!$this->accountRepository->emailExists($email)) {
        return (array('status' => 0,
           'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
           'consoleError' => "[USER {$email}]: ".__CLASS__.":".__FUNCTION__.": Error validating password reset, email account in not registered."));
      }
    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
         'consoleError' => "[USER {$email}]: ".$e));

    }

    // Get the user ID
    try {
      $userId = $this->accountRepository->getUserId($email);

    } catch (Exception $e) {
      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
                    'consoleError' => "[USER {$email}]: ".$e));
    }

    // Get the user's reset token details
    try {
      $resetToken = $this->accountRepository->getUserResetPasswordToken($userId);

    } catch (Exception $e) {
      return (array('status' => 0,
                    'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
                    'consoleError' => "[USER {$userId}]: ".$e));
    }

    $hashedPasswordToken = $resetToken['user_password_token'];
    $passwordTokenTime = new DateTime($resetToken['user_password_token_time']);

     $currentTime = new DateTime(getCurrentTime('Y-m-d H:i:s'));

     $passwordTokenTimeDiff = $passwordTokenTime->diff($currentTime);

     // Use util function to get time difference
     if(getTotalInterval($passwordTokenTimeDiff, 'minutes') > 15) {
       return (array('status' => 0,
          'userError'=> getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_token_expired'),
          'consoleError' => "[USER {$userId}]: ".__CLASS__.":".__FUNCTION__.": Error validating password reset, password reset token has expired"));
     }

     if(!$this->compareHash($lostPasswordToken, $hashedPasswordToken)) {
       return (array('status' => 0,
          'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
          'consoleError' => "[USER {$userId}]: ".__CLASS__.":".__FUNCTION__.": Error validating password reset, email token and hashed database token do not match."));
     }

     return true;
   }


   /**
    * Verify and change a user's password given the user's email, new password, and a lost password token
    *
    * @param string email
    * @param string lostPasswordToken
    * @param string password
    * @param string retype password
    *
    * @return true|array true on success, array on error
    */
   public function resetPassword($email, $lostPasswordToken, $password, $retypePassword) {

     // Verify reset password token
     $result = $this->validatePasswordReset($email, $lostPasswordToken);
     if(isset($result['status']) && $result['status'] == 0) {
       // Default error
       return (array('status' => 0,
          'userError' => $result['userError'],
          'consoleError' => $result['consoleError']));
     }

     // Verify the account exists
     try {
       if(!$this->accountRepository->emailExists($email)) {
         return (array('status' => 0,
            'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
            'consoleError' => "[USER {$email}]: ".__CLASS__.":".__FUNCTION__.": Error resetting password, email account in not registered."));
       }
     } catch (Exception $e) {
       return (array('status' => 0,
          'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
          'consoleError' => "[USER {$email}]: ".$e));

     }

     // Get the user ID
     try {
       $userId = $this->accountRepository->getUserId($email);

     } catch (Exception $e) {
       return (array('status' => 0,
                     'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'password_update_error'),
                     'consoleError' => "[USER {$email}]: Error resetting password, ".$e));
     }

     // Set new password
     $result = $this->changePassword($userId, $password, $retypePassword);
     if(isset($result['status']) && $result['status'] == 0) {
       // Default error
       return (array('status' => 0,
          'userError' => $result['userError'],
          'consoleError' => $result['consoleError']));
     }

     return getI18nMessage(ACCOUNT_I18_MESSAGES, './accounts/login&message=Password Successfully Reset');
   }

   /**
    * Verify account's existing password and change.
    *
    * @param string existingPassword accounts' existing password to be verified
    * @param string newPassword
    * @param string retypeNewPassword
    */
   public function verifyAndChangePassword($accountId, $existingPassword, $newPassword, $retypeNewPassword) {

     // Verify the existing password
     $result = $this->verifyPassword($accountId, $existingPassword);
     if(isset($result['status']) && $result['status'] === 0) {

       return (array('status' => 0,
        'userError' => $result['userError'],
        'consoleError' => $result['consoleError']));
      }

      // Set new password
      $result = $this->changePassword($accountId, $newPassword, $retypeNewPassword);
      if(isset($result['status']) && $result['status'] === 0) {
        // Default error
        return (array('status' => 0,
           'userError' => $result['userError'],
           'consoleError' => $result['consoleError']));
      }

      return getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_password_change_success');

   }

  /**
   * Generate token to be used for verification
   *
   * @return int Verification token
   */
  public function generateToken() {
    return bin2hex(openssl_random_pseudo_bytes(16));
  }

  /**
   * Hash input using php default hash functions
   *
   * @param string password input string to be hashed
   * @return string hashed input or false on failure
   * @throws Exception
   */
  public function hashToken($password) {
    $hashedToken = password_hash($password, PASSWORD_DEFAULT);
    if(!$hashedToken) {
      throw new Exception(__CLASS__.":".__FUNCTION__.": Error hashing input.");
    }
    return $hashedToken;
  }

  /**
   * Compare a value to a hashed value to see if they match
   *
   * @param string password input string to be compared to the a Hash
   * @param string passwordHash hashed value for passowrd to be compared to
   * @return boolean true if the inputs match, false if inputs do not match
   */
  public function compareHash($password, $passwordHash) {
    return (password_verify($password, $passwordHash));
  }

}

?>

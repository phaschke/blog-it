<?php

/**
 * Accounts class
 */
 Class Account {
   //Properties

   /**
     * @var int The accounts ID
     */
   protected $accountId = null;

   /**
     * @var string The accounts email
     */
   protected $accountEmail = null;

   /**
     * @var string The accounts name
     */
   protected $accountName = null;

   /**
     * @var string The accounts real name
     */
   protected $accountRealName = null;


   /**
     * @var string The accounts oauth provider, (if applicable)
     */
   protected $oAuthProvider = null;

   /**
     * @var string The account's oauthid, (if applicable)
     */
   protected $oAuthId = null;

   /**
     * @var int when the account was created
     */
   protected $createdAt = null;

   /**
     * @var int when the account last logged in
     */
   protected $lastLogin = null;

   /**
     * @var string the account's permission
     */
   protected $permission = null;

   /**
     * @var int the account's verification status
     */
   private $verified = null;

   /**
     * @var string the account's verification status
     */
   private $bio = null;

   /**
     * @var int the time the account was muted
     */
   private $mutedAt = null;

   /**
     * @var string the reason the account was muted
     */
   private $mutedReason = null;

   /**
    * Set the account objects properties using supplied ArrayAccess
    *
    * @param assoc the accounts property values
    */
   public function __construct($data = array()) {

     // Refactor this m8...
     if(isset($data['user_id'])) $this->setAccountId($data['user_id']);
     if(isset($data['user_email'])) $this->setAccountEmail($data['user_email']);
     if(isset($data['user_name'])) $this->setAccountName($data['user_name']);
     if(isset($data['user_real_name'])) $this->setAccountRealName($data['user_real_name']);
     if(isset($data['user_oauth_provider'])) $this->setOAuthProvider($data['user_oauth_provider']);
     if(isset($data['user_oauth_id'])) $this->setOAuthId($data['user_oauth_id']);
     if(isset($data['user_created_at'])) $this->setCreatedAt($data['user_created_at']);
     if(isset($data['user_last_login'])) $this->setLastLogin($data['user_last_login']);
     if(isset($data['user_permission'])) $this->setAccountPermission($data['user_permission']);
     if(isset($data['user_verified'])) $this->setAccountVerified($data['user_verified']);
     if(isset($data['user_bio'])) $this->setAccountBio($data['user_bio']);

     if(isset($data['muted_at'])) $this->setMutedAt($data['muted_at']);
     if(isset($data['muted_reason'])) $this->setMutedReason($data['muted_reason']);

     if(isset($data['account_id'])) $this->setAccountId($data['account_id']);
     if(isset($data['account_email'])) $this->setAccountEmail($data['account_email']);
     if(isset($data['account_name'])) $this->setAccountName($data['account_name']);
     if(isset($data['account_real_name'])) $this->setAccountRealName($data['account_real_name']);
     if(isset($data['account_oauth_provider'])) $this->setOAuthProvider($data['account_oauth_provider']);
     if(isset($data['account_oauth_id'])) $this->setOAuthId($data['account_oauth_id']);
     if(isset($data['account_created_at'])) $this->setCreatedAt($data['account_created_at']);
     if(isset($data['account_last_login'])) $this->setLastLogin($data['account_last_login']);
     if(isset($data['account_permission'])) $this->setAccountPermission($data['account_permission']);
     if(isset($data['account_verified'])) $this->setAccountVerified($data['account_verified']);
     if(isset($data['account_bio'])) $this->setAccountBio($data['account_bio']);
   }

   public function setAccountId($accountId) {
     $this->accountId = (int) $accountId;
   }

   public function getAccountId() {
     return $this->accountId;
   }

   public function setAccountEmail($accountEmail) {
     $this->accountEmail = $accountEmail;
   }

   public function getAccountEmail() {
     return $this->accountEmail;
   }

   public function setAccountName($accountName) {
     $this->accountName = $accountName;
   }

   public function getAccountName() {
     return $this->accountName;
   }

   public function setAccountRealName($accountRealName) {
     $this->accountRealName = $accountRealName;
   }

   public function getAccountRealName() {
     return $this->accountRealName;
   }

   public function setOAuthProvider($oAuthProvider) {
     $this->oAuthProvider = $oAuthProvider;
   }

   public function getOAuthProvider() {
     return $this->oAuthProvider;
   }

   public function setOAuthId($oAuthId) {
     $this->oAuthId = $oAuthId;
   }

   public function getOAuthId() {
     return $this->oAuthId;
   }

   public function setCreatedAt($createdAt) {
     $this->createdAt = $createdAt;
   }

   public function getCreatedAt($dateTimeFormat=null) {
     $formattedDateTime = $this->createdAt;
     if($dateTimeFormat && $formattedDateTime) {
       $formattedDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->createdAt)->format($dateTimeFormat);
     }
     return $formattedDateTime;
   }

   public function setLastLogin($lastLogin) {
     $this->lastLogin = $lastLogin;
   }

   public function getLastLogin($dateTimeFormat=null) {
     // If the account has never logged on, return 'never'.
     if(empty($this->lastLogin)) return "Never";
     $formattedDateTime = $this->lastLogin;
     if($dateTimeFormat && $formattedDateTime) {
       $formattedDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->lastLogin)->format($dateTimeFormat);
     }
     return $formattedDateTime;
   }

   public function setAccountPermission($permission) {
     $this->permission = (int) $permission;
   }

   public function getAccountPermission() {
     return $this->permission;
   }

   public function setAccountVerified($verified) {
     $this->verified = (int) $verified;
   }

   public function getAccountVerified() {
     return $this->verified;
   }

   public function setAccountBio($bio) {
     $this->bio = $bio;
   }

   public function getAccountBio() {
     return $this->bio;
   }

   public function setMutedAt($mutedAt) {
     $this->mutedAt = $mutedAt;
   }

   public function getMutedAt($dateTimeFormat=null) {
     $formattedDateTime = $this->mutedAt;
     if($dateTimeFormat && $formattedDateTime) {
       $formattedDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->mutedAt)->format($dateTimeFormat);
     }
     return $formattedDateTime;
   }

   public function setMutedReason($mutedReason) {
     $this->mutedReason = $mutedReason;
   }

   public function getMutedReason() {
     return $this->mutedReason;
   }

   public function returnAsArray($dateFormat=null) {
     return array(
       'account_id' => $this->getAccountId(),
       'account_email' => $this->getAccountEmail(),
       'account_name' => $this->getAccountName(),
       'account_oauth_provider' => $this->getOAuthProvider(),
       'account_oauth_id' => $this->getOAuthId(),
       'account_created_at' => $this->getCreatedAt($dateFormat),
       'account_last_login' => $this->getLastLogin($dateFormat),
       'account_permission' => $this->getAccountPermission(),
       'account_verified' => $this->getAccountVerified(),
       'account_bio' => $this->getAccountBio(),
       'muted_at' => $this->getMutedAt($dateFormat),
       'muted_reason' => $this->getMutedReason()
     );
   }

 } // End Accounts class

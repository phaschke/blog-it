<?php

/**
 * Account Model
 */
Class AccountModel {

  private $accountRepository;
  private $editLockManager;

  public function __construct(AccountRepository $accountRepository, $editLockManager) {

    $this->accountRepository = $accountRepository;
    $this->editLockManager = $editLockManager;

  }

  /**
   * Get a user account information by the username, make use the get account function
   */
  public function getAccountByUsername($username) {

    try {
      $accountId = $this->accountRepository->getIdFromUsername($username);
      if(is_bool($accountId) && $accountId === false) {
        // Account was not found
        return (array('status' => 0,
           'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_not_found')));
      }

      return $this->getAccount($accountId);

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_find_error'),
         'consoleError'=>$e));
    }
  }

  /**
   * Get user account information by the user ID.
   */
  public function getAccount($accountId) {
    try {
      $account = $this->accountRepository->getAccount($accountId);
      if(is_bool($account) && $account === false) {
        // Account was not found
        return (array('status' => 0,
           'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_not_found')));
      }

      return $account;

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_find_error'),
         'consoleError'=>$e));
    }
  }

  /**
   * Get user account information by the user ID.
   */
  public function getAccounts($numRecords=10000, $offset=0, $onlyIsMuted=false) {

    try {

      $totalCount = $this->accountRepository->getAccountCount($onlyIsMuted);

      $accounts = $this->accountRepository->getAccounts($numRecords, $offset, $onlyIsMuted);

      return array('totalCount'=>$totalCount, 'accounts'=>$accounts['accounts']);

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_load_error'),
         'consoleError'=>$e));
    }
  }

  /**
   * Lock account
   */
  public function manageAccount($accountId) {

    if(!$this->editLockManager->lock('ACCOUNT', $accountId)) {
      return (array('status' => 0,
        'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_manage_error'),
        'consoleError' => __CLASS__.":".__METHOD__."Error setting edit object lock"));
    }
    return true;
  }

  /**
   * Mute an account
   */
  public function muteAccount($mutedReason) {

    $result = $this->editLockManager->verifyLockStatus('ACCOUNT');
    if(is_bool($result) && !$result) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_mute_error'),
         'consoleError'=>"User sessions edit type is mismatched."));
    }

    // set post object post ID and user ID
    $accountId = $result['EDIT_ID'];
    $mutedAt = date("Y-m-d H:i:s");

    try {
      $this->accountRepository->muteAccount($accountId, $mutedAt, trim($mutedReason));

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_mute_error'),
         'consoleError'=>$e));
    }
  }

  /**
   * Unmute an account
   */
  public function unmuteAccount() {

    $result = $this->editLockManager->verifyLockStatus('ACCOUNT');
    if(is_bool($result) && !$result) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_unmute_error'),
         'consoleError'=>"User sessions edit type is mismatched."));
    }

    // set post object post ID and user ID
    $accountId = $result['EDIT_ID'];

    try {
      $this->accountRepository->unmuteAccount($accountId);

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_unmute_error'),
         'consoleError'=>$e));
    }
  }

  /**
   * Update a users account bio.
   */
  public function updateAccountBio($accountId, $accountBio) {

    try {
      $this->accountRepository->updateAccountBio($accountId, trim($accountBio));

    } catch (Exception $e) {
      return (array('status' => 0,
         'userError' => getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_save_bio_error'),
         'consoleError'=>$e));
    }

    return getI18nMessage(ACCOUNT_I18_MESSAGES, 'account_save_bio_success');
  }

}

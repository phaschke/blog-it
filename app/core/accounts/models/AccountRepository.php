<?php

/**
 * User Repository related functions
 */
Class AccountRepository {

  private $connection;

  public function __construct(DBConnection $DBConnection) {

    $this->connection = $DBConnection->getConnection();
  }

  /**
   * Get the account ID from a unique user name
   *
   * @param string $username
   * @return int|false account Id
   * @throws PDOException
   */
  public function getIdFromUsername($username) {

    $sql = $this->connection->prepare('SELECT user_id FROM users WHERE user_name = :username LIMIT 1');
    $sql->execute(array(':username' => $username));

    if($sql->rowCount() < 1) {
      return false;
    }
    $row = $sql->fetch();

    return $row['user_id'];
  }

  /**
   * Get a user given an user Id
   * @param int userId
   * @return account|false
   */
  public function getAccount($userId) {

    $sql = $this->connection->prepare('SELECT T1.*, T2.muted_at, T2.muted_reason, T3.user_oauth_provider
       FROM users AS T1
       LEFT JOIN users_disciplined AS T2 ON T1.user_id=T2.user_id
       LEFT JOIN users_oauth_credentials AS T3 ON T1.user_id=T3.user_id
       WHERE T1.user_id = :userId LIMIT 1');

    $sql->execute(array(':userId' => $userId));

    if($sql->rowCount() < 1) {
      return false;
    }
    $row = $sql->fetch();

    return new Account($row);
  }

  /**
   * Get the total record count from the account record table
   * Either all users, or only muted users
   *
   * @param boolean onlyIsMuted Boolean if all users, or only used who are muted should be fetched
   * @return int count
   * @throws PDOException
   */
  public function getAccountCount($onlyIsMuted) {

    if($onlyIsMuted == 'true') {

      $sql = $this->connection->prepare('SELECT COUNT(*)
        FROM users AS T1
        LEFT JOIN users_disciplined AS T2 ON T1.user_id=T2.user_id
        WHERE (muted_at IS NOT NULL)');

    } else {

      $sql = $this->connection->prepare('SELECT COUNT(*) FROM users');
    }

    $sql->execute();

    return $sql->fetchColumn();
  }

  /**
   * Returns all (or a range of) accounts by creation date DESC
   *
   * @param boolean onlyIsMuted Boolean if all users, or only used who are muted should be fetched
   * @return Array A two element array with accounts => returned accounts and totalRows => total rows selected.
   */
  public function getAccounts($numRecords, $offset, $onlyIsMuted) {

    if($onlyIsMuted == 'true') {
      // Get only accounts that are muted.
      $sql = $this->connection->prepare('SELECT SQL_CALC_FOUND_ROWS T1.*, T2.muted_at, T2.muted_reason
         FROM users AS T1
         LEFT JOIN users_disciplined AS T2 ON T1.user_id=T2.user_id
         WHERE (muted_at IS NOT NULL)
         ORDER BY T1.user_name LIMIT :numRecords OFFSET :numOffset');

    } else {
      // Get all accounts.
      $sql = $this->connection->prepare('SELECT SQL_CALC_FOUND_ROWS T1.*, T2.muted_at, T2.muted_reason
         FROM users AS T1
         LEFT JOIN users_disciplined AS T2 ON T1.user_id=T2.user_id
         ORDER BY T1.user_name LIMIT :numRecords OFFSET :numOffset');

    }

    $sql->bindValue(":numRecords", (int) $numRecords, PDO::PARAM_INT);
    $sql->bindValue(":numOffset", (int) $offset, PDO::PARAM_INT);

    $sql->execute();
    $accounts = array();

    while($row = $sql->fetch()) {
      $account = new Account($row);
      $accounts[] = $account;
    }
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalAccounts = $this->connection->query($sql)->fetch();

    return (array("accounts" => $accounts, "totalRetrievedAccounts" => $totalAccounts[0]));

  }

  /**
   * Remove a user record from the database given a user ID
   *
   * @param int Account ID
   *
   * @throws PDOException
   */
   public function delete($userId) {

     $sql = $this->connection->prepare('DELETE FROM users WHERE user_id = :userId');
     $sql->execute(array(':userId' => $userId));
   }

  /**
   * TODO fix this
   *
   * @throws PDOException
   */
  public function insert(Account $account, $hashedPassword, $verificationTokenForDatabase) {

    try {
      // Begin a transaction, turning off autocommit
      $this->connection->beginTransaction();

      // Store new user in database
      $sql = $this->connection->prepare('INSERT INTO users (user_email, user_name, user_created_at)
       VALUES (:email, :username, FROM_UNIXTIME(:timestamp))');

      $result = $sql->execute(array(':email' => $account->getAccountEmail(), ':username' => $account->getAccountName(), ':timestamp' => $account->getCreatedAt()));

      //Retrieve the id of the newly inserted user
      $userId = $this->connection->lastInsertId();

      // And add password and verification token to the DB
      $sql = $this->connection->prepare('INSERT INTO users_credentials (user_id, user_password, user_verification_token)
         VALUES (:userId, :hashedPassword, :verificationToken)');
      $result = $sql->execute(array(':userId' => $userId, 'hashedPassword' => $hashedPassword, 'verificationToken' => $verificationTokenForDatabase));

      // No error, commit queries
      $this->connection->commit();

      return $userId;

    } catch(Exception $e) {
      // Error with queries rollback to begin transation
      $this->connection->rollBack();

      return (array('status' => 0,
        'consoleError' => $e));
    }

  }

  /**
   * Retrieve a user's permission given a user's id
   *
   * @param int userId
   * @return string userPermission enum a,b,c
   *
   * @throws PDOException
   */
  public function getUserPermission($userId) {

    $sql = $this->connection->prepare('SELECT user_permission FROM users WHERE user_id = :userId');
    $result = $sql->execute(array(':userId' => $userId));
    $result = $sql->fetch(PDO::FETCH_ASSOC);

    return $result['user_permission'];
  }

  /**
   * Check if a OAuth user is already added to the users table.
   *
   * @param string userOAuthId
   * @param string userOAuthProvider
   *
   * @return boolean|int false if user does not exist in the table, user_id if account exists
   * @throws PDOException
   */
  public function verifyOAuthUser(Account $account) {

    $sql = $this->connection->prepare('SELECT user_id FROM users_oauth_credentials WHERE
      user_oauth_provider = :oAuthProvider AND user_oauth_id = :oAuthUid');
    $sql->execute(array(':oAuthProvider' => $account->getOAuthProvider(), ':oAuthUid' => $account->getOAuthId()));
    return $sql->fetch();
  }

  /**
   * Add the records for a OAuth user
   *
   * @param string userOAuthId
   * @param string userOAuthProvider
   * @param string userEmail
   * @param string userName
   *
   * @return int|array int userId on success, array of error code and console message on error.
   * @throws PDOException
   */
  public function addOAuthUser(Account $account) {

    try {
      // Begin a transaction, turning off autocommit
      $this->connection->beginTransaction();

      $sql = $this->connection->prepare('INSERT INTO users (user_email) VALUES (:userEmail)');
      $sql->execute(array(':userEmail' => $account->getAccountEmail()));

      //Retrieve the id of the newly inserted post
      $userId = $this->connection->lastInsertId();

      $sql = $this->connection->prepare('INSERT INTO users_oauth_credentials (user_id, user_real_name, user_oauth_provider, user_oauth_id)
        VALUES (:userId, :userRealName, :oAuthProvider, :oAuthUid)');
      $result = $sql->execute(array(':userId' => $userId, ':userRealName' => $account->getAccountRealName(),
        ':oAuthProvider' => $account->getOAuthProvider(), ':oAuthUid' => $account->getOAuthId()));

      // No error, commit queries
      $this->connection->commit();

      return $userId;

    } catch(Exception $e) {
      // Error with queries rollback to begin transation
      $this->connection->rollBack();

      return (array('status' => 0,
        'consoleError' => $e));
    }
  }

  /**
   * Update account given an account object
   */
  public function updateAccount(Account $account) {

    $sql = $this->connection->prepare(
      'UPDATE users
      SET user_email = COALESCE(:userEmail, user_email),
          user_name = COALESCE(:userName, user_name)
      WHERE user_id = :userId');
    $sql->execute(array(':userEmail' => $account->getAccountEmail(), ':userName' => $account->getAccountName(),
      'userId' => $account->getAccountId()));
  }

  /**
   * Update account given an account object
   */
  public function updateAccountBio($accountId, $accountBio) {

    $sql = $this->connection->prepare(
      'UPDATE users
      SET user_bio = :userBio
      WHERE user_id = :userId');
    $sql->execute(array('userId' => $accountId, ':userBio' => $accountBio));
  }

  /**
   * Update the records for a OAuth user
   *
   * @param int userId
   * @param string userOAuthId
   * @param string userOAuthProvider
   * @param string userEmail
   * @param string userName
   * @throws PDOException
   */
  public function updateOAuthUser(Account $account) {

    try {

      // Begin a transaction, turning off autocommit
      $this->connection->beginTransaction();

      $sql = $this->connection->prepare('UPDATE users SET user_email = :userEmail WHERE user_id = :userId');
      $sql->execute(array(':userEmail' => $account->getAccountEmail(), 'userId' => $account->getAccountId()));

      $sql = $this->connection->prepare('UPDATE users_oauth_credentials SET user_oauth_provider = :oAuthProvider, user_oauth_id = :oAuthUid,
        user_real_name = :userRealName WHERE user_id = :userId');
      $result = $sql->execute(array(':userId' => $account->getAccountId(), ':oAuthProvider' => $account->getOAuthProvider(),
        ':oAuthUid' => $account->getOAuthId(), ':userRealName' => $account->getAccountRealName()));

      // No error, commit queries
      $this->connection->commit();

    } catch(Exception $e) {
      // Error with queries rollback to begin transation
      $this->connection->rollBack();

      return (array('status' => 0,
        'consoleError' => $e));
    }
  }

  /**
   * Get a user's information for login
   *
   * @param int userId
   * @return array user's data
   * @throws PDOException
   */
  public function getUserLoginData($userId) {
    $sql = $this->connection->prepare('SELECT user_email, user_name, user_permission FROM users WHERE user_id = :userId');
    $sql->execute(array(':userId' => $userId));
    return $sql->fetch();
  }

  /**
   * Update the current login time
   *
   * @param int userId
   * @throws PDOException
   */
  public function updateLoginTime($userId) {
    $sql = $this->connection->prepare('UPDATE users SET user_last_login = NOW() WHERE user_id = :userId');
    $sql->execute(array(':userId' => $userId));
  }

  /**
   * Get a user's password given a userid
   *
   * @param int userId
   * @return string password hashed
   * @throws PDOException
   */
  public function getUserPassword($userId) {
    $sql = $this->connection->prepare('SELECT user_password FROM users_credentials WHERE user_id = :userId');
    $result = $sql->execute(array(':userId' => $userId));
    $result = $sql->fetch(PDO::FETCH_ASSOC);

    return $result['user_password'];
  }

  /**
   * Set a user's password given a userid
   *
   * @param int userId
   * @return true
   * @throws PDOException
   */
  public function setUserPassword($userId, $password) {
    $sql = $this->connection->prepare('UPDATE users_credentials SET user_password = :password WHERE user_id = :userId');
    $sql->execute(array(':password' => $password, ':userId' => $userId));

    return true;
  }

  /**
   * Add a failed login attempt to an account
   *
   * @param int userId
   * @return boolean true on success
   * @throws PDOException
   */
  public function addLoginAttempt($userId) {

    $currentTimeStamp = (new DateTime())->format('U');
    $sql = $this->connection->prepare('INSERT INTO users_login_attempts (user_id, time) VALUES (:userId, :currentTime)');
    $sql->execute(array(':userId' => $userId, ':currentTime' => $currentTimeStamp));

    return true;
  }

  /**
   * Check if a given email is in the users table
   *
   * @param string $email
   * @return boolean True if the email is in the database, false if the email is not
   * @throws PDOException
   */
  public function emailExists($email) {

    $sql = $this->connection->prepare('SELECT user_email FROM users WHERE user_email = :email');
    $sql->execute(array(':email' => $email));
    return ($sql->rowCount() > 0);

  }

  /**
   * Check if a given username is in the users table
   *
   * @param string $username
   * @return boolean True if the username is in the database, false if not
   * @throws PDOException
   */
  public function usernameExists($username) {

    $sql = $this->connection->prepare('SELECT user_name FROM users WHERE user_name = :username');
    $sql->execute(array(':username' => $username));
    return ($sql->rowCount() > 0);

  }

  /**
   * Return the ID of a user given the users email
   *
   * @param string email
   * @return int The users id or false if no user was found or there was an error
   * @throws PDOException
   */
  public function getUserId($email) {

     $sql = $this->connection->prepare('SELECT user_id FROM users WHERE user_email = :email');
     $result = $sql->execute(array(':email' => $email));
     $result = $sql->fetch(PDO::FETCH_ASSOC);

     return $result['user_id'];
  }

  /**
   * Set user to verified given a user ID
   *
   * @param int userId
   * @return boolean true if user is set to verified
   * @throws PDOException
   */
  public function setUserVerified($userId) {
    $sql = $this->connection->prepare('UPDATE users_credentials SET user_verified = 1 WHERE user_id = :userId');
    $result = $sql->execute(array(':userId' => $userId));

    return true;
  }

  /**
   * Return a user's verification token
   *
   * @param int userId the user id of the user
   * @return boolean true if account is verified, false if not
   * @throws Exception|PDOException
   */
  public function getUserVerificationToken($userId) {
    // Get token from user id
    $sql = $this->connection->prepare('SELECT user_verification_token FROM users_credentials WHERE user_id = :userId');
    $result = $sql->execute(array(':userId' => $userId));
    $result = $sql->fetch(PDO::FETCH_ASSOC);

    return $result['user_verification_token'];
  }

  /**
   * Set a user's verification token
   *
   * @param int userId the user id of the user
   * @return boolean true if account is verified, false if not
   * @throws PDOException
   */
  public function setUserVerificationToken($userId, $verificationToken) {

     $sql = $this->connection->prepare('UPDATE users_credentials SET user_verification_token = :verificationToken WHERE user_id = :userId');
     $result = $sql->execute(array(':verificationToken' => $verificationToken, ':userId' => $userId));

     return true;

  }

  /**
   * Return if user is verified given userId
   *
   * @param int userId the user id of the user
   * @return boolean true if account is verified, false if not
   * @throws PDOException
   */
  public function isVerified($userId) {

    $sql = $this->connection->prepare('SELECT COUNT(*) FROM users_credentials WHERE user_id = :userId AND user_verified = 1');
    $sql->execute(array(':userId' => $userId));
    $numRows = (int) $sql->fetchColumn();

    if($numRows != 1) {
      return false;
    }
    return true;
  }

  /**
   * Return the int value of the number of login attempts in the past set amount of timeout
   *
   * @param int userId
   * @return int number of login attempts
   * @throws PDOException
   */
  public function getLoginAttempts($userId, $time) {

    $sql = $this->connection->prepare('SELECT * FROM users_login_attempts WHERE user_id = :userId AND time > :time');
    $result = $sql->execute(array(':userId' => $userId, ':time' => $time));

    $numLoginAttempts = (int)$sql->rowCount();

    return $numLoginAttempts;
  }

  /**
   * Set a user's reset token
   *
   * @param int userId
   * @param string passwordToken
   * @param string tokenTime
   * @return true
   * @throws PDOException
   */
  public function setUserResetPasswordToken($userId, $passwordToken, $tokenTime) {
    $sql = $this->connection->prepare('UPDATE users_credentials SET user_password_token = :passwordToken, user_password_token_time = :tokenTime WHERE user_id = :userId');
    $result = $sql->execute(array(':passwordToken' => $passwordToken, ':tokenTime' => $tokenTime, ':userId' => $userId));

    return true;
  }

  /**
   * Get a user's reset token
   *
   * @param int userId
   * @return array return password token and token time
   * @throws PDOException
   */
  public function getUserResetPasswordToken($userId) {
    $sql = $this->connection->prepare('SELECT user_password_token, user_password_token_time FROM users_credentials WHERE user_id = :userId');
    $sql->execute(array(':userId' => $userId));

    return $sql->fetch(PDO::FETCH_ASSOC);
  }

  /**
   * Mute an account
   *
   * @param int userId
   * @param string mutedAt
   * @param string mutedReason
   * @throws PDOException
   */
  public function muteAccount($userId, $mutedAt, $mutedReason) {
    $sql = $this->connection->prepare('INSERT INTO users_disciplined (user_id, muted_at, muted_reason)
        VALUES (:userId, :mutedAt, :mutedReason) ON DUPLICATE KEY UPDATE muted_reason = :mutedReason');
    $sql->execute(array(':userId' => $userId, ':mutedAt' => $mutedAt, ':mutedReason' => $mutedReason));

  }

  /**
   * Unmute an account
   *
   * @param int userId
   * @throws PDOException
   */
  public function unmuteAccount($userId) {
    $sql = $this->connection->prepare('DELETE FROM users_disciplined WHERE user_id = :userId');
    $sql->execute(array(':userId' => $userId));
  }



}



?>

<?php
// Define core module services.
return array(
    'pages' => [ 'type' => 'core',
                   'folder' => 'pages',
                   'className' => 'PagesService',
                   'public' => [],
                   'user' => [],
                   'privileged' => ['update']
                 ],
    'account' => [ 'type' => 'core',
                   'folder' => 'accounts',
                   'className' => 'AccountService',
                   'public' => ['login', 'register', 'resendVerificaton', 'sendPasswordReset', 'resetPassword', 'completeRegistration'],
                   'user' => ['changeEmail', 'saveAccountBio', 'changePassword'],
                   'privileged' => ['getMany', 'muteAccount', 'unmuteAccount']
                 ],
    'post' => [ 'type' => 'core',
                'folder' => 'posts',
                'className' => 'PostService',
                'public' => ['getPosts'],
                'user' => [],
                'privileged' => ['add', 'delete', 'update', 'togglePostVisibility']
              ],
    'category' => [ 'type' => 'core',
                    'folder' => 'posts',
                    'className' => 'CategoryService',
                    'public' => ['getCategories', 'getCategory'],
                    'user' => [],
                    'privileged' => ['add', 'delete', 'update']
              ],
    'editLockManager' => [ 'type' => 'resource',
                'folder' => 'editlockmanager',
                'className' => 'EditLockManagerService',
                'public' => [],
                'user' => [],
                'privileged' => ['unlockEditObject']
              ],
  );

?>

<?php

class HomeController extends Controller {

  private $postModel;

  public function __construct() {

    parent::__construct();

    $session = new Session();

    $DBConnection = new DBConnection();
    $postRepository = new PostRepository($DBConnection);
    $pagesRepository = new PagesRepository($DBConnection);
    $categoryRepository = new CategoryRepository($DBConnection);

    $this->postModel = new PostModel($postRepository, null);
    $this->pagesModel = new PagesModel($pagesRepository);
    $this->categoryModel = new CategoryModel($categoryRepository);

  }

  /**
   *
   */
  public function show() {

    $pageContent = array();
    $pageContent['pageTitle'] = "Home";
    $pageContent['session'] = $this->session;


    // Get posts
    $result = $this->postModel->getPosts(HOMEPAGE_NUM_POSTS, 0, NULL, 'published', NULL);
    if(isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error";
      if($result['userError']) {
        $error = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['getPostsError'] = $error;

    } else {
      $pageContent['posts'] = $result['posts'];
      $pageContent['totalPosts'] = $result['totalCount'];
    }

    // Get year/months of posts for side bar
    $result = $this->postModel->getPostsPublicationDates(9, 'published', 'F Y');
    if(isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error";
      if($result['userError']) {
        $error = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['getPubDateError'] = $error;

    } else {
      $pageContent['publicationDates'] = $result;
    }

    // Get post categories for side bar
    $result = $this->categoryModel->getUsedCategories($count=15);
    if(isset($result['status']) && $result['status'] == 0) {
      // Default error
      $error = "Error";
      if($result['userError']) {
        $error = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
      $pageContent['getCategoriesError'] = $error;

    } else {

      $pageContent['categories'] = $result['categories'];
    }

    $pageContent['settings'] = $this->pagesModel->getSettings();

    if(($pageContent['settings']['twitter_flag'] == 1) || ($pageContent['settings']['instagram_flag'] == 1) || ($pageContent['settings']['facebook_flag'] == 1) || ($pageContent['settings']['youtube_flag'] == 1)) {
      $pageContent['showSocial'] = true;
    } else {
      $pageContent['showSocial'] = false;
    }

    require_once HOME_VIEW_PATH.DS."homepage.php";
  }

}

?>

<?php
//======================================================================
// PAGES MODULE CONFIG
//======================================================================

/**
 * Path To Accounts Module Folder
 */
define("HOME_PATH", __DIR__);

/**
 * Path To Account Views
 */
define("HOME_VIEW_PATH", HOME_PATH.DS."views");

define("HOMEPAGE_NUM_POSTS", 5);

?>

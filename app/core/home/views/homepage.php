<?php include_once LAYOUTS_PATH . "/header.php" ?>

  <div class="container">

    <div class="row">

      <div class="col-sm-8 blog-main">

        <div>
          <?php if(isset($pageContent['settings']['about_text_short'])) { ?>
          <div class="sidebar-module sidebar-module-inset">
            <p class="margin-top-sm"><?php echo $pageContent['settings']['about_text_short'];?></p>
          </div>
          <?php } ?>
        </div>

        <?php include_once POSTS_VIEW_PATH.DS."partials".DS."postsList.part.php" ?>

        <nav class="blog-pagination">
          <a class="btn btn-outline-primary" href="./posts/archive">Older</a>
          <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav>

      </div><!-- /.blog-main -->

      <div class="col-sm-3 offset-sm-1 blog-sidebar">


          <?php if($pageContent['showSocial']) { ?>
          <div class="sidebar-module">

            <div class="row justify-content-center margin-top-sm social-media-img">
              <?php if($pageContent['settings']['twitter_flag'] == 1) {?>
                <a href="<?php echo $pageContent['settings']['twitter_link'];?>" target="_blank" class="margin-right-md">
                  <i class="fab fa-twitter fa-2x"></i>
                </a>
              <?php } ?>
              <?php if($pageContent['settings']['instagram_flag'] == 1) {?>
                <a href="<?php echo $pageContent['settings']['instagram_link'];?>" target="_blank" class="margin-right-md">
                  <i class="fab fa-instagram fa-2x"></i>
                </a>
              <?php } ?>
              <?php if($pageContent['settings']['facebook_flag'] == 1) {?>
                <a href="<?php echo $pageContent['settings']['facebook_link'];?>" target="_blank" class="margin-right-md">
                  <i class="fab fa-facebook fa-2x"></i>
                </a>
              <?php } ?>
              <?php if($pageContent['settings']['youtube_flag'] == 1) {?>
                <a href="<?php echo $pageContent['settings']['youtube_link'];?>" target="_blank" class="margin-right-md">
                  <i class="fab fa-youtube fa-2x"></i>
                </a>
              <?php } ?>
            </div>
          </div>
          <?php } ?>


        <hr>
        <div class="sidebar-module">
          <h4 class="margin-bottom-sm">Archives</h4>
          <?php include_once POSTS_VIEW_PATH.DS."partials".DS."archiveListings.part.php" ?>
        </div>
        <hr>
        <div class="sidebar-module">
          <h4 class="margin-bottom-sm">Categories</h4>
          <?php include_once POSTS_VIEW_PATH.DS."partials".DS."categoryList.part.php" ?>
        </div>
      </div><!-- /.blog-sidebar -->
    </div><!-- /.row -->
  </div><!-- /.container -->

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

<?php

return array(
  'home'      => [ 'display' => true,
                   'privileged' => false,
                   'tag' => 'Home',
                   'controller' => 'home',
                   'action' => '',
                   'aliasControllers' => [],
                   'aliasActions' => []
                 ],
 'about'     => [ 'display' => true,
                  'privileged' => false,
                  'tag' => 'About',
                  'controller' => 'pages',
                  'action' => 'about',
                  'aliasControllers' => [],
                  'aliasActions' => []
                ],
  'archives'  => [ 'display' => true,
                   'privileged' => false,
                   'tag' => 'Blog Posts',
                   'controller' => 'posts',
                   'action' => 'archive',
                   'aliasControllers' => [],
                   'aliasActions' => ['bymonth', 'category', 'show', 'list']
                 ],
  'breweries' => [ 'display' => true,
                   'privileged' => false,
                   'tag' => 'Visited Breweries',
                   'controller' => 'breweries',
                   'action' => 'map',
                   'aliasControllers' => [],
                   'aliasActions' => ['list', 'show']
                 ],
  'admin'     => [ 'display' => true,
                   'privileged' => true,
                   'tag' => 'Admin Panel',
                   'controller' => 'admin',
                   'action' => 'panel',
                   'aliasControllers' => ['posts', 'breweries', 'pages'],
                   'aliasActions' => ['add', 'edit', 'manage', 'managecategories']
                 ]
);


?>

<?php
//======================================================================
// PAGES MODULE CONFIG
//======================================================================

/**
 * Path To Accounts Module Folder
 */
define("PAGES_PATH", __DIR__);

define("PAGES_I18_MESSAGES", PAGES_PATH.DS."resources".DS."lang".DS."en".DS."pages.php");

/**
 * Define module specific page css/js resources
 */
define("PAGES_PAGE_RESOURCES", array(
         [ 'type' => 'js',
           //'path' => 'http://js.nicedit.com/nicEdit-latest.js',
           'path' => 'resources/nicEdit_v0.9r25/nicEdit.js',
         ]
       ));

/**
 * Require Pages Model
 */
require_once PAGES_PATH.DS."models".DS."PagesModel.php";

/**
 * Require Pages Repository Model
 */
require_once PAGES_PATH.DS."models".DS."PagesRepository.php";




/**
 * Path To Pages Views
 */
define("PAGES_VIEW_PATH", PAGES_PATH.DS."views");

/**
 * Path To Admin Pages Views
 */
define("ADMIN_PAGES_VIEW_PATH", PAGES_VIEW_PATH.DS."admin");

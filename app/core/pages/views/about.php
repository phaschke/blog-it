<?php include_once LAYOUTS_PATH . "/header.php" ?>

  <div class="container">

    <?php if(isset($pageContent['settings']['about_text_long'])) { ?>
    <div class="row">
      <div class="col-sm-12">
        <div class="row justify-content-center">
          <h2>About</h2>
        </div>
        <div>
          <p class="margin-top-sm"><?php echo $pageContent['settings']['about_text_long'];?></p>
        </div>
      </div>
    </div>
    <?php } ?>

    <?php if($pageContent['showSocial']) { ?>
    <hr>
    <div class="row justify-content-center margin-top-lg">
      <h2>Social</h2>
    </div>
    <div class="row justify-content-center margin-top-lg social-media-img">
      <?php if($pageContent['settings']['twitter_flag'] == 1) {?>
        <a href="<?php echo $pageContent['settings']['twitter_link'];?>" target="_blank" class="margin-right-lg">
          <i class="fab fa-twitter fa-3x"></i>
        </a>
      <?php } ?>
      <?php if($pageContent['settings']['instagram_flag'] == 1) {?>
        <a href="<?php echo $pageContent['settings']['instagram_link'];?>" target="_blank" class="margin-right-lg">
          <i class="fab fa-instagram fa-3x"></i>
        </a>
      <?php } ?>
      <?php if($pageContent['settings']['facebook_flag'] == 1) {?>
        <a href="<?php echo $pageContent['settings']['facebook_link'];?>" target="_blank" class="margin-right-lg">
          <i class="fab fa-facebook fa-3x"></i>
        </a>
      <?php } ?>
      <?php if($pageContent['settings']['youtube_flag'] == 1) {?>
        <a href="<?php echo $pageContent['settings']['youtube_link'];?>" target="_blank" class="margin-right-lg">
          <i class="fab fa-youtube fa-3x"></i>
        </a>
      <?php } ?>
    </div>
    <?php } ?>

    <?php if($pageContent['settings']['email_flag'] == 1) { ?>
    <hr>
    <div class="row justify-content-center margin-top-lg">
      <h2>Contact Us</h2>
    </div>
    <div class="row justify-content-center margin-top-md text-center">
      <a href="mailto:<?php echo $pageContent['settings']['email'];?>"><i class="far fa-envelope fa-3x"></i></a>
    </div>
    <div class="row justify-content-center margin-top-sm text-center">
      <p>Send contact emails to <a href="mailto:<?php echo $pageContent['settings']['email'];?>"><?php echo $pageContent['settings']['email'];?></a>.</p>
    </div>
    <?php } ?>
  </div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

<div class="card">
  <div class="card-header">
    <i class="fas fa-cogs"></i> Manage Settings
  </div>
  <div class="card-block">
    <a class="btn btn-primary" href="./pages/manage">Manage Settings</a>
  </div>
</div>

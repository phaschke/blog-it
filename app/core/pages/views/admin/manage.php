<?php
  include_once LAYOUTS_PATH . DS . "header.php";

  if(isset($pageContent['message'])){
    include_once LAYOUTS_PATH . DS . "errorMessage.part.php";
    return false;
  }
?>

<div class="container">

  <div class="row margin-bottom-md">
    <div class="col-sm-12">
      <a class="btn btn-outline-primary" href="<?php echo ADMIN_PANEL ?>">&#x2190; Back to Panel</a>
    </div>
  </div>

  <div class="row justify-content-center margin-bottom-md">
    <h2>Manage Website Settings</h2>
  </div>

  <div id="alerts-holder-top">
    <!-- Alert holder top-->
  </div>


  <form id="manageWebsiteForm">

    <div class="row margin-top-lg">
      <div class="col-sm-12">
        <h4>About Text</h4>
      </div>
    </div>
    <hr>

    <div class="form-group">
      <label for="aboutSummary">About Summary</label>
      <textarea class="form-control" id="aboutSummary" name="aboutSummary" placeholder="About Summary" rows="3"><?php if(isset($pageContent['settings']['about_text_short'])) echo $pageContent['settings']['about_text_short']; ?></textarea>
      <small id="aboutSummaryErrorHolder" class="text-danger"></small>
    </div>

    <div class="form-group">
      <label for="aboutBody">About Body</label>
      <textarea class="form-control" id="aboutBody" name="aboutBody" placeholder="About Body" rows="10"><?php if(isset($pageContent['settings']['about_text_long'])) echo $pageContent['settings']['about_text_long']; ?></textarea>
      <small id="aboutBodyErrorHolder" class="text-danger"></small>
    </div>

    <div class="row margin-top-lg">
      <div class="col-sm-12">
        <h4>Contact Email</h4>
      </div>
    </div>
    <hr>

    <div class="form-check">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="showContactEmail" name="showContactEmail" <?php if($pageContent['settings']['email_flag'] == 1) echo "checked";?>>
        Show Contact Email
      </label>
    </div>

    <div class="form-group">
      <label for="contactEmail">Contact Email</label>
      <input type="text" class="form-control" id="contactEmail" name="contactEmail" maxlength="511" placeholder="Contact Email" value="<?php if(isset($pageContent['settings']['email'])) echo $pageContent['settings']['email'];?>">
      <small id="contactEmailErrorHolder" class="text-danger"></small>
    </div>


    <div class="row margin-top-lg">
      <div class="col-sm-12">
        <h4>Social Media Profile Links</h4>
      </div>
    </div>
    <hr>

    <div class="form-check">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="showTwitter" name="showTwitter" <?php if($pageContent['settings']['twitter_flag'] == 1) echo "checked";?>>
        Show Twitter Link
      </label>
    </div>

    <div class="form-group">
      <label for="twitterLink">Twitter Link</label>
      <input type="text" class="form-control" id="twitterLink" name="twitterLink" maxlength="511" placeholder="Twitter Link" value="<?php if(isset($pageContent['settings']['twitter_link'])) echo $pageContent['settings']['twitter_link'];?>">
      <small id="twitterLinkErrorHolder" class="text-danger"></small>
    </div>

    <div class="form-check">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="showInstagram" name="showInstagram" <?php if($pageContent['settings']['instagram_flag'] == 1) echo "checked";?>>
        Show Instagram Link
      </label>
    </div>

    <div class="form-group">
      <label for="instagramLink">Instagram Link</label>
      <input type="text" class="form-control" id="instagramLink" name="instagramLink" maxlength="511" placeholder="Instagram Link" value="<?php if(isset($pageContent['settings']['instagram_link'])) echo $pageContent['settings']['instagram_link'];?>">
      <small id="instagramLinkErrorHolder" class="text-danger"></small>
    </div>

    <div class="form-check">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="showFacebook" name="showFacebook" <?php if($pageContent['settings']['facebook_flag'] == 1) echo "checked";?>>
        Show Facebook Link
      </label>
    </div>

    <div class="form-group">
      <label for="facebookLink">Facebook Link</label>
      <input type="text" class="form-control" id="facebookLink" name="facebookLink" maxlength="511" placeholder="Facebook Link" value="<?php if(isset($pageContent['settings']['facebook_link'])) echo $pageContent['settings']['facebook_link'];?>">
      <small id="facebookLinkErrorHolder" class="text-danger"></small>
    </div>

    <div class="form-check">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="showYoutube" name="showYoutube" <?php if($pageContent['settings']['youtube_flag'] == 1) echo "checked";?>>
        Show YouTube Link
      </label>
    </div>

    <div class="form-group">
      <label for="youtubeLink">YouTube Link</label>
      <input type="text" class="form-control" id="youtubeLink" name="youtubeLink" maxlength="511" placeholder="YouTube Link" value="<?php if(isset($pageContent['settings']['youtube_link'])) echo $pageContent['settings']['youtube_link'];?>">
      <small id="youtubeLinkErrorHolder" class="text-danger"></small>
    </div>


    <div class="form-group">
      <div class="pull-right">
        <button type="button" id="saveSettingsButton" class="btn btn-primary" onclick="BlogIt.Settings.saveSettings()">
          Save Settings
        </button>
      </div>

      <div class="margin-top-md" id="alerts-holder-bottom">
        <!-- Alert holder -->
      </div>
    </div>

  </form>

</div>

<script type="text/javascript">
//<![CDATA[
bkLib.onDomLoaded(function() {
    nicEditors.editors.push(
        new nicEditor({fullPanel:false, buttonList:['bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 'ul', 'fontSize', 'fontFamily', 'fontFormat', 'subscript', 'superscript',
          'strikethrough', 'removeformat', 'indent', 'outdent', 'hr', 'image', 'forecolor', 'bgcolor', 'link', 'unlink']}).panelInstance(
            document.getElementById("aboutBody")
        )
    );
    var editor = nicEditors.findEditor('aboutBody');
});
//]]>
</script>

<?php include_once LAYOUTS_PATH . DS . "footer.php" ?>

<?php
/**
 * Pages error messages
 *
 *
 */

return [

  'settings_save_error'     => 'There was an error saving the settings, please try again.',
  'settings_save_success'   => 'Successfully saved settings.',
  'get_settings_error'      => 'Error getting website settings.',
  'no_settings'             => 'Settings have not been set'

];

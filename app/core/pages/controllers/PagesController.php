<?php

class PagesController extends Controller
{
    private $pagesModel;
    private $pageResources = PAGES_PAGE_RESOURCES;

    public function __construct()
    {
        parent::__construct();

        $session = new Session();

        $DBConnection = new DBConnection();
        $pagesRepository = new PagesRepository($DBConnection);

        $this->pagesModel = new PagesModel($pagesRepository);
    }

    /**
     *
     * (Public Action)
     */
    public function about()
    {
        $pageContent = array();
        $pageContent['pageTitle'] = "About";
        $pageContent['session'] = $this->session;

        $pageContent['settings'] = $this->pagesModel->getSettings();

        if (($pageContent['settings']['twitter_flag'] == 1) || ($pageContent['settings']['instagram_flag'] == 1) || ($pageContent['settings']['facebook_flag'] == 1) || ($pageContent['settings']['youtube_flag'] == 1)) {
            $pageContent['showSocial'] = true;
        } else {
            $pageContent['showSocial'] = false;
        }

        require_once PAGES_VIEW_PATH.DS."about.php";
    }

    /**
     *
     * (Private Action)
     */
    public function manage()
    {
        $pageContent = array();
        $pageContent['pageTitle'] = "Manage Website Settings";
        $pageContent['pageResources'] = $this->pageResources;
        $pageContent['session'] = $this->session;

        $pageContent['settings'] = $this->pagesModel->getSettings();

        require_once ADMIN_PAGES_VIEW_PATH.DS."manage.php";
    }
}

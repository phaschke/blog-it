<?php
// Include parent class
require_once SERVICES_PARENT;
/**
 * Pages Service Class
 */
 Class PagesService extends Service {

   private $pagesModel;

   public function __construct() {

     $session = new Session();

     $DBConnection = new DBConnection();
     $pagesRepository = new PagesRepository($DBConnection);
     $this->pagesModel = new PagesModel($pagesRepository);

   }

   /**
    * Update website settings
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function update($parameters) {

     $settings = array();
     $settings['aboutSummary'] = ($parameters['aboutSummary'] !== null) ? $parameters['aboutSummary'] : "";
     $settings['aboutBody'] = ($parameters['aboutBody'] !== null) ? $parameters['aboutBody'] : "";
     $settings['showContactEmail'] = $parameters['showContactEmail'];
     $settings['contactEmail'] = ($parameters['contactEmail'] !== null) ? $parameters['contactEmail'] : "";
     $settings['showTwitter'] = $parameters['showTwitter'];
     $settings['twitterLink'] = ($parameters['twitterLink'] !== null) ? $parameters['twitterLink'] : "";
     $settings['showInstagram'] = $parameters['showInstagram'];
     $settings['instagramLink'] = ($parameters['instagramLink'] !== null) ? $parameters['instagramLink'] : "";
     $settings['showFacebook'] = $parameters['showFacebook'];
     $settings['facebookLink'] = ($parameters['facebookLink'] !== null) ? $parameters['facebookLink'] : "";
     $settings['showYoutube'] = $parameters['showYoutube'];
     $settings['youtubeLink'] = ($parameters['youtubeLink'] !== null) ? $parameters['youtubeLink'] : "";

     $result = $this->pagesModel->saveSettings($settings);
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }

   }


 }




?>

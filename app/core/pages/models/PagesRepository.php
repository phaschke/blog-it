<?php

/**
 * Pages Repository related functions
 */
Class PagesRepository {

  private $connection;

  public function __construct(DBConnection $DBConnection) {

    $this->connection = $DBConnection->getConnection();
  }

  /**
   * Insert or update website settings (Always into row 1)
   *
   * @param array settings
   * @throws PDOException
   */
  public function insertUpdateSettings($settings) {

    $sql = $this->connection->prepare('INSERT INTO about (setting_id, about_text_long, about_text_short, email_flag, email,
      twitter_flag, twitter_link, instagram_flag, instagram_link, facebook_flag, facebook_link, youtube_flag, youtube_link)
      VALUES (1, :aboutTextLong, :aboutTextShort, :emailFlag, :email, :twitterFlag, :twitterLink, :instagramFlag,
        :instagramLink, :facebookFlag, :facebookLink, :youtubeFlag, :youtubeLink)
      ON DUPLICATE KEY UPDATE setting_id=1, about_text_long=:aboutTextLong, about_text_short=:aboutTextShort,
        email_flag=:emailFlag, email=:email, twitter_flag=:twitterFlag, twitter_link=:twitterLink, instagram_flag=:instagramFlag,
        instagram_link=:instagramLink, facebook_flag=:facebookFlag, facebook_link=:facebookLink, youtube_flag=:youtubeFlag, youtube_link=
        :youtubeLink');

    $sql->execute(array(':aboutTextLong' => $settings['aboutBody'], ':aboutTextShort' => $settings['aboutSummary'],
      ':emailFlag' => $settings['showContactEmail'], ':email' => $settings['contactEmail'],
      ':twitterFlag' => $settings['showTwitter'], ':twitterLink' => $settings['twitterLink'],
      ':instagramFlag' => $settings['showInstagram'], ':instagramLink' => $settings['instagramLink'],
      ':facebookFlag' => $settings['showFacebook'], ':facebookLink' => $settings['facebookLink'],
      ':youtubeFlag' => $settings['showYoutube'], ':youtubeLink' => $settings['youtubeLink'] ));
  }

  /**
   * Get settings as an array
   */
  public function getSettings() {

    $sql = $this->connection->prepare('SELECT * from about WHERE setting_id=1');
    $sql->execute();
    $settings = $sql->fetch();
    
    return $settings;
  }


}





?>

<?php
/**
 * Pages Model Class
 */
 class PagesModel
 {
     //Properties

     private $pagesRepository;

     public function __construct(PagesRepository $pagesRepository)
     {
         $this->pagesRepository = $pagesRepository;
     }

     /**
      * Saves website settings to the database
      *
      * @param array settings array
      * @return string|array success message string or error array
      */
     public function saveSettings($settings)
     {
         try {
             // Verify input flags
             $this->verifyLinkFlag($settings['showContactEmail'], $settings['contactEmail']);
             $this->verifyLinkFlag($settings['showTwitter'], $settings['twitterLink']);
             $this->verifyLinkFlag($settings['showInstagram'], $settings['instagramLink']);
             $this->verifyLinkFlag($settings['showFacebook'], $settings['facebookLink']);
             $this->verifyLinkFlag($settings['showYoutube'], $settings['youtubeLink']);

             $this->pagesRepository->insertUpdateSettings($settings);
         } catch (Exception $e) {
             return (array('status' => 0,
          'userError' => getI18nMessage(PAGES_I18_MESSAGES, 'settings_save_error'),
          'consoleError'=>$e));
         }

         return getI18nMessage(PAGES_I18_MESSAGES, 'settings_save_success');
     }

     /**
      * Get settings from repo as an array
      *
      * @return array
      */
     public function getSettings()
     {
         try {
             $settings = $this->pagesRepository->getSettings();
             //if(count($settings) <= 0) {
             //   return getI18nMessage(PAGES_I18_MESSAGES, 'no_settings');
             //}
             return $settings;
         } catch (Exception $e) {
             return (array('status' => 0,
          'userError' => getI18nMessage(PAGES_I18_MESSAGES, 'get_settings_error'),
          'consoleError'=>$e));
         }
     }

     /**
      * Helper function to check if a flag is checked to ensure the corresponding link value is given.
      *
      * @param string flag
      * @param string correspondingValue
      *
      * @return true true if valid
      * @throws Exception
      */
     private function verifyLinkFlag($flag, $correspondingValue)
     {
         if ($flag === 1 && $correspondingValue == "") {
             throw new Exception('Link must be present when flag is 1.');
         }
         return true;
     }
 }

<?php

class Session {

  private static $instance;
  private static $isSessionStarted;

  function __construct() {

    require_once(__DIR__ . "/config.php");
    require_once(__DIR__ . "/CustomSessionHandler.php");

    $sessionHandler = new CustomSessionHandler();

    session_set_save_handler($sessionHandler, true);


  }

  public function startSession($sessionName=SESSION_NAME, $secure=USE_HTTPS) {

    // Get session cookie parameters
    $cookieParams = session_get_cookie_params();
    // Set the parameters
    session_set_cookie_params(time() + 3600, $cookieParams["path"], $cookieParams["domain"], $secure, HTTP_ONLY);

    // Change the session name
    session_name($sessionName);
    // Now we can start the session
    //if(!isset($_SESSION)) {
      //error_log("here");
        session_start();
    //}

    //setcookie($cookieName, $cookieValue, time() + 3600, '/', $rootDomain);

    // This line regenerates the session and deletes the old one.
    //if(!isset($_SESSION)) {
      session_regenerate_id(true);
    //}
  }

  /**
   *
   */
  public function destorySession() {
    return session_destroy();
  }

  /**
   *
   */
  public function setSessionVariable($key, $value) {
    return $_SESSION[$key] = $value;
  }

  /**
   *
   */
  public function getSessionVariable($key) {
    return $_SESSION[$key];
  }

  /**
   *
   */
  public function unsetSessionVariable($key) {
    unset($_SESSION[$key]);
  }

  /**
   * Return the status if there is a current session for the user
   *
   * @return boolean true is session has started, false if no session exists
   */
  public function isSessionStarted() {

    //error_log("Session Status ".session_status());
    if(!isset($_SESSION)) {

    $this->startSession();

      if(!isset($_SESSION['UUID']))  {
        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
        return false;
      }

    }

    return true;

    /*if (session_status() !== PHP_SESSION_ACTIVE) {
      return false;
    }
    return true;*/

    //return function_exists ( 'session_status' ) ? ( PHP_SESSION_ACTIVE == session_status () ) : ( ! empty ( session_id () ) );
    //if(session_status() == PHP_SESSION_ACTIVE) {
    //  error_log("SESSION EXISTS");
    //  return true;
    //}
    //error_log("SESSION DNE");
    //return false;
  }



}



?>

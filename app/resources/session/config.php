<?php
//======================================================================
// MAILER CONFIG
//======================================================================

  /**
   * Path To Mailer Module Folder
   */
  define("SESSION_PATH",  __DIR__);

  /**
   * Path To Session Class
   */
  define("SESSION", SESSION_PATH."/Session.php");

  /**
   * Include The Session Class
   */
  require_once SESSION;

  // Force the session to only use valid session IDs
  ini_set('session.use_strict_mode', 1);

  // Disallow access to session cookie by JavaScript.
  ini_set('session.cookie_httponly', 1);

  // Force the session to only use cookies, not URL variables.
  ini_set('session.use_only_cookies', 1);

  ini_set('session.gc_maxlifetime', 0);

  define("USE_HTTPS", false);
  // Make sure the session cookie is not accessible via javascript.
  define("HTTP_ONLY", true);
  define("SESSION_NAME", "_s");



?>

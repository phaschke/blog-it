<?php

class CustomSessionHandler implements SessionHandlerInterface {

  private $savePath;

  /**
   * Database connection;
   * @var DBConnection
   */
  private $DBConnection;

  /**
   * Open the connection to the database and remove old sessions
   *
   * @param string savePath
   * @param string sessionName
   * @return boolean
   */
  public function open($savePath, $sessionName) {

    require_once(DB_CONNECTION);
    // Connect to database
    $DBConnectionClass = new DBConnection();
    $this->DBConnection = $DBConnectionClass->getConnection();

    $limit = time() - (3600 * 24);
    $sql = $this->DBConnection->prepare('DELETE FROM users_sessions WHERE access < :ts');

  	$result = $sql->execute(array(':ts' => $limit));

    return $result;
  }

  /**
   * Close the connection to the database
   *
   * @return boolean
   */
  public function close() {
    // Close database connection
    $this->DBConnection = null;
    return true;
  }

  /**
   * Read a sessions data
   *
   * @param string id
   * @return string
   */
  public function read($id) {
    $sql = $this->DBConnection->prepare('SELECT data FROM users_sessions WHERE id = :id');
  	$sql->execute(array(':id' => $id));
  	$result = $sql->fetch(PDO::FETCH_ASSOC);

  	return (string) $result['data'];
  }

  /**
   * Write a sessions data
   *
   * @param string id
   * @param string data
   * @return boolean
   */
  public function write($id, $data) {

    $sql = $this->DBConnection->prepare('REPLACE INTO users_sessions (id, access, data) VALUES (:id, :access, :data)');
    return $sql->execute(array(':id' => $id, 'access' => time(), 'data' => $data));
  }

  /**
   * Destory a Session
   *
   * @param string id
   * @return boolean
   */
  public function destroy($id) {
    $sql = $this->DBConnection->prepare('DELETE FROM users_sessions WHERE id = :id');
    return $sql->execute(array(':id' => $id));
  }

  /**
   * Implement garbage collection of old sessions
   *
   * @param string lifeTime
   * @return boolean
   */
  public function gc($lifetime) {
    $sql = $this->DBConnection->prepare('DELETE FROM user_sessions WHERE access < :old');
    $result = $sql->execute(array(':old' => time() - intval($lifetime)));
  }


}




?>

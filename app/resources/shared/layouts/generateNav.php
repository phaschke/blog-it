<?php
$navData = include_once(NAV_DATA);
foreach($navData as $nav) {
  if($nav['display'] && !$nav['privileged']) {
    if( (($controller == $nav['controller']) || (in_array($controller, $nav['aliasControllers']))) && (($action == $nav['action']) || (in_array($action, $nav['aliasActions']))) ) {
      echo '<li class="nav-item"><a class="nav-link active" href="./'.$nav['controller'].'/'.$nav['action'].'">'.$nav['tag'].'</a></li>';
    } else {
      echo '<li class="nav-item"><a class="nav-link" href="./'.$nav['controller'].'/'.$nav['action'].'">'.$nav['tag'].'</a></li>';
    }
  }
  if($nav['display'] && $nav['privileged']) {
    if(isset($pageContent['session']) && $pageContent['session']['permission'] == 'c') {
      if( (($controller == $nav['controller']) || (in_array($controller, $nav['aliasControllers']))) && (($action == $nav['action']) || (in_array($action, $nav['aliasActions']))) ) {
        echo '<li class="nav-item"><a class="nav-link active" href="./'.$nav['controller'].'/'.$nav['action'].'">'.$nav['tag'].'</a></li>';
      } else {
        echo '<li class="nav-item"><a class="nav-link" href="./'.$nav['controller'].'/'.$nav['action'].'">'.$nav['tag'].'</a></li>';
      }
    }
  }
}
?>

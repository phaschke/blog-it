
  <footer class="blog-footer">
    <p>&copy; 2018 BrewLegit2Quit.com</p>
    <p>
      <?php
      $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

      $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
      echo '<a href="' . $escaped_url . '#">Back to Top</a>';
       ?>
    </p>
  </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->




    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="core/js/libraries/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

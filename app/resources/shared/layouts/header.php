<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- This tag MUST be included on every page -->
    <base href="<?php echo BASE_URL ?>"/>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="resources/images/favicon.ico">

    <!-- Use the set page title, or default -->
    <?php
      if($pageContent['pageTitle']) {
        echo "<title>".$pageContent['pageTitle']." | BlogIt!</title>";
      } else {
        echo "<title>BlogIt!</title>";
      }
    ?>

    <!-- Bootstrap core CSS -->
    <link href="resources/bootstrap-4.0.0-beta-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Default Style -->
    <link href="css/defaultstyle.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Oswald|Roboto+Condensed:300" rel="stylesheet">

    <script src="resources/jquery-3.2.1.min.js"></script>
    <script src="resources/popper.js"></script>

    <!-- <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script> -->
    <script src="resources/bootstrap-4.0.0-beta-dist/js/bootstrap.min.js"></script>

    <!-- Add Handlebars Template Source Files-->
    <script type="text/javascript" src="core/js/libraries/handlebars-v4.0.5.js"></script>
		<script type="text/javascript" src="core/js/libraries/handlebars-select-helper.js"></script>
    <script src="core/js/templates/templates.js"></script>

    <!-- Add Moment.js -->
    <script src="resources/moment/moment.min.js"></script>

    <!-- Add bootstrap-datetimepicker -->
    <script src="resources/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <link href="resources/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Add font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Add Main JS Library/Namespace -->
    <script type="text/javascript" src="core/js/libraries/Utils.js"></script>

    <!-- Add Default Pagination Libary -->
    <script type="text/javascript" src="core/js/libraries/Pagination.js"></script>

    <!-- Core javascript libraries -->
    <script type="text/javascript" src="core/js/libraries/Account.js"></script>
    <script type="text/javascript" src="core/js/libraries/Post.js"></script>
    <script type="text/javascript" src="core/js/libraries/Admin.js"></script>
    <script type="text/javascript" src="core/js/libraries/Settings.js"></script>


    <!-- Include any required resources as defined in the controller -->
    <?php
      if(isset($pageContent['pageResources']) && $pageContent['pageResources']) {
        foreach($pageContent['pageResources'] as $pageResource) {

          if($pageResource['type'] == 'css') {
            echo '<link href="'.$pageResource['path'].'" rel="stylesheet">';
          }
          if($pageResource['type'] == 'js') {
            echo '<script type="text/javascript" src="'.$pageResource['path'].'"></script>';
          }
        }
      }
    ?>
  </head>

  <nav class="nav-border navbar navbar-expand-lg navbar-custom bg-custom">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <?php
        $controller = $action = "";
        if(isset($_GET['controller'])) {
        $controller = $_GET['controller'];
        }
        if(isset($_GET['action'])) {
        $action = $_GET['action'];
        }
        ?>
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <?php
          // layout partcial to display navigation bar
          include_once(LAYOUTS_PATH.DS.'generateNav.php');
          ?>
        </li>
      </ul>
      <ul class="navbar-nav navbar-right">
        <?php
        if(!$pageContent['session']['status']){ ?>
        <li class="nav-item">
          <a class="nav-link" href="./accounts/login"><i class="fas fa-sign-in-alt"></i> Sign In</a>
        </li>
        <?php
        } else { ?>
        <li class="nav-item">
          <a class="nav-link" href="./accounts/settings">
            <i class="fas fa-user-circle"> </i> <?php echo $pageContent['session']['username'] ?>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./accounts/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
        </li>
        <?php } ?>
      </ul>
    </div>
  </nav>

  <div class="blog-header">
    <div class="container">
      <h1 class="blog-title">Blog It!</h1>
      <p class="lead blog-description">The simple and easy blogging platform.</p>
    </div>
  </div>

  <body>

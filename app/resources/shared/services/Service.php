<?php
/**
 * Services parent class.
 *
 */
 Class Service {

   public $defaultError;

   public function __construct() {

     // TODO: use i18n function
     $this->defaultError = "An error occurred, please try again.";
   }

   /**
    * Provide a test action.
    */
   public function test() {
     echo json_encode(array('success_flag' => 1,
       'success_message' => "Service is Working."));
     return true;
   }

   /**
    * Helper function to check if a resulting value is an error, if it is,
    * log the error and return an error to the service requestor, otherwise return true on success.
    *
    * @param array $result the resulting array from the model
    * @return boolean true on success
    */
   public function checkError($result) {

     if(is_array($result) && isset($result['status']) && ($result['status'] === 0)) {

       $error = "";
       if(isset($result['userError'])) {
         $error = $result['userError'];
       } else {
         // Default error
         $error = $this->defaultError;
       }
       if(LOG_ERRORS && isset($result['consoleError'])) {
         error_log($result['consoleError']);
       }

       // Return as an error
       echo json_encode(array('success_flag' => 0,
         'error_message' => $error));
       return false;
     }

     return true;
   }

   /**
    * Helper function to verify the required parameters were passed to the service.
    *
    * @param array $paramNames an array of the require parameter names.
    * @param array $parameters the array of parameters provided from the requestor
    *
    * @return boolean true if all parameters are set, array otherwise
    */
   public function verifyParameters($requiredParameters, $parameters) {

     foreach($requiredParameters as $parameter) {
       if(!isset($parameters[$parameter])) {
         return (array('status' => 0,
          //TODO: Replace with i18n message
            'userError' => "Error completing service call, please try again.",
            'consoleError'=>__CLASS__." ".__FUNCTION__.": Error: The service call is missing the required parameter, ".$parameter));
       }
       return true;
     }
   }

 }


?>

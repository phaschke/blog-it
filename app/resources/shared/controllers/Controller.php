<?php
class Controller {

  public $session = null;
  public $defaultError;

  public function __construct() {

    $session = new Session();
    if($session->isSessionStarted()){

      if((!isset($_SESSION['USERNAME']) || (empty($_SESSION['USERNAME'])))) {
        redirect(COMPLETE_REGISTRATION_URL);
      }

      $this->session = array();
      $this->session['status'] = true;

      $this->session['username'] = $session->getSessionVariable('USERNAME');
      $this->session['permission'] = $session->getSessionVariable('PERMISSION');
      $this->session['accountId'] = $session->getSessionVariable('UUID');
    }

    // TODO: use i18n function
    $this->defaultError = "An error occurred, please try again.";
  }

  /**
   * Helper function to check if a resulting value is an error, if it is,
   * log the error and return an error to the service requestor, otherwise return true on success.
   *
   * @param array $result the resulting array from the model
   * @return boolean true on success
   */
  public function checkError($result) {

    if(is_array($result) && isset($result['status']) && ($result['status'] === 0)) {

      $error = "";
      if(isset($result['userError'])) {
        $error = $result['userError'];
      } else {
        // Default error
        $error = $this->defaultError;
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }

      return false;
    }

    return true;
  }

}

<?php
/**
 * Validation i18n Messages
 *
 *
 */

return [

  'not_null'        => 'The input cannot be empty.',
  'min_length'      => 'The input must be at least :min length.',
  'max_length'      => 'The input can not exceed :max length.',
  'email_error'     => 'The email must be valid.',
  'password_match'  => 'The passwords must match.',

  'field_error'     => 'Input error with :field field, :error.',




];




?>

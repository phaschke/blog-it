<?php
/**
 * Service i18n Messages
 *
 */

return [

  'default_error'        => 'An error occurred, please try again.',
  'verify_params_error'  => 'Error completing service call, please try again.'

];

?>

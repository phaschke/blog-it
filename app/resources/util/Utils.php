<?php

/**
 * Redirect to a given URL using the PHP header Location
 *
 * @param string $url The url to redirect to
 */
function redirect($url) {
  header("Location: ".$url);
  die();
}

/**
 * Verify input given a mixed input and array of predefined rules
 *
 * @param mixed $input
 * @param array $rules array of rules to compare the input to
 */
function verifyInput($input, $rules) {

  foreach($rules as $rule) {
    if(is_array($rule)) {
      $attributes = $rule[1];
      $rule = $rule[0];
    }
    switch($rule) {
      case 'required':
        if(empty($input)) {
          return array('isError' => 0,
            'errorMessage' => "Error logging in, input cannot be empty.");
        }
      break;

      case 'email':
        if(!filter_var($input, FILTER_VALIDATE_EMAIL)) {
          return array('isError' => 0,
            'errorMessage' => "Error logging in, invalid email.");
        }
      break;

      case 'minLength':
        if(strlen($input) < $attributes) {
          return array('isError' => 0,
            'errorMessage' => "Input must at least length ".$attributes);
        }
      break;

      default:
        error_log(__FUNCTION__." Error, invalid rule called");
        return array('isError' => 0,
          'errorMessage' => "Invalid rule was called.");
    }
  }
  return true;
}

/**
 * Get defined error messages
 *
 * @param string $source the file path to the messages
 * @param string $message the message key
 * @param array $params optional parameters to be templated
 * @return string templated message
 */
function getI18nMessage($source, $messageKey, $params=[]) {
  // Default message
  $template = "Unknown Message.";

  try {
    $messages = include_once($source);
    $template = $messages[$messageKey];

    return strtr($template, $params);

  } catch (Exception $e) {

    if(LOG_ERRORS) {
      error_log($e->getMessage());
    }
    return $template;

  }

}

/**
 * Return the current datetime given a format
 *
 * @param string $format the datetime format
 * @return string the current formatted datetime
 */
function getCurrentTime($format) {
  $now = new DateTime();
  // Warning using default timezone will make the diff function not work as expected.
  //date_default_timezone_set($this->timezone);
  return $now->format($format);
  //'Y-m-d H:i:s'
}

/**
 * Helper function to get the total interval in a specific time
 *
 * @param int $interval
 * @param string $type
 */
function getTotalInterval($interval, $type){
    switch($type){
      case 'years':
          return $interval->format('%Y');
          break;
      case 'months':
          $years = $interval->format('%Y');
          $months = 0;
          if($years){
              $months += $years*12;
          }
          $months += $interval->format('%m');
          return $months;
          break;
      case 'days':
          return $interval->format('%a');
          break;
      case 'hours':
          $days = $interval->format('%a');
          $hours = 0;
          if($days){
              $hours += 24 * $days;
          }
          $hours += $interval->format('%H');
          return $hours;
          break;
      case 'minutes':
          $days = $interval->format('%a');
          $minutes = 0;
          if($days){
              $minutes += 24 * 60 * $days;
          }
          $hours = $interval->format('%H');
          if($hours){
              $minutes += 60 * $hours;
          }
          $minutes += $interval->format('%i');
          return $minutes;
          break;
      case 'seconds':
          $days = $interval->format('%a');
          $seconds = 0;
          if($days){
              $seconds += 24 * 60 * 60 * $days;
          }
          $hours = $interval->format('%H');
          if($hours){
              $seconds += 60 * 60 * $hours;
          }
          $minutes = $interval->format('%i');
          if($minutes){
              $seconds += 60 * $minutes;
          }
          $seconds += $interval->format('%s');
          return $seconds;
          break;
      case 'milliseconds':
          $days = $interval->format('%a');
          $seconds = 0;
          if($days){
              $seconds += 24 * 60 * 60 * $days;
          }
          $hours = $interval->format('%H');
          if($hours){
              $seconds += 60 * 60 * $hours;
          }
          $minutes = $interval->format('%i');
          if($minutes){
              $seconds += 60 * $minutes;
          }
          $seconds += $interval->format('%s');
          $milliseconds = $seconds * 1000;
          return $milliseconds;
          break;
      default:
          return NULL;
    }
  }

?>

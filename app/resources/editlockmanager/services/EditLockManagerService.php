<?php

// Include parent class
require_once SERVICES_PARENT;
/**
 * Pages Service Class
 */
 Class EditLockManagerService extends Service {

   private $editLockManager;

   public function __construct() {

     $session = new Session();
     $DBConnection = new DBConnection();

     $editLockManagerRepository = new EditLockManagerRepository($DBConnection);
     $this->editLockManager = new EditLockManager($session, $editLockManagerRepository);

   }

   /**
    * Remove a set lock for a user
    */
   public function unlockEditObject() {
     $this->editLockManager->unlock();
   }

 }


 ?>

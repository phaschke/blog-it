<?php

class EditLockManager {

  private $session;
  private $editLockManagerRepository;

  public function __construct(Session $session, EditLockManagerRepository $editLockManagerRepository) {

    $this->session = $session;
    $this->editLockManagerRepository = $editLockManagerRepository;

  }

  /**
   * Using the session object check if a session is started.
   */
  private function checkSession() {
    return $this->session->isSessionStarted();
  }

  /**
   * Get the current user's ID from the session and add the object type and id to the users session
   *
   * @param objectType required The object type being edited
   * @param objectId required The object id being edited
   */
  public function lock($objectType, $objectId) {

    if(!$this->session->isSessionStarted()) {
      return false;
    }

    try {
      $userId = $this->session->getSessionVariable("UUID");

      $this->editLockManagerRepository->removeLock($userId);
      $this->editLockManagerRepository->setLock($userId, $objectType, $objectId);
      return ($this->session->setSessionVariable('EDIT_TYPE', strtoupper($objectType)) && $this->session->setSessionVariable('EDIT_ID', $objectId));

    } catch (Exception $e) {

      return false;
    }
  }

  /**
   * Remove the user's session object edit lock variables when they leave the editing page
   *
   */
  public function unlock() {


    if(!$this->checkSession()) {
      return false;
    }

    try {
      $userId = $this->session->getSessionVariable("UUID");

      $this->editLockManagerRepository->removeLock($userId);
      $this->session->unsetSessionVariable('EDIT_TYPE');
      $this->session->unsetSessionVariable('EDIT_ID');
      return true;

    } catch (Exception $e) {

      return false;
    }



  }

  /**
   * Verify that the current user's session variables match the object being modified.
   *
   * @param objectType required The object type being edited
   * @param objectId required The object id being edited
   *
   * @return array|Boolean array of user id and object id if valid, false if mismatched
   */
  public function verifyLockStatus($objectType) {

    if(!$this->checkSession()) {
      return false;
    }

    if($this->session->getSessionVariable('EDIT_TYPE') != strtoupper($objectType)) {
      return false;
    }

    return array('EDIT_ID' => $this->session->getSessionVariable('EDIT_ID'),
                 'USER_ID' => $this->session->getSessionVariable('UUID'));

  }


}



?>

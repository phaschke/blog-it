<?php

/**
 * Edit Lock Manager Repository related functions
 */
Class EditLockManagerRepository {

  private $connection;

  public function __construct(DBConnection $DBConnection) {

    $this->connection = $DBConnection->getConnection();
  }

  /**
   * Lock a object
   *
   * @param string userId
   * @param string lockObjectType
   * @param int lockObjectId
   * @throws PDOException
   */
  public function setLock($userId, $lockObjectType, $lockObjectId) {

    $sql = $this->connection->prepare('INSERT INTO locks (user_id, lock_object_type, lock_object_id)
      values (:userId, :lockObjectType, :lockObjectId)');

    $sql->execute(array(':userId' => $userId, ':lockObjectType' => $lockObjectType,
      ':lockObjectId' => $lockObjectId ));
  }

  /**
   * Given lock details check if lock exists.
   * @param string userId
   * @param string lockObjectType
   * @param int lockObjectId
   * @return boolean
   * @throws PDOException
   */
  public function getLock($userId, $lockObjectType) {

    $sql = $this->connection->prepare('SELECT lock_object_id FROM locks
      WHERE user_id=:userId AND lockObjectType=:lockObjectType');

    $sql->execute(array(':userId' => $userId, ':lockObjectType' => $lockObjectType,
      ':lockObjectId' => $lockObjectId ));

  }

  /**
   * Remove a users lock given their uuid
   * @param string userId
   */
  public function removeLock($userId) {

    $sql = $this->connection->prepare('DELETE FROM locks WHERE user_id=:userId');

    $sql->execute(array(':userId' => $userId));

  }


}

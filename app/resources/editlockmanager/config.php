<?php
//======================================================================
// EDIT LOCK MANAGER
//======================================================================

   /**
    * Include The Edit Lock Manager Class
    */
   require_once __DIR__.DS."EditLockManager.php";

   /**
    * Include The Edit Lock Manager Repository Class
    */
   require_once __DIR__.DS."EditLockManagerRepository.php";
?>

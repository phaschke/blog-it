<?php

/**
 * Image class
 */
 Class Image {
   //Properties
   /**
     * @var int The unique image id
     */
   protected $imageId = null;

   /**
     * @var int The User ID who authored the brewery
     */
   protected $userId = null;

   /**
     * @var int The ID which of the object the image is associated with
     */
   protected $associatedObjectId = null;

   /**
     * @var string
     */
   protected $imageUploadDate = null;

   /**
     * @var string The type of image upload or linked
     */
   protected $imageType = null;

   /**
     * @var int
     */
   protected $imageOrder = null;

   /**
     * @var string
     */
   protected $imageFilePath = null;

   /**
     * @var string
     */
   protected $imageURL = null;

   /**
     * @var string
     */
   protected $imageTitle = null;

   /**
     * @var string
     */
   protected $imageDescription = null;

   /**
    * Set the brewery objects properties using supplied ArrayAccess
    *
    * @todo Add HTML Purifier
    *
    * @param assoc the posts properity values
    */
   public function __construct($data = array()) {

     if(isset($data['image_id'])) $this->setImageId($data['image_id']);
     if(isset($data['user_id'])) $this->setUserId($data['user_id']);
     if(isset($data['associated_object_id'])) $this->setAssociatedObjectId($data['associated_object_id']);
     if(isset($data['image_upload_date'])) $this->setImageUploadDate($data['image_upload_date']);
     if(isset($data['image_type'])) $this->setImageType($data['image_type']);
     if(isset($data['image_order'])) $this->setImageOrder($data['image_order']);
     if(isset($data['image_file_path'])) $this->setImageFilePath($data['image_file_path']);
     if(isset($data['image_url'])) $this->setImageURL($data['image_url']);
     if(isset($data['image_title'])) $this->setImageTitle($data['image_title']);
     if(isset($data['image_description'])) $this->setImageDescription($data['image_description']);
   }

   public function setImageId($imageId) {
     $this->imageId = (int) $imageId;
   }

   public function getImageId() {
     return $this->imageId;
   }

   public function setUserId($userId) {
     $this->userId = (int) $userId;
   }

   public function getUserId() {
     return $this->userId;
   }

   public function setAssociatedObjectId($associatedObjectId) {
     $this->associatedObjectId = $associatedObjectId;
   }

   public function getAssociatedObjectId() {
     return $this->associatedObjectId;
   }

   public function setImageUploadDate($imageUploadDate) {
     $this->imageUploadDate = $imageUploadDate;
   }

   public function getImageUploadDate($dateFormat=null) {
     $formattedDate = $this->imageUploadDate;
     if($dateFormat) {
       $formattedDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->imageUploadDate)->format($dateFormat);
     }
     return $formattedDate;
   }

   public function setImageType($imageType) {
     $this->imageType = trim($imageType);
   }

   public function getImageType() {
     return $this->imageType;
   }

   public function setImageOrder($imageOrder) {
     $this->imageOrder = (int)$imageOrder;
   }

   public function getImageOrder() {
     return $this->imageOrder;
   }

   public function setImageFilePath($imageFilePath) {
     $this->imageFilePath = trim($imageFilePath);
   }

   public function getImageFilePath() {
     return $this->imageFilePath;
   }

   public function setImageURL($imageURL) {
     $this->imageURL = trim($imageURL);
   }

   public function getImageURL() {
     return $this->imageURL;
   }

   public function setImageTitle($imageTitle) {
     $this->imageTitle = trim($imageTitle);
   }

   public function getImageTitle() {
     return $this->imageTitle;
   }

   public function setImageDescription($imageDescription) {
     $this->imageDescription = trim($imageDescription);
   }

   public function getImageDescription() {
     return $this->imageDescription;
   }

   public function returnAsArray($dateFormat=null) {
     return array(
       'image_id' => $this->getImageId(),
       'user_id' => $this->getUserId(),
       'associated_object_id' => $this->getAssociatedObjectId(),
       'image_upload_date' => $this->getImageUploadDate($dateFormat),
       'image_type' => $this->getImageType(),
       'image_order'=> $this->getImageOrder(),
       'image_file_path' => $this->getImageFilePath(),
       'image_url' => $this->getImageURL(),
       'image_title' => $this->getImageTitle(),
       'image_description' => $this->getImageDescription(),
     );
   }
 }


?>

<?php
/**
 * Image Handler error messages
 *
 *
 */

return [

  'invalid_file_type'        => 'The upload was an invalid file type, please try again.',
  'invalid_file_size'        => 'The upload image size was too large.',
  'image_upload_error'       => 'There was an error uploading the image, please try again.',





];




?>

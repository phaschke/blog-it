<?php
//======================================================================
// FILE HANDLER CONFIG
//======================================================================

   /**
    * Define relative path to the upload folder
    */
   define("RELATIVE_UPLOAD_FOLDER_PATH", __DIR__."/../../../public_html/uploads");

   define("FILE_HANDLER_PATH", __DIR__);

   define("IMAGE_HANDLER_I18_MESSAGES", FILE_HANDLER_PATH.DS."lang".DS."lang".DS."en".DS."imageHandler.php");

   /**
    * Include The Image Handler Class
    */
   require_once __DIR__.DS."ImageHandler.php";


   /**
    * Include The Object Image Class
    */
   require_once __DIR__.DS."Image.php";
?>

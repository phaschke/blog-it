<?php

Class ImageHandler {

  /**
   * Return/generate and return the upload folder for an image using the current date.
   *
   * @param string baseDirectory the base directory of the images
   * @return array of the path of the directory for the file to be moved into,
   *               year of the path,
   *               month of the path
   */
  public function getUploadFolder($baseDirectory) {

    // Get current month and year
    $month = date('m');
    $year = date('Y');

    $path = $baseDirectory.DS.$year.DS.$month;

    if (!file_exists($path)) {
      if(!mkdir($path, 0750, true)) {
        return false;
      }
    }

    return array('path' => $path,
                 'year' => $year,
                 'month' => $month
                );
  }

  /**
   * Return a unique filename for an image file.
   * Format is USERID_[randomGeneratedId]
   *
   * @param string the prefix for the randomly generated filename
   * @return string the generated unique file name
   */
  public function generateFileName($prefix) {
    return uniqid($prefix."_");
  }

  /**
   * Return a unique filename for an image file.
   *
   * @param int userId the user uploading the image
   * @return string the generated unique file name
   */
  public function checkFileType($file) {

    $fileTypes = array('jpeg','jpg','png','gif');
    $fileExt = pathinfo($file['name'], PATHINFO_EXTENSION);
    return in_array(strtolower($fileExt), $fileTypes);

  }

  /**
   * Verify the max size of the file
   *
   * @param file file the file to be checked
   * @param int userId the user uploading the image
   * @return boolean true is the file is of a valid size, false otherwise
   */
  public function checkFileSize($file, $maxSize) {
    return $file['size'] < $maxSize;
  }

  /**
   * Given a valid image file, move the file.
   *
   * @param string tempFileName
   * @param string destination
   * @param string newFileName
   */
  public function moveFile($file, $destination, $newFileName) {
    $file1 = $file;
    $fileName = $file['name'];
    $file = $file['tmp_name'];

    $exploded = explode('.',$fileName);
    $ext = $exploded[count($exploded) - 1];

    if ($ext == 'jpg' || $ext == 'jpeg') {
        $convertedImage = imagecreatefromjpeg($file);
    } else if ($ext == 'png') {
        $convertedImage = imagecreatefrompng($file);
    } else if ($ext == 'gif') {
        $convertedImage = imagecreatefromgif($file);
    } else if ($ext == 'bmp') {
        $convertedImage = imagecreatefrombmp($file);
    } else {
        return false;
    }

    // Quality 0 - 100
    $quality = 70;

    if(imagejpeg($convertedImage, $destination.DS.$newFileName.'.jpg', $quality)) {

      return imagedestroy($convertedImage);
    } else {
      return false;
    }

    //return move_uploaded_file($fileTmpName, $destination, $newFileName);
  }

  /**
   * Delete images given a path
   *
   * @param string imagePath
   *
   * @return boolean
   */
  public function deleteImage($imagePath) {
    
    return unlink(RELATIVE_UPLOAD_FOLDER_PATH.$imagePath);
  }



}





?>

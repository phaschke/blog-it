<?php

class Emailer {

  private $emailHost;
  private $emailSMTPAuth;
  private $emailUsername;
  private $emailPassword;
  private $emailSMTPSecure;
  private $emailPort;

  private $ErrorLogger;
  private $mailer;

  function __construct() {

    $config = parse_ini_file( CONFIG_FILE );

    $this->emailHost = $config['emailHost'];
    $this->emailSMTPAuth = $config['emailSMTPAuth'];
    $this->emailUsername = $config['emailUsername'];
    $this->emailPassword = $config['emailPassword'];
    $this->emailSMTPSecure = $config['emailSMTPSecure'];
    $this->emailPort = $config['emailPort'];

  }

  public function sendEmail($fromEmailAddress, $fromEmailName, $emailRecepient, $subject, $emailBody, $emailAltBody) {

    try {

      require_once __DIR__."/PHPMailer/PHPMailerAutoload.php";
      $this->mailer = new PHPMailer(true);

      $this->mailer->isSMTP();                        // Set mailer to use SMTP
  		$this->mailer->Host = $this->emailHost;				        // Specify main and backup SMTP servers
  		$this->mailer->SMTPAuth = $this->emailSMTPAuth;       // Enable SMTP authentication
  		$this->mailer->Username = $this->emailUsername;       // SMTP username
  		$this->mailer->Password = $this->emailPassword;      // SMTP password
  		$this->mailer->SMTPSecure = $this->emailSMTPSecure;   // Enable TLS encryption, `ssl` also accepted
  		$this->mailer->Port = $this->emailPort;

      //error_log("in email: FromEmailAddress=".$fromEmailAddress." FromEmailName=".$fromEmailName." EmailRecepient=".$emailRecepient." Subject=".$subject." EmailBody=".$emailBody." AltBody=".$emailAltBody);

      $this->mailer->setFrom($fromEmailAddress, $fromEmailName);

  		$this->mailer->addAddress($emailRecepient);     // Add a recipient

  		$this->mailer->Subject = $subject;

  		$this->mailer->Body = $emailBody;

  		$this->mailer->AltBody = $emailAltBody;

  		$result = $this->mailer->send();

  		return true;

    } catch(Exception $e) {

			error_log("Could not send email: ".$this->mailer->ErrorInfo);

      return false;

    }
  }

}




 ?>

<?php

// Read data from config file.
$config = parse_ini_file( CONFIG_FILE );

define("ACCOUNT_EMAIL_ADDRESS", $config['accountSendersEmailAddress']);
define("ACCOUNT_EMAIL_NAME", $config['accountSendersName']);

/**
 * Function to return the contents of a new user email with a verification Link
 *
 * @param string email the users emailField
 * @param string tokenForLink the token for the verification link
 * @return array the array of email contents
 */
function generateNewUserEmail($email, $tokenForLink) {

  $subject = "Verify your ".WEBSITE_NAME." account.";

  $body = "Please click the link below to verify your ".WEBSITE_NAME." account to join the fun!.<br>".
    "<a href=".BASE_URL."accounts/verify&email=".$email."&token=".$tokenForLink.">Click Here</a><br><br>
    <i>This has been an automated message, please do not reply to this email.</i>";

  $altBody = "Please click the link below to verify your ".WEBSITE_NAME." account to join the fun!. "
    .BASE_URL."accounts/verify&email=".$email."&token=".$tokenForLink.
    " This has been an automated message, please do not reply to this email.";

  return(array('senderEmailAddress' => ACCOUNT_EMAIL_ADDRESS,
               'senderEmailName' => ACCOUNT_EMAIL_NAME,
               'subject' => $subject,
               'body' => $body,
               'altBody' => $altBody
             ));
}

/**
 * Function to return the contents of a resend verification email
 *
 * @param string email the users emailField
 * @param string tokenForLink the token for the verification link
 * @return array the array of email contents
 */
function generateVerificationEmail($email, $tokenForLink) {

  $subject = "Verify your ".WEBSITE_NAME." account.";

  $body = "Please click the link below to verify your ".WEBSITE_NAME." account to join the fun!.<br>".
    "<a href=".BASE_URL."accounts/verify&email=".$email."&token=".$tokenForLink.">Click Here</a><br><br>
    <i>This has been an automated message, please do not reply to this email.</i>";

  $altBody = "Please click the link below to verify your ".WEBSITE_NAME." account to join the fun!. "
    .BASE_URL."accounts/verify&email=".$email."&token=".$tokenForLink.
    " This has been an automated message, please do not reply to this email.";

  return(array('senderEmailAddress' => ACCOUNT_EMAIL_ADDRESS,
               'senderEmailName' => ACCOUNT_EMAIL_NAME,
               'subject' => $subject,
               'body' => $body,
               'altBody' => $altBody
             ));
}

function generatePasswordResetEmail($email, $tokenForLink) {

  $subject = "Reset your ".WEBSITE_NAME." account password.";

  $body = "Please click the link below to change your ".WEBSITE_NAME." account password.<br>".
    "<a href=".BASE_URL."accounts/resetpassword&email=".$email."&token=".$tokenForLink.">Click Here</a><br>".
    "This reset token will expire in 15 minutes.<br><br>".
    "<i>This has been an automated message, please do not reply to this email.</i>";

  $altBody = "Please click the link below to change your ".WEBSITE_NAME." account to password. "
    .BASE_URL."accounts/resetpassword&email=".$email."&token=".$tokenForLink.
    " This reset token will expire in 15 minutes.".
    " This has been an automated message, please do not reply to this email.";

  return(array('senderEmailAddress' => ACCOUNT_EMAIL_ADDRESS,
               'senderEmailName' => ACCOUNT_EMAIL_NAME,
               'subject' => $subject,
               'body' => $body,
               'altBody' => $altBody
             ));
}



?>

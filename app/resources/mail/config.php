<?php
//======================================================================
// MAILER CONFIG
//======================================================================

  /**
   * Path To Mailer Module Folder
   */
   define("EMAIL_PATH",  __DIR__);

   /**
    * Path To Email Templates
    */
   define("EMAIL_TEMPLATES", EMAIL_PATH."/EmailTemplates.php");

   /**
    * Include The Mailer Class
    */
   require_once EMAIL_PATH."/Emailer.php";
?>

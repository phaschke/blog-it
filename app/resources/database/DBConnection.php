<?php

require_once(DB_CONNECTION_INTERFACE);

class DBConnection implements DBConnectionInterface {

  private $dbHost;
	private $dbUsername;
	private $dbPassword;
	private $dbName;
	private $connection;

  function __construct() {

    // Read data from config file.
    $config = parse_ini_file( CONFIG_FILE );

    $this->dbHost = $config['dbHost'];
    $this->dbUsername = $config['dbUsername'];
		$this->dbPassword = $config['dbPassword'];
		$this->dbName = $config['dbName'];

    // Try to connect to the database.
    if(!isset($this->connection)) {

      try{
        $conn = new PDO("mysql:host=$this->dbHost;dbname=$this->dbName", $this->dbUsername, $this->dbPassword);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->connection = $conn;

      } catch (PDOException $e) {
        //TODO: error log flag
        error_log(__CLASS__.":".__FUNCTION__.": Database connection failed: " . $e->getMessage());
        exit();
      }
    }
  }

  /**
   *
   */
  public function getConnection() {
    return $this->connection;
  }

}

?>

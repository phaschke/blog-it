<?php
//======================================================================
// DATABASE CONNECTION CONFIG
//======================================================================

  /**
   * Path To Database Module Folder
   */
   define("DB_PATH",  __DIR__);

  /**
   * Path To Database Connection Interface
   */
  define("DB_CONNECTION_INTERFACE", __DIR__."/DBConnectionInterface.php");

  /**
   * Path To Database Connection
   */
   define("DB_CONNECTION", DB_PATH."/DBConnection.php");

   /*
    * Load The Database Connection
    */
   require_once(DB_CONNECTION);

?>

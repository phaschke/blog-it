<?php

class Routes {

  private $controllers;
  private $modules = [];

  public function __construct() {

    // Load controller data
    $this->controllers = include_once(CONTROLLER_DATA);

    // List of valid and allowed controllers and their actions
    if(USE_GOOGLE_OAUTH) {
      array_push($this->controllers['oauth']['public'], 'google');
    }
    if(USE_FACEBOOK_OAUTH) {
      array_push($this->controllers['oauth']['public'], 'facebook');
    }

    // Load add on modules...
    $this->modules = $this->loadModules();
  }

  public function requestController($controller, $action) {

    // Check requested controller and action are allowed

    // Check if controller exists and action is not privileged
    if( (array_key_exists($controller, $this->controllers)) && (in_array($action, $this->controllers[$controller]['public'])) ) {
      $this->call($controller, $action);

    // Check if controller exists and if user is logged in
    } elseif( (array_key_exists($controller, $this->controllers)) && (in_array($action, $this->controllers[$controller]['user'])) ) {

      $session = new Session();
      if($session->isSessionStarted()){
        $this->call($controller, $action);
      } else {
        redirect(BASE_URL."./error/permission_denied");
      }

    // Check  if controller exists and action exists and is privileged
    // If privileged, check permissions, if denied, redirect to no permission error page
    } elseif( (array_key_exists($controller, $this->controllers)) && (in_array($action, $this->controllers[$controller]['privileged'])) ) {


      // Check privilege
      $session = new Session();

      $DBConnection = new DBConnection();
      $accountRepository = new AccountRepository($DBConnection);
      $accountAuthService = new AccountAuthService($accountRepository, $session);

      if($accountAuthService->checkPrivilege('c')) {
        $this->call($controller, $action);

      } else {
        redirect(BASE_URL."./error/permission_denied");
      }

    // Redirect to not found error page if controller and/or action does not exists
    } else {
      $this->call('error', 'not_found');
    }

  }

  private function call($controller, $action) {

    // Require the file that matches the controller namespace
    // TODO: FIX
    $controllerData = $this->controllers[$controller];

    if(isset($controllerData['type']) && $controllerData['type'] == 'core') {
      require_once CORE_FOLDER_PATH.DS.$controllerData['folder'].DS."controllers".DS.$controllerData['className'].".php";

    } else {
      require_once MODULES_PATH.DS.$controllerData['folder'].DS."controllers".DS.$controllerData['className'].".php";
    }

    $controllerClass = $controllerData['className'];

    $controller = new $controllerClass($this->modules);

    // Call the action
    $controller->{ $action }();

  }

  /**
   * Load addon module config and controller data
   */
  private function loadModules() {
    $modules = array_diff(scandir(MODULES_PATH), array('.', '..'));

    foreach($modules as $module) {

      try {
        $modulePath = MODULES_PATH . DS . $module;
        // Require module config
        if(file_exists(stream_resolve_include_path($modulePath . "/config.php"))) {
          require_once($modulePath . "/config.php");
        }

        // Require module actions (if exists)
        if(file_exists(stream_resolve_include_path($modulePath . "/controllers/controllerConfig.php"))) {

          $actions = include_once($modulePath . "/controllers/controllerConfig.php");
          $this->controllers += $actions;
        }

      } catch(Exception $e) {

        if(LOG_ERRORS) {
          error_log("Failed to load module: ".$module." Error: "+$e);
        }
      }
      return $modules;
    }
  }

}

?>

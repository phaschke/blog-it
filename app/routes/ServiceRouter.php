<?php

class ServiceRouter {

  private $services;

  public function __construct() {

    require_once("../../../app/config/config.php");

    // Load controller data
    $this->services = include_once(SERVICES_DATA);

    // Load add on modules...
    $this->loadModuleServices();
  }

  public function requestService($service, $action, $parameters) {
    // Check requested service and action are allowed
    // Check if service exists and action is not privileged
    if((array_key_exists($service, $this->services)) && (in_array($action, $this->services[$service]['public']))) {
      $this->call($service, $action, $parameters);

    // Check if controller exists and if user is logged in
    } elseif( (array_key_exists($service, $this->services)) && (in_array($action, $this->services[$service]['user'])) ) {

      $session = new Session();
      if($session->isSessionStarted()){
        $parameters['ACCOUNT_ID'] = $session->getSessionVariable('UUID');
        $this->call($service, $action, $parameters);
      } else {
        redirect(BASE_URL."./error/permission_denied");
      }

    // Check if service exists and action exists and is privileged
    // If privileged, check permissions, if denied, send replay with error message
    } elseif( (array_key_exists($service, $this->services)) && (in_array($action, $this->services[$service]['privileged']))) {

        // Check privilege
        $session = new Session();

        $DBConnection = new DBConnection();
        $accountRepository = new AccountRepository($DBConnection);
        $accountAuthService = new AccountAuthService($accountRepository, $session);

        if($accountAuthService->checkPrivilege('c')) {
          $parameters['ACCOUNT_ID'] = $session->getSessionVariable('UUID');
          $this->call($service, $action, $parameters);

        } else {
          echo json_encode(array('success_flag' => 0,
            'error_message' => "You are not authorized to call this service."));
          return true;
        }

      // Redirect to not found error page if controller and/or action does not exists
      } else {
        echo json_encode(array('success_flag' => 0,
          'error_message' => "The requested service and/or action does not exist."));
        return true;
      }
  }

  private function call($service, $action, $parameters) {

    // Require the file that matches the controller namespace
    $serviceData = $this->services[$service];

    if(isset($serviceData['type']) && $serviceData['type'] == 'core') {
      require_once(CORE_FOLDER_PATH.DS.$serviceData['folder'].DS."services".DS.$serviceData['className'].".php");

    } elseif(isset($serviceData['type']) && $serviceData['type'] == 'resource') {
      require_once(RESOURCE_PATH.DS.$serviceData['folder'].DS."services".DS.$serviceData['className'].".php");

    } else {
      require_once(MODULES_PATH.DS.$serviceData['folder'].DS."services".DS.$serviceData['className'].".php");
    }

    $serviceClass = $serviceData['className'];

    $service = new $serviceClass();
    // Call the action in the service with any parameters
    $service->{ $action }($parameters);

  }

  /**
   * Scan through add on modules and load services
   */
  private function loadModuleServices() {

    $modules = array_diff(scandir(MODULES_PATH), array('.', '..'));

    foreach($modules as $module) {

      try {
        $modulePath = MODULES_PATH . DS . $module;
        // Require module config
        if(file_exists(stream_resolve_include_path($modulePath . "/config.php"))) {
          require_once $modulePath . "/config.php";
        }

        // Require module service actions (if exists)
        if(file_exists(stream_resolve_include_path($modulePath . "/services/serviceConfig.php"))) {
          $actions = include_once $modulePath . "/services/serviceConfig.php";

          $this->services += $actions;
        }

      } catch(Exception $e) {
        if(LOG_ERRORS) {
          error_log("Failed to load module services: ".$module." Error: "+$e);
        }
      }
    }
  }


}




?>

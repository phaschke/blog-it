 <?php

/**
 * Brewery class
 */
 Class Brewery {
   //Properties
   /**
     * @var int The Post ID
     */
   protected $breweryId = null;

   /**
     * @var int The User ID who authored the brewery
     */
   protected $userId = null;

   /**
     * @var string The User who authored the brewery
     */
   protected $userName = null;

   /**
     * @var string
     */
   protected $breweryName = null;

   /**
     * @var string brewery logo URL
     */
   protected $breweryLogoUrl = null;

   /**
     * @var int includeInCount if the brewery should be including in the total count
     */
   protected $includeInCount = null;

   /**
     * @var int brewery rating
     */
   protected $rating = null;

   /**
     * @var string website the brewery website url
     */
   protected $website = null;

   /**
     * @var string notes brewery notes
     */
   protected $notes = null;

   /**
     * @var string a post tag to link
     */
   protected $linkedPostTag = null;

   /**
     * @var string
     */
   protected $dateVisited = null;

   /**
     * @var string
     */
   protected $breweryStreetAddress = null;

   /**
     * @var string
     */
   protected $breweryCity = null;

   /**
     * @var string
     */
   protected $breweryState = null;

   /**
     * @var string
     */
   protected $breweryZip = null;

   /**
     * @var string
     */
   protected $breweryCountry = null;

   /**
     * @var string
     */
   protected $breweryLat = null;

   /**
     * @var string
     */
   protected $breweryLng = null;

   /**
     * @var int
     */
   protected $breweryNumber = null;

   /**
    * @var array
    */
   protected $beerList;

   /**
    * @var array
    */
   protected $linkedPosts;

   /**
    * @var int
    */
   protected $visits;

   /**
    * Set the brewery objects properties using supplied ArrayAccess
    *
    * @todo Add HTML Purifier
    *
    * @param assoc the posts properity values
    */
   public function __construct($data = array()) {

     if(isset($data['brewery_id'])) $this->setBreweryId($data['brewery_id']);
     if(isset($data['user_id'])) $this->setUserId($data['user_id']);
     if(isset($data['user_name'])) $this->setUserName($data['user_name']);
     if(isset($data['brewery_name'])) $this->setBreweryName($data['brewery_name']);
     if(isset($data['logo_url'])) $this->setBreweryLogo($data['logo_url']);
     if(isset($data['include_in_count'])) $this->setIncludeInCount($data['include_in_count']);
     if(isset($data['brewery_rating'])) $this->setRating($data['brewery_rating']);
     if(isset($data['brewery_website'])) $this->setWebsite($data['brewery_website']);
     if(isset($data['brewery_notes'])) $this->setNotes($data['brewery_notes']);
     if(isset($data['linked_post_tag'])) $this->setLinkedPostTag($data['linked_post_tag']);
     if(isset($data['brewery_visit_date'])) $this->setDateVisited($data['brewery_visit_date']);
     if(isset($data['brewery_street_address'])) $this->setBreweryStreetAddress($data['brewery_street_address']);
     if(isset($data['brewery_city'])) $this->setBreweryCity($data['brewery_city']);
     if(isset($data['brewery_state'])) $this->setBreweryState($data['brewery_state']);
     if(isset($data['brewery_zip'])) $this->setBreweryZip($data['brewery_zip']);
     if(isset($data['brewery_country'])) $this->setBreweryCountry($data['brewery_country']);
     if(isset($data['brewery_lat'])) $this->setBreweryLat($data['brewery_lat']);
     if(isset($data['brewery_lng'])) $this->setBreweryLng($data['brewery_lng']);
     if(isset($data['brewery_number'])) $this->setBreweryNumber($data['brewery_number']);
     if(isset($data['beer_list'])) $this->setBeerList($data['beer_list']);
     if(isset($data['visits'])) $this->setNumberVisits($data['visits']);
   }

   public function setBreweryId($breweryId) {
     $this->breweryId = (int) $breweryId;
   }

   public function getBreweryId() {
     return $this->breweryId;
   }

   public function setUserId($userId) {
     $this->userId = (int) $userId;
   }

   public function getUserId() {
     return $this->userId;
   }

   public function setUserName($userName) {
     $this->userName = $userName;
   }

   public function getUserName() {
     return $this->userName;
   }

   public function setBreweryName($breweryName) {
     $this->breweryName = $breweryName;
   }

   public function getBreweryName() {
     return $this->breweryName;
   }

   public function setBreweryLogo($breweryLogoUrl) {
     $this->breweryLogoUrl = $breweryLogoUrl;
   }

   public function getBreweryLogo() {
     return $this->breweryLogoUrl;
   }

   public function setIncludeInCount($includeInCount) {
     $this->includeInCount = (int) $includeInCount;
   }

   public function getIncludeInCount() {
     return $this->includeInCount;
   }

   public function setRating($rating) {
     $this->rating = (int) $rating;
   }

   public function getRating() {
     return $this->rating;
   }

   public function setWebsite($website) {
     $this->website = $website;
   }

   public function getWebsite() {
     return $this->website;
   }

   public function setNotes($notes) {
     $this->notes = $notes;
   }

   public function getNotes() {
     return $this->notes;
   }

   public function setLinkedPostTag($linkedPostTag) {
     $this->linkedPostTag = $linkedPostTag;
   }

   public function getLinkedPostTag() {
     return $this->linkedPostTag;
   }

   public function setDateVisited($dateVisited) {
     $this->dateVisited = $dateVisited;
   }

   public function getDateVisited($dateFormat=null) {
     $formattedDate = $this->dateVisited;
     if($dateFormat) {
       $formattedDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->dateVisited)->format($dateFormat);
     }
     return $formattedDate;
   }

   public function setBreweryStreetAddress($breweryStreetAddress) {
     $this->breweryStreetAddress = $breweryStreetAddress;
   }

   public function getBreweryStreetAddress() {
     return $this->breweryStreetAddress;
   }

   public function setBreweryCity($breweryCity) {
     $this->breweryCity = $breweryCity;
   }

   public function getBreweryCity() {
     return $this->breweryCity;
   }

   public function setBreweryState($breweryState) {
     $this->breweryState = $breweryState;
   }

   public function getBreweryState() {
     return $this->breweryState;
   }

   public function setBreweryZip($breweryZip) {
     $this->breweryZip = $breweryZip;
   }

   public function getBreweryZip() {
     return $this->breweryZip;
   }

   public function setBreweryCountry($breweryCountry) {
     $this->breweryCountry = $breweryCountry;
   }

   public function getBreweryCountry() {
     return $this->breweryCountry;
   }

   public function setBreweryLat($breweryLat) {
     $this->breweryLat = $breweryLat;
   }

   public function getBreweryLat() {
     return $this->breweryLat;
   }

   public function setBreweryLng($breweryLng) {
     $this->breweryLng = $breweryLng;
   }

   public function getBreweryLng() {
     return $this->breweryLng;
   }

   public function setBreweryNumber($breweryNumber) {
     $this->breweryNumber = $breweryNumber;
   }

   public function getBreweryNumber() {
     return $this->breweryNumber;
   }

   public function setBeerList($beerList) {
     $this->beerList = $beerList;
   }

   public function getBeerList() {
     return $this->beerList;
   }

   public function setLinkedPosts($linkedPosts) {
     $this->linkedPosts = $linkedPosts;
   }

   public function getLinkedPosts($returnType="url") {
     if($returnType == "url") {
       $returnArray = array();
       foreach($this->linkedPosts as $linkedPost) {
         array_push($returnArray, array('url'=>POST_URL.$linkedPost['post_id'], 'title'=>$linkedPost['post_title']));
       }
       return $returnArray;
     }
     return $this->linkedPosts;
   }


   public function setNumberVisits($visits) {
     $this->visits = $visits;
   }

   public function getNumberVisits() {
     return $this->visits;
   }

   public function returnAsArray($dateFormat=null) {
     $data = array();
     if(null !== $this->getBreweryId()) $data['brewery_id'] = $this->getBreweryId();
     if(null !== $this->getUserId()) $data['user_id'] = $this->getUserId();
     if(null !== $this->getUserName()) $data['user_name'] = $this->getUserName();
     if(null !== $this->getBreweryName()) $data['brewery_name'] = $this->getBreweryName();
     if(null !== $this->getBreweryLogo()) $data['logo_url'] = $this->getBreweryLogo();
     if(null !== $this->getIncludeInCount()) $data['include_in_count'] = $this->getIncludeInCount();
     if(null !== $this->getRating()) $data['brewery_rating'] = $this->getRating();
     if(null !== $this->getWebsite()) $data['brewery_website'] = $this->getWebsite();
     if(null !== $this->getNotes()) $data['brewery_notes'] = $this->getNotes();
     if(null !== $this->getLinkedPostTag()) $data['linked_post_tag'] = $this->getLinkedPostTag();
     if(null !== $this->getDateVisited($dateFormat)) $data['brewery_visit_date'] = $this->getDateVisited($dateFormat);
     if(null !== $this->getBreweryStreetAddress()) $data['brewery_street_address'] = $this->getBreweryStreetAddress();
     if(null !== $this->getBreweryCity()) $data['brewery_city'] = $this->getBreweryCity();
     if(null !== $this->getBreweryState()) $data['brewery_state'] = $this->getBreweryState();
     if(null !== $this->getBreweryZip()) $data['brewery_zip'] = $this->getBreweryZip();
     if(null !== $this->getBreweryCountry()) $data['brewery_country'] = $this->getBreweryCountry();
     if(null !== $this->getBreweryLat()) $data['brewery_lat'] = $this->getBreweryLat();
     if(null !== $this->getBreweryLng()) $data['brewery_lng'] = $this->getBreweryLng();
     if(null !== $this->getBreweryNumber()) $data['brewery_number'] = $this->getBreweryNumber();
     if(null !== $this->getBeerList()) $data['beer_list'] = $this->getBeerList();
     if(null !== $this->getNumberVisits()) $data['visits'] = $this->getNumberVisits();

     return $data;

     /*return array(
       'brewery_id' => $this->getBreweryId(),
       'user_id' => $this->getUserId(),
       'user_name' => $this->getUserName(),
       'brewery_name' => $this->getBreweryName(),
       'logo_url' => $this->getBreweryLogo(),
       'include_in_count' => $this->getIncludeInCount(),
       'brewery_rating' => $this->getRating(),
       'brewery_website' => $this->getWebsite(),
       'brewery_notes' => $this->getNotes(),
       'brewery_visit_date' => $this->getDateVisited($dateFormat),
       'brewery_street_address' => $this->getBreweryStreetAddress(),
       'brewery_city' => $this->getBreweryCity(),
       'brewery_state' => $this->getBreweryState(),
       'brewery_zip' => $this->getBreweryZip(),
       'brewery_country' => $this->getBreweryCountry(),
       'brewery_lat' => $this->getBreweryLat(),
       'brewery_lng' => $this->getBreweryLng(),
       'brewery_number' => $this->getBreweryNumber(),
       'beer_list' => $this->getBeerList(),
       'visits' => $this->getNumberVisits()
     );*/
   }

 }



?>

<?php

/**
* Beer class
*/
Class Beer {

  /**
    * @var int Beer ID
    */
  protected $beerId = null;

  /**
    * @var int The Brewery ID
    */
  protected $breweryId = null;

  /**
    * @var string The Beer ID
    */
  protected $beerName = null;

  /**
    * @var string The Beer Notes
    */
  protected $beerNotes = null;

  public function __construct($data = array()) {

    if(isset($data['beer_id'])) $this->setBeerId($data['beer_id']);
    if(isset($data['brewery_id'])) $this->setBreweryId($data['brewery_id']);
    if(isset($data['beer_name'])) $this->setBeerName($data['beer_name']);
    if(isset($data['beer_notes'])) $this->setBeerNotes($data['beer_notes']);

  }

  public function setBeerId($beerId) {
    $this->beerId = (int) $beerId;
  }

  public function getBeerId() {
    return $this->beerId;
  }

  public function setBreweryId($breweryId) {
    $this->breweryId = (int) $breweryId;
  }

  public function getBreweryId() {
    return $this->breweryId;
  }

  public function setBeerName($beerName) {
    $this->beerName = $beerName;
  }

  public function getBeerName() {
    return $this->beerName;
  }

  public function setBeerNotes($beerNotes) {
    $this->beerNotes = $beerNotes;
  }

  public function getBeerNotes() {
    return $this->beerNotes;
  }

  public function returnAsArray() {
    return array(
      'beer_id' => $this->getBeerId(),
      'brewery_id' => $this->getBreweryId(),
      'beer_name' => $this->getBeerName(),
      'beer_notes' => $this->getBeerNotes(),
    );
  }

}

?>

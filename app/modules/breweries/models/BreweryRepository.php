<?php

/**
 * Post Repository related functions
 */
Class BreweryRepository {

  private $connection;

  public function __construct(DBConnection $DBConnection) {

    $this->connection = $DBConnection->getConnection();
  }

  /**
   * Insert a brewery
   *
   * @param brewery a Brewery object
   * @return int newly inserted brewery ID
   * @throws PDOException
   */
  public function insertBrewery(Brewery $brewery) {

    try {
      // Begin a transaction, turning off autocommit
      $this->connection->beginTransaction();

      $sql = $this->connection->prepare('INSERT INTO breweries (user_id, brewery_name, include_in_count, brewery_rating,
        brewery_website, brewery_notes, linked_post_tag, brewery_visit_date, brewery_lat, brewery_lng, brewery_street_address, brewery_city,
        brewery_state, brewery_zip, brewery_country)
        VALUES (:userId, :breweryName, :includeInCount, :rating, :website, :notes, :linkedPostTag, :dateVisited, :breweryLat, :breweryLng,
          :breweryStreetAddress, :breweryCity, :breweryState, :breweryZip, :breweryCountry)');

      $sql->execute(array(':userId' => $brewery->getUserId(), ':breweryName' => $brewery->getBreweryName(),
        ':includeInCount' => $brewery->getIncludeInCount(), ':rating' => $brewery->getRating(),
        ':website' => $brewery->getWebsite(), ':notes' => $brewery->getNotes(), ':linkedPostTag' => $brewery->getLinkedPostTag(),
        ':dateVisited' => $brewery->getDateVisited(), ':breweryLat' => $brewery->getBreweryLat(),
        ':breweryLng' => $brewery->getBreweryLng(), ':breweryStreetAddress' => $brewery->getBreweryStreetAddress(),
        ':breweryCity' => $brewery->getBreweryCity(), ':breweryState' => $brewery->getBreweryState(),
        ':breweryZip' => $brewery->getBreweryZip(), ':breweryCountry' => $brewery->getBreweryCountry() ));

      //Retrieve the id of the newly inserted brewery
      $breweryId = $this->connection->lastInsertId();

      $sql = $this->connection->prepare('INSERT INTO breweries_beers (brewery_id, beer_name, beer_notes)
        VALUES (:breweryId, :beerName, :beerNotes)');

      foreach($brewery->getBeerList() as $beer) {
        $sql->execute(array(':breweryId' => $breweryId, ':beerName' => $beer['beer_name'],
          ':beerNotes' => $beer['beer_notes'] ));
      }

      // No error, commit queries
      $this->connection->commit();

      return $breweryId;

    } catch(Exception $e) {
      // Error with queries rollback to begin transation
      $this->connection->rollBack();

      throw new Exception($e);
    }
  }


  /**
   * Update a brewery
   *
   * @param Brewery brewery a Brewery object
   * @throws PDOException
   */
  public function updateBrewery(Brewery $brewery) {

    try {
      // Begin a transaction, turning off autocommit
      $this->connection->beginTransaction();

      $sql = $this->connection->prepare('UPDATE breweries SET user_id = :userId, brewery_name = :breweryName,
        include_in_count = :includeInCount, brewery_rating = :rating, brewery_website = :website,
        brewery_notes = :notes, linked_post_tag = :linkedPostTag, brewery_visit_date =  :dateVisited, brewery_lat = :breweryLat,
        brewery_lng = :breweryLng, brewery_street_address = :breweryStreetAddress, brewery_city = :breweryCity,
        brewery_state = :breweryState, brewery_zip = :breweryZip, brewery_country = :breweryCountry
        WHERE brewery_id = :breweryId');
      $sql->execute(array(':userId' => $brewery->getUserId(), ':breweryName' => $brewery->getBreweryName(),
        ':includeInCount' => $brewery->getIncludeInCount(), ':rating' => $brewery->getRating(),
        ':website' => $brewery->getWebsite(), ':notes' => $brewery->getNotes(), ':linkedPostTag' => $brewery->getLinkedPostTag(),
        ':dateVisited' => $brewery->getDateVisited(), ':breweryLat' => $brewery->getBreweryLat(),
        ':breweryLng' => $brewery->getBreweryLng(), ':breweryStreetAddress' => $brewery->getBreweryStreetAddress(),
        ':breweryCity' => $brewery->getBreweryCity(), ':breweryState' => $brewery->getBreweryState(),
        ':breweryZip' => $brewery->getBreweryZip(), ':breweryCountry' => $brewery->getBreweryCountry(),
        ':breweryId' => $brewery->getBreweryId() ));

        $updateSql = $this->connection->prepare('UPDATE breweries_beers SET beer_name = :beerName, beer_notes = :beerNotes
          WHERE beer_id = :beerId');
        $insertSql = $this->connection->prepare('INSERT INTO breweries_beers (brewery_id, beer_name, beer_notes)
          VALUES (:breweryId, :beerName, :beerNotes)');
        $deleteSql = $this->connection->prepare('DELETE FROM breweries_beers WHERE beer_id = :beerId');

        foreach($brewery->getBeerList() as $beer) {

          if(isset($beer['delete']) && isset($beer['beer_id']) ) {

            $deleteSql->execute(array(':beerId' => $beer['beer_id'] ));

          } else if(isset($beer['beer_id'])) {

            $updateSql->execute(array(':beerName' => $beer['beer_name'], ':beerNotes' => $beer['beer_notes'],
              ':beerId' => $beer['beer_id'] ));

          } else {

            $insertSql->execute(array(':breweryId' => $brewery->getBreweryId(), ':beerName' => $beer['beer_name'],
              ':beerNotes' => $beer['beer_notes'] ));
          }
        }

        // No error, commit queries
        $this->connection->commit();

    } catch(Exception $e) {
      // Error with queries rollback to begin transation
      $this->connection->rollBack();

      throw new Exception($e);
    }
  }

  /**
   * Return all unique lat lng pairs. If repeats add visits > 1
   */
  public function getMapMarkers() {

    $sql = $this->connection->prepare('SELECT T1.lat AS brewery_lat, T1.lng AS brewery_lng, T2.brewery_id, T2.brewery_name, T1.visits
      FROM (
        SELECT count(*) AS visits, ANY_VALUE(TRUNCATE(brewery_lat, 4)) AS lat, ANY_VALUE(TRUNCATE(brewery_lng, 4)) AS lng
        FROM breweries GROUP BY TRUNCATE(brewery_lat, 4), TRUNCATE(brewery_lng, 4)) AS T1
      JOIN (
        SELECT ANY_VALUE(brewery_id) AS brewery_id, ANY_VALUE(brewery_name) AS brewery_name, ANY_VALUE(TRUNCATE(brewery_lat, 4)) AS lat, ANY_VALUE(TRUNCATE(brewery_lng, 4)) AS lng
        FROM breweries
        GROUP BY TRUNCATE(brewery_lat, 4), TRUNCATE(brewery_lng, 4)) AS T2
      ON T1.lat = T2.lat AND T1.lng = T2.lng');

    $sql->execute();
    $breweries = array();

    while($row = $sql->fetch()) {
      $breweries[] = new Brewery($row);
    }

    return (array('breweries' => $breweries));

  }

  /**
   * Get a breweries beer list
   *
   * @param int breweryId Brewery id
   * @return array breweries beer list
   *
   * @throws PDOException
   */
  public function getBreweryBeerList($breweryId) {

    $sql = $this->connection->prepare('SELECT *
      FROM breweries_beers
      WHERE brewery_id = :breweryId');
    $sql->execute(array(':breweryId' => $breweryId));

    $beerList = array();
    while($beer = $sql->fetch()) {
      $beerList[] = $beer;
    }

    return $beerList;

  }

  /**
   * Get a list of linked post IDs
   *
   * @param string linkedPostTag
   * @return array
   *
   * @throws PDOException
   */
  public function getLinkedPosts($linkedPostTag) {

    $sql = $this->connection->prepare('SELECT T1.post_id, T2.post_title
      FROM posts_tags as T1
      INNER JOIN posts AS T2 ON T1.post_id = T2.post_id
      WHERE tag_text = :linkedPostTag AND T2.post_status = "published"');
    $sql->execute(array(':linkedPostTag' => $linkedPostTag));

    $linkedPosts = array();
    while($post = $sql->fetch()) {
      $linkedPosts[] = array('post_id'=>$post['post_id'], 'post_title'=>$post['post_title']);
    }

    return $linkedPosts;

  }

  /**
   * Get a brewery
   *
   * @param int breweryId Brewery id
   * @return Brewery brewery Brewery object
   *
   * @throws PDOException
   */
  public function getBrewery($breweryId) {

    $sql = $this->connection->prepare('SELECT T1.*, T2.user_name, T3.logo_url
      FROM breweries AS T1
      INNER JOIN users AS T2 ON T1.user_id=T2.user_id
      LEFT JOIN breweries_logos AS T3 ON T1.brewery_id = T3.associated_object_id
      WHERE brewery_id = :breweryId');
    $sql->execute(array(':breweryId' => $breweryId));

    $row = $sql->fetch();

    if($sql->rowCount() < 1) {
      return false;
    }

    $brewery = new Brewery($row);

    $brewery->setBeerList($this->getBreweryBeerList($breweryId));
    $brewery->setLinkedPosts($this->getLinkedPosts($brewery->getLinkedPostTag()));

    return $brewery;
  }

  /**
   * Get breweries
   *
   * @param int numEntries
   * @param int offset
   * @return array Array of brewery objects
   *
   * @throws PDOException
   */
  public function getBreweries($numRecords=10000, $offset=0, $searchParameters=[]) {

    // Dynamically generate WHERE sql query
    $whereClause = "";
    if(count($searchParameters) > 0) {
      if(isset($searchParameters['brewery_lat']) && $searchParameters['brewery_lat'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " TRUNCATE(brewery_lat, 4) = :brewery_lat";
      }
      if(isset($searchParameters['brewery_lng']) && $searchParameters['brewery_lng'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " TRUNCATE(brewery_lng, 4) = :brewery_lng";
      }
      if(isset($searchParameters['brewery_name']) && $searchParameters['brewery_name'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " brewery_name LIKE :brewery_name";
      }
      if(isset($searchParameters['brewery_state']) && $searchParameters['brewery_state'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " brewery_state LIKE :brewery_state";
      }
      if(isset($searchParameters['brewery_city']) && $searchParameters['brewery_city'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " brewery_city LIKE :brewery_city";
      }
      if(isset($searchParameters['brewery_country']) && $searchParameters['brewery_country'] != "") {
        if($whereClause != "") $whereClause .= " AND";
        $whereClause .= " brewery_country LIKE :brewery_country";
      }
    }

    if($whereClause != "") {
      $sql = $this->connection->prepare('SELECT COUNT(*) AS count FROM breweries WHERE include_in_count = 1 AND'.$whereClause);
    } else {
      $sql = $this->connection->prepare('SELECT COUNT(*) AS count FROM breweries WHERE include_in_count = 1');
    }
    if(isset($searchParameters['brewery_lat']) && $searchParameters['brewery_lat'] != "") $sql->bindValue(":brewery_lat", trim($searchParameters['brewery_lat']), PDO::PARAM_STR);
    if(isset($searchParameters['brewery_lng']) && $searchParameters['brewery_lng'] != "") $sql->bindValue(":brewery_lng", trim($searchParameters['brewery_lng']), PDO::PARAM_STR);
    if(isset($searchParameters['brewery_name']) && $searchParameters['brewery_name'] != "") $sql->bindValue(":brewery_name", trim($searchParameters['brewery_name']).'%', PDO::PARAM_STR);
    if(isset($searchParameters['brewery_state']) && $searchParameters['brewery_state'] != "") $sql->bindValue(":brewery_state", trim($searchParameters['brewery_state']).'%', PDO::PARAM_STR);
    if(isset($searchParameters['brewery_city']) && $searchParameters['brewery_city'] != "") $sql->bindValue(":brewery_city", trim($searchParameters['brewery_city']).'%', PDO::PARAM_STR);
    if(isset($searchParameters['brewery_country']) && $searchParameters['brewery_country'] != "") $sql->bindValue(":brewery_country", trim($searchParameters['brewery_country']).'%', PDO::PARAM_STR);


    $sql->execute();
    $result = $sql->fetch();
    $totalCount = $result['count'];

    if($whereClause != "") {
      $whereClause = "WHERE".$whereClause;
    }

    $sql = $this->connection->prepare('SELECT T1.*, T2.logo_url
      FROM breweries AS T1
      LEFT JOIN breweries_logos AS T2 ON T1.brewery_id = T2.associated_object_id '.$whereClause.' ORDER BY brewery_visit_date DESC LIMIT :numRecords OFFSET :numOffset');

      if(isset($searchParameters['brewery_lat']) && $searchParameters['brewery_lat'] != "") $sql->bindValue(":brewery_lat", trim($searchParameters['brewery_lat']), PDO::PARAM_STR);
      if(isset($searchParameters['brewery_lng']) && $searchParameters['brewery_lng'] != "") $sql->bindValue(":brewery_lng", trim($searchParameters['brewery_lng']), PDO::PARAM_STR);
      if(isset($searchParameters['brewery_name']) && $searchParameters['brewery_name'] != "") $sql->bindValue(":brewery_name", trim($searchParameters['brewery_name']).'%', PDO::PARAM_STR);
      if(isset($searchParameters['brewery_state']) && $searchParameters['brewery_state'] != "") $sql->bindValue(":brewery_state", trim($searchParameters['brewery_state']).'%', PDO::PARAM_STR);
      if(isset($searchParameters['brewery_city']) && $searchParameters['brewery_city'] != "") $sql->bindValue(":brewery_city", trim($searchParameters['brewery_city']).'%', PDO::PARAM_STR);
      if(isset($searchParameters['brewery_country']) && $searchParameters['brewery_country'] != "") $sql->bindValue(":brewery_country", trim($searchParameters['brewery_country']).'%', PDO::PARAM_STR);
    $sql->bindValue(":numRecords", $numRecords, PDO::PARAM_INT);
    $sql->bindValue(":numOffset", $offset, PDO::PARAM_INT);

    $sql->execute();
    $breweries = array();

    if($whereClause == "") {
      // Do not include brewery number if custom search
      $i=$totalCount-$offset;
      while($row = $sql->fetch()) {
        if($row['include_in_count'] == 1) {
          $row['brewery_number'] = $i;
          $i--;
        }
        $breweries[] = new Brewery($row);
      }

    } else {
      while($row = $sql->fetch()) {
        $breweries[] = new Brewery($row);
      }

    }


    return (array('count' => $totalCount, 'breweries' => $breweries));

  }

  /**
   * Get the total record count from the brewery record table
   *
   * @return int
   * @throws PDOException
   */
  public function getBreweriesCount() {
    $sql = 'SELECT COUNT(*) FROM breweries';
    $totalRows = $this->connection->query($sql)->fetch();

    return $totalRows[0];
  }

  /**
   * Delete a brewery
   *
   * @param int breweryId Brewery id
   * @throws PDOException
   */
   public function deleteBrewery($breweryId) {
     $sql = $this->connection->prepare('DELETE FROM breweries WHERE brewery_id = :breweryId');
     $sql->execute(array(':breweryId' => $breweryId ));
   }

   /**
    * Return logo given a brewery id
    *
    * @param int id brewery id
    * @return Image
    * @throws PDOException
    */
    public function getLogo($breweryId) {

      $sql = $this->connection->prepare('SELECT logo_id AS image_id, logo_type AS image_type, logo_file_path AS image_file_path, logo_url AS image_url
        FROM breweries_logos
        WHERE associated_object_id = :breweryId');

      $sql->execute(array(':breweryId' => $breweryId) );

      $row = $sql->fetch();

      // Check if there were any records to pull
      if($sql->rowCount() != 0) {
        return new Image($row);
      }
      return false;

    }

    /**
     * Update a brewery logo
     *
     * @param Image image the image object to be updated
     * @throws PDOException
     */
     public function addLogo(Image $image) {

       $sql = $this->connection->prepare('INSERT INTO breweries_logos (associated_object_id, logo_type, logo_file_path, logo_url)
        VALUES (:breweryId, :logoType, :logoFilePath, :logoURL)');

       $sql->execute(array(':breweryId' => $image->getAssociatedObjectId(), ':logoType' => $image->getImageType(),
          ':logoFilePath' => $image->getImageFilePath(),':logoURL' => $image->getImageURL()));
     }

   /**
    * Update a brewery logo
    *
    * @param Image image the image object to be updated
    * @throws PDOException
    */
    public function updateLogo(Image $image) {

      $sql = $this->connection->prepare('UPDATE breweries_logos
        SET logo_type = :logoType, logo_file_path = :logoFilePath, logo_url = :logoURL
        WHERE associated_object_id = :breweryId');

      $sql->execute(array(':logoType' => $image->getImageType(), ':logoFilePath' => $image->getImageFilePath(),
        ':logoURL' => $image->getImageURL(), ':breweryId' => $image->getAssociatedObjectId()));
    }

    /**
     * Delete a brewery logo
     *
     * @param int breweryId the image object to be deleted
     * @throws PDOException
     */
     public function deleteLogo($breweryId) {

       $sql = $this->connection->prepare('DELETE FROM breweries_logos
         WHERE associated_object_id = :breweryId');

       $sql->execute(array(':breweryId' => $breweryId ));
     }


   /**
    * Insert a brewery image
    *
    * @param Image image the image object to be inserted
    * @throws PDOException
    */
    public function insertImage(Image $image) {

      $sql = $this->connection->prepare('INSERT INTO breweries_images (user_id, associated_object_id, image_upload_date,
        image_type, image_order, image_file_path, image_url, image_title, image_description)
        VALUES (:userId, :associatedObjectId, :imageUploadDate, :imageType, :imageOrder, :imageFilePath, :imageURL, :imageTitle, :imageDescription)');
      $sql->execute(array(':userId' => $image->getUserId(), ':associatedObjectId' => $image->getAssociatedObjectId(),
        ':imageUploadDate' => $image->getImageUploadDate(), ':imageType' => $image->getImageType(),
        ':imageOrder' => $image->getImageOrder(), ':imageFilePath' => $image->getImageFilePath(),
        ':imageURL' => $image->getImageURL(), ':imageTitle' => $image->getImageTitle(),
        ':imageDescription' => $image->getImageDescription() ));
    }

    /**
     * Update a brewery image
     *
     * @param Image image the image object to be updated
     * @throws PDOException
     */
     public function updateImage(Image $image) {

       $sql = $this->connection->prepare('UPDATE breweries_images
         SET image_order = :imageOrder, image_title = :imageTitle, image_description = :imageDescription
         WHERE image_id = :imageId');

       $sql->execute(array(':imageOrder' => $image->getImageOrder(), ':imageTitle' => $image->getImageTitle(),
         ':imageDescription' => $image->getImageDescription(), ':imageId' => $image->getImageId() ));
     }

     /**
      * Delete a Image
      *
      * @param int imageId the ID of the image to remove
      * @throws PDOException
      */
      public function deleteImage($imageId, $breweryId) {
        $sql = $this->connection->prepare('DELETE FROM breweries_images WHERE image_id = :imageId AND associated_object_id = :breweryId');
        $sql->execute(array(':imageId' => $imageId, ':breweryId' => $breweryId ));
      }

      /**
       * Delete all images assigned to a brewery
       *
       * @param int imageId the ID of the image to remove
       * @throws PDOException
       */
       public function deleteImages($breweryId) {
         $sql = $this->connection->prepare('DELETE FROM breweries_images WHERE associated_object_id = :breweryId');
         $sql->execute(array(':breweryId' => $breweryId ));
       }

      /**
       * Get an image given an image ID
       *
       * @param int imageId
       *
       * @return Image image object
       */
      public function getImage($imageId) {

        $sql = $this->connection->prepare('SELECT *
            FROM breweries_images
            WHERE image_id = :imageId');
        $sql->execute(array(':imageId' => $imageId));

        $row = $sql->fetch();

        return new Image($row);

      }

      /**
       * Get all images given an associatedObjectId
       *
       * @param int associatedObjectId
       * @param string image type
       *
       * @return array images Image objects
       */
      public function getImages($associatedObjectId, $imageType=null) {

        $sql = $this->connection->prepare('SELECT *
           FROM breweries_images
           WHERE associated_object_id = :associatedObjectId
           AND (:type IS NULL OR image_type = :type)
           ORDER BY image_order ASC');

        $sql->bindValue(":associatedObjectId", (int) $associatedObjectId, PDO::PARAM_INT);
        $sql->bindValue(":type", $imageType, PDO::PARAM_STR);

        $sql->execute();
        $images = array();

        while($row = $sql->fetch()) {
          $image = new Image($row);
          $images[] = $image;
        }
        $sql = "SELECT FOUND_ROWS() AS totalRows";
        $totalRows = $this->connection->query($sql)->fetch();

        return (array("images" => $images, "totalRecords" => $totalRows[0]));

      }

}

 ?>

<?php
/**
 * Brewery Model Class
 */
 Class BreweryModel {
   //Properties

   private $breweryRepository;
   private $editLockManager;

   public function __construct(BreweryRepository $breweryRepository, $editLockManager) {

     $this->breweryRepository = $breweryRepository;
     $this->editLockManager = $editLockManager;

   }

   /**
    * Retrieve a single brewery
    *
    * @param int breweryId
    *
    * @return Brewery|array brewey object or error array
    */
   public function getBrewery($breweryId) {

     try {
       $brewery = $this->breweryRepository->getBrewery($breweryId);
       if(is_bool($brewery) && $brewery === false) {
         // No post was found
         return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_not_found')));
       }
       return $brewery;

     } catch(Exception $e) {
       return (array('status' => 0,
        'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'get_brewery_error'),
        'consoleError' => $e));
     }
   }

   /**
    * Retrieve brewery records
    *
    * @param int numRecords Number of breweries to retrieve
    * @param int offset Offset of records
    *
    * @return array Brewery objects or error messages
    */
   public function getBreweries($numRecords=100000, $offset=0, $searchParameters=[]) {

     try {
       return $this->breweryRepository->getBreweries($numRecords, $offset, $searchParameters);

     } catch(Exception $e) {
       return (array('status' => 0,
         'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'get_breweries_error'),
         'consoleError' => $e));
     }
   }

   /**
    * Retrieve brewery records
    *
    * @param int numRecords Number of breweries to retrieve
    * @param int offset Offset of records
    *
    * @return array Brewery objects or error messages
    */
   public function getMapMarkers() {

     try {
       return $this->breweryRepository->getMapMarkers();

     } catch(Exception $e) {
       return (array('status' => 0,
         'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'get_brewery_markers'),
         'consoleError' => $e));
     }
   }

   /**
    * Get total brewery record count
    *
    * @return array number of records or error message array
    */
   public function getBreweriesCount() {
     try {
       return array('count' => $this->breweryRepository->getBreweriesCount());

     } catch(Exception $e) {

       return (array('status' => 0,
         'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'get_breweries_error'),
         'consoleError' => $e));
     }
   }

   /**
    * Call brewery repository insert function
    *
    * @param brewery a brewery object
    *
    * @return string|array message success, message array on error
    */
   public function addBrewery(Brewery $brewery) {

      try {
        $breweryId = $this->breweryRepository->insertBrewery($brewery);

      } catch(Exception $e) {
        return (array('status' => 0,
           'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'insert_brewery_error'),
           'consoleError'=>$e));
      }

      return getI18nMessage(BREWERY_I18_MESSAGES, 'insert_brewery_success', array(':breweryId' => $breweryId));
    }

    /**
     * Start editing brewery
     *
     * @param int breweryId
     *
     * @return Brewery|array
     */
    public function editBrewery($breweryId) {

      if(!$this->editLockManager->lock('BREWERY', $breweryId)) {
        return (array('status' => 0,
          'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'breweries_edit_error'),
          'consoleError' => __CLASS__.":".__METHOD__."Error setting edit object lock"));
      }

      try {
        $result = $this->breweryRepository->getBrewery($breweryId);
        if(is_bool($result) && $result === false) {
          // No post was found
          return (array('status' => 0,
             'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_not_found')));
        }

      } catch(Exception $e) {

        return (array('status' => 0,
          'userError' => $result['userError'],
          'consoleError' => $result['consoleError']));
      }

      return $result;
    }

    /**
     * Update an existing brewery
     *
     * @param Brewery brewery object
     *
     * @return string|array message on success, message array on error
     */
    public function updateBrewery(Brewery $brewery) {

      // get and verify session variables EDIT_TYPE and EDIT_ID
      $result = $this->editLockManager->verifyLockStatus('BREWERY');
      if(is_bool($result) && !$result) {
        return (array('status' => 0,
           'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'save_brewery_error'),
           'consoleError'=> __CLASS__.":".__METHOD__."User sessions edit type is mismatched."));
      }
      $breweryId = $result['EDIT_ID'];
      // set Brewery object brewery ID
      $brewery->setBreweryId($breweryId);
      $brewery->setUserId($result['USER_ID']);

      // call repo function
      try {
        $this->breweryRepository->updateBrewery($brewery);

        $beerList = $this->breweryRepository->getBreweryBeerList($brewery->getBreweryId());
        return array('message' => getI18nMessage(BREWERY_I18_MESSAGES, 'save_brewery_success', array(':breweryId' => $breweryId)),
                     'beerList' => $beerList );

      } catch(Exception $e) {

        return (array('status' => 0,
           'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'save_brewery_error'),
           'consoleError'=>$e));
      }
    }

   /**
    * Delete a brewery
    *
    * @return string|array redirect URL on success, message array on error
    */
    public function deleteBrewery() {

      // get and verify session variables EDIT_TYPE and EDIT_ID
      $result = $this->editLockManager->verifyLockStatus('BREWERY');
      if(is_bool($result) && !$result) {
        return (array('status' => 0,
           'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'save_brewery_error'),
           'consoleError'=> __CLASS__.":".__METHOD__."User sessions edit type is mismatched."));
      }

      $breweryId = $result['EDIT_ID'];

      // Call helper function to remove all images
      $result = $this->deleteImages($breweryId);
      if(is_array($result) && isset($result['status']) && ($result['status'] === 0)) {
        // If error removing images...
        return result;
      }

      $result = $this->deleteLogo();
      if(is_array($result) && isset($result['status']) && ($result['status'] === 0)) {
        // If error removing images...
        return result;
      }

      // call repo function
      try {
        $this->breweryRepository->deleteBrewery($breweryId);
      } catch(Exception $e) {

        return (array('status' => 0,
           'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'delete_brewery_error'),
           'consoleError'=>$e));
      }

      return MANAGE_BREWERIES_URL;

    }

   /**
    * Get a lat/lng coordinate pair from an address
    *
    * @param string address
    *
    * @return array
    */
   public function getLatLngFromAddress($address) {

     $address = urlencode($address);

     error_log($address);

	    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";


    	// Make the HTTP request
    	$data = @file_get_contents($url);

    	// Parse the json response
    	$jsonData = json_decode($data,true);

    	// If the json data is valid, return lat/lng
      if($jsonData["status"] === "OK") {

        $latLng = array(
          	'lat' => $jsonData["results"][0]["geometry"]["location"]["lat"],
          	'lng' => $jsonData["results"][0]["geometry"]["location"]["lng"],
      	);

      	return $latLng;

      }

      return (array('status' => 0,
         'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'lat_long_error'),
         'consoleError'=> __CLASS__.":".__METHOD__.": Error getting brewery lat long coordinates from address: ".$address));
   }

   /**
    * Handle adding a brewery image.
    *
    * @param Image The image object to be added
    *
    * @return string|array Successfuly string message, or error array.
    */
   public function addImage(Image $image) {

     // get and verify session variables EDIT_TYPE and EDIT_ID
     $result = $this->editLockManager->verifyLockStatus('BREWERY');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_error'),
          'consoleError'=> __CLASS__.":".__METHOD__.": User sessions edit type is mismatched."));
     }

     $breweryId = $result['EDIT_ID'];

     // Image upload
     if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {

       $file = $_FILES['file'];

       $imageHandler = new ImageHandler();
       if(!$imageHandler->checkFileType($file)) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'invalid_file_type'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Invalid file name during image upload."));
       }

       if(!$imageHandler->checkFileSize($file, 5242880)) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'invalid_file_size'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Invalid file size during image upload."));
       }

       $uploadDirectory = $imageHandler->getUploadFolder(IMAGE_UPLOAD_FOLDER_PATH.BREWERIES_IMAGE_FOLDER);
       if(!$uploadDirectory) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'image_upload_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Error creating image upload directory."));
       }

       $generatedFileName = $imageHandler->generateFileName($breweryId."_".$image->getUserId());
       if(!$generatedFileName) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'image_upload_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Error generating file name."));
       }

       if(!$imageHandler->moveFile($file, $uploadDirectory['path'], $generatedFileName)) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'image_upload_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Error converting image to jpg and moving into directory."));
       }

       $imageURL = BREWERIES_IMAGE_BASE_URL.DS.$uploadDirectory['year'].DS.$uploadDirectory['month'].DS.$generatedFileName.'.jpg';

       $image->setImageFilePath(BREWERIES_IMAGE_FOLDER.DS.$uploadDirectory['year'].DS.$uploadDirectory['month'].DS.$generatedFileName.'.jpg');
       $image->setImageURL($imageURL);

       $imageType = 'upload';

     // Image URL given
     } else {

       $imageType = 'url';

       if($image->getImageURL() == "") {

         return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'breweries_image_save_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Missing external image URL."));
       }
     }

     $image->setAssociatedObjectId($breweryId);
     $image->setImageType($imageType);
     $image->setImageUploadDate(date('Y-m-d H:i:s'));

     try {
       $this->breweryRepository->insertImage($image);

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

     return array('breweryId' => $breweryId,
                  'message' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_success'));

   }

   /**
    * Handle adding a brewery logo.
    *
    * @param Image The image object to be added
    *
    * @return string|array Successfuly string message, or error array.
    */
   public function updateLogo(Image $image) {

     // get and verify session variables EDIT_TYPE and EDIT_ID
     $result = $this->editLockManager->verifyLockStatus('BREWERY');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_error'),
          'consoleError'=> __CLASS__.":".__METHOD__.": User sessions edit type is mismatched."));
     }

     $breweryId = $result['EDIT_ID'];
     $imageHandler = new ImageHandler();

     // Image upload
     if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {

       $file = $_FILES['file'];


       if(!$imageHandler->checkFileType($file)) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'invalid_file_type'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Invalid file name during image upload."));
       }

       if(!$imageHandler->checkFileSize($file, 5242880)) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'invalid_file_size'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Invalid file size during image upload."));
       }

       $uploadDirectory = IMAGE_UPLOAD_FOLDER_PATH.BREWERIES_IMAGE_FOLDER.DS.BREWERIES_LOGO_FOLDER;

       $generatedFileName = $imageHandler->generateFileName($breweryId."_".$image->getUserId());
       if(!$generatedFileName) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'image_upload_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Error generating file name."));
       }

       if(!$imageHandler->moveFile($file, $uploadDirectory, $generatedFileName)) {
         return (array('status' => 0,
            'userError' => getI18nMessage(LANG_PATH.DS."imageHandler.en.php", 'image_upload_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Error converting image to jpg and moving into directory."));
       }

       $imageURL = BREWERIES_IMAGE_BASE_URL.DS.BREWERIES_LOGO_FOLDER.DS.$generatedFileName.'.jpg';

       $image->setImageFilePath(BREWERIES_IMAGE_FOLDER.DS.BREWERIES_LOGO_FOLDER.DS.$generatedFileName.'.jpg');
       $image->setImageURL($imageURL);
       $imageType = 'upload';

     // Image URL given
     } else {
       $imageType = 'url';

       if($image->getImageURL() == "") {

         return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'breweries_image_save_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.": Missing external image URL."));
       }
     }

     $image->setImageType($imageType);
     $image->setAssociatedObjectId($breweryId);

     try {

       $oldLogo = $this->breweryRepository->getLogo($breweryId);

       // Logo has already been added
       if($oldLogo instanceof Image) {

         if($oldLogo->getImageType() === 'upload') {

           // Remove old logo from upload folder before adding a new one
           if(!$imageHandler->deleteImage($oldLogo->getImageFilePath())) {
             return (array('status' => 0,
                  'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'update_brewery_logo_error'),
                  'consoleError'=> __CLASS__.":".__METHOD__.": Failed to unlink brewery image."));
           }
         }
         $this->breweryRepository->updateLogo($image);
          // update image
       } else {

         // Image has not been added yet, add
         $this->breweryRepository->addLogo($image);

       }

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'update_brewery_logo_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

     return array('breweryLogoUrl' => $image->getImageURL(),
                  'message' => getI18nMessage(BREWERY_I18_MESSAGES, 'update_brewery_logo_success'));

   }

   /**
    * Update a brewery image given an Image object
    *
    * @param Image image
    *
    * @return array brewery
    */
   public function updateImage(Image $image) {

     // get and verify session variables EDIT_TYPE and EDIT_ID
     $result = $this->editLockManager->verifyLockStatus('BREWERY');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_error'),
          'consoleError'=> __CLASS__.":".__METHOD__.": User sessions edit type is mismatched."));
     }

     $breweryId = $result['EDIT_ID'];

     try {
       $this->breweryRepository->updateImage($image);

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'get_brewery_images_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

     return array('breweryId' => $breweryId,
                  'message' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_success'));

   }

   /**
    * Delete a specific brewery's logo
    *
    */
   public function deleteLogo() {

     // get and verify session variables EDIT_TYPE and EDIT_ID
     $result = $this->editLockManager->verifyLockStatus('BREWERY');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_error'),
          'consoleError'=> __CLASS__.":".__METHOD__.": User sessions edit type is mismatched."));
     }

     $breweryId = $result['EDIT_ID'];

     try {

       $oldLogo = $this->breweryRepository->getLogo($breweryId);

       // Logo has already been added
       if($oldLogo instanceof Image) {

         if($oldLogo->getImageType() === 'upload') {

           $imageHandler = new ImageHandler();

           // Remove old logo from upload folder before adding a new one
           if(!$imageHandler->deleteImage($oldLogo->getImageFilePath())) {
             return (array('status' => 0,
                  'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'update_brewery_logo_error'),
                  'consoleError'=> __CLASS__.":".__METHOD__.": Failed to unlink brewery image."));
           }
         }
       }

       // Delete the record
       $this->breweryRepository->deleteLogo($breweryId);

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'delete_brewery_image_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

     return array('breweryLogoUrl' => "",
                  'message' => getI18nMessage(BREWERY_I18_MESSAGES, 'delete_brewery_image_success'));

   }

   /**
    * Delete a specific brewery image
    *
    * @param int imageId the specific image to remove
    */
   public function deleteImage($imageId) {

     // get and verify session variables EDIT_TYPE and EDIT_ID
     $result = $this->editLockManager->verifyLockStatus('BREWERY');
     if(is_bool($result) && !$result) {
       return (array('status' => 0,
          'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'brewery_image_save_error'),
          'consoleError'=> __CLASS__.":".__METHOD__.": User sessions edit type is mismatched."));
     }

     $breweryId = $result['EDIT_ID'];

     try {
       // Get image to be deleted
       $image = $this->breweryRepository->getImage($imageId);

       // If the image is a locally uploaded image remove the image from the folder
       if(null !== $image->getImageFilePath() && $image->getImageFilePath() != "") {

         $imageHandler = new ImageHandler();
         if(!$imageHandler->deleteImage($image->getImageFilePath())) {
           return (array('status' => 0,
                'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'delete_brewery_image_error'),
                'consoleError'=> __CLASS__.":".__METHOD__.": Failed to unlink brewery image."));
         }
       }

       $this->breweryRepository->deleteImage($imageId, $breweryId);

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'delete_brewery_image_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

     return array('breweryId' => $breweryId,
                  'message' => getI18nMessage(BREWERY_I18_MESSAGES, 'delete_brewery_image_success'));

   }

   /**
    * Delete all images associated with a brewery id
    *
    * @param int breweryId
    */
   public function deleteImages($breweryId) {

     try {
       // Get all locally uploaded images
       $uploadedImages = $this->breweryRepository->getImages($breweryId, "upload");

       $imageHandler = new ImageHandler();

       // Loop through and remove the images from the folder if there are uploaded images
       foreach($uploadedImages["images"] as $uploadedImage) {

         $imageHandler->deleteImage($uploadedImage->getImageFilePath());
       }

       // Remove all images associated with a brewery
       $uploadedImages = $this->breweryRepository->deleteImages($breweryId);

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'delete_brewery_image_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

     return true;
   }

   /**
    * Retrieve a specific image
    *
    * @param int imageId the specific image to get
    */
   public function getImage($imageId) {

     try {
       return $this->breweryRepository->getImage($imageId);

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'get_brewery_images_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

   }

   /**
    * Retrieve all images given a brewery id
    */
   public function getImages($breweryId) {

     try {
       return $this->breweryRepository->getImages($breweryId);

     } catch(Exception $e) {

       return (array('status' => 0,
            'userError' => getI18nMessage(BREWERY_I18_MESSAGES, 'get_brewery_images_error'),
            'consoleError'=> __CLASS__.":".__METHOD__.":". $e));
     }

   }


 }

?>

<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">

  <div class="row justify-content-center margin-bottom-md">
    <?php if(isset($pageContent['searchTitle'])) { ?>
      <h1><?php echo $pageContent['searchTitle'] ?></h1>

    <?php } else { ?>
      <h1>Brewery List</h1>
    <?php } ?>
  </div>

  <div class="row justify-content-center">
    <a class="btn btn-outline-primary margin-bottom-sm" href="./breweries/map">View The Map</a>
  </div>

  <hr>

  <form id="brewerySearchForm">

    <div class="row margin-bottom-md">
      <div class="col-sm-12">
        <h4>Search Breweries</h4>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <select class="form-control" id="brewerySearchSelect">
              <option value="brewery_name">By Brewery Name</option>
              <option value="brewery_state">By State</option>
              <option value="brewery_city">By City</option>
              <option value="brewery_country">By Country</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <input type="text" class="form-control" id="searchTerm" name="searchTerm" maxlength="50" placeholder="Search Term" required>
          <small id="searchTermErrorHolder" class="text-danger"></small>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <button type="button" id="searchBreweriesButton" class="btn btn-primary" onclick="BlogIt.Brewery.searchBreweries()">Search</button>
        </div>
      </div>
    </div>

  </form>

  <hr>

  <div id="BreweryEntries">
    <p>Loading...</p>
  </div>

  <div id="pagination"></div>

  <script>
    BlogIt.Brewery.initializeBreweryPagination("BlogIt.Brewery.listBreweries", false, "BreweryEntries", "view", BlogIt.Brewery.getSearchParameters('<?php if(isset($pageContent['searchParameters'])) echo $pageContent['searchParameters']; ?>'));
  </script>

</div>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

<!-- Used by BlogIt.Brewery.initializeBreweryPagination and BlogIt.Brewery.listBreweries. -->
<script id="brewery_list_template" type="text/x-handlebars-template">
  {{#each breweries}}
  <a href="./breweries/show/{{brewery_id}}">
  <div class="media margin-bottom-lg">
    {{#if logo_url}}<img class="mr-3 breweryLogoList" src="{{logo_url}}" alt="Brewery Logo">{{/if}}
    <div class="media-body {{#unless logo_url}}breweryLogoSpacer{{/unless}}">
      <h4 class="media margin-bottom-sm">{{#if brewery_number}}<b>#{{brewery_number}}</b>&nbsp;{{/if}}{{brewery_name}}</h4>
      <h6 class="text-muted">{{brewery_city}}, {{brewery_state}}</h6>
      <p>Visited on {{brewery_visit_date}}</p>
    </div>
  </div>
  </a>
{{/each}}
</script>

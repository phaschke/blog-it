<?php
  include_once LAYOUTS_PATH . DS . "header.php";

  if(isset($pageContent['message'])){
    include_once LAYOUTS_PATH . DS . "errorMessage.part.php";
    return false;
  }
?>

<div class="container">

  <div class="row justify-content-center">
    <a class="btn btn-outline-primary margin-right-sm" href="./breweries/map">View The Map</a>
    <a class="btn btn-outline-primary" href="./breweries/list">View The List</a>
  </div>

  <hr>

  <div class="row margin-bottom-sm">
    <div class="brewery-title col-sm-12 justify-content-center text-center">
      <h1><?php echo $pageContent['brewery']->getBreweryName()?> </h1>
    </div>
  </div>
  <div class="row margin-bottom-lg">
    <div class="brewery-visit-date col-sm-12 justify-content-center text-center">
      <h4>Visited on <?php echo $pageContent['brewery']->getDateVisited('F jS, Y \a\t g:i A')?> </h4>
    </div>
  </div>

  <div class="row">

    <!-- Start left column -->
    <div class="col-sm-8">
      <?php
        if(null !== $pageContent['brewery']->getNotes() && $pageContent['brewery']->getNotes() != ""){ ?>
          <div>
            <h4 class="margin-bottom-sm">Notes:</h4>
            <p><?php echo $pageContent['brewery']->getNotes(); ?></p>
          </div>

          <hr>
        <?php } ?>

      <div class="margin-top-lg" id="beerListHolder">
        <!-- Hold beer list template -->
      </div>


    </div> <!-- End left column -->

    <!-- Start right side column -->
    <div class="col-sm-3 offset-sm-1">

      <?php if(!null == $pageContent['brewery']->getBreweryLogo()) { ?>
      <div class="text-center margin-bottom-sm" id="breweryLogoHolder">
        <img src="<?php echo $pageContent['brewery']->getBreweryLogo()?>" class="img img-responsive img-circle" alt="Brewery Logo">
      </div>
      <?php } ?>


      <div class="text-center">
        <p><i class="fas fa-map-marker-alt"></i> <?php echo $pageContent['brewery']->getBreweryStreetAddress()."<br>".$pageContent['brewery']->getBreweryCity().", ".$pageContent['brewery']->getBreweryState()?> </p>
      </div>
      <div class="text-center">
        <p><?php if(null !== $pageContent['brewery']->getWebsite() && $pageContent['brewery']->getWebsite() != ""){ echo '<i class="fas fa-link"></i> <a href='.$pageContent['brewery']->getWebsite().' target="_blank">'.$pageContent['brewery']->getWebsite().'</a>';}?> </p>
      </div>


      <?php if(null !== $pageContent['brewery']->getRating() && $pageContent['brewery']->getRating() != 0){ ?>

        <div class="text-center">
          <hr>
          <h4 class="margin-bottom-sm">Rating:</h4>
          <div id="breweryRatingHolder">
            <img src="<?php echo $pageContent['breweryRating']['image']?>" class="img img-responsive img-circle" alt="Brewery Logo">
          <p><i><?php echo $pageContent['breweryRating']['text']?></i></p>
          </div>
        </div>
      <?php } ?>

      <?php if(count($pageContent['brewery']->getLinkedPosts("url")) != 0){ ?>

        <div class="text-center margin-top-lg">
          <hr>
          <h4 class="margin-bottom-sm">Linked Posts:</h4>
          <?php foreach($pageContent['brewery']->getLinkedPosts("url") as $post) {
            echo '<p><a href="'.$post['url'].'">'.$post['title'].'</a></p>';
          }
          ?>
        </div>
      <?php } ?>

    </div> <!-- End right side column -->

  </div>


  <!--<div id="breweryImageHolder">

  </div>-->
  <hr>
  <div class="col-sm-12">
    <div id="imageCarousel">
    <!-- Holds brewery image carousel if images are present -->
    </div>
  </div>

    <script>
    document.addEventListener("DOMContentLoaded", function(event) {

      BlogIt.Brewery.getBreweryImages(<?php echo $pageContent['brewery']->getBreweryId()?>, "imageCarousel", BlogIt.Brewery.displayBreweryImageCarousel);
      BlogIt.Brewery.setBeerList(<?php echo json_encode($pageContent['brewery']->getBeerList()); ?>);
      BlogIt.Brewery.displayBeer();
    });
    </script>

</div>

<?php include_once LAYOUTS_PATH . DS . "footer.php" ?>

<!-- Used by BlogIt.Brewery.displayBreweryImageCarousel -->
<script id="brewery_image_carousel_template" type="text/x-handlebars-template">

  <div id="myCarousel" class="carousel slide bg-inverse w-50 ml-auto mr-auto" data-ride="carousel">
  <ol class="carousel-indicators">
    {{#each images}}
      {{#if @first}}
        <li data-target="#myCarousel" data-slide-to="{{@index}}" class="active"></li>
      {{else}}
        <li data-target="#myCarousel" data-slide-to="{{@index}}"></li>
      {{/if}}
    {{/each}}
  </ol>

  <div class="carousel-inner" role="listbox">
    {{#each images}}
      {{#if @first}}
        <div class="carousel-item active">
          <img class="d-block w-100" src="{{image_url}}" alt="{{image_title}}">
            <div class="carousel-caption">
              <h3>{{image_title}}</h3>
              <p>{{image_description}}</p>
            </div>
        </div>
      {{else}}
        <div class="carousel-item">
          <img class="d-block w-100" src="{{image_url}}" alt="{{image_title}}">
            <div class="carousel-caption">
              <h3>{{image_title}}</h3>
              <p>{{image_description}}</p>
            </div>
        </div>
      {{/if}}
    {{/each}}
  </div>

  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</script>


<!-- Used by BlogIt.Brewery.displayBeer -->
<script id="beer_display_template" type="text/x-handlebars-template">
  {{#if beers}}
  <div>
    <h4 class="margin-bottom-sm">Beers Tried:</h4>
  </div>
  {{/if}}
  {{#each beers}}
  <div class="media margin-bottom-sm">
    <img class="mr-3" src="resources/images/beerIcon.png" alt="Sample photo">
    <div class="media-body">
      <h5>{{beer_name}}</h5>
      {{#if beer_notes}}
      <p>{{{beer_notes}}}</p>
      {{/if}}
    </div>
  </div>
  {{/each}}
</script>

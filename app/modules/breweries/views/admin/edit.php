<?php
  include_once LAYOUTS_PATH . DS . "header.php";

  if(isset($pageContent['message'])){
    include_once LAYOUTS_PATH . DS . "errorMessage.part.php";
    return false;
  }
?>

<div class="container">
  <div class="blog-brewery-form">

    <div class="row margin-bottom-md">
      <div class="col-sm-12">
        <a class="btn btn-outline-primary" href="<?php echo MANAGE_BREWERIES_URL ?>">&#x2190; Back to Manage</a>
      </div>
    </div>

    <div class="row justify-content-center">
      <h2>Edit Brewery</h2>
    </div>

    <div class="text-right py-2">
      <button type="button" id="deleteBreweryButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.deleteBreweryModal())">
        Delete Brewery
      </button>
    </div>

      <div id="alerts-holder-top">
        <!-- Alert holder -->
      </div>
      <div class="alert alert-info" role="alert">
        Brewery Added by User <?php echo $pageContent['brewery']->getUserName(); ?>
      </div>

      <form id="editBreweryForm">

        <div class="row">
          <div class="col-sm-12">
            <h4>Basic Information</h4>
          </div>
        </div>
        <hr>

        <div class="form-group">
          <label for="breweryName">Brewery Name<small>*</small></label>
          <input type="text" class="form-control" id="breweryName" name="breweryName" maxlength="255" placeholder="Brewery Name" value="<?php echo $pageContent['brewery']->getBreweryName(); ?>" required>
          <small id="breweryNameErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="dateVisited">Date Visited<small>*</small></label>
          <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control" id="dateVisited" name="dateVisited" value="<?php echo $pageContent['brewery']->getDateVisited('m/d/Y g:i A'); ?>" required/>
            <span class="input-group-addon">
              <span class="fa fa-calendar"></span>
            </span>
          </div>
          <div>
            <small id="dateVisitedErrorHolder" class="text-danger"></small>
          </div>
        </div>

        <div class="form-check">
          <label class="form-check-label">
            <input type="checkbox" class="form-check-input" id="useInTotalCount" name="useInTotalCount" <?php if($pageContent['brewery']->getIncludeInCount() == 1) {echo 'checked';} ?>>
            Count Brewery in Total Count
          </label>
        </div>

        <div class="form-group">
          <label for="postBody">Brewery Notes</label>
          <textarea class="form-control" id="breweryNotes" name="breweryNotes" placeholder="Brewery Notes" rows="4"><?php echo $pageContent['brewery']->getNotes(); ?></textarea>
          <small id="breweryNotes" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="breweryWebsite">Brewery Website</label>
          <input type="text" class="form-control" id="breweryWebsite" name="breweryWebsite" maxlength="511" placeholder="http:// or https://" value="<?php echo $pageContent['brewery']->getWebsite(); ?>">
          <small id="breweryWebsiteErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="linkPosts">Link to Posts by Tag</label>
          <input type="text" class="form-control" id="linkPosts" name="linkPosts" maxlength="127" placeholder="Post Tag" value="<?php echo $pageContent['brewery']->getLinkedPostTag(); ?>">
          <small id="linkPostsErrorHolder" class="text-danger"></small>
        </div>

        <div class="row margin-top-lg">
          <div class="col-sm-12">
            <h4>Address Information</h4>
          </div>
        </div>
        <hr>

        <!--<div class="alert alert-info" role="alert">
          <h4 class="alert-heading">Adding an Address</h4>
          <p>Below you can enter the full address of the brewery and click the "Verify Address" button to automatically generate the Lat Long coordinates, or you can manually
             enter the Lat Long Coordinates by hand and verify the address using the "Verify Address" button.</p>
          <br>
          <p class="mb-0">Note that the "Verify Address" button will not always generate the valid coordinates for a given address.</p>
        </div>-->

        <div class="form-group">
          <label for="breweryStreetAddress">Street Address<small>*</small></label>
          <input type="text" class="form-control" id="breweryStreetAddress" name="breweryStreetAddress" maxlength="255" placeholder="Street Address" value="<?php echo $pageContent['brewery']->getBreweryStreetAddress(); ?>" required>
          <small id="breweryStreetAddressErrorHolder" class="text-danger"></small>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="breweryCity">City<small>*</small></label>
              <input type="text" class="form-control" id="breweryCity" name="breweryCity" maxlength="255" placeholder="City" value="<?php echo $pageContent['brewery']->getBreweryCity(); ?>" required>
              <small id="breweryCityErrorHolder" class="text-danger"></small>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="breweryState">State/Province<small>*</small></label>
              <input type="text" class="form-control" id="breweryState" name="breweryState" maxlength="255" placeholder="State" value="<?php echo $pageContent['brewery']->getBreweryState(); ?>" required>
              <small id="breweryStateErrorHolder" class="text-danger"></small>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="breweryState">Zip Code</label>
              <input type="text" class="form-control" id="breweryZip" name="breweryZip" maxlength="10" placeholder="Zip Code" value="<?php echo $pageContent['brewery']->getBreweryZip(); ?>" required>
              <small id="breweryZipErrorHolder" class="text-danger"></small>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="breweryCountry">Country<small>*</small></label>
          <input type="text" class="form-control" id="breweryCountry" name="breweryCountry" maxlength="255" placeholder="Country" value="United States" value="<?php echo $pageContent['brewery']->getBreweryCountry(); ?>" required>
          <small id="breweryCountryErrorHolder" class="text-danger"></small>
        </div>

        <hr>
        <div class="form-check">
          <label class="form-check-label">
            <input type="checkbox" checked="checked" class="form-check-input" id="useLatLongCheck" name="useLatLongCheck">
            Use Manually Entered Coordinates
          </label>
        </div>

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="breweryLat">Lat<small>*</small></label>
              <input type="text" class="form-control" id="breweryLat" name="breweryLat" maxlength="25" placeholder="Lat" value="<?php echo $pageContent['brewery']->getBreweryLat(); ?>" required>
              <small id="breweryLatErrorHolder" class="text-danger"></small>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="breweryLng">Long<small>*</small></label>
              <input type="text" class="form-control" id="breweryLng" name="breweryLng" maxlength="25" placeholder="Long" value="<?php echo $pageContent['brewery']->getBreweryLng(); ?>" required>
              <small id="breweryLngErrorHolder" class="text-danger"></small>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="pull-right">
            <button type="button" id="getCoordsButton" class="btn btn-primary margin-bottom-sm" onclick="BlogIt.Brewery.openLatLngSite()">
              Get Coordinates
            </button>
            <button type="button" id="verifyAddressButton" class="btn btn-primary margin-bottom-sm" onclick="BlogIt.Brewery.verifyAddress()">
              Verify Address
            </button>
          </div>
          <div id="lat-long-alerts-holder">
            <!-- Alert holder -->
          </div>
      </div>

      <!-- Google Map -->
      <div class="row">
        <div id="map" class="col-lg-12" style="height:500px;"></div>
      </div>

      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLaUXoog6C2f1UQkdT838J7CeftGJkulM&callback=initMap"></script>

      <script>
        var map;
        function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
            center: {lat:39.50, lng: -98.35},
            zoom: 3
          });

          BlogIt.Brewery.addMapMarker({lat: <?php echo $pageContent['brewery']->getBreweryLat(); ?>, lng: <?php echo $pageContent['brewery']->getBreweryLng(); ?>});

          google.maps.event.addListener(map, 'click', function(event) {
            BlogIt.Brewery.addMapMarker(event.latLng);
            document.getElementById("breweryLat").value = event.latLng.lat();
            document.getElementById("breweryLng").value = event.latLng.lng();
          });
        }

        // Register datetimepicker
        $(function () {
        	$('#datetimepicker1').datetimepicker();
        });

        // Set beer list
        document.addEventListener("DOMContentLoaded", function(event) {
          BlogIt.Brewery.setBeerList(<?php echo json_encode($pageContent['brewery']->getBeerList()); ?>);
          BlogIt.Brewery.displayBeerListForEdit();
        });

        window.onbeforeunload = function() {
          BlogIt.Post.unlockEditObject();
          return "Are you sure you wish to leave the page?";
        }

      </script>

      <div class="row margin-top-lg">
        <div class="col-sm-12">
          <h4>Brewery Rating</h4>
        </div>
      </div>
      <hr>
      <div class="form-group">
        <label for="breweryRatingSelect">Brewery Rating</label>
        <select class="form-control" id="breweryRatingSelect">
          <?php
          foreach($pageContent['breweryRatings'] as $key=>$rating) {
            if($pageContent['brewery']->getRating() == $key) {
              echo '<option value="'.$key.'" selected="selected">'.$rating['text'].'</option>';
            } else {
              echo '<option value="'.$key.'">'.$rating['text'].'</option>';
            }
          }
          ?>
        </select>
      </div>

      <div class="row margin-top-lg">
        <div class="col-sm-12">
          <h4>Beers Tried</h4>
        </div>
      </div>
      <hr>

      <div class="form-group">
        <div class="pull-right">
          <button type="button" id="addBreweryBeerButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.addBeerModal())">
            Add Beer
          </button>
        </div>
      </div>

      <div id="beerListHolder">
        <p>No beers have been added.</p>
      </div>

      <div class="row margin-top-lg">
        <div class="col-sm-12">
          <h4>Brewery Logo</h4>
        </div>
      </div>
      <hr>
      <div class="form-group">
        <div class="pull-right">
          <button type="button" id="addBreweryLogo" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.addLogoImage())">
            Update Logo Image
          </button>
          <button type="button" id="openDeleteLogoModalButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.deleteBreweryLogoModal())" <?php if(null == $pageContent['brewery']->getBreweryLogo()) { echo 'disabled';}?>>
            Delete Logo
          </button>

        </div>
      </div>
      <div id="breweryLogoHolder">
        <?php if(!null == $pageContent['brewery']->getBreweryLogo()) {
          echo '<img src="'.$pageContent['brewery']->getBreweryLogo().'" class="img img-responsive img-circle" alt="Brewery Logo">';
        } else {
          echo '<p>Logo has not been added.</p>';
        } ?>
      </div>

      <div class="row margin-top-lg">
        <div class="col-sm-12">
          <h4>Add Images</h4>
        </div>
      </div>
      <hr>

        <div class="form-group">
          <div class="pull-right">
            <button type="button" id="uploadBreweryImagesButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.addBreweryImageModal())">
              Add Images
            </button>
          </div>
        </div>

        <div class="form-group">
          <div id="breweryImageHolder">
          </div>
        </div>

        <script>
          BlogIt.Brewery.getBreweryImages(<?php echo $pageContent['brewery']->getBreweryId(); ?>, "breweryImageHolder", BlogIt.Brewery.displayImagesForEdit);
        </script>

        <hr>

        <div class="form-group margin-bottom-lg margin-top-lg">
          <div class="pull-right">
            <button type="button" id="updateBreweryButton" class="btn btn-primary" onclick="BlogIt.Brewery.updateBrewery()">
              Save Changes
            </button>
          </div>
          <div id="alerts-holder-bottom">
            <!-- Alert holder -->
          </div>
      </div>
    </form> <!-- End From -->

    </div>
</div>

<?php include_once LAYOUTS_PATH . "/modal.php" ?>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>


<!-- Used by BlogIt.Brewery.displayImagesForEdit -->
<script id="brewery_image_album_edit_template" type="text/x-handlebars-template">
  <div class="row">
    {{#each images}}
    <div class="col-md-4">
      <div class="thumbnail" onclick="BlogIt.UI.openModal(BlogIt.Brewery.editBreweryImageModal({{image_id}}))" >
        <img src="{{image_url}}" alt="{{image_title}}" style="width:100%;max-height:100%;">
        <div class="caption">
          <h4>{{image_title}}</h4>
          <p>{{image_description}}</p>
        </div>
      </div>
    </div>
    {{/each}}
  </div>
</script>


<!-- Used by BlogIt.Brewery.deleteBreweryModal -->
<script id="brewery_delete_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Confirm Brewery Deletion</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      Do you really want to delete this brewery?
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" onclick="BlogIt.Brewery.deleteBrewery()">Delete Brewery</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Brewery.uploadBreweryImageModal -->
<script id="brewery_upload_image_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Add Brewery Image</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form id="uploadBreweryImageForm">

        <div class="form-group">
          <label for="breweryImageURL">Add by URL</label>
          <input type="text" class="form-control" id="breweryImageURL" name="breweryImageURL" placeholder="Image URL" onblur="BlogIt.Brewery.previewURL(this, '#breweryImagePreview', 'breweryImageURLTitleErrorHolder')" maxlength="1023">
          <small id="breweryImageURLTitleErrorHolder" class="text-danger"></small>
        </div>

        <p>--- or ---</p>

        <div class="form-group">
          <label for="breweryImageFile">Upload Image</label>
          <input type="file" class="form-control" id="breweryImageFile" name="breweryImageFile" onchange="BlogIt.Brewery.previewUploadURL(this, '#breweryImagePreview', 'breweryImageURL', 'breweryImageFileErrorHolder')">
          <small id="breweryImageFileErrorHolder" class="text-danger"></small>
        </div>

        <div id="breweryImagePreviewDiv">
          <img id="breweryImagePreview" src="resources/images/noImage.jpg" alt="No Image"/>
        </div>

        <br>

        <div class="form-group">
          <label for="breweryImageTitle">Image Title</label>
          <input type="text" class="form-control" id="breweryImageTitle" name="breweryImageTitle" placeholder="Image Title" maxlength="127">
          <small id="breweryImageTitleErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postSummary">Image Description</label>
          <textarea class="form-control" id="breweryImageDescription" name="breweryImageDescription" placeholder="Image Description" maxlength="255" rows="3"></textarea>
          <small id="breweryImageDescription" class="text-danger"></small>
        </div>

      </form>

      <div id="alerts-holder-add-image">
        <!-- Alert holder -->
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" id="addBreweryImageButton" class="btn btn-primary" onclick="BlogIt.Brewery.addBreweryImage()">Add Image</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Brewery.editBreweryImageModal -->
<script id="brewery_edit_image_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Edit Brewery Image</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form id="editBreweryImageForm">

        <div id="breweryImagePreviewDiv">
          <img id="breweryImagePreview" src="{{image_url}}" alt="No Image"/>
        </div>

        <br>

        <div class="form-group">
          <label for="breweryImageTitle">Image Title</label>
          <input type="text" class="form-control" id="breweryImageTitle" name="breweryImageTitle" placeholder="Image Title" value="{{image_title}}" maxlength="127">
          <small id="breweryImageTitleErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="postSummary">Image Description</label>
          <textarea class="form-control" id="breweryImageDescription" name="breweryImageDescription" placeholder="Image Description" maxlength="255" rows="3">{{image_description}}</textarea>
          <small id="breweryImageDescription" class="text-danger"></small>
        </div>

      </form>

      <div id="alerts-holder-edit-image">
        <!-- Alert holder -->
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" id="deleteBreweryImageButton" class="btn btn-primary" onclick="BlogIt.Brewery.deleteBreweryImageModal({{image_id}})">Delete Image</button>
      <button type="button" id="editBreweryImageButton" class="btn btn-primary" onclick="BlogIt.Brewery.editBreweryImage({{image_id}})">Save Image</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</script>


<!-- Used by BlogIt.Brewery.deleteBreweryImageModal -->
<script id="brewery_delete_image_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Confirm Image Deletion</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      Do you really want to delete this image?
    </div>
    <div id="alerts-holder-delete-image">
      <!-- Alert holder -->
    </div>
    <div class="modal-footer">
      <button type="button" id="deleteBreweryImageButton" class="btn btn-primary" onclick="BlogIt.Brewery.deleteBreweryImage({{image_id}})">Delete Image</button>
      <button type="button" class="btn btn-secondary" onclick="BlogIt.UI.closeModal()">Cancel</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Brewery.updateBreweryLogo -->
<script id="brewery_upload_logo_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Update Brewery Logo</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form id="uploadBreweryLogoForm">

        <div class="form-group">
          <label for="breweryImageURL">Add by URL</label>
          <input type="text" class="form-control" id="breweryImageURL" name="breweryImageURL" placeholder="Image URL" onblur="BlogIt.Brewery.previewURL(this, '#breweryImagePreview', 'breweryImageURLTitleErrorHolder')" maxlength="1023">
          <small id="breweryImageURLTitleErrorHolder" class="text-danger"></small>
        </div>

        <p>--- or ---</p>

        <div class="form-group">
          <label for="breweryImageFile">Upload Image</label>
          <input type="file" class="form-control" id="breweryImageFile" name="breweryImageFile" onchange="BlogIt.Brewery.previewUploadURL(this, '#breweryImagePreview', 'breweryImageURL', 'breweryImageFileErrorHolder')">
          <small id="breweryImageFileErrorHolder" class="text-danger"></small>
        </div>

        <div id="breweryImagePreviewDiv">
          <img id="breweryImagePreview" src="resources/images/noImage.jpg" alt="No Image"/>
        </div>


      </form>

      <div id="alerts-holder-add-logo">
        <!-- Alert holder -->
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" id="addBreweryLogoButton" class="btn btn-primary" onclick="BlogIt.Brewery.updateBreweryLogo()">Update Logo</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Brewery.updateBreweryLogo -->
<script id="beer_display_logo_template" type="text/x-handlebars-template">
  {{#if logo_url}}
    <img src="{{logo_url}}" class="img img-responsive img-circle" alt="Brewery Logo">
  {{else}}
    <p>Logo has not been added.</p>
  {{/if}}
</script>

<!-- Used by BlogIt.Brewery.deleteLogoImageModal -->
<script id="brewery_delete_logo_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Confirm Logo Deletion</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      Do you really want to delete this logo?
    </div>
    <div id="alerts-holder-delete-logo">
      <!-- Alert holder -->
    </div>
    <div class="modal-footer">
      <button type="button" id="deleteBreweryLogoButton" class="btn btn-primary" onclick="BlogIt.Brewery.deleteBreweryLogo()">Delete Logo</button>
      <button type="button" class="btn btn-secondary" onclick="BlogIt.UI.closeModal()">Cancel</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Brewery.addBeerModal -->
<script id="beer_add_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Add Beer</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="addBreweryImageForm">

        <br>

        <div class="form-group">
          <label for="beerName">Beer Name</label>
          <input type="text" class="form-control" id="beerName" name="beerName" maxlength="127">
          <small id="beerNameErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="beerNotes">Beer Notes</label>
          <textarea class="form-control" id="beerNotes" name="beerNotes" maxlength="255" rows="3"></textarea>
          <small id="beerNotesErrorHolder" class="text-danger"></small>
        </div>

      </form>

      <div id="alerts-holder-add-beer">
        <!-- Alert holder -->
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" id="addBeerButton" class="btn btn-primary" onclick="BlogIt.Brewery.addBeer()">Add Beer</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Brewery.displayBeerListForEdit -->
<script id="beer_display_edit_template" type="text/x-handlebars-template">
  {{#each beers}}
    {{#unless delete}}
    <div class="card">
      <div class="card-block">
        <h4 class="card-title">{{beer_name}}</h4>
        {{#if beer_notes}}
        <p class="card-text">{{{beer_notes}}}</p>
        {{/if}}
        <button type="button" id="editBreweryImageModalBeerButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.editBeerModal({{@index}}))">Edit Beer</button>
      </div>
    </div>
    {{/unless}}
  {{/each}}
</script>

<!-- Used by BlogIt.Brewery.editBeerModal -->
<script id="beer_edit_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Edit Beer</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="editBreweryImageForm">

        <br>

        <div class="form-group">
          <label for="beerName">Beer Name</label>
          <input type="text" class="form-control" id="beerName" name="beerName" value="{{beer_name}}" maxlength="127">
          <small id="beerNameErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="beerNotes">Beer Notes</label>
          <textarea class="form-control" id="beerNotes" name="beerNotes" maxlength="255" rows="3">{{beer_notes}}</textarea>
          <small id="beerNotesErrorHolder" class="text-danger"></small>
        </div>

      </form>

      <div id="alerts-holder-edit-beer">
        <!-- Alert holder -->
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" id="editBeerButton" class="btn btn-primary" onclick="BlogIt.Brewery.editBeer({{beer_index}})">Save Beer</button>
      <button type="button" id="deleteBeerButton" class="btn btn-primary" onclick="BlogIt.Brewery.deleteBeer({{beer_index}})">Delete Beer</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</script>

<?php include_once LAYOUTS_PATH . DS . "header.php" ?>

<div class="container">

  <div class="row margin-bottom-md">
    <div class="col-sm-12">
      <a class="btn btn-outline-primary" href="<?php echo ADMIN_PANEL ?>">&#x2190; Back to Panel</a>
    </div>
  </div>

  <div class="row margin-bottom-md">
    <div class="col-sm-12 justify-content-center text-center">
      <h2>Manage Breweries</h2>
    </div>
  </div>

  <div class="margin-bottom-md" id="BreweryEntries">
    <p>Loading...</p>
  </div>

  <div class="margin-bottom-lg" id="pagination"></div>

  <script>
    BlogIt.Brewery.initializeBreweryPagination("BlogIt.Brewery.manageBreweries", false, "BreweryEntries", "edit", BlogIt.Brewery.getSearchParameters());
  </script>

</div>

<?php include_once LAYOUTS_PATH . DS . "footer.php" ?>

<!-- Used by BlogIt.Brewery.initializeBreweryPagination and BlogIt.Brewery.manageBreweries. -->
<script id="brewery_manage_list_template" type="text/x-handlebars-template">
  {{#each breweries}}
  <div class="card">
    <div class="card-block">
      <h4 class="card-title">{{#if brewery_number}}<b>#{{brewery_number}} </b>{{/if}} {{brewery_name}}</h4>
      <h6 class="card-subtitle mb-2 text-muted">Visited on {{brewery_visit_date}}</h6>
      <p class="card-text margin-bottom-sm">{{brewery_street_address}} {{brewery_city}}, {{brewery_state}} {{brewery_zip}}</p>
      <a class="btn btn-primary" href="./breweries/edit/{{brewery_id}}">Edit Brewery</a>
    </div>
  </div>
{{/each}}
</script>

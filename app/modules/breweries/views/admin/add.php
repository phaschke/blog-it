<?php include_once LAYOUTS_PATH . "/header.php" ?>

<div class="container">

  <script>
    window.onbeforeunload = function() {
      return "Are you sure you wish to leave the page?";
    }
  </script>

  <div class="blog-post-form">

    <div class="row margin-bottom-md">
      <div class="col-sm-12">
        <a class="btn btn-outline-primary" href="<?php echo ADMIN_PANEL ?>">&#x2190; Back to Panel</a>
      </div>
    </div>

    <div class="row margin-bottom-md">
      <div class="col-sm-12 justify-content-center text-center">
        <h2>Add a Brewery</h2>
      </div>
    </div>

      <div id="alerts-holder-top">
        <!-- Alert holder top-->
      </div>

      <form id="addBreweryForm">

        <div class="form-group">

          <div class="row">
            <div class="col-sm-12">
              <h4>Basic Information</h4>
            </div>
          </div>
          <hr>

          <label for="breweryName">Brewery Name<small>*</small></label>
          <input type="text" class="form-control" id="breweryName" name="breweryName" maxlength="255" placeholder="Brewery Name" required>
          <small id="breweryNameErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="dateVisited">Date Visited<small>*</small></label>
          <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control" id="dateVisited" name="dateVisited"/>
            <span class="input-group-addon">
              <span class="fa fa-calendar"></span>
            </span>
          </div>
          <div>
            <small id="dateVisitedErrorHolder" class="text-danger"></small>
          </div>
        </div>

        <div class="form-check">
          <label class="form-check-label">
            <input type="checkbox" class="form-check-input" id="useInTotalCount" name="useInTotalCount" checked>
            Count Brewery in Total Count
          </label>
        </div>

        <div class="form-group">
          <label for="postBody">Brewery Notes</label>
          <textarea class="form-control" id="breweryNotes" name="breweryNotes" placeholder="Brewery Notes" rows="4"></textarea>
          <small id="breweryNotes" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="breweryWebsite">Brewery Website</label>
          <input type="text" class="form-control" id="breweryWebsite" name="breweryWebsite" maxlength="511" placeholder="http:// or https://">
          <small id="breweryWebsiteErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="linkPosts">Link to Posts by Tag</label>
          <input type="text" class="form-control" id="linkPosts" name="linkPosts" maxlength="127" placeholder="Post Tag">
          <small id="linkPostsErrorHolder" class="text-danger"></small>
        </div>

        <div class="row margin-top-lg">
          <div class="col-sm-12">
            <h4>Address Information</h4>
          </div>
        </div>
        <hr>

        <!--<div class="alert alert-info" role="alert">
          <h4 class="alert-heading">Adding an Address</h4>
          <p>Below you can enter the full address of the brewery and click the "Verify Address" button to automatically generate the Lat Long coordinates, or you can manually
             enter the Lat Long Coordinates by hand and verify the address using the "Verify Address" button.</p>
          <br>
          <p class="mb-0">Note that the "Verify Address" button will not always generate the valid coordinates for a given address.</p>
        </div>-->

        <div class="form-group">
          <label for="breweryStreetAddress">Street Address<small>*</small></label>
          <input type="text" class="form-control" id="breweryStreetAddress" name="breweryStreetAddress" maxlength="255" placeholder="Street Address" required>
          <small id="breweryStreetAddressErrorHolder" class="text-danger"></small>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="breweryCity">City<small>*</small></label>
              <input type="text" class="form-control" id="breweryCity" name="breweryCity" maxlength="255" placeholder="City" required>
              <small id="breweryCityErrorHolder" class="text-danger"></small>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="breweryState">State/Province<small>*</small></label>
              <input type="text" class="form-control" id="breweryState" name="breweryState" maxlength="255" placeholder="State" required>
              <small id="breweryStateErrorHolder" class="text-danger"></small>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="breweryState">Zip Code</label>
              <input type="text" class="form-control" id="breweryZip" name="breweryZip" maxlength="10" placeholder="Zip Code" required>
              <small id="breweryZipErrorHolder" class="text-danger"></small>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="breweryCountry">Country<small>*</small></label>
          <input type="text" class="form-control" id="breweryCountry" name="breweryCountry" maxlength="255" placeholder="Country" value="United States" required>
          <small id="breweryCountryErrorHolder" class="text-danger"></small>
        </div>

        <hr>
        <div class="form-check">
          <label class="form-check-label">
            <input type="checkbox" checked="checked" class="form-check-input" id="useLatLongCheck" name="useLatLongCheck">
            Use Manually Entered Coordinates
          </label>
        </div>

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="breweryLat">Lat<small>*</small></label>
              <input type="text" class="form-control" id="breweryLat" name="breweryLat" maxlength="25" placeholder="Lat" required>
              <small id="breweryLatErrorHolder" class="text-danger"></small>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="breweryLng">Long<small>*</small></label>
              <input type="text" class="form-control" id="breweryLng" name="breweryLng" maxlength="25" placeholder="Long" required>
              <small id="breweryLngErrorHolder" class="text-danger"></small>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="pull-right">
            <button type="button" id="getCoordsButton" class="btn btn-primary margin-bottom-sm" onclick="BlogIt.Brewery.openLatLngSite()">
              Get Coordinates
            </button>
            <button type="button" id="verifyAddressButton" class="btn btn-primary margin-bottom-sm" onclick="BlogIt.Brewery.verifyAddress()">
              Verify Address
            </button>
          </div>
          <div id="lat-long-alerts-holder">
            <!-- Alert holder -->
          </div>
      </div>

      <!-- Google Map -->
      <div class="row">
        <div id="map" class="col-lg-12" style="height:500px;margin-left:auto;margin-right:auto;"></div>
      </div>

      <script>
        var map;
        function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
            center: {lat:39.50, lng: -98.35},
            zoom: 3
          });

          google.maps.event.addListener(map, 'click', function(event) {
            BlogIt.Brewery.addMapMarker(event.latLng);
            document.getElementById("breweryLat").value = event.latLng.lat();
            document.getElementById("breweryLng").value = event.latLng.lng();
          });
        }

        // Register datetimepicker
        $(function () {
        	$('#datetimepicker1').datetimepicker();
        });

      </script>

      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLaUXoog6C2f1UQkdT838J7CeftGJkulM&callback=initMap"></script>

      <div class="row margin-top-lg">
        <div class="col-sm-12">
          <h4>Brewery Rating</h4>
        </div>
      </div>
      <hr>

      <div class="form-group">
        <label for="breweryRatingSelect">Brewery Rating</label>
        <select class="form-control" id="breweryRatingSelect">
          <?php
          foreach($pageContent['breweryRatings'] as $key=>$rating) {
            echo '<option value="'.$key.'">'.$rating['text'].'</option>';
          }
          ?>
        </select>
      </div>

      <div class="row margin-top-lg">
        <div class="col-sm-12">
          <h4>Beers Tried</h4>
        </div>
      </div>
      <hr>

      <div class="form-group">
        <div class="pull-right">
          <button type="button" id="addBreweryBeerButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.addBeerModal())">
            Add Beer
          </button>
        </div>
      </div>

      <div id="beerListHolder">
        <p>No beers have been added.</p>
      </div>


      <div class="alert alert-info" role="alert">
        <h4 class="alert-heading">Adding Images</h4>
        <p>A Logo and Images can be added once a brewery has been created.</p>
      </div>



      <hr>

      <div class="form-group">
        <div class="pull-right">
          <button type="button" id="addBreweryButton" class="btn btn-primary" onclick="BlogIt.Brewery.addBrewery()">
            Add Brewery
          </button>
        </div>
        <div id="alerts-holder-bottom">
          <!-- Alert holder bottom-->
        </div>

      </div>
    </form> <!-- End From -->

    </div>

</div>

<?php include_once LAYOUTS_PATH . "/modal.php" ?>

<?php include_once LAYOUTS_PATH . "/footer.php" ?>

<!-- Used by BlogIt.Brewery.addBeerModal -->
<script id="beer_add_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Add Beer</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="addBreweryImageForm">

        <br>

        <div class="form-group">
          <label for="beerName">Beer Name</label>
          <input type="text" class="form-control" id="beerName" name="beerName" maxlength="127">
          <small id="beerNameErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="beerNotes">Beer Notes</label>
          <textarea class="form-control" id="beerNotes" name="beerNotes" maxlength="255" rows="3"></textarea>
          <small id="beerNotesErrorHolder" class="text-danger"></small>
        </div>

      </form>

      <div id="alerts-holder-add-beer">
        <!-- Alert holder -->
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" id="addBeerButton" class="btn btn-primary" onclick="BlogIt.Brewery.addBeer()">Add Beer</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</script>

<!-- Used by BlogIt.Brewery.displayBeerListForEdit -->
<script id="beer_display_edit_template" type="text/x-handlebars-template">
  {{#each beers}}
    {{#unless delete}}
    <div class="card">
      <div class="card-block">
        <h4 class="card-title">{{beer_name}}</h4>
        {{#if beer_notes}}
        <p class="card-text">{{{beer_notes}}}</p>
        {{/if}}
        <button type="button" id="editBreweryImageModalBeerButton" class="btn btn-primary" onclick="BlogIt.UI.openModal(BlogIt.Brewery.editBeerModal({{@index}}))">Edit Beer</button>
      </div>
    </div>
    {{/unless}}
  {{/each}}
</script>

<!-- Used by BlogIt.Brewery.editBeerModal -->
<script id="beer_edit_modal_template" type="text/x-handlebars-template">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalLabel">Edit Beer</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="editBreweryImageForm">

        <br>

        <div class="form-group">
          <label for="beerName">Beer Name</label>
          <input type="text" class="form-control" id="beerName" name="beerName" value="{{beer_name}}" maxlength="127">
          <small id="beerNameErrorHolder" class="text-danger"></small>
        </div>

        <div class="form-group">
          <label for="beerNotes">Beer Notes</label>
          <textarea class="form-control" id="beerNotes" name="beerNotes" maxlength="255" rows="3">{{beer_notes}}</textarea>
          <small id="beerNotesErrorHolder" class="text-danger"></small>
        </div>

      </form>

      <div id="alerts-holder-edit-beer">
        <!-- Alert holder -->
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" id="editBeerButton" class="btn btn-primary" onclick="BlogIt.Brewery.editBeer({{beer_index}})">Save Beer</button>
      <button type="button" id="deleteBeerButton" class="btn btn-primary" onclick="BlogIt.Brewery.deleteBeer({{beer_index}})">Delete Beer</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</script>

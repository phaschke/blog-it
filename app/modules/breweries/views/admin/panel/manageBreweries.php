<div class="card">
  <div class="card-header">
    <i class="fas fa-beer"></i> Manage Breweries
  </div>
  <div class="card-block">
    <a class="btn btn-primary margin-bottom-sm" href="./breweries/add">Add</a>
    <a class="btn btn-primary margin-bottom-sm" href="./breweries/manage">Manage</a>
  </div>
</div>

<?php
//======================================================================
// BREWERIES MODULE CONFIG
//======================================================================

/**
 * Path To Breweries Module Folder
 */
define("BREWERIES_PATH", __DIR__);

define("MANAGE_BREWERIES_URL", BASE_URL."breweries/manage");

define("BREWERIES_IMAGE_FOLDER", "/breweries");

define("BREWERIES_IMAGE_BASE_URL", BASE_URL."uploads".BREWERIES_IMAGE_FOLDER);

define("BREWERIES_LOGO_FOLDER", "logos");

define("BREWERY_RATINGS", BREWERIES_PATH."/resources/breweryRatings.php");

define("POST_URL", BASE_URL."posts".DS."show".DS);

define("BREWERY_I18_MESSAGES", BREWERIES_PATH.DS."resources".DS."lang".DS."en".DS."breweries.php");

/**
 * Define module specific page css/js resources
 */
define("BREWERY_PAGE_RESOURCES", array(
        [ 'type' => 'css',
          'path' => 'modules/breweries/css/brewerystyle.css',
        ],
        [ 'type' => 'js',
          'path' => 'modules/breweries/js/Brewery.js',
        ],
        [ 'type' => 'js',
          'path' => 'modules/breweries/js/marker-clusterer-v3/markerclusterer.js',
        ]
      ));

/**
 * TODO: Path To Brewery Interface
 */

/**
 * TODO: Path To Brewery Repository Interface
 */

/**
 * TODO: Path To Brewery Model Interface
 */

/**
 * Require Brewery Model
 */
require_once(BREWERIES_PATH."/models/Brewery.php");

/**
 * Require Beer Model
 */
require_once(BREWERIES_PATH."/models/Beer.php");

/**
 * Require Brewery Repository Model
 */
require_once(BREWERIES_PATH."/models/BreweryRepository.php");

/**
 * Require Brewery Model
 */
require_once(BREWERIES_PATH."/models/BreweryModel.php");

/**
 * Path To Brewery Views
 */
 define("BREWERIES_VIEW_PATH", BREWERIES_PATH.DS."views");

/**
 * Path To Admin Brewery Views
 */
 define("ADMIN_BREWERIES_VIEW_PATH", BREWERIES_VIEW_PATH.DS."admin");


?>

<?php
//======================================================================
// BREWERY RATINGS
//======================================================================

return array(
  '0' => ['text' => 'No Rating.'],
  '1' => ['text' => 'Not So Good.', 'image' => 'modules/breweries/resources/rating1.png'],
  '2' => ['text' => 'Alright.', 'image' => 'modules/breweries/resources/rating2.png'],
  '3' => ['text' => 'Solid, but nothing special.', 'image' => 'modules/breweries/resources/rating3.png'],
  '4' => ['text' => 'Great!', 'image' => 'modules/breweries/resources/rating4.png'],
  '5' => ['text' => 'Fantastically Amazing!', 'image' => 'modules/breweries/resources/rating5.png']
);


?>

<?php
/**
 * Brewery error messages
 *
 *
 */

return [

  'lat_long_error'            => 'Could not get lat long coordinates, please try again.',
  'insert_brewery_error'      => 'Failed to add brewery, please try again.',
  'insert_brewery_success'    => 'Successfully added brewery, edit the brewery <a href="./breweries/edit/:breweryId">here</a> or view the brewery <a href="./breweries/show/:breweryId">here</a>.',
  'delete_brewery_error'      => 'Failed to delete brewery, please try again.',
  'get_breweries_error'       => 'Failed to retrieve breweries, please try again.',
  'get_brewery_markers'       => 'Failed to retrieve brewery markers, please try again.',
  'get_brewery_error'         => 'Failed to retrieve brewery, please try again.',
  'save_brewery_error'        => 'Failed to save brewery, please try again.',
  'save_brewery_success'      => 'Successfully saved brewery. view the brewery <a href="./breweries/show/:breweryId">here</a>.',
  'no_breweries_message'      => 'No breweries to display.',
  'brewery_not_found'         => 'Requested brewery not found.',
  'breweries_edit_error'      => 'Failed to open brewery for editing, please try again.',
  'brewery_image_save_error'    => 'Failed to save the image, please try again.',
  'brewery_image_save_success'  => 'Successfully saved image.',
  'get_brewery_images_error'    => 'Failed to get image(s).',
  'no_brewery_images_message'   => 'No brewery images to display.',
  'delete_brewery_image_error'  => 'Failed to delete brewery image(s), please try again.',
  'delete_brewery_image_success'=> 'Successfully removed brewery image(s).',
  'update_brewery_logo_success' => 'Successfully updated brewery logo.',
  'update_brewery_logo_error'   => 'Failed to update brewery logo, please try again.'

];

?>

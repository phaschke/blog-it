<?php

class BreweriesController extends Controller {

  private $breweryModel;
  private $pageResources = BREWERY_PAGE_RESOURCES;

  public function __construct() {

    parent::__construct();

    $session = new Session();
    $DBConnection = new DBConnection();

    $editLockManagerRepository = new EditLockManagerRepository($DBConnection);
    $editLockManager = new EditLockManager($session, $editLockManagerRepository);

    $breweryRepository = new BreweryRepository($DBConnection);
    $this->breweryModel = new BreweryModel($breweryRepository, $editLockManager);

  }

  /**
   * Produce the view to see a the index page .
   * (Public Action)
   */
  public function map() {

    $pageContent['pageTitle'] = "View Map";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    require_once( BREWERIES_VIEW_PATH.DS."map.php" );

  }

  /**
   * Produce the view to show a single brewery.
   * (Public Action)
   */
  public function show() {

    if ( !isset($_GET["id"]) || !$_GET["id"] ) {
      redirect(BASE_URL);
      return;
    }

    $pageContent['pageTitle'] = "View A Brewery";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    $breweryId = $_GET["id"];

    $result = $this->breweryModel->getBrewery($breweryId);
    if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

      if($result['userError']) {
        $pageContent['message'] = $result['userError'];
        $pageContent['pageTitle'] = "Brewery Not Found";
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
    } else {

      $pageContent['brewery'] = $result;

      $breweryRatings = require_once(BREWERY_RATINGS);
      $pageContent['breweryRating'] = $breweryRatings[$result->getRating()];
    }


    require_once( BREWERIES_VIEW_PATH.DS."show.php" );

  }

  /**
   * Produce the view to see a list of multiple breweries with querying functions.
   * (Public Action)
   */
  public function list() {
    if ( (isset($_GET["lat"]) && $_GET["lat"]) && (isset($_GET["lng"]) && $_GET["lng"]) ) {
      $pageContent['searchTitle'] = "Multiple Visits To A Brewery";
      $pageContent['searchParameters'] = '{"brewery_lat":'.$_GET["lat"].', "brewery_lng":'.$_GET["lng"].'}';
    }

    $pageContent['pageTitle'] = "View The Brewery List";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    require_once( BREWERIES_VIEW_PATH.DS."list.php" );

  }

  /**
   * Produce the view to add a brewery.
   * (Privileged Action)
   */
  public function add() {
    $pageContent['pageTitle'] = "Add a Brewery";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    $pageContent['breweryRatings'] = require_once(BREWERY_RATINGS);

    require_once( ADMIN_BREWERIES_VIEW_PATH.DS."add.php" );
  }

  /**
   * Produce the view to edit a previously brewery.
   * (Privileged Action)
   */
  public function edit() {
    $pageContent['pageTitle'] = "Edit a Brewery";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    if ( !isset($_GET["id"]) || !$_GET["id"] ) {
      redirect(ADMIN_PANEL);
      return;
    }

    $breweryId = $_GET["id"];

    $result = $this->breweryModel->editBrewery($breweryId);
    if(is_array($result) && isset($result['status']) && $result['status'] == 0) {

      if($result['userError']) {
        $pageContent['message'] = $result['userError'];
      }
      if(LOG_ERRORS && isset($result['consoleError'])) {
        error_log($result['consoleError']);
      }
    }
    $pageContent['brewery'] = $result;

    $pageContent['breweryRatings'] = require_once(BREWERY_RATINGS);

    require_once( ADMIN_BREWERIES_VIEW_PATH.DS."edit.php" );

  }

  /**
   * Produce the view to see all posts avaliable for editing.
   * (Privileged Action)
   */
  public function manage() {
    $pageContent['pageTitle'] = "Manage Breweries";
    $pageContent['pageResources'] = $this->pageResources;
    $pageContent['session'] = $this->session;

    require_once( ADMIN_BREWERIES_VIEW_PATH.DS."manage.php" );
  }



}



 ?>

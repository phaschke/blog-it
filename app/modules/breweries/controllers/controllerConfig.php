<?php
//======================================================================
// BREWERY CONTROLLER ACTIONS
//======================================================================

return array(
  'breweries' => [ 'type' => 'addon',
                   'folder' => 'breweries',
                   'className' => 'BreweriesController',
                   'public' => ['map', 'list', 'show'],
                   'user' => [],
                   'privileged' => ['add', 'edit', 'manage']
  ]
);

?>

<?php
//======================================================================
// BREWERY SERVICE ACTIONS
//======================================================================

return array(

  'brewery' => [ 'type' => 'addon',
                 'folder' => 'breweries',
                 'className' => 'BreweryService',
                 'public' => ['getMany', 'getMapMarkers', 'getCordinatesFromAddress', 'getImages', 'getImage'],
                 'user' => [],
                 'privileged' => ['add', 'update', 'delete', 'addImage', 'updateImage', 'deleteImage', 'deleteImages', 'updateLogo', 'deleteLogo']
  ]
);

?>

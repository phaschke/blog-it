<?php
// Include parent class
require_once SERVICES_PARENT;
/**
 * Brewery Service Class
 */
 Class BreweryService extends Service {

   private $breweryModel;

   public function __construct() {

     $session = new Session();
     $DBConnection = new DBConnection();

     $editLockManagerRepository = new EditLockManagerRepository($DBConnection);
     $editLockManager = new EditLockManager($session, $editLockManagerRepository);

     $breweryRepository = new BreweryRepository($DBConnection);
     $this->breweryModel = new BreweryModel($breweryRepository, $editLockManager);

     $imageHandler = new ImageHandler();

   }

   /**
    * Fuction to get many brewery records
    */
   public function getMany($parameters) {

     $searchParameters = $parameters['searchParameters'];

     $numRecords = intval($parameters['numRecords']);
     $offset = intval($parameters['offset']);

     $result = $this->breweryModel->getBreweries($numRecords, $offset, $searchParameters);

     if($this->checkError($result)) {

       // Check if there are any returned brewery records
       if(count($result['breweries']) === 0) {

         $result['message'] = getI18nMessage(BREWERY_I18_MESSAGES, 'no_breweries_message');

       } else {
         $breweryData = array();

         // Format the return dates
         foreach ($result['breweries'] as $brewery) {
           $breweryData[] = $brewery->returnAsArray("F jS, Y \a\\t g:i A");
         }
         $result['breweries'] = $breweryData;

       }

       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Fuction to get many brewery records
    */
   public function getMapMarkers($parameters) {

     $result = $this->breweryModel->getMapMarkers();

     if($this->checkError($result)) {

       // Check if there are any returned brewery records
       if(count($result['breweries']) === 0) {

         $result['message'] = getI18nMessage(BREWERY_I18_MESSAGES, 'no_breweries_message');

       } else {
         $breweryData = array();

         // Format the return dates
         foreach ($result['breweries'] as $brewery) {
           $breweryData[] = $brewery->returnAsArray();
         }
         $result['breweries'] = $breweryData;

       }

       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Delete a Brewery while in edit mode.
    */
   public function delete() {
     $result = $this->breweryModel->deleteBrewery();
     // Verify if the result is an error.
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Return the lat/lng coordinates from a given address.
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return array lat/lng coordinates
    */
   public function getCordinatesFromAddress($parameters) {

     $address = $parameters['address'];

     $result = $this->breweryModel->getLatLngFromAddress($address);

     // Verify if the result is an error.
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Add a brewery
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function add($parameters) {

     if(!isset($parameters['beerList'])) {
       $parameters['beerList'] = array();
     }

     // Retrieve Brewery object from parameters
     $brewery = $this->returnBreweryFromParameters($parameters);


     if(is_bool($brewery) && !$brewery) {

       echo json_encode(array('success_flag' => 0,
         'error_message' => getI18nMessage(BREWERY_I18_MESSAGES, 'insert_brewery_error')));
       return false;
     }

     // Add the brewery and check the success
     $result = $this->breweryModel->addBrewery($brewery);
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Update a brewery (in edit mode)
    *
    * @param array $parameters The given parameter(s) from the requestor
    * @return string error/success message
    */
   public function update($parameters) {

     if(!isset($parameters['beerList'])) {
       $parameters['beerList'] = array();
     }

     // Retrieve Brewery object from parameters
     $brewery = $this->returnBreweryFromParameters($parameters);

     if(is_bool($brewery) && !$brewery) {

       echo json_encode(array('success_flag' => 0,
         'error_message' => getI18nMessage(BREWERY_I18_MESSAGES, 'save_brewery_error')));
       return false;
     }

     $result = $this->breweryModel->updateBrewery($brewery);
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result['message'],
         'beer_list' => $result['beerList']));
       return true;
     }

   }

   /**
    * Add an brewery image
    */
   public function addImage($parameters) {

     // Create image object
     $imageData = array('user_id' => $parameters['ACCOUNT_ID']);

     if(isset($parameters['imageURL']) && !is_null($parameters['imageURL'])) {
       $imageData['image_url'] = $parameters['imageURL'];
     }
     if(isset($parameters['imageTitle']) && !is_null($parameters['imageTitle'])) {
       $imageData['image_title'] = $parameters['imageTitle'];
     }
     if(isset($parameters['imageDescription']) && !is_null($parameters['imageDescription'])) {
       $imageData['image_description'] = $parameters['imageDescription'];
     }

     $image = new Image($imageData);

     $result = $this->breweryModel->addImage($image);
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Add an brewery logo
    */
   public function updateLogo($parameters) {

     // Create image object
     $imageData = array('user_id' => $parameters['ACCOUNT_ID']);

     if(isset($parameters['imageURL']) && !is_null($parameters['imageURL'])) {
       $imageData['image_url'] = $parameters['imageURL'];
     }

     $image = new Image($imageData);

     $result = $this->breweryModel->updateLogo($image);
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Delete an brewery logo
    */
   public function deleteLogo() {

     $result = $this->breweryModel->deleteLogo();
     if($this->checkError($result)) {
       echo json_encode(array('success_flag' => 1,
         'success_message' => $result));
       return true;
     }
   }

   /**
    * Edit an existing brewery image
    */
   public function updateImage($parameters) {

     $requiredParameters = array('imageId');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       // Create image object
       $imageData = array('image_id' => $parameters['imageId'],
                          'image_title' => $parameters['imageTitle'],
                          'image_description' => $parameters['imageDescription']
                          );
       $image = new Image($imageData);

       $result = $this->breweryModel->updateImage($image);
       if($this->checkError($result)) {
         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }

     }
   }

   /**
    * Delete a specific image.
    */
   public function deleteImage($parameters) {

     $requiredParameters = array('imageId');
     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->breweryModel->deleteImage($parameters['imageId']);
       if($this->checkError($result)) {

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }

   }

   /**
    * Delete all images associated with a brewery
    */
   public function deleteImages($parameters) {

     $result = $this->breweryModel->deleteImages();
     if($this->checkError($result)) {

       echo json_encode(array('success_flag' => 1,
         'success_message' => $image));
       return true;
     }

   }

   /**
    * Get a specific image.
    */
   public function getImage($parameters) {

     $requiredParameters = array('imageId');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->breweryModel->getImage($parameters['imageId']);
       if($this->checkError($result)) {

         $image = $result->returnAsArray('Y-m-d H:i:s');


         echo json_encode(array('success_flag' => 1,
           'success_message' => $image));
         return true;
       }
     }

   }

   /**
    * Get all images corresponding to a brewery Id
    */
   public function getImages($parameters) {

     $requiredParameters = array('breweryId');

     if($this->checkError($this->verifyParameters($requiredParameters, $parameters))) {

       $result = $this->breweryModel->getImages($parameters['breweryId']);
       if($this->checkError($result)) {

         // Check if there are any returned brewery records
         if(count($result['images']) === 0) {

           $result['message'] = getI18nMessage(BREWERY_I18_MESSAGES, 'no_brewery_images_message');

         } else {
           $imageData = array();

           // Format the return dates
           foreach ($result['images'] as $image) {
             $imageData[] = $image->returnAsArray('Y-m-d H:i:s');
           }
           $result['images'] = $imageData;

         }

         echo json_encode(array('success_flag' => 1,
           'success_message' => $result));
         return true;
       }
     }
   }

   /**
    * Helper function to return a brewey object given an array of parameters
    *
    * @param array $parameters
    * @return Brewery|False The brewery object or false
    */
   public function returnBreweryFromParameters($parameters) {
     try {
       $breweryData = array("user_id" => $parameters['ACCOUNT_ID'],
                            "brewery_name" => $parameters['breweryName'],
                            "include_in_count" => $parameters['includeInCount'],
                            "brewery_notes" => $parameters['notes'],
                            "linked_post_tag" => $parameters['linkPosts'],
                            "brewery_website" => $parameters['website'],
                            "brewery_visit_date" => DateTime::createFromFormat('m/d/Y g:i A', $parameters['dateVisited'])->format('Y-m-d H:i:s'),
                            "brewery_street_address" => $parameters['streetAddress'],
                            "brewery_city" => $parameters['breweryCity'],
                            "brewery_state" => $parameters['breweryState'],
                            "brewery_zip" => $parameters['breweryZip'],
                            "brewery_country" => $parameters['breweryCountry'],
                            "brewery_lat" => $parameters['breweryLat'],
                            "brewery_lng" => $parameters['breweryLng'],
                            "brewery_rating" => $parameters['breweryRating'],
                            "beer_list" => $parameters['beerList']
                           );

       return new Brewery($breweryData);

     } catch(Exception $e) {

       if(LOG_ERRORS) {
         error_log(__CLASS__.' '.__FUNCTION__." Missing required service parameters.");
       }
       return false;
     }
   }

 }




?>

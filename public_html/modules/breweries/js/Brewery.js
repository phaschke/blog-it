/**
 * Namespace to hold functions with Brewery related functions.
 * @namespace
 */
BlogIt.Brewery = {

  // Define the service interface
  SERVICE_REQUESTOR: "core/serviceInterfaces/serviceRequestor.php",
  NAME_SPACE: "BlogIt.Brewery:",
  BEER_LIST: [],

  marker: null,

  /**
   * Validate the add/edit brewery form using Lib validator
   *
   * @return boolean
   */
  validateBreweryForm: function() {
    if(!BlogIt.Lib.verifyField("breweryName", "breweryNameErrorHolder", [ "required" ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("dateVisited", "dateVisitedErrorHolder", [ "required" ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("breweryCity", "breweryCityErrorHolder", [ "required" ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("breweryState", "breweryStateErrorHolder", [ "required" ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("breweryCountry", "breweryCountryErrorHolder", [ "required" ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("breweryLat", "breweryLatErrorHolder", [ "required", "number", "latitudecoord" ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("breweryLng", "breweryLngErrorHolder", [ "required", "number", "longitudecoord" ])) {
      return false;
    }

    return true;

  },

  /**
   *
   */
  addBrewery: function() {
    var successAlertHolder = "alerts-holder-top";
    var errorAlertHolder = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(successAlertHolder);
    BlogIt.UI.clearAlerts(errorAlertHolder);

    if(!BlogIt.Brewery.validateBreweryForm()) {
      return false;
    }

    var includeInCount = 1;
    if(!document.getElementById("useInTotalCount").checked) {
      includeInCount = 0;
    }

    var dataArray = {
      'service': 'brewery',
      'action': 'add',
      'breweryName': document.getElementById("breweryName").value,
      'includeInCount' : includeInCount,
      'website': document.getElementById("breweryWebsite").value,
      'notes': document.getElementById("breweryNotes").value,
      'linkPosts': document.getElementById("linkPosts").value,
      'dateVisited': document.getElementById("dateVisited").value,
      'streetAddress': document.getElementById("breweryStreetAddress").value,
      'breweryCity': document.getElementById("breweryCity").value,
      'breweryState': document.getElementById("breweryState").value,
      'breweryZip': document.getElementById("breweryZip").value,
      'breweryCountry': document.getElementById("breweryCountry").value,
      'breweryLat': document.getElementById("breweryLat").value,
      'breweryLng': document.getElementById("breweryLng").value,
      'breweryRating': document.getElementById("breweryRatingSelect").value,
      'beerList': this.BEER_LIST
    };

    BlogIt.UI.disableElement("addBreweryButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        BlogIt.UI.enableElement("addBreweryButton");
        BlogIt.UI.addAlert(errorAlertHolder, "danger", data["error_message"], false);

      } else { //No errors*/
        BlogIt.UI.enableElement("addBreweryButton");
        document.getElementById("addBreweryForm").reset();

        this.BEER_LIST = [];
        BlogIt.Brewery.displayBeerListForEdit();

        BlogIt.UI.addAlert(successAlertHolder, "success", data["success_message"], false);

        // Scroll to success message on top
        document.body.scrollTop = document.documentElement.scrollTop = 0;

      }
    });
  },

  updateBrewery: function() {
    var successAlertHolder = "alerts-holder-top";
    var errorAlertHolder = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(successAlertHolder);
    BlogIt.UI.clearAlerts(errorAlertHolder);

    if(!BlogIt.Brewery.validateBreweryForm()) {
      return false;
    }

    var includeInCount = 1;
    if(!document.getElementById("useInTotalCount").checked) {
      includeInCount = 0;
    }

    var dataArray = {
      'service': 'brewery',
      'action': 'update',
      'breweryName': document.getElementById("breweryName").value,
      'includeInCount' : includeInCount,
      'website': document.getElementById("breweryWebsite").value,
      'notes': document.getElementById("breweryNotes").value,
      'linkPosts': document.getElementById("linkPosts").value,
      'dateVisited': document.getElementById("dateVisited").value,
      'streetAddress': document.getElementById("breweryStreetAddress").value,
      'breweryCity': document.getElementById("breweryCity").value,
      'breweryState': document.getElementById("breweryState").value,
      'breweryZip': document.getElementById("breweryZip").value,
      'breweryCountry': document.getElementById("breweryCountry").value,
      'breweryLat': document.getElementById("breweryLat").value,
      'breweryLng': document.getElementById("breweryLng").value,
      'breweryRating': document.getElementById("breweryRatingSelect").value,
      'beerList': this.BEER_LIST
    };

    BlogIt.UI.disableElement("updateBreweryButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        BlogIt.UI.enableElement("updateBreweryButton");
        BlogIt.UI.addAlert(errorAlertHolder, "danger", data["error_message"], true);

      } else { //No errors
        BlogIt.UI.enableElement("updateBreweryButton");

        BlogIt.UI.addAlert(successAlertHolder, "success", data["success_message"], true);

        BlogIt.Brewery.setBeerList(data["beer_list"]);
        // Scroll to success message on top
        document.body.scrollTop = document.documentElement.scrollTop = 0;

      }
    });
  },

  /**
   * Get search parameters form search form and conduct search
   */
  searchBreweries() {

    var searchParameters = {};
    var searchElement = document.getElementById("brewerySearchSelect").value;
    if(searchElement != "") {
      searchParameters[searchElement] = BlogIt.Lib.trimWhiteSpace(document.getElementById("searchTerm").value);
    }

    BlogIt.Brewery.initializeBreweryPagination("BlogIt.Brewery.listBreweries", false, "BreweryEntries", "view", searchParameters);

  },

  /**
   * Get search parameters form search form
   */
  getSearchParameters: function(searchParameters) {
    searchParametersJson = "";
    if(searchParameters) {
      searchParametersJson = JSON.parse(searchParameters);
    }

    return searchParametersJson;
  },

  /**
   * Initialize pagination of breweries
   *
   * @param string paginationElement holder id
   */
  initializeBreweryPagination: function(mode, initializeContent, paginationElement, type, searchParameters) {

    var itemsPerPage = 25;

    var dataArray = {
      'service': 'brewery',
      'action': 'getMany',
      'searchParameters': searchParameters,
      'numRecords': itemsPerPage,
      'offset' : 0
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Brewery:initializeBreweryPagination: "+data["error_message"]);

        var code = '<p>'+data["error_message"]+'</p>';
        document.getElementById("BreweryEntries").innerHTML = code;

      } else { //No errors

        code = "";

        if(data["success_message"]["count"] > 0) {

          var breweries = {"breweries": data["success_message"]["breweries"]};

          if(type == 'edit') {

            BlogIt.UI.compileAndExecuteHandlebars('BreweryEntries', 'brewery_manage_list_template', [], breweries, "clear");

          } else {

            // Call templater
            BlogIt.UI.compileAndExecuteHandlebars('BreweryEntries', 'brewery_list_template', [], breweries, "clear");

          }

          var totalRecords = data["success_message"]["count"];

          Pagination.Init(document.getElementById('pagination'), {
              itemsPerPage: itemsPerPage, // items per page
              size: Math.ceil(totalRecords/itemsPerPage), // pages size
              page: 1,  // selected page
              step: 3   // pages before and after current
          },
          mode,
          initializeContent,
          [paginationElement, searchParameters]
          );

        } else {

          var code = '<p>'+data["success_message"]["message"]+'</p>';
          document.getElementById(paginationElement).innerHTML = code;
        }
      }
      return true;
    });

  },

  /**
   * Fetch and display brewery listings
   */
  listBreweries: function(args) {

    var offset = args[0];
    var recordsPerPage = args[1];
    var paginationElement = args[2];
    var searchParameters = args[3];

    var dataArray = {
      'service': 'brewery',
      'action': 'getMany',
      'searchParameters': searchParameters,
      'numRecords': recordsPerPage,
      'offset': offset
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        console.log("BlogIt.Brewery:listBreweries: "+data["error_message"]);


        var code = '<p>'+data["error_message"]+'</p>';
        document.getElementById(paginationElement).innerHTML = code;

      } else { //No errors

        var breweries = {"breweries": data["success_message"]["breweries"]};

        var code = "";
        if(breweries.length == 0) {

          code += '<p>'+data["success_message"]["message"]+'</p>';
          document.getElementById(paginationElement).innerHTML = code;

        } else {

          // Call templater
          BlogIt.UI.compileAndExecuteHandlebars('BreweryEntries', 'brewery_list_template', [], breweries, "clear");

        }
      }
    });
  },

  /**
   * Fetch and display brewery listings for managing
   */
  manageBreweries: function(args) {

    var offset = args[0];
    var recordsPerPage = args[1];
    var paginationElement = args[2];

    var dataArray = {
      'service': 'brewery',
      'action': 'getMany',
      'numRecords': recordsPerPage,
      'offset': offset
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Brewery:manageBreweries: "+data["error_message"]);

        var code = '<p>'+data["error_message"]+'</p>';
        document.getElementById(paginationElement).innerHTML = code;

      } else { //No errors
        var breweries = {"breweries": data["success_message"]["breweries"]};

        var code = '';
        if(breweries.length == 0) {

          code += '<p>'+data["success_message"]["message"]+'</p>'
        } else {

          BlogIt.UI.compileAndExecuteHandlebars('BreweryEntries', 'brewery_manage_list_template', [], breweries, "clear");
        }
      }
    });
  },

  deleteBreweryModal: function() {

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'brewery_delete_modal_template', [], "", "clear");

  },

  deleteBrewery: function() {

    var alertHolder = 'alerts-holder-top';
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'brewery',
      'action': 'delete'
    };

    BlogIt.UI.disableElement("deleteBreweryButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        BlogIt.UI.enableElement("deleteBreweryButton");

        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors
        BlogIt.UI.enableElement("deleteBreweryButton");

        // Redirect to admin panel
        BlogIt.Utils.redirect(data["success_message"]);

      }
    });
  },

  /**
   *
   */
  verifyAddress: function() {

    var alertHolder = "lat-long-alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    /*if(document.getElementById("useLatLongCheck").checked == false) {

      if(!BlogIt.Lib.verifyField("breweryStreetAddress", "breweryStreetAddressErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryCity", "breweryCityErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryState", "breweryStateErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryZip", "breweryZipErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryCountry", "breweryCountryErrorHolder", [ "required" ])) {
        return false;
      }

      var streetAddress = document.getElementById("breweryStreetAddress").value;
      var breweryCity = document.getElementById("breweryCity").value;
      var breweryState = document.getElementById("breweryState").value;
      var breweryZip = document.getElementById("breweryZip").value;
      var breweryCountry = document.getElementById("breweryCountry").value;


      var dataArray = {
        'service': 'brewery',
        'action': 'getCordinatesFromAddress',
        'address': streetAddress + " " +  breweryCity + ", " + breweryState + " " + breweryZip,
      };

      BlogIt.UI.disableElement("verifyAddressButton");

      BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

        if (data["success_flag"] === 0) { //Error

          console.log("BlogIt.Brewery.verifyAddress: "+data["error_message"]);

          BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

          BlogIt.UI.enableElement("verifyAddressButton");

        } else { //No errors

          BlogIt.UI.enableElement("verifyAddressButton");

          document.getElementById("breweryLat").value = data["success_message"]["lat"];
          document.getElementById("breweryLng").value = data["success_message"]["lng"];

          BlogIt.Brewery.addMapMarker(data["success_message"]);

        }

      });

    } else {*/
      // Manually entered lat long

      if(!BlogIt.Lib.verifyField("breweryLat", "breweryLatErrorHolder", [ "required", "number", "latitudecoord" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryLng", "breweryLngErrorHolder", [ "required", "number", "longitudecoord" ])) {
        return false;
      }

      var breweryLat = document.getElementById("breweryLat").value;
      var breweryLng = document.getElementById("breweryLng").value;

      BlogIt.Brewery.addMapMarker({"lat": parseFloat(breweryLat), "lng": parseFloat(breweryLng)});

    //}


  },

  /**
   * Displays a copyable address and opens a site to get the lat/lng in a new tab
   */
  openLatLngSite: function() {

      var alertHolder = "lat-long-alerts-holder";
      BlogIt.UI.clearAlerts(alertHolder);

      if(!BlogIt.Lib.verifyField("breweryStreetAddress", "breweryStreetAddressErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryCity", "breweryCityErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryState", "breweryStateErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryZip", "breweryZipErrorHolder", [ "required" ])) {
        return false;
      }

      if(!BlogIt.Lib.verifyField("breweryCountry", "breweryCountryErrorHolder", [ "required" ])) {
        return false;
      }

      var streetAddress = document.getElementById("breweryStreetAddress").value;
      var breweryCity = document.getElementById("breweryCity").value;
      var breweryState = document.getElementById("breweryState").value;
      var breweryZip = document.getElementById("breweryZip").value;
      var breweryCountry = document.getElementById("breweryCountry").value;

      BlogIt.UI.addAlert(alertHolder, "info", streetAddress+" "+breweryCity+", "+breweryState+" "+breweryZip+" "+breweryCountry, true);

      window.open('https://www.latlong.net/convert-address-to-lat-long.html', '_blank')

  },

  /**
   *
   */
  addMapMarker: function(latLng) {

    if(BlogIt.Brewery.marker) {
      BlogIt.Brewery.marker.setMap(null);
    }

    // Draw marker and zoom in.
    BlogIt.Brewery.marker = new google.maps.Marker({
      position: latLng,
      map: map
    });
    map.panTo(latLng);
    map.setZoom(15);

    return true;
  },

  loadBreweryMap: function() {

    var dataArray = {
      'service': 'brewery',
      'action': 'getMapMarkers'
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("Error: BlogIt.Brewery:loadBreweryMap: "+data["error_message"]);

      } else { //No errors

        var breweries = data["success_message"]["breweries"];

        if(breweries.length == 0) {

          code += '<p>'+data["success_message"]["message"]+'</p>'
        } else {

          var breweryList = [];
          for(var i=0; i < breweries.length; ++i) {

            var breweryLat = breweries[i].brewery_lat;
            var breweryLng = breweries[i].brewery_lng;

            breweryList.push({
              "latLng":{
                "lat": parseFloat(breweryLat),
                "lng": parseFloat(breweryLng)
              },
              "id":breweries[i].brewery_id,
              "name":breweries[i].brewery_name,
              "visits":breweries[i].visits
              });
          }
          BlogIt.Brewery.populateMapMarkers(breweryList);
        }

      }

      return true;

    });
  },

  /**
   *
   */
  populateMapMarkers: function(breweryList) {

    var icon = 'resources/images/beerIcon.png';
    var markers = breweryList.map(function(brewery, i) {
      var label = "";

      if(brewery.visits > 1) {
        label = "(x"+brewery.visits+")";
        var marker = new google.maps.Marker({
          position: brewery.latLng,
          label: label,
          title: brewery.name,
          icon: icon,
          url: "./breweries/list/?lat="+brewery.latLng.lat+"&lng="+brewery.latLng.lng,
        });

      } else {

        var marker = new google.maps.Marker({
          position: brewery.latLng,
          label: label,
          title: brewery.name,
          icon: icon,
          url: "./breweries/show/"+brewery.id,
        });
      }

      google.maps.event.addListener(marker, 'click', function(evt) {
        window.location.href = this.url;
      });
      return marker;
    });

    var options = {
            imagePath: 'modules/breweries/js/marker-clusterer-v3/images/m'
        };

    var markerCluster = new MarkerClusterer(map, markers, options);
  },

  /**
   * Open the brewery image uploading modal
   */
  addBreweryImageModal: function() {

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'brewery_upload_image_modal_template', [], "", "clear");
  },

  /**
   * Add Logo Image
   */
  addLogoImage() {

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'brewery_upload_logo_modal_template', [], "", "clear");

  },

  /**
   * Upload a brewery image
   */
  addBreweryImage: function() {

    var alertHolder = "alerts-holder-add-image";
    BlogIt.UI.clearAlerts(alertHolder);

    var urlInputElement = document.getElementById("breweryImageURL");
    var fileInputElement = document.getElementById("breweryImageFile");
    var url = "";
    var file = "";
    var formData = new FormData();

    if(BlogIt.Lib.trimWhiteSpace(urlInputElement.value) !== "") {

      // Use URL Image
      // Verify URL name
      if(!BlogIt.Brewery.verifyImageFileName(urlInputElement.value, "breweryImageURL", "breweryImageURLTitleErrorHolder")) {
        return false;
      }

      url = BlogIt.Lib.trimWhiteSpace(urlInputElement.value);
      formData.append('imageURL', url);

    } else if (fileInputElement.files && fileInputElement.files[0]) {

      // Use file upload
      // Verify file name
      if(!BlogIt.Brewery.verifyImageFileName(fileInputElement.files[0].name, "breweryImageFile", "breweryImageFileErrorHolder")) {
        return false;
      }

      // Verify the image size
      if(!BlogIt.Brewery.verifyImageFileSize(fileInputElement.files[0].size, "breweryImageFile", "breweryImageFileErrorHolder")) {
        return false;
      }

      file = fileInputElement.files[0];
      formData.append('file', file);



    } else {
      // No images selected
      BlogIt.UI.addAlert(alertHolder, "danger", "No image to upload!", true);
      return false;
    }

    formData.append('service', 'brewery');
    formData.append('action', 'addImage');

    formData.append('imageTitle', BlogIt.Lib.trimWhiteSpace(document.getElementById("breweryImageTitle").value));
    formData.append('imageDescription', BlogIt.Lib.trimWhiteSpace(document.getElementById("breweryImageDescription").value));


    BlogIt.UI.disableElement("addBreweryImageButton");
    BlogIt.UI.addAlert(alertHolder, "info", "Uploading Image...");

    var myData;

    $.ajax({
      type: 'POST',
      url: this.SERVICE_REQUESTOR,
      data: formData,
      dataType: 'json',
      processData: false,  // tell jQuery not to process the data
      contentType: false,   // tell jQuery not to set contentType

      success: function(data) {
        myData = data;
      },

      error: function(data) { // if your PHP script return an erroneous header, you'll land here

        console.log("Error: AJAX call failed, Please try again.");
        myData = {
          "success_flag": 0,
          "error_message": "Error: AJAX call failed, Please try again."
        };

      },

      complete: function(data) {
        //console.log("ajax: complete");
        BlogIt.UI.clearAlerts(alertHolder);

        if(myData["success_flag"] == 0) {
          //error
          BlogIt.UI.enableElement("addBreweryImageButton");
          BlogIt.UI.addAlert(alertHolder, "danger", myData["error_message"], false);

        } else {
          //success
          BlogIt.UI.enableElement("addBreweryImageButton");
          document.getElementById("uploadBreweryImageForm").reset();

          BlogIt.UI.addAlert(alertHolder, "success", myData["success_message"]["message"], false);

          BlogIt.UI.closeModal();

          BlogIt.Brewery.getBreweryImages(myData["success_message"]["breweryId"], "breweryImageHolder", BlogIt.Brewery.displayImagesForEdit);

        }

      }
    });

  },

  /**
   * Upload a brewery logo
   */
  updateBreweryLogo: function() {

    var alertHolder = "alerts-holder-add-logo";
    BlogIt.UI.clearAlerts(alertHolder);

    var urlInputElement = document.getElementById("breweryImageURL");
    var fileInputElement = document.getElementById("breweryImageFile");
    var url = "";
    var file = "";
    var formData = new FormData();

    if(BlogIt.Lib.trimWhiteSpace(urlInputElement.value) !== "") {

      // Use URL Image
      // Verify URL name
      if(!BlogIt.Brewery.verifyImageFileName(urlInputElement.value, "breweryImageURL", "breweryImageURLTitleErrorHolder")) {
        return false;
      }

      url = BlogIt.Lib.trimWhiteSpace(urlInputElement.value);
      formData.append('imageURL', url);

    } else if (fileInputElement.files && fileInputElement.files[0]) {

      // Use file upload
      // Verify file name
      if(!BlogIt.Brewery.verifyImageFileName(fileInputElement.files[0].name, "breweryImageFile", "breweryImageFileErrorHolder")) {
        return false;
      }

      // Verify the image size
      if(!BlogIt.Brewery.verifyImageFileSize(fileInputElement.files[0].size, "breweryImageFile", "breweryImageFileErrorHolder")) {
        return false;
      }

      file = fileInputElement.files[0];
      formData.append('file', file);



    } else {
      // No images selected
      BlogIt.UI.addAlert(alertHolder, "danger", "No image to upload!", true);
      return false;
    }

    formData.append('service', 'brewery');
    formData.append('action', 'updateLogo');

    BlogIt.UI.disableElement("addBreweryLogoButton");
    BlogIt.UI.addAlert(alertHolder, "info", "Uploading Image...");

    var myData;

    $.ajax({
      type: 'POST',
      url: this.SERVICE_REQUESTOR,
      data: formData,
      dataType: 'json',
      processData: false,  // tell jQuery not to process the data
      contentType: false,   // tell jQuery not to set contentType

      success: function(data) {
        myData = data;
      },

      error: function(data) { // if your PHP script return an erroneous header, you'll land here

        console.log("Error: AJAX call failed, Please try again.");
        myData = {
          "success_flag": 0,
          "error_message": "Error: AJAX call failed, Please try again."
        };

      },

      complete: function(data) {
        //console.log("ajax: complete");
        BlogIt.UI.clearAlerts(alertHolder);

        if(myData["success_flag"] == 0) {
          //error
          console.log("Error:updateBreweryLogo");
          BlogIt.UI.enableElement("addBreweryLogoButton");
          BlogIt.UI.addAlert(alertHolder, "danger", myData["error_message"], false);

        } else {
          //success
          BlogIt.UI.enableElement("addBreweryLogoButton");
          document.getElementById("uploadBreweryLogoForm").reset();

          BlogIt.UI.addAlert(alertHolder, "success", myData["success_message"]["message"], false);

          BlogIt.UI.closeModal();

          BlogIt.UI.enableElement("openDeleteLogoModalButton");

          BlogIt.UI.compileAndExecuteHandlebars('breweryLogoHolder', 'beer_display_logo_template', [], {"logo_url": myData["success_message"]["breweryLogoUrl"]}, "clear");
        }

      }
    });

  },

  /**
   * Open brewery logo delete modal
   */
  deleteBreweryLogoModal: function() {

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'brewery_delete_logo_modal_template', [], "", "clear");

  },

  /**
   * Handle deleting logo
   */
  deleteBreweryLogo: function() {

    var alertHolder = "alerts-holder-delete-logo";
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'brewery',
      'action': 'deleteLogo'
    };

    BlogIt.UI.disableElement("deleteBreweryLogoButton");
    BlogIt.UI.addAlert(alertHolder, "info", "Deleting Logo...");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      BlogIt.UI.clearAlerts(alertHolder);

      if (data["success_flag"] === 0) { //Error

        //error
        BlogIt.UI.enableElement("deleteBreweryLogoButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else {

        //success
        BlogIt.UI.enableElement("deleteBreweryLogoButton");

        BlogIt.UI.closeModal();

        BlogIt.UI.compileAndExecuteHandlebars('breweryLogoHolder', 'beer_display_logo_template', [], "", "clear");

        BlogIt.UI.disableElement("openDeleteLogoModalButton");

      }
    });

  },

  /**
   * Helper function to verify the size of a file
   */
  verifyImageFileSize: function(size, field, errorHolder) {

    if(size < 5242880) {

      BlogIt.UI.clearInputFieldStyling(field);
      BlogIt.UI.setFieldError(errorHolder, "");
      return true;

    } else {

      BlogIt.UI.addInputFieldErrorStyling(field);
      BlogIt.UI.setFieldError(errorHolder, "File is too big, image must be under 5MB.");
      return false;

    }
  },

  /**
   * Helper function to verify the name of a file
   */
  verifyImageFileName: function(imageName, field, errorHolder) {

    var ext =  imageName.substr((imageName.lastIndexOf('.') +1)).toUpperCase();

    if(ext == 'JPG' || ext == 'PNG' || ext == 'GIF' || ext == 'JPEG') {

      BlogIt.UI.clearInputFieldStyling(field);
      BlogIt.UI.setFieldError(errorHolder, "");
      return true;

    } else {

      BlogIt.UI.addInputFieldErrorStyling(field);
      BlogIt.UI.setFieldError(errorHolder, "Invalid file, must be an image.");
      return false;

    }
  },

  /**
   * Helper function to preview an image local upload before backend submit
   */
  previewUploadURL: function(input, previewDiv, clearURLInput, inputErrorHolder) {

    if (input.files && input.files[0]) {

      document.getElementById(clearURLInput).value = "";

      if(!BlogIt.Brewery.verifyImageFileName(input.files[0].name, input.id, inputErrorHolder)) {
        return false;
      }

      // Verify the image size
      if(!BlogIt.Brewery.verifyImageFileSize(input.files[0].size, input.id, inputErrorHolder)) {
        return false;
      }

      var reader = new FileReader();

      reader.onload = function(e) {
        $(previewDiv).attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  },

  /**
   * Helper function to preview an image when given a URL
   */
  previewURL: function(input, previewDiv, inputErrorHolder) {

    if(input.value !== ""){

      if(!BlogIt.Brewery.verifyImageFileName(input.value, input.id, inputErrorHolder)) {
        return false;
      }

      $(previewDiv).attr('src', BlogIt.Lib.trimWhiteSpace(input.value));
    }
  },

  /**
   * Get brewery images given a brewery id and call display callback function
   */
  getBreweryImages: function(breweryId, displayHolderDiv, callback) {

    var dataArray = {
      'service': 'brewery',
      'action': 'getImages',
      'breweryId': breweryId
    };

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error
        console.log(this.NAME_SPACE+" Error: Failed to get brewery images.");

      } else { //No errors

        var parameterList = {data, displayHolderDiv};

        if (callback && typeof(callback) === "function") {
          callback(data, displayHolderDiv)
        }

      }
    });
  },

  /**
   * Callback function for getBreweryImages to display images for editing
   */
  displayImagesForEdit: function(data, displayHolderDiv) {

    var images = {"images": data["success_message"]["images"]};

    var totalRecords = Number(data["success_message"]["totalRecords"]);
    var code = '';

    if(totalRecords <= 0) {
      code += '<p>'+data["success_message"]["message"]+'</p>';

      document.getElementById(displayHolderDiv).innerHTML = code;

    } else {

      BlogIt.UI.compileAndExecuteHandlebars(displayHolderDiv, 'brewery_image_album_edit_template', [], images, "clear");

    }
  },

  /**
   * Open the brewery image edit modal
   */
  editBreweryImageModal: function(imageId) {

    var alertHolder = "alerts-holder-edit-image";
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'brewery',
      'action': 'getImage',
      'imageId': imageId
    };

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        console.log("BlogIt.Brewery.editBreweryImageModal: Error: Failed to get brewery image.");

        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false)

      } else { //No errors

        BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'brewery_edit_image_modal_template', [], data["success_message"], "clear");

      }
    });
  },

  /**
   * Edit a brewery image
   */
  editBreweryImage: function(imageId) {

    var alertHolder = "alerts-holder-edit-image";
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'brewery',
      'action': 'updateImage',
      'imageId': imageId,
      'imageTitle': BlogIt.Lib.trimWhiteSpace(document.getElementById("breweryImageTitle").value),
      'imageDescription': BlogIt.Lib.trimWhiteSpace(document.getElementById("breweryImageDescription").value)
    };

    BlogIt.UI.disableElement("editBreweryImageButton");
    BlogIt.UI.addAlert(alertHolder, "info", "Saving Image...");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      BlogIt.UI.clearAlerts(alertHolder);

      if (data["success_flag"] === 0) { //Error

        //error
        BlogIt.UI.enableElement("editBreweryImageButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else {

        //success
        BlogIt.UI.enableElement("editBreweryImageButton");

        BlogIt.UI.closeModal();

        BlogIt.Brewery.getBreweryImages(data["success_message"]["breweryId"], "breweryImageHolder", BlogIt.Brewery.displayImagesForEdit);
      }
    });

  },

  /**
   * Open brewery image delete modal
   */
  deleteBreweryImageModal: function(imageId) {

    var data = {"image_id":imageId};

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'brewery_delete_image_modal_template', [], data, "clear");

  },

  /**
   *
   */
  deleteBreweryImage: function(imageId) {

    var alertHolder = "alerts-holder-delete-image";
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'brewery',
      'action': 'deleteImage',
      'imageId': imageId
    };

    BlogIt.UI.disableElement("deleteBreweryImageButton");
    BlogIt.UI.addAlert(alertHolder, "info", "Deleting Image...");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      BlogIt.UI.clearAlerts(alertHolder);

      if (data["success_flag"] === 0) { //Error

        //error
        BlogIt.UI.enableElement("deleteBreweryImageButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else {

        //success
        BlogIt.UI.enableElement("deleteBreweryImageButton");

        BlogIt.UI.closeModal();

        BlogIt.Brewery.getBreweryImages(data["success_message"]["breweryId"], "breweryImageHolder", BlogIt.Brewery.displayImagesForEdit);
      }
    });
  },

  /**
   * Callback function to display brewery images
   */
  displayBreweryImageCarousel: function(data, displayHolderDiv) {

    var totalRecords = Number(data["success_message"]["totalRecords"]);

    if(totalRecords > 0) {

      var images = {"images": data["success_message"]["images"]};

      BlogIt.UI.compileAndExecuteHandlebars(displayHolderDiv, 'brewery_image_carousel_template', [], images, "clear");
    }
  },

  /**
   * Open add beer modal
   */
  addBeerModal: function() {

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'beer_add_modal_template', [], "", "clear");

  },

  /**
   * Add beer
   */
  addBeer: function() {

    var alertHolder = "alerts-holder-add-beer";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("beerName", "beerNameErrorHolder", [ "required" ])) {
      return false;
    }

    this.BEER_LIST.push({
      beer_name:document.getElementById("beerName").value,
      beer_notes:document.getElementById("beerNotes").value
    });

    BlogIt.UI.closeModal();

    this.displayBeerListForEdit();
  },

  /**
   * Display Beer List For Editing
   */
  displayBeerListForEdit: function() {

    var beers = {"beers": this.BEER_LIST};

    BlogIt.UI.compileAndExecuteHandlebars('beerListHolder', 'beer_display_edit_template', [], beers, "clear");

  },

  /**
   * Display Beer List
   */
  displayBeer: function() {

    var beers = {"beers": this.BEER_LIST};

    BlogIt.UI.compileAndExecuteHandlebars('beerListHolder', 'beer_display_template', [], beers, "clear");

  },

  /**
   * Open edit beer modal
   */
  editBeerModal: function(beerIndex) {

    var beer = this.BEER_LIST[beerIndex];
    beer["beer_index"] = beerIndex;

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'beer_edit_modal_template', [], beer, "clear");

  },

  /**
   * Edit beer
   */
  editBeer: function(beerIndex) {

    var alertHolder = "alerts-holder-add-beer";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("beerName", "beerNameErrorHolder", [ "required" ])) {
      return false;
    }

    this.BEER_LIST[beerIndex].beer_name = document.getElementById("beerName").value;
    this.BEER_LIST[beerIndex].beer_notes = document.getElementById("beerNotes").value;

    BlogIt.UI.closeModal();

    this.displayBeerListForEdit();

  },

  /**
   * Delete beer
   */
  deleteBeer: function(beerIndex) {

    this.BEER_LIST[beerIndex].delete = "true";

    console.log(this.BEER_LIST);

    BlogIt.UI.closeModal();

    this.displayBeerListForEdit();

  },

  /**
   * Edit beer
   */
  setBeerList(beerList) {

    this.BEER_LIST = beerList;

  }

}

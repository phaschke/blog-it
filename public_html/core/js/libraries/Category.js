/**
 * Namespace to hold functions with Category related functions.
 * @namespace
 */
BlogIt.Category = {

  // Define the service interface
  SERVICE_REQUESTOR: "core/serviceInterfaces/serviceRequestor.php",

  /**
   *
   */
  listCategories: function(categoryCountHolder, categoryListHolder, clearAlerts) {

    var alertHolder = "alerts-holder-top";
    if(clearAlerts) {
      BlogIt.UI.clearAlerts(alertHolder);
    }

    var dataArray = {
      'service': 'category',
      'action': 'getCategories'
    };


    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      document.getElementById(categoryListHolder).innerHTML = "";

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors

        if (data["success_message"]["message"]) {

          BlogIt.UI.addAlert(alertHolder, "info", data["success_message"]["message"], false);

        } else {

          var totalCategories = data["success_message"]["totalCategories"];

          document.getElementById(categoryCountHolder).innerHTML = '<p>Displaying '+totalCategories+' Total Post Categories.</p>';

          var uneditableCategory = data["success_message"]["categories"][0];
          BlogIt.UI.compileAndExecuteHandlebars(categoryListHolder, 'category_uneditable_list_template', [], uneditableCategory, "append");

          var categories = {"categories": data["success_message"]["categories"].slice(1)};
          BlogIt.UI.compileAndExecuteHandlebars(categoryListHolder, 'category_list_item_template', [], categories, "append");

        }
      }

    });
  },

  /**
   *
   */
  validateCategoryForm: function() {
    if(!BlogIt.Lib.verifyField("categoryName", "categoryNameErrorHolder", [ "required" ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("categoryDescription", "categoryDescriptionErrorHolder", [ "required" ])) {
      return false;
    }

    return true;

  },

  /**
   *
   */
  addCategoryModal: function() {

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'category_add_modal_template', [], "", "clear");

  },

  /**
   *
   */
  addCategory: function() {

    var alertHolder = "alerts-holder-top";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Category.validateCategoryForm()) {
      return false;
    }

    var dataArray = {
      'service': 'category',
      'action': 'add',
      'categoryName': document.getElementById("categoryName").value,
      'categoryDescription': document.getElementById("categoryDescription").value
    };

    BlogIt.UI.disableElement("addCategoryButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.enableElement("addCategoryButton");
        BlogIt.UI.closeModal();
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], true);

      } else { //No errors
        BlogIt.UI.enableElement("addCategoryButton");
        BlogIt.UI.closeModal();
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], true);

        BlogIt.Category.listCategories("category-count-holder", "category-list-holder", false);

      }

    });
  },

  deleteCategoryModal: function(categoryId) {

    var category = {'categoryId': categoryId};

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'category_delete_modal_template', [], category, "clear");

  },

  deleteCategory: function(categoryId) {

    var alertHolder = "alerts-holder-top";
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'category',
      'action': 'delete',
      'categoryId': categoryId
    };

    BlogIt.UI.disableElement("deleteCategoryButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.enableElement("deleteCategoryButton");
        BlogIt.UI.closeModal();
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], true);

      } else { //No errors

        BlogIt.UI.enableElement("deleteCategoryButton");
        BlogIt.UI.closeModal();
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], true);

        BlogIt.Category.listCategories("category-count-holder", "category-list-holder", false);

        // Scroll to success message on top
        document.body.scrollTop = document.documentElement.scrollTop = 0;
      }

    });
  },

  updateCategoryModal: function(categoryId) {

      var alertHolder = "alerts-holder-top";
      BlogIt.UI.clearAlerts(alertHolder);

      var dataArray = {
        'service': 'category',
        'action': 'getCategory',
        'categoryId': categoryId
      };

      BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

        if (data["success_flag"] == 0) { //Error

          BlogIt.UI.closeModal();
          BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], true);
          BlogIt.UI.closeModal();

        } else { //No errors

          var category = data["success_message"];

          BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'category_update_modal_template', [], category, "clear");

        }
      });
  },

  updateCategory: function(categoryId) {
    var alertHolder = "alerts-holder-top";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Category.validateCategoryForm()) {
      return false;
    }

    var dataArray = {
      'service': 'category',
      'action': 'update',
      'categoryId': categoryId,
      'categoryName': document.getElementById("categoryName").value,
      'categoryDescription': document.getElementById("categoryDescription").value
    };

    BlogIt.UI.disableElement("updateCategoryButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.enableElement("updateCategoryButton");
        BlogIt.UI.closeModal();
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_log"], true);

      } else { //No errors
        BlogIt.UI.enableElement("updateCategoryButton");
        BlogIt.UI.closeModal();
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], true);

        BlogIt.Category.listCategories("category-count-holder", "category-list-holder", false);

        // Scroll to success message on top
        document.body.scrollTop = document.documentElement.scrollTop = 0;

      }

    });
  },


}

/**
 * Namespace to hold post related functions.
 * @namespace
 */
BlogIt.Post = {

  // Define the service interface
  SERVICE_REQUESTOR: "core/serviceInterfaces/serviceRequestor.php",
  IS_CHANGED: 0,


  unlockEditObject: function() {

    var dataArray = {
      'service': 'editLockManager',
      'action': 'unlockEditObject'
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("Failed to remove lock");

      } else { //No errors

        console.log("Successfully removed lock");

      }
    });

  },

  /**
   * Validate the post add/edit form
   */
  validatePostForm: function() {
    if (!BlogIt.Lib.verifyField("postTitle", "postTitleErrorHolder", ["required"])) {
      return false;
    }

    if (!BlogIt.Lib.verifyField("postSummary", "postSummaryErrorHolder", ["required"])) {
      return false;
    }

    return true;
  },

  /**
   * Verify post contents and call add post script.
   */
  publishPost: function(save) {

    var alertHolderTop = "alerts-holder-top";
    var alertHolderBottom = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(alertHolderTop);
    BlogIt.UI.clearAlerts(alertHolderBottom);

    if (!BlogIt.Post.validatePostForm()) {
      return false;
    }

    var postBody = this.validatePostBody("postBody");

    var postStatus = "published";
    if (save != null && save != "") {
      postStatus = "saved";
    }

    var dataArray = {
      'service': 'post',
      'action': 'add',
      'postStatus': postStatus,
      'postTitle': document.getElementById("postTitle").value,
      'postSummary': document.getElementById("postSummary").value,
      'postBody': postBody,
      'postTags': document.getElementById("postTags").value,
      'postCategory': document.getElementById("postCategory").value
    };

    BlogIt.UI.disableElement("addPostButton");
    BlogIt.UI.disableElement("savePostButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.enableElement("addPostButton");
        BlogIt.UI.enableElement("savePostButton");
        BlogIt.UI.addAlert(alertHolderBottom, "danger", data["error_message"], false);

      } else { //No errors
        BlogIt.UI.enableElement("addPostButton");
        BlogIt.UI.enableElement("savePostButton");

        document.getElementById("addPostForm").reset();
        BlogIt.UI.addAlert(alertHolderTop, "success", data["success_message"], false);
        document.body.scrollTop = document.documentElement.scrollTop = 0;

      }
    });
  },

  /**
   * Delete a Post
   */
  deletePost: function() {
    var successAlertHolder = "alerts-holder-top";
    var errorAlertHolder = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(successAlertHolder);
    BlogIt.UI.clearAlerts(errorAlertHolder);

    var dataArray = {
      'service': 'post',
      'action': 'delete'
    };

    BlogIt.UI.disableElement("deletePostButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error
        BlogIt.UI.closeModal();
        BlogIt.UI.enableElement("deletePostButton");
        BlogIt.UI.addAlert(successAlertHolder, "danger", data["error_message"], true);
        document.body.scrollTop = document.documentElement.scrollTop = 0;

      } else { //No errors

        //REDIRECT TO MANANGE PAGE
        BlogIt.Utils.redirect(data["success_message"]);

      }

    });

  },

  /**
   * Update a previously added post
   */
  updatePost: function(status) {

    var successAlertHolder = "alerts-holder-top";
    var errorAlertHolder = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(successAlertHolder);
    BlogIt.UI.clearAlerts(errorAlertHolder);

    if (!BlogIt.Post.validatePostForm()) {
      return false;
    }

    var postBody = this.validatePostBody("postBody");

    var postStatus = 'save';
    if (status == 'publish') {
      postStatus = 'published';
    }

    var dataArray = {
      'service': 'post',
      'action': 'update',
      'postStatus': postStatus,
      'postTitle': document.getElementById("postTitle").value,
      'postSummary': document.getElementById("postSummary").value,
      'postBody': postBody,
      'postTags': document.getElementById("postTags").value,
      'postCategory': document.getElementById("postCategory").value
    };

    BlogIt.UI.disableElement("updatePostButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.enableElement("updatePostButton");
        BlogIt.UI.addAlert(errorAlertHolder, "danger", data["error_message"], true);

      } else { //No errors
        BlogIt.UI.enableElement("updatePostButton");

        BlogIt.UI.addAlert(successAlertHolder, "success", data["success_message"], true);
        // Scroll to success message on top
        document.body.scrollTop = document.documentElement.scrollTop = 0;

      }
    });
  },

  /**
   *
   */
  deletePostModal: function() {

    BlogIt.UI.compileAndExecuteHandlebars('modal-content', 'post_delete_modal_template', [], "", "clear");
  },

  /**
   * Toggle a posts visibility
   */
  togglePostVisibility: function(status) {

    var successAlertHolder = "alerts-holder-top";
    var errorAlertHolder = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(successAlertHolder);
    BlogIt.UI.clearAlerts(errorAlertHolder);

    var dataArray = {
      'service': 'post',
      'action': 'togglePostVisibility'
    };

    BlogIt.UI.disableElement("togglePostVisibilityButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.enableElement("togglePostVisibilityButton");
        BlogIt.UI.addAlert(errorAlertHolder, "danger", data["error_message"], true);

      } else { //No errors
        BlogIt.UI.enableElement("togglePostVisibilityButton");

        var button = document.getElementById("togglePostVisibilityButton");
        if (button.innerHTML == "Hide Post") {
          button.innerHTML = "Unhide Post";
        } else {
          button.innerHTML = "Hide Post";
        }

        BlogIt.UI.addAlert(successAlertHolder, "success", data["success_message"], true);
        // Scroll to success message on top
        document.body.scrollTop = document.documentElement.scrollTop = 0;
      }
    });
  },

  /**
   * Get search parameters form search form
   */
  getSearchParameters: function(searchParameters) {
    searchParametersJson = "";
    if (searchParameters) {
      searchParametersJson = JSON.parse(searchParameters);
    }

    return searchParametersJson;
  },

  /**
   * Get search parameters form search form and conduct search
   */
  searchPosts() {

    var searchParameters = {};
    var searchElement = document.getElementById("postSearchSelect").value;
    if (searchElement != "") {
      searchParameters[searchElement] = BlogIt.Lib.trimWhiteSpace(document.getElementById("searchTerm").value);
    }

    BlogIt.Post.initializePostPagination("", "published", null, false, "BlogIt.Post.listPosts", "posts-holder", "view", searchParameters);

  },

  /**
   * Initialize pagination of posts
   *
   * @param string paginationElement holder id
   */
  initializePostPagination: function(yearMonth, postStatus, postCategory, initializeContent, callback, postsHolder, type, searchParameters) {

    var itemsPerPage = 25;

    var dataArray = {
      'service': 'post',
      'action': 'getPosts',
      'yearMonth': yearMonth,
      'postStatus': postStatus,
      'postCategory': postCategory,
      'numRecords': itemsPerPage,
      'offset': 0,
      'searchParameters': searchParameters
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Post:initializePostPagination: " + data["error_message"]);

        var code = '<p>' + data["error_message"] + '</p>';

        document.getElementById(postsHolder).innerHTML = code;

      } else { //No errors

        var posts = {
          "posts": data["success_message"]["posts"]
        };

        var totalRecords = Number(data["success_message"]["totalCount"]);
        var code = '';

        if (totalRecords <= 0) {
          code += '<p>' + data["success_message"]["message"] + '</p>';

          document.getElementById(postsHolder).innerHTML = code;

        } else {

          if (type == "edit") {


            BlogIt.UI.compileAndExecuteHandlebars(postsHolder, 'post_manage_list_template', [], posts, "clear");

          } else {

            BlogIt.UI.compileAndExecuteHandlebars(postsHolder, 'post_list_template', [], posts, "clear");
          }

          Pagination.Init(document.getElementById('pagination'), {
              itemsPerPage: itemsPerPage, // items per page
              size: Math.ceil(totalRecords / itemsPerPage), // pages size
              page: 1, // selected page
              step: 3 // pages before and after current
            },
            callback,
            initializeContent,
            [postsHolder, yearMonth, postStatus, postCategory, searchParameters]
          );
        }
      }
    });
  },

  /**
   *
   */
  listPosts: function(args) {

    var offset = args[0];
    var recordsPerPage = args[1];
    var postsHolder = args[2];
    var yearMonth = args[3];
    var postStatus = args[4];
    var postCategory = args[5];
    var searchParameters = args[5];

    var dataArray = {
      'service': 'post',
      'action': 'getPosts',
      'numRecords': recordsPerPage,
      'offset': offset,
      'yearMonth': yearMonth,
      'postStatus': postStatus,
      'postCategory': postCategory,
      'searchParameters': searchParameters
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Post:listPosts: " + data["error_message"]);

        var code = '<p>' + data["error_message"] + '</p>';
        document.getElementById(postsHolder).innerHTML = code;

      } else { //No errors

        var posts = {
          "posts": data["success_message"]["posts"]
        };

        var code = '';
        if (posts.length == 0) {

          code += '<p>' + data["success_message"]["message"] + '</p>';
          document.getElementById(postsHolder).innerHTML = code;

        } else {

          BlogIt.UI.compileAndExecuteHandlebars(postsHolder, 'post_list_template', [], posts, "clear");

        }
      }
    });
  },

  /**
   * Get Posts
   */
  managePosts: function(args) {

    var offset = args[0];
    var recordsPerPage = args[1];
    var postsHolder = args[2];
    var yearMonth = args[3];
    var postStatus = args[4];

    var dataArray = {
      'service': 'post',
      'action': 'getPosts',
      'numRecords': recordsPerPage,
      'offset': offset,
      'yearMonth': yearMonth,
      'postStatus': postStatus
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Post:managePosts: " + data["error_message"]);

        var code = '<p>' + data["error_message"] + '</p>';
        document.getElementById(postsHolder).innerHTML = code;

      } else { //No errors

        var posts = {
          "posts": data["success_message"]["posts"]
        };

        if (posts.length == 0) {
          var code = '';
          code += '<p>' + data["success_message"]["message"] + '</p>'
          document.getElementById(postsHolder).innerHTML = code;

        } else {

          BlogIt.UI.compileAndExecuteHandlebars(postsHolder, 'post_manage_list_template', [], posts, "clear");

        }


      }

      return true;
    });

  },

  /**
   * Provide a post preview
   */
  postPreview: function() {
    var editor = new nicEditors.findEditor('postBody');
    postBody = "";
    postBody = editor.getContent();

    var e = document.getElementById("postCategory");
    var postCategory = e.options[e.selectedIndex].text;

    var data = {
      blogTitle: document.getElementById("postTitle").value,
      currentDate: "[Current Date]",
      blogAuthor: "[Author]",
      blogCategory: postCategory,
      blogSummary: document.getElementById("postSummary").value,
      blogBody: postBody
    };

    BlogIt.UI.executeHandlebars("#postPreview", "postPreview", data);
  },

  /**
   * Validate the post body field which implements niceEdit
   */
  validatePostBody: function(textAreaId) {
    var editor = new nicEditors.findEditor(textAreaId);
    postBody = "";
    postBody = editor.getContent();
    return postBody;
  }



}

/**
 * Namespace to hold functions with Pagination related functions.
 * @namespace
 */

 /* * * * * * * * * * * * * * * * *
 * Pagination
 * javascript page navigation
 * * * * * * * * * * * * * * * * */

var Pagination = {

    code: '',
    holder: null,

    // --------------------
    // Utility
    // --------------------

    // converting initialize data
    Extend: function(data) {
        data = data || {};
        Pagination.itemsPerPage = data.itemsPerPage || 50;
        Pagination.size = data.size || 50;
        Pagination.page = data.page || 1;
        Pagination.step = data.step || 3;
    },

    // add pages by number (from [s] to [f])
    Add: function(s, f) {
        for (var i = s; i < f; i++) {
            if(i == Pagination.page) {
              Pagination.code += '<li class="page-item active" onclick="Pagination.Click(this)"><a class="page-link">'+ i +'</a></li>';
            } else {
              Pagination.code += '<li class="page-item" onclick="Pagination.Click(this)"><a class="page-link">'+ i +'</a></li>';
            }
        }
    },

    // add last page with separator
    Last: function() {
        Pagination.code += '<li class="page-item">...</li><li class="page-item" onclick="Pagination.Click(this)"><a class="page-link">'+ Pagination.size +'</a></li>';
    },

    // add first page with separator
    First: function() {
        Pagination.code += '<li class="page-item" onclick="Pagination.Click(this)"><a class="page-link">1</a></li><li class="page-item">...</li>';
    },



    // --------------------
    // Handlers
    // --------------------

    // change page
    Click: function(f) {
        //console.log(+f.childNodes[0].innerHTML);
        Pagination.page = +f.childNodes[0].innerHTML;
        Pagination.Start();
    },

    // previous page
    Prev: function() {
        Pagination.page--;
        if (Pagination.page < 1) {
            Pagination.page = 1;
        }
        //console.log(Pagination.page);
        Pagination.Start();
    },

    // next page
    Next: function() {
        Pagination.page++;
        if (Pagination.page > Pagination.size) {
            Pagination.page = Pagination.size;
        }
        //console.log(Pagination.page);
        Pagination.Start();
    },



    // --------------------
    // Script
    // --------------------

    // find pagination type
    Start: function() {

        //console.log("in start "+callback);
        if (Pagination.size < Pagination.step * 2 + 6) {
            Pagination.Add(1, Pagination.size + 1);
        }
        else if (Pagination.page < Pagination.step * 2 + 1) {
            Pagination.Add(1, Pagination.step * 2 + 4);
            Pagination.Last();
        }
        else if (Pagination.page > Pagination.size - Pagination.step * 2) {
            Pagination.First();
            Pagination.Add(Pagination.size - Pagination.step * 2 - 2, Pagination.size + 1);
        }
        else {
            Pagination.First();
            Pagination.Add(Pagination.page - Pagination.step, Pagination.page + Pagination.step + 1);
            Pagination.Last();
        }
        Pagination.Create();
    },

    // --------------------
    // Initialization
    // --------------------

    // create skeleton
    Create: function() {

      //console.log("Page-here");

        var html = [
            '<nav aria-label="Manage Brewery Navigation">',
              '<ul class="pagination justify-content-center">',
                '<li class="page-item" id="paginationPrevButton" onclick="Pagination.Prev()"><a class="page-link">Previous</a></li>', // previous button
                Pagination.code,
                '<li class="page-item" id="paginationNextButton" onclick="Pagination.Next()"><a class="page-link">Next</a></li>',  // next button
              '</ul>',
            '</nav>'
        ];
        Pagination.holder.innerHTML = html.join('');

        if(Pagination.page == Pagination.size) {
          document.getElementById("paginationNextButton").className += " disabled";
        }
        if(Pagination.page == 1) {
          document.getElementById("paginationPrevButton").className += " disabled";
        }

        Pagination.code = '';


        if (typeof Pagination.callback === 'string' && Pagination.initializeContent == true) {

          //OFFSET, ITEMS PER PAGE
          var args = [(Pagination.page*Pagination.itemsPerPage)-Pagination.itemsPerPage, Pagination.itemsPerPage];



          if(Pagination.callbackArgs) {

            if(Pagination.callbackArgs instanceof Array) {

              args = args.concat(Pagination.callbackArgs);
            }

            if(typeof Pagination.callbackArgs === 'string') {
              args.push(Pagination.callbackArgs);
            }
          }

          //console.log(args);

          Pagination.executeFunctionByName(Pagination.callback, window, args);
        }

        Pagination.initializeContent = true;
    },

    // init
    Init: function(e, data, callback, initializeContent, callbackArgs) {

      //console.log(callbackArgs);

      Pagination.holder = e;
      Pagination.callback = callback;
      Pagination.initializeContent = initializeContent;
      Pagination.callbackArgs = callbackArgs;
      Pagination.Extend(data);
      Pagination.Start();
    },

    executeFunctionByName: function(functionName, context, args ) {

      var args = Array.prototype.slice.call(arguments, 2);
      var namespaces = functionName.split(".");
      var func = namespaces.pop();
      for (var i = 0; i < namespaces.length; i++) {
          context = context[namespaces[i]];
      }
      return context[func].apply(context, args);
    }
};

var BlogIt = {

};

/**
 * Namespace to hold functions with reusable logic or calculation.
 * @namespace
 */
BlogIt.Lib = {
  verifyField: function(field, errorHolder, rules) {
    var focus = true; // TODO: Implement variable in call.
    var value = document.getElementById(field).value;
    var rule;
    var result;
    outerLoop:
      for (var i = 0, j = rules.length; i < j; i++) {
        if (rules[i]["rule"]) {
          rule = rules[i]["rule"];
        } else {

          rule = rules[i];
        }
        switch (rule) {
          case 'required':
            if (BlogIt.Utils.isEmpty(value)) {
              result = {
                "error": "This field is required."
              };
              break outerLoop;
            }
            break;
          case 'email':
            if (!BlogIt.Utils.validateEmail(value)) {
              result = {
                "error": "Email is not valid."
              };
              break outerLoop;
            }
            break;
          case 'minlength':
            var minLength = rules[i]["minLength"];
            if (value.length < minLength) {
              result = {
                "error": "The minimum length is " + minLength + "."
              };
              break outerLoop;
            }
            break;
            case 'maxlength':
              var maxLength = rules[i]["maxLength"];
              if (value.length > maxLength) {
                result = {
                  "error": "The maximum length is " + maxLength + "."
                };
                break outerLoop;
              }
              break;
          case 'number':
            if (isNaN(value)) {
              result = {
                "error": "Value must be a number."
              };
              break outerLoop;
            }
          break;
          case 'latitudecoord':
            if (isFinite(value) && Math.abs(value >= 90)) {
              result = {
                "error": "Invalid Latitude Coordinate."
              };
              break outerLoop;
            }
          break;
          case 'longitudecoord':
            if (isFinite(value) && Math.abs(value >= 180)) {
              result = {
                "error": "Invalid Longitude Coordinate."
              };
              break outerLoop;
            }
          break;
          case 'date':
            if (!BlogIt.Lib.isValidDate(value)) {
              result = {
                "error": "Invalid Date. (MM/DD/YYYY)"
              };
              break outerLoop;
            }
          break;

          default:
            console.log("VerifyField: Encounted a invalid rule.");
        }
      }
    if (result) {
      BlogIt.UI.addInputFieldErrorStyling(field);
      BlogIt.UI.setFieldError(errorHolder, result["error"]);
      if(focus) {
        document.getElementById(field).focus();
      }
      return false;

    } else {
      BlogIt.UI.clearInputFieldStyling(field);
      BlogIt.UI.setFieldError(errorHolder, "");
      return true;
    }
  },

  /**
   * Function to verify password fields are equal to each other.
   *
   * @param string passwordField first password field
   * @param string retypePasswordField second password field
   * @param string errorHolder where the error should be displayed
   */
  verifyPasswordFields: function(passwordField, retypePasswordField, errorHolder) {
    var password = document.getElementById(passwordField).value;
    var retypePassword = document.getElementById(retypePasswordField).value;

    if(password != retypePassword) {
      BlogIt.UI.addInputFieldErrorStyling(passwordField);
      BlogIt.UI.addInputFieldErrorStyling(retypePasswordField);

      BlogIt.UI.setFieldError(errorHolder, "Passwords must match.");
      return false;
    }

    BlogIt.UI.clearInputFieldStyling(passwordField);
    BlogIt.UI.clearInputFieldStyling(retypePasswordField);
    BlogIt.UI.setFieldError(errorHolder, "");
    return true;
  },

  /**
   * Function to execute a json AJAX call
   *
   * @param url string The url to the desired file
   * @param dataArray array The array of data to be sent in the call
   *
   * @return jsonArray
   */
  AJAXCall: function(url, type, dataArray, callback) {

    var myData;

    $.ajax({
      type: type,
      url: url,
      data: dataArray,
      dataType: 'json',
      timeout: 15000, // sets timeout to 15 seconds

      success: function(data) {
        myData = data;
      },

      error: function(data) { // if your PHP script return an erroneous header, you'll land here

        console.log("Error: AJAX call failed, Please try again.");
        // TODO: change to success_flag and error_message
        myData = {
          "success_flag": 0,
          "error_message": "Error: AJAX call failed, Please try again."
        };
      },

      complete: function(data) {
        //console.log("ajax: complete");
        if (callback && typeof(callback) === "function") {
          callback(myData);
        }
      }
    });
  },

  /**
   * Validates that the input string is a valid date formatted as "mm/dd/yyyy"
   */
  isValidDate: function(dateString) {
      // First check for the pattern
      if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
          return false;

      // Parse the date parts to integers
      var parts = dateString.split("/");
      var day = parseInt(parts[1], 10);
      var month = parseInt(parts[0], 10);
      var year = parseInt(parts[2], 10);

      // Check the ranges of month and year
      if(year < 1000 || year > 3000 || month == 0 || month > 12)
          return false;

      var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

      // Adjust for leap years
      if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
          monthLength[1] = 29;

      // Check the range of the day
      return day > 0 && day <= monthLength[month - 1];
  },

  /**
   * Disallow any other characters than [0-9, A-Z, a-z, _] from being entered on a input field
   */
  disallowInputChars: function(e) {
    var charValue= String.fromCharCode(e.keyCode);

    if( ((e.which != 8 ) && (e.which != 95 ) && !((e.which >= 65) && (e.which <= 90)) && !((e.which >= 97) && (e.which <= 122)) && !((e.which >= 48) && (e.which <= 57)) ) || (e.which == 32 ) ){
        e.preventDefault();
    }
    return true;
  },

  /**
   * Remove spaces from an input field
   */
  removeWhitespaceFromInput: function(e) {

  },

  /**
   * Trim leading and tailing whitespace from a string.
   * Uses a regex for browser compatability.
   */
  trimWhiteSpace(inputString) {
    return inputString.replace(/^\s+|\s+$/g,'');
  }


}; // End BlogIt.Lib namespace


/**
 * Namespace to hold functions that are generic utilities
 * @namespace
 */
BlogIt.Utils = {
  /*
   * Helper function to determine if a string is empty, null, or white space.
   * Returns true is the string is empty, null, or white space.
   */
  isEmpty: function(string) {
    return (!string || !string.length || !string.trim());
  },

  /**
   * Helper function to determine if a string is a valid email address.
   *
   * @param string email The email to be validated
   * @return boolean true is the string is valid, false otherwise
   */
  validateEmail: function(email) {
    var regexEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
    return (email.match(regexEmail));
  },

  /**
   * Function to redirect to a provided URL.
   *
   * @param string url of the page to be redirected to
   */
  redirect: function(url) {
    location.href = url;
    return true;
  },

  /**
   * Function to set a input box to a values
   *
   * @param string field The field ID to be set
   * @param value the contents to replace the value
   */
  setInputBox: function(field, value) {
    document.getElementById(field).value = value;
  }

}; // End BlogIt.Utils namespace


/**
 * Namespace to hold functions that manipulate the DOM
 * @namespace
 */
BlogIt.UI = {

  addAlert: function(alertHolderID, alertType, alertMessage, dismissable) {
    var elementToAdd = '';
    if (dismissable) {
      elementToAdd = '<div class="alert alert-' + alertType + ' alert-dismissable fade-in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + alertMessage + '</div>';
    } else {
      elementToAdd = '<div class="alert alert-' + alertType + ' fade-in">' + alertMessage + '</div>';
    }
    this.appendElementToElement(alertHolderID, elementToAdd);
  },

  clearAlerts: function(alertHolderID) {
    $("#" + alertHolderID).empty();
  },

  setFieldError: function(errorHolderId, message) {
    $("#" + errorHolderId).html(message);
    return true;
  },

  addInputFieldErrorStyling: function(elementID) {
    // Change input focus class.
    this.setElementClass(elementID, "form-control is-invalid");
  },

  clearInputFieldStyling: function(elementID) {
    // Clear styling on password field.
    this.setElementClass(elementID, "form-control");
  },

  /**
   * Function to clear the error styling of a given field
   *
   * @param string field
   * @param string errorHolder
   */
  clearFieldError: function(field, errorHolder) {
    BlogIt.UI.clearInputFieldStyling(field);
    BlogIt.UI.setFieldError(errorHolder, "");
  },

  /*
   * Helper function to set the class of an element.
   */
  setElementClass: function(elementID, classToSet) {
    document.getElementById(elementID).className = classToSet;
    return true;
  },

  /*
   * Helper function to add an element to another element given it's ID.
   */
  appendElementToElement: function(elementID, elementToAdd) {
    $("#" + elementID).append(elementToAdd);
    return true;
  },

  /*
   * Helper function to remove an element given it's element ID from another element also given it's element ID
   */
  removeElementFromElement: function(elementID, elementIDToRemove) {
    $("#" + elementID).find("#" + elementIDToRemove).remove();
    return true;
  },

  disableElement: function(elementID) {
    try {
      document.getElementById(elementID).disabled = true;
    } catch (err) {
      console.log("DisableElement: Error disabling element with ID " + elementID + ". " + err);
      return false;
    }
    return true;
  },

  enableElement: function(elementID) {
    try {
      document.getElementById(elementID).disabled = false;
    } catch (err) {
      console.log("EnableElement: Error enabling element with ID " + elementID + ". " + err);
      return false;
    }
    return true;
  },

  /**
   * Function to execute and append a precompiled handlebars template to a div.
   * To compile templates (unix terminal) from public_html/core/: sudo handlebars -m [templates folder] -f [path/name of compiled file]
   * sudo handlebars -m js/templates/ -f js/templates/templates.js
   *
   * @param templateHolderSelector string The selector of the div to be cleared and populated with the template.
   * @param templateName string The name of the precompiled template to use.
   * @param data jsonArray The data to be used in the execution of the template.
   *
   * @return boolean true on success.
   */
  executeHandlebars: function(templateHolderSelector, templateName, data) {
    $(templateHolderSelector).html("");
    $(templateHolderSelector).append(Handlebars.templates[templateName](data));

    return true;
  },

  /**
   * Function to compile a handlebars template and append it to the page given:
   *
   * @param templateHolderSelector string The selector of the holder for the template to be appended to.
   * @param mainTemplateId string The id of the template to be used. (THE TEMPLATE IS STORED ON THE VIEW/PAGE)
   * @param partialTemplatesArray array An array of partial templates used within the main template (optional).
   * @param jsonData jsonArray The data to be used in the template.
   */
  compileAndExecuteHandlebars: function(templateHolderSelector, mainTemplateId, partialTemplatesArray, jsonData, appendClearFlag) {

    if(appendClearFlag === "clear") {
      $("#" + templateHolderSelector).html("");
    }

    //Compile template:
    var theTemplate = Handlebars.compile($("#" + mainTemplateId).html());
    //Add partcials with the same name as id:
    $.each(
      partialTemplatesArray,
      function(index, value) {
        Handlebars.registerPartial(value, $("#" + value).html());
      });
    //Append template to parent with data:
    $("#" + templateHolderSelector).append(theTemplate(jsonData));
  },

  /**
   * Function to open a modal defaulting to the loading text
   */
   openModal: function(callback, parameterList) {

     $('#modal').modal('show');

     if (callback && typeof(callback) === "function") {
       callback.apply(this, parameterList);
     }
   },

   closeModal: function() {

     $('#modal').modal('hide');

   },

}; // End BlogIt.UI namespace

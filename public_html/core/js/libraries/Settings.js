/**
 * Namespace to hold functions with Settings related functions.
 * @namespace
 */
BlogIt.Settings = {

  // Define the service interface
  SERVICE_REQUESTOR: "core/serviceInterfaces/serviceRequestor.php",

  saveSettings: function() {

    var alertHolderTop = "alerts-holder-top";
    var alertHolderBottom = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(alertHolderTop);
    BlogIt.UI.clearAlerts(alertHolderBottom);

    BlogIt.UI.clearFieldError("contactEmail", "contactEmailErrorHolder");
    BlogIt.UI.clearFieldError("instagramLink", "instagramLinkErrorHolder");
    BlogIt.UI.clearFieldError("facebookLink", "facebookLinkErrorHolder");
    BlogIt.UI.clearFieldError("youtubeLink", "youtubeLinkErrorHolder");

    var showContactEmail = 0;
    var contactEmail = "";
    if(document.getElementById("showContactEmail").checked) {
      if(!BlogIt.Lib.verifyField("contactEmail", "contactEmailErrorHolder", [ "required" ])) {
        return false;
      }
      showContactEmail = 1;
    }
    contactEmail = document.getElementById("contactEmail").value;

    var showTwitter = 0;
    var twitterLink = "";
    if(document.getElementById("showTwitter").checked) {
      if(!BlogIt.Lib.verifyField("twitterLink", "twitterLinkErrorHolder", [ "required" ])) {
        return false;
      }
      showTwitter = 1;
    }
    twitterLink = document.getElementById("twitterLink").value;

    var showInstagram = 0;
    var instagramLink = "";
    if(document.getElementById("showInstagram").checked) {
      if(!BlogIt.Lib.verifyField("instagramLink", "instagramLinkErrorHolder", [ "required" ])) {
        return false;
      }
      showInstagram = 1;
    }
    instagramLink = document.getElementById("instagramLink").value;

    var showFacebook = 0;
    var facebookLink = "";
    if(document.getElementById("showFacebook").checked) {
      if(!BlogIt.Lib.verifyField("facebookLink", "facebookLinkErrorHolder", [ "required" ])) {
        return false;
      }
      showFacebook = 1;
    }
    facebookLink = document.getElementById("facebookLink").value;

    var showYoutube = 0;
    var youtubeLink = "";
    if(document.getElementById("showYoutube").checked) {
      if(!BlogIt.Lib.verifyField("youtubeLink", "youtubeLinkErrorHolder", [ "required" ])) {
        return false;
      }
      showYoutube = 1;
    }
    youtubeLink = document.getElementById("youtubeLink").value;

    var dataArray = {
      'service': 'pages',
      'action': 'update',
      'aboutSummary': document.getElementById("aboutSummary").value,
      'aboutBody': this.validateAboutBody("aboutBody"),
      'showContactEmail': showContactEmail,
      'contactEmail': contactEmail,
      'showTwitter': showTwitter,
      'twitterLink': twitterLink,
      'showInstagram': showInstagram,
      'instagramLink': instagramLink,
      'showFacebook': showFacebook,
      'facebookLink': facebookLink,
      'showYoutube': showYoutube,
      'youtubeLink': youtubeLink
    }

    BlogIt.UI.disableElement("saveSettingsButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        BlogIt.UI.enableElement("saveSettingsButton");
        BlogIt.UI.addAlert(alertHolderBottom, "danger", data["error_message"], false);

      } else { //No errors
        BlogIt.UI.enableElement("saveSettingsButton");

        BlogIt.UI.addAlert(alertHolderTop, "success", data["success_message"], false);
        document.body.scrollTop = document.documentElement.scrollTop = 0;

      }
    });

  },

  /**
   * Validate the about body field which implements niceEdit
   */
  validateAboutBody: function(textAreaId) {
    var editor = new nicEditors.findEditor(textAreaId);
    aboutBody = "";
    aboutBody = editor.getContent();
    return aboutBody;
  }



}

/**
 * Namespace to hold functions with Account related functions.
 * @namespace
 */
BlogIt.Account = {

  SERVICE_REQUESTOR: "core/serviceInterfaces/serviceRequestor.php",

  login: function() {
    var alertHolder = "alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    if (!BlogIt.Lib.verifyField("emailField", "emailErrorHolder", ["required", "email"])) {
      return false;
    }

    if (!BlogIt.Lib.verifyField("passwordField", "passwordErrorHolder", ["required"])) {
      return false;
    }

    BlogIt.UI.disableElement("loginButton");

    var email = document.getElementById("emailField").value;
    var password = document.getElementById("passwordField").value;

    var dataArray = {
      'service': 'account',
      'action': 'login',
      'email': email,
      'password': password,
    };

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors

        BlogIt.Utils.redirect(data["success_message"]);
        //alert("success");
      }

      BlogIt.UI.enableElement("loginButton");

    });
  },

  /**
   *
   */
  register: function() {
    var alertHolder = "alerts-holder-bottom";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("emailField", "emailErrorHolder", ["required", "email"])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("usernameField", "usernameErrorHolder", [ "required", {"rule":"minlength", "minLength":4}, {"rule":"maxlength", "maxLength":25} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("passwordField", "passwordErrorHolder", [ "required", {"rule":"minlength", "minLength":6} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("retypePasswordField", "retypePasswordErrorHolder", [ "required", {"rule":"minlength", "minLength":6} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyPasswordFields("passwordField", "retypePasswordField", "retypePasswordErrorHolder")) {
      return false;
    }

    BlogIt.UI.addAlert(alertHolder, "info", "Creating your account...", false);

    var dataArray = {
      'service': 'account',
      'action': 'register',
      'email': document.getElementById("emailField").value,
      'username': document.getElementById("usernameField").value,
      'password': document.getElementById("passwordField").value,
      'retypePassword': document.getElementById("retypePasswordField").value,
    };

    BlogIt.UI.disableElement("registerButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error
        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors
        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], false);
        document.getElementById("registerForm").reset();

      }

      BlogIt.UI.enableElement("registerButton");

    });

  },

  /**
   *
   */
  completeRegistration: function() {
    var alertHolder = "alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("usernameField", "usernameErrorHolder", [ "required", {"rule":"minlength", "minLength":4}, {"rule":"maxlength", "maxLength":25} ])) {
      return false;
    }

    var dataArray = {
      'service': 'account',
      'action': 'completeRegistration',
      'username': document.getElementById("usernameField").value,
    };

    BlogIt.UI.disableElement("completeRegistrationButton");

    BlogIt.UI.addAlert(alertHolder, "info", "Updating Your Account...", false);

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors

        BlogIt.Utils.redirect(data["success_message"]);

      }
      BlogIt.UI.enableElement("completeRegistrationButton");

    });

  },

  /**
   *
   */
  resendVerificaton: function() {
    var alertHolder = "alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("emailField", "emailErrorHolder", ["required", "email"])) {
      return false;
    }

    var dataArray = {
      'service': 'account',
      'action': 'resendVerificaton',
      'email': document.getElementById("emailField").value,
    };

    BlogIt.UI.disableElement("resendVerificationButton");

    BlogIt.UI.addAlert(alertHolder, "info", "Sending Verification Email...", false);

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error
        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.enableElement("resendVerificationButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors
        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], false);

      }
      document.getElementById("resendVerificationForm").reset();

    });
  },

  /**
   * Verify and pass data from forgotPassword page to backend handler to generate and send an account recovery email
   */
  sendPasswordReset: function() {
    var alertHolder = "alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("emailField", "emailErrorHolder", ["required", "email"])) {
      return false;
    }

    var dataArray = {
      'service': 'account',
      'action': 'sendPasswordReset',
      'email': document.getElementById("emailField").value,
    };

    BlogIt.UI.disableElement("forgotPasswordButton");

    BlogIt.UI.addAlert(alertHolder, "info", "Sending Password Reset Email...", false);

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error
        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.enableElement("forgotPasswordButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors
        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], false);

      }
      document.getElementById("forgotPasswordForm").reset();

    });
  },

  /**
   * Verify password reset input from resetPassword page
   *
   * @var string email The user email inserted when reset page is created
   * @var string token The user token inserted when reset page is created
   */
  resetPassword: function(email, token) {
    var alertHolder = "alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("passwordField", "passwordErrorHolder", [ "required", {"rule":"minlength", "minLength":6} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("retypePasswordField", "retypePasswordErrorHolder", [ "required", {"rule":"minlength", "minLength":6} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyPasswordFields("passwordField", "retypePasswordField", "retypePasswordErrorHolder")) {
      return false;
    }

    var dataArray = {
      'service': 'account',
      'action': 'resetPassword',
      'email': email,
      'token': token,
      'password': document.getElementById("passwordField").value,
      'retypePassword': document.getElementById("retypePasswordField").value,
    };

    BlogIt.UI.disableElement("resetPasswordButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_message"] === 0) { //Error

        BlogIt.UI.enableElement("resetPasswordButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors
        BlogIt.Utils.redirect(data["success_message"]);

      }
      document.getElementById("resetPasswordForm").reset();

    });
  },

  /**
   * Initialize pagination of accounts
   *
   */
  initializeAccountsPagination: function(initializeContent, callback, accountsHolder, onlyIsMuted) {

    var itemsPerPage = 25;

    var dataArray = {
      'service': 'account',
      'action': 'getMany',
      'onlyIsMuted': onlyIsMuted,
      'numRecords': itemsPerPage,
      'offset' : 0
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Post:initializeAccountPagination: "+data["error_message"]);

        var code = '<p>'+data["error_message"]+'</p>';

        document.getElementById(accountsHolder).innerHTML = code;

      } else { //No errors

        var accounts = {"accounts": data["success_message"]["accounts"]};

        var totalRecords = Number(data["success_message"]["totalCount"]);

        var code = '';

        if(totalRecords <= 0) {
          code += '<p>'+data["success_message"]["message"]+'</p>';

          document.getElementById(accountsHolder).innerHTML = code;

        } else {

          BlogIt.UI.compileAndExecuteHandlebars(accountsHolder, 'account_manage_list_template', [], accounts, "clear");


          Pagination.Init(document.getElementById('pagination'), {
              itemsPerPage: itemsPerPage, // items per page
              size: Math.ceil(totalRecords/itemsPerPage), // pages size
              page: 1,  // selected page
              step: 3   // pages before and after current
            },
            callback,
            initializeContent,
            [accountsHolder, onlyIsMuted]
          );
        }
      }
    });
  },

  /**
   * Get Posts
   */
  manageAccounts: function(args) {

    var offset = args[0];
    var recordsPerPage = args[1];
    var accountsHolder = args[2];
    var onlyIsMuted = args[3];

    var dataArray = {
      'service': 'account',
      'action': 'getMany',
      'numRecords': recordsPerPage,
      'offset': offset,
      'onlyIsMuted': onlyIsMuted,
    }

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Post:manageAccounts: "+data["error_message"]);

        var code = '<p>'+data["error_message"]+'</p>';
        document.getElementById(accountsHolder).innerHTML = code;

      } else { //No errors

        var accounts = {"accounts": data["success_message"]["accounts"]};

        if(accounts.length == 0) {
          var code = '';
          code += '<p>'+data["success_message"]["message"]+'</p>'
          document.getElementById(accountsHolder).innerHTML = code;

        } else {

          BlogIt.UI.compileAndExecuteHandlebars(accountsHolder, 'account_manage_list_template', [], accounts, "clear");

        }
      }
    });
  },

  /**
   * Mute an account
   */
  muteAccount: function() {

    var alertHolder = "alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("muteReason", "muteReasonErrorHolder", [ "required" ])) {
      return false;
    }

    var dataArray = {
      'service': 'account',
      'action': 'muteAccount',
      'reason': BlogIt.Lib.trimWhiteSpace(document.getElementById("muteReason").value)
    }
    BlogIt.UI.disableElement("muteAccountButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] == 0) { //Error

        console.log("BlogIt.Post:muteAccount: "+data["error_message"]);

        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.enableElement("muteAccountButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors

        location.reload();
      }
    });
  },

  /**
   * Unmute an account
   */
  unmuteAccount: function() {

    var alertHolder = "alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'account',
      'action': 'unmuteAccount'
    }
    BlogIt.UI.disableElement("unmuteAccountButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        console.log("BlogIt.Account:unmuteAccount: "+data["error_message"]);

        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.enableElement("unmuteAccountButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], false);

      } else { //No errors

        location.reload();
      }
    });
  },

  /**
   * Change an account's bio
   */
  saveAccountBio: function() {

    var alertHolder = "account-bio-alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    var dataArray = {
      'service': 'account',
      'action': 'saveAccountBio',
      'accountBio': BlogIt.Lib.trimWhiteSpace(document.getElementById("accountBio").value)
    }

    BlogIt.UI.disableElement("saveAccountBioButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        console.log("BlogIt.Account:saveAccountBio: "+data["error_message"]);

        BlogIt.UI.clearAlerts(alertHolder);
        BlogIt.UI.enableElement("saveAccountBioButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], true);

      } else { //No errors

        BlogIt.UI.enableElement("saveAccountBioButton");
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], true);

      }
    });
  },

  /**
   * Change an accounts password
   */
  changePassword: function() {

    var alertHolder = "password-alerts-holder";
    BlogIt.UI.clearAlerts(alertHolder);

    if(!BlogIt.Lib.verifyField("existingPasswordField", "existingPasswordErrorHolder", [ "required", {"rule":"minlength", "minLength":6} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("passwordField", "passwordErrorHolder", [ "required", {"rule":"minlength", "minLength":6} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyField("retypePasswordField", "retypePasswordErrorHolder", [ "required", {"rule":"minlength", "minLength":6} ])) {
      return false;
    }

    if(!BlogIt.Lib.verifyPasswordFields("passwordField", "retypePasswordField", "retypePasswordErrorHolder")) {
      return false;
    }

    var dataArray = {
      'service': 'account',
      'action': 'changePassword',
      'existingPassword': document.getElementById("existingPasswordField").value,
      'newPassword': document.getElementById("passwordField").value,
      'retypeNewPassword': document.getElementById("retypePasswordField").value,
    }

    BlogIt.UI.disableElement("changePasswordButton");

    BlogIt.Lib.AJAXCall(this.SERVICE_REQUESTOR, 'POST', dataArray, function(data) {

      if (data["success_flag"] === 0) { //Error

        BlogIt.UI.enableElement("changePasswordButton");
        BlogIt.UI.addAlert(alertHolder, "danger", data["error_message"], true);

      } else { //No errors

        BlogIt.UI.enableElement("changePasswordButton");
        BlogIt.UI.addAlert(alertHolder, "success", data["success_message"], true);

      }
      document.getElementById("changePasswordForm").reset();

    });
  },

  /**
   * *TO BE IMPLEMENTED IN THE FUTURE*
   * Change an account's email address
   */
  changeEmail: function() {

    var dataArray = {
      'service': 'account',
      'action': 'changeEmail'
    }
  },

} // End BlogIt.Account namespace

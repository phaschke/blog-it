<?php

$service = $action = "";
$parameters = array();

// Store POST or GET parameters
foreach($_REQUEST as $key=>$value) {
  if($key === 'service') {
    $service = $value;
  } elseif($key === 'action') {
    $action = $value;
  } else {
    $parameters[$key] = $value;
  }
}

// Verify the required service and action are specified.
if($service === "" || $action === "") {

  error_log(__FILE__." Could not request service, missing required parameters. A service and action are required.");

  echo json_encode(array('success_flag' => 0,
    'error_message' => "Could not request service, missing required parameters. A service and action are required."));
  return false;
}


require_once( "../../../app/routes/ServiceRouter.php" );

$serviceRouter = new ServiceRouter;

// Forward the request to the service router.
$serviceRouter->requestService($service, $action, $parameters);


?>

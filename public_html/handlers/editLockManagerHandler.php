<?php

//----------------------------------------------------
// Ajax Handler
//----------------------------------------------------
if (array_key_exists('function', $_POST)) {

  require_once( "../../app/config/config.php" );

  $session = new Session();
  $editLockManager = new EditLockManager($session);

	$function = $_POST['function'];

	switch ($function) {

    case 'unlockEditObject':

      $editLockManager->unlock();

      echo json_encode(array('returnValue' => 1,
        'returnMessage' => "here"));
      return true;

    break;


  	default:
      if(LOG_ERRORS) {
        error_log("handler.php: A function was called that doesn't exist in the edit lock manager Handler.");
      }
  		break;
	}

} else {
  error_log("handler.php: Unable to read function from POST variable.");
}



?>

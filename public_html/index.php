<?php

require_once( "../app/config/config.php" );

if(isset($_GET['controller']) && $_GET['controller'] === "home") {
  // Home controller
  $controller = $_GET['controller'];
  $action = 'show';

} elseif (isset($_GET['controller']) && isset($_GET['action'])) {

  $controller = $_GET['controller'];
  $action = $_GET['action'];

} else {
  redirect(HOME_PAGE_URL);
}

require_once( ROUTES_PATH.'/routes.php' );

$routes = new Routes();
$routes->requestController($controller, $action);



?>
